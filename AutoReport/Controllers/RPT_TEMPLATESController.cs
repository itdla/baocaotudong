﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using Microsoft.Office.Interop.Word;

namespace AutoReport.Controllers
{
    public class RPT_TEMPLATESController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();

        private string m_ControllerName = "RPT_TEMPLATES";

        // GET: RPT_TEMPLATES/Index
        public ActionResult Index()
        {
            ViewBag.ControllerName = m_ControllerName;
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            return View();
        }

        // GET: RPT_TEMPLATES/Create
        public ActionResult Create()
        {
            ViewBag.ControllerName = m_ControllerName;
            RPT_TEMPLATES item = new RPT_TEMPLATES();
            return View(item);
        }

        [HttpPost]
        public ActionResult Create(RPT_TEMPLATES iteminfo, List<string[]> lst_tables, List<string[]> lst_values)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;

            try
            {
                result.strMess = CheckBeforeSave(iteminfo, true);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                iteminfo.Rpt_TemplatesKey = Guid.NewGuid();
                iteminfo.Templates_ID = iteminfo.Templates_ID.Trim();
                iteminfo.RecordStatus = 1;
                iteminfo.CreatedBy = MyAccount.UserID;
                iteminfo.ModifiedBy = MyAccount.UserID;
                iteminfo.CreatedOn = DateTime.Now;
                iteminfo.ModifiedOn = DateTime.Now;

                db.RPT_TEMPLATES.Add(iteminfo);

                #region tables

                if (lst_tables != null)
                {
                    foreach (string[] arrItem in lst_tables)
                    {
                        if (string.IsNullOrEmpty(arrItem[1]))
                            continue;
                        RPT_TEMPLATES_DETAIL_TABLEINDEX itemtbl = new RPT_TEMPLATES_DETAIL_TABLEINDEX();
                        itemtbl.Rpt_Templates_Detail_TableIndexKey = Guid.NewGuid();
                        itemtbl.Rpt_TemplatesKey = iteminfo.Rpt_TemplatesKey;
                        itemtbl.Rpt_Templates_TableNameKey = Guid.Parse(arrItem[1]);
                        db.RPT_TEMPLATES_DETAIL_TABLEINDEX.Add(itemtbl);
                    }
                }

                #endregion

                #region values

                if (lst_values != null)
                {
                    for (int i = 0; i < lst_values.Count; i++)
                    {
                        string[] arritem = lst_values[i];
                        if (string.IsNullOrEmpty(arritem[1].Trim()) && string.IsNullOrEmpty(arritem[2].Trim()))
                            continue;

                        RPT_TEMPLATES_DETAIL_VALUECONST itemValue = new RPT_TEMPLATES_DETAIL_VALUECONST();
                        itemValue.Rpt_Templates_Detail_ValueConstKey = Guid.NewGuid();
                        itemValue.Rpt_TemplatesKey = iteminfo.Rpt_TemplatesKey;
                        itemValue.CodeID = arritem[1].Trim();
                        itemValue.CodeName = arritem[2].Trim();
                        itemValue.Sort = i;
                        db.RPT_TEMPLATES_DETAIL_VALUECONST.Add(itemValue);
                    }
                }

                #endregion

                db.SaveChanges();
                result.strMess = "Đã lưu thông tin";
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        // GET: RPT_TEMPLATES/Edit/5
        public ActionResult Edit(Guid? id)
        {
            ViewBag.ControllerName = m_ControllerName;

            RPT_TEMPLATES item = db.RPT_TEMPLATES.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // POST: RPT_TEMPLATES/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(RPT_TEMPLATES iteminfo, List<string[]> lst_tables, List<string[]> lst_values, string[] lst_tables_del, string[] lst_values_del)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;

            try
            {
                result.strMess = CheckBeforeSave(iteminfo, false);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                var oldItem = db.RPT_TEMPLATES.Find(iteminfo.Rpt_TemplatesKey);
                if (oldItem == null)
                    throw new Exception("Không tìm thấy thông tin");

                iteminfo.Templates_ID = iteminfo.Templates_ID.Trim();
                string[] prop_set_list = { "Templates_ID", "Templates_Name", "Note" };

                foreach (string propname in prop_set_list)
                {
                    PropertyInfo pNew = iteminfo.GetType().GetProperty(propname);
                    if (pNew == null) continue;
                    object pNew_Value = pNew.GetValue(iteminfo);
                    oldItem.GetType().GetProperty(propname).SetValue(oldItem, pNew_Value, null);
                }

                oldItem.ModifiedBy = MyAccount.UserID;
                oldItem.ModifiedOn = DateTime.Now;

                db.Entry(oldItem).State = EntityState.Modified;

                #region tables

                if (lst_tables != null)
                {
                    foreach (string[] arrItem in lst_tables)
                    {
                        if (string.IsNullOrEmpty(arrItem[1]))
                            continue;
                        RPT_TEMPLATES_DETAIL_TABLEINDEX itemtbl = new RPT_TEMPLATES_DETAIL_TABLEINDEX();
                        itemtbl.Rpt_Templates_Detail_TableIndexKey = Guid.NewGuid();
                        itemtbl.Rpt_TemplatesKey = iteminfo.Rpt_TemplatesKey;
                        itemtbl.Rpt_Templates_TableNameKey = Guid.Parse(arrItem[1]);
                        db.RPT_TEMPLATES_DETAIL_TABLEINDEX.Add(itemtbl);
                    }
                }

                #endregion

                #region values

                if (lst_values != null)
                {
                    for (int i = 0; i < lst_values.Count; i++)
                    {
                        string[] arritem = lst_values[i];
                        if (string.IsNullOrEmpty(arritem[1].Trim()) && string.IsNullOrEmpty(arritem[2].Trim()))
                            continue;

                        RPT_TEMPLATES_DETAIL_VALUECONST itemValue = new RPT_TEMPLATES_DETAIL_VALUECONST();
                        itemValue.Rpt_Templates_Detail_ValueConstKey = Guid.NewGuid();
                        itemValue.Rpt_TemplatesKey = iteminfo.Rpt_TemplatesKey;
                        itemValue.CodeID = arritem[1].Trim();
                        itemValue.CodeName = arritem[2].Trim();
                        itemValue.Sort = i;
                        db.RPT_TEMPLATES_DETAIL_VALUECONST.Add(itemValue);
                    }
                }

                #endregion

                db.SaveChanges();
                result.strMess = "Đã lưu thông tin";
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        // GET: RPT_TEMPLATES/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RPT_TEMPLATES rPT_TEMPLATES = db.RPT_TEMPLATES.Find(id);
            if (rPT_TEMPLATES == null)
            {
                return HttpNotFound();
            }
            return View(rPT_TEMPLATES);
        }

        private string CheckBeforeSave(RPT_TEMPLATES iteminfo, bool isNew)
        {
            return string.Empty;
        }

        public JsonResult LoadList(int page, int pageSize, string SearchKey)
        {
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.strURLEdit = "/Web_Mod/Edit";
            table.strURLDetail = "/Web_Mod/Details";
            table.strURLDelete = "/Web_Mod/Delete";

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            var searchQuery = from f in db.RPT_TEMPLATES
                              where f.RecordStatus == 1
                              select f;
            var data = searchQuery.ToList().Select(d => new
            {
                d.Rpt_TemplatesKey,
                d.Templates_ID,
                d.Templates_Name,
                Tenfile = "<a href='" + d.FileReference + "' target='_blank'>" + d.FileName + "</a>",
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/Edit/" + d.Rpt_TemplatesKey + "'>Chỉnh sửa</a>" +
                "<button type=\"button\" class=\"btn btn-danger btn_xoa\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.Rpt_TemplatesKey + "'>Xóa </button>",
            }).Skip((page - 1) * pageSize).Take(pageSize).OrderByDescending(p => p.Templates_Name).ToList();

            table.intTotalPage = GetTotalPage(searchQuery.Count(), pageSize);

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã biểu mẫu", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên biểu mẫu", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên File", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 200 });
            //

            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_TemplatesKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadValueList(Guid? TemplatesKey)
        {
            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Rpt_Templates_Detail_ValueConstKey",
                Field = "Rpt_Templates_Detail_ValueConstKey",
                DataType = GridDetail.eDataType.IntegerType,
                Width = "2%",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Mã trường",
                Field = "CodeID",
                DataType = GridDetail.eDataType.StringType,
                Width = "38%",
                IsShow = true,
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tên trường",
                Field = "CodeName",
                DataType = GridDetail.eDataType.StringType,
                Width = "40%",
                Enable = true,
                IsShow = true
            });

            var zDetails = db.RPT_TEMPLATES_DETAIL_VALUECONST.Where(p => p.Rpt_TemplatesKey == TemplatesKey).OrderBy(p => p.Sort).ToList();
            if (zDetails.Count > 0)
            {
                for (int i = 0; i < zDetails.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                        zDetails[i].Rpt_Templates_Detail_ValueConstKey,
                        zDetails[i].CodeID,
                        zDetails[i].CodeName,
                    });
                }
            }
            else
            {
                arrBody.Add(new object[grid.Header.Count]);
            }
            grid.Data = arrBody;
            return new CustomJsonResult { Data = grid };
        }

        public ActionResult LoadTableList(Guid? TemplatesKey)
        {
            List<SelectListItem> selectLists = new List<SelectListItem>();
            var listtable = (from t in db.RPT_TEMPLATES_TABLENAME
                             where t.RecordStatus == 1
                             orderby t.TableID
                             select t).ToList();
            foreach (var item in listtable)
            {
                SelectListItem selectListItem = new SelectListItem();
                selectListItem.Text = string.Format("{0} - {1}", item.TableID, item.TableName);
                selectListItem.Value = item.Rpt_Templates_TableNameKey.ToString();
                selectLists.Add(selectListItem);
            }
            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Rpt_Templates_Detail_TableIndexKey",
                Field = "Rpt_Templates_Detail_TableIndexKey",
                DataType = GridDetail.eDataType.IntegerType,
                Width = "20",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Rpt_Templates_TableNameKey",
                Field = "Rpt_Templates_TableNameKey",
                DataType = GridDetail.eDataType.StringType,
                Width = "75",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tên bảng",
                Field = "TableName",
                DataType = GridDetail.eDataType.StringType,
                Width = "75",
                Enable = true,
                IsShow = true,
                LayoutType = GridDetail.eLayoutType.DropdownList,
                DataSource = selectLists
            });

            var zDetails = (from tl in db.RPT_TEMPLATES
                            join a in db.RPT_TEMPLATES_DETAIL_TABLEINDEX on tl.Rpt_TemplatesKey equals a.Rpt_TemplatesKey
                            join tbl in db.RPT_TEMPLATES_TABLENAME on a.Rpt_Templates_TableNameKey equals tbl.Rpt_Templates_TableNameKey
                            where tl.Rpt_TemplatesKey == TemplatesKey
                            orderby tbl.TableID
                            select new
                            {
                                a.Rpt_Templates_Detail_TableIndexKey,
                                tbl.TableName,
                                tbl.Rpt_Templates_TableNameKey
                            }).ToList();

            if (zDetails.Count > 0)
            {
                for (int i = 0; i < zDetails.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                        zDetails[i].Rpt_Templates_Detail_TableIndexKey,
                        zDetails[i].Rpt_Templates_TableNameKey,
                        zDetails[i].Rpt_Templates_TableNameKey
                    });
                }
            }
            else
            {
                arrBody.Add(new object[grid.Header.Count]);
            }
            grid.Data = arrBody;
            return new CustomJsonResult { Data = grid };
        }

        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;

            Guid id = new Guid(Request["id"]);

            RPT_TEMPLATES template = db.RPT_TEMPLATES.Find(id);

            if (template != null)
            {
                if (file != null)
                {
                    var currenttime = DateTime.Now.Ticks;
                    string webpage = "/Uploads/" + currenttime + "_" + file.FileName;
                    var path = Server.MapPath("/Uploads/" + currenttime + "_" + file.FileName);
                    file.SaveAs(path);
                    /*
                     * Start - Read Field Name From File: Mr.Linh
                     */
                    var pathtemplate = Server.MapPath("/Uploads/" + currenttime + "_template_" + file.FileName);
                    file.SaveAs(pathtemplate);
                    var rpt = db.RPT_TEMPLATES_DETAIL_VALUECONST.Where(p => p.Rpt_TemplatesKey == id).ToList();
                    foreach (var item in rpt)
                    {
                        db.Entry(item).State = EntityState.Deleted;
                    }
                    db.SaveChanges();
                    List<string[]> data = ReadFieldName(path);
                    int i = 1;
                    foreach (var t in data)
                    {
                        RPT_TEMPLATES_DETAIL_VALUECONST a = new RPT_TEMPLATES_DETAIL_VALUECONST();
                        a.Rpt_Templates_Detail_ValueConstKey = Guid.NewGuid();
                        a.Rpt_TemplatesKey = id;
                        a.CodeID = t[0];
                        a.CodeName = t[1];
                        a.CodeValue = t[2];
                        a.Sort = i++;
                        db.RPT_TEMPLATES_DETAIL_VALUECONST.Add(a);
                    }
                    db.SaveChanges();
                    /*
                     * End - Read Field Name From File: Mr.Linh
                     */

                    template.FileName = file.FileName;
                    template.FileReference = webpage;
                }
                db.Entry(template).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                result.intStatus = -1;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult UploadFile_Raw(HttpPostedFileBase file)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;

            try
            {
                Guid id = new Guid(Request["id"]);

                RPT_TEMPLATES template = db.RPT_TEMPLATES.Find(id);

                if (template == null)
                    throw new Exception("không tìm thấy thông tin báo cáo mẫu");

                if (file != null)
                {
                    var currenttime = DateTime.Now.Ticks;
                    string webpage = "/Uploads/" + currenttime + "_" + file.FileName;
                    var path = Server.MapPath("/Uploads/" + currenttime + "_" + file.FileName);
                    file.SaveAs(path);

                    template.FileName = file.FileName;
                    template.FileReference = webpage;

                    db.Entry(template).State = EntityState.Modified;
                    db.SaveChanges();

                    result.strMess = "Tải lên thành công";
                }
                else
                    result.strMess = "Chưa có file lưu trữ";
            }
            catch (Exception ex)
            {
                result.strMess = ex.Message;
                result.intStatus = 0;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public string[] Split(string st)
        {
            string[] result = new string[3];
            result[0] = st.Substring(0, st.IndexOf(":"));
            st = st.Substring(st.IndexOf(":") + 1);
            result[1] = st.Substring(0, st.IndexOf(":"));
            st = st.Substring(st.IndexOf(":") + 1);
            result[2] = st;
            return result;
        }

        public List<string[]> ReadFieldName(string path)
        {
            List<string[]> ds = new List<string[]>();
            String read = string.Empty;
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
            Document doc = new Document();
            try
            {
                object fileName = path;
                // Define an object to pass to the API for missing parameters
                object missing = System.Type.Missing;
                doc = word.Documents.Open(ref fileName,
                        ref missing, ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing);
                for (int i = 0; i < doc.Paragraphs.Count; i++)
                {
                    read += doc.Paragraphs[i + 1].Range.Text.Trim() + System.Environment.NewLine;
                    if (Regex.Matches(read, "<#").Count == Regex.Matches(read, "#>").Count)
                    {
                        int start = -1, end = -1;
                        while ((start = read.IndexOf("<#")) >= 0)
                        {
                            if ((end = read.IndexOf("#>")) > 0)
                            {
                                string[] item = new string[3];
                                item = Split(read.Substring(start + 2, end - start - 2));
                                ds.Add(item);
                                string temp = read.Substring(start, end - start + 2);
                                string[] x = temp.Split(':');
                                string temp1 = "";
                                int j = 0;
                                while (temp.Length > 0)
                                {
                                    if (temp.Length > 250)
                                    {
                                        temp1 = temp.Substring(0, 250);
                                        temp = temp.Substring(250);
                                    }
                                    else
                                    {
                                        temp1 = temp;
                                        temp = "";
                                    }
                                    FindAndReplace(word, temp1, j == 0 ? x[0].Substring(2) : "");
                                    j++;
                                }
                                read = read.Substring(end + 2);
                                start = -1; end = -1;
                            }
                        }
                        read = string.Empty;
                    }
                }
                    ((_Document)doc).Close();
                ((_Application)word).Quit();

            }
            catch (Exception ex)
            {
                ((_Document)doc).Close();
                ((_Application)word).Quit();
            }
            //Loai bo trung
            for (int i = 0; i < ds.Count - 1; i++)
            {
                for (int j = i + 1; j < ds.Count; j++)
                {
                    string[] x = ds[i], y = ds[j];
                    if (x[0] == y[0])
                    {
                        ds.RemoveAt(j);
                    }
                }
            }
            return ds;
        }

        private void FindAndReplace(Microsoft.Office.Interop.Word.Application doc, object findText, object replaceWithText)
        {
            object missing = System.Reflection.Missing.Value;
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            //execute find and replace

            doc.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
