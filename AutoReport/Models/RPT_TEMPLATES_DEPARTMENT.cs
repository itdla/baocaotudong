//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutoReport.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RPT_TEMPLATES_DEPARTMENT
    {
        public System.Guid Rpt_Templates_DepartmentKey { get; set; }
        public Nullable<System.Guid> Rpt_TemplatesKey { get; set; }
        public Nullable<System.Guid> DepartmentKey { get; set; }
    }
}
