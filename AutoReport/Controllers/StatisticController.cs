﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
namespace AutoReport.Controllers
{
    public class StatisticController : BaseController
    {
        // GET: Sys
        AutoReportEntities db = new AutoReportEntities();
        public ActionResult Index()
        {
            if (!CheckPermission("StatisticMenu"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            return View();
        }
        public ActionResult Board()
        {
            if (!CheckPermission("Statistic_Board"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.Users = db.SYS_Users.Count();
            ViewBag.Employees = db.CAT_EMPLOYEES.Count();
            ViewBag.RPT_TEMPLATES = db.RPT_TEMPLATES.Where(p => p.RecordStatus == 1).Count();
            ViewBag.RPT_REPORTHEADER = db.RPT_REPORTHEADER.Where(p => p.RecordStatus == 1).Count();

            return View();
        }
        public ActionResult Report()
        {
            if (!CheckPermission("Statistic_Report"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            return View();
        }
        public ActionResult User()
        {
            if (!CheckPermission("Statistic_Users"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            return View();
        }
        public JsonResult LoadReportList(int page, int pageSize, string SearchKey, string datepickerfrom = "", string datepickerto = "")
        {
            //Guid Document_ID = new Guid(this.Request["id"]);
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            var searchQuery = from f in db.RPT_REPORTHEADER
                              select f;
            if (!String.IsNullOrEmpty(datepickerfrom) && !String.IsNullOrEmpty(datepickerto))
            {
                DateTime timefrom = DateTime.ParseExact(datepickerfrom + " 00:00:00", "dd/MM/yyyy hh:mm:ss", null);
                DateTime timeto = DateTime.ParseExact(datepickerto + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                searchQuery = searchQuery.Where(p => p.CreatedOn >= timefrom && p.CreatedOn <= timeto);
            }
            //if (!string.IsNullOrEmpty(SearchKey))
            //{
            //    searchQuery = searchQuery.Where(p => p.d.Name.Contains(SearchKey) || p.d.FileName.Contains(SearchKey));
            //}
            List<Guid> ListReportHeaderKey = searchQuery.ToList().Select(p => p.Rpt_ReportHeaderKey).ToList();
            int VALUECONST = db.RPT_REPORTDETAIL_VALUECONST.Where(p => ListReportHeaderKey.Contains(p.Rpt_ReportHeaderKey)).Count();
            int TABLEINDEX = db.RPT_REPORTDETAIL_TABLEINDEX.Where(p => ListReportHeaderKey.Contains(p.Rpt_ReportHeaderKey)).Count();

            List<int> data = new List<int>();
            data.Add(searchQuery.Count());
            data.Add(VALUECONST);
            data.Add(TABLEINDEX);

            table.intTotalPage = 1;

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "Tổng số báo cáo", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tổng số trường", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tổng số phụ lục", IsShow = true, Format = "" });


            //
            for (int i = 0; i < data.Count; i++)
            {
                
            }
            TableItem item = new TableItem();
            item.ID = "1";
            item.isShowDelete = false;

            item.isShowDetail = false;

            item.isShowEdit = false;

            item.objData = data;
            table.lstData.Add(item);

            return new CustomJsonResult { Data = table };
        }
        public JsonResult LoadUserList(int page, int pageSize, string SearchKey, string datepickerfrom = "", string datepickerto = "")
        {
            //Guid Document_ID = new Guid(this.Request["id"]);
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            var searchQuery = from f in db.SYS_Users
                              select f;
            if (!String.IsNullOrEmpty(datepickerfrom) && !String.IsNullOrEmpty(datepickerto))
            {
                DateTime timefrom = DateTime.ParseExact(datepickerfrom + " 00:00:00", "dd/MM/yyyy hh:mm:ss", null);
                DateTime timeto = DateTime.ParseExact(datepickerto + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
                searchQuery = searchQuery.Where(p => p.CreatedOn >= timefrom && p.CreatedOn <= timeto);
            }
            //if (!string.IsNullOrEmpty(SearchKey))
            //{
            //    searchQuery = searchQuery.Where(p => p.d.Name.Contains(SearchKey) || p.d.FileName.Contains(SearchKey));
            //}

            List<int> data = new List<int>();
            data.Add(searchQuery.Count());
            data.Add(searchQuery.Where(p=>p.Active).Count());
            data.Add(searchQuery.Where(p => !p.Active).Count());

            table.intTotalPage = 1;

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "Tổng số tài khoản", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Đang hoạt động", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Ngưng kích hoạt", IsShow = true, Format = "" });


            TableItem item = new TableItem();
            item.ID = "1";
            item.isShowDelete = false;

            item.isShowDetail = false;

            item.isShowEdit = false;

            item.objData = data;
            table.lstData.Add(item);

            return new CustomJsonResult { Data = table };
        }
    }
}