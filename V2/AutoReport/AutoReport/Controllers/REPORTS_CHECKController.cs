﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using System.Transactions;
using AutoReport.Utils;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace AutoReport.Controllers
{
    public class REPORTS_CHECKController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "REPORTS_CHECK";

        // GET: REPORTS_CHECK/Index
        public ActionResult Index()
        {
            if (!GrantAccess("REPORTS_HEADERS_INPUT"))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult ReviewData(int? id)
        {
            if (!GrantAccess("REPORTS_HEADERS_APPROVE"))
                return B_NotPermission();
            var item = db.REPORTS_HEADERS.Find(id);
            ViewBag.ControllerName = m_ControllerName;
            return View(item);
        }

        public ActionResult Decline(int? id)
        {
            if (!GrantAccess("REPORTS_HEADERS_INPUT"))
                return B_NotPermission();
            var item = db.REPORTS_HEADERS.Find(id);
            ViewBag.ControllerName = m_ControllerName;
            return View(item);
        }

        public ActionResult ReportsCompleted()
        {
            if (!GrantAccess("REPORTS_HEADERS_EXPORT"))
                return B_NotPermission();
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult GetListInputCompleted(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;

                //giới hạn số báo cáo, theo nhân viên của phòng ban
                var lstRestricted = GetListTemplateRestricted().Select(p => p.TemplateHeaderKey).ToList();

                var lstItems = (from d in db.REPORTS_HEADERS
                                where d.RecordStatus == 1 && d.ApproveStatus <= 4 &&
                                lstRestricted.Contains(d.TemplateKey)
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.DateEnd,
                                    d.ApproveStatus
                                }).ToList();

                if (!GrantAccess("REPORTS_HEADERS_APPROVE"))
                    lstItems.Clear();

                TableModel table = new TableModel();
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemName", Caption = "Tên Báo Cáo", Width = 60, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "DateEnd", Caption = "Hạn Hoàn Thành", Width = 15, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "StatusDesc", Caption = "Tình Trạng", Width = 15, Filtered = true });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemName,
                    DateEnd = p.DateEnd == null ? "-" : p.DateEnd.Value.ToString(TextUtil.DateLocalFormat),
                    StatusDesc = db.REPORTS_STATUS_APPROVE.Find(p.ApproveStatus)?.ItemName ?? "-"
                }).ToList();

                table.CalcTotalPage(q1.Count);
                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        public ActionResult CreateTabPages(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                var lst_values = db.REPORTS_VALUECONST_VALUES.Where(p => p.ReportHeaderKey == ReportHeaderKey).ToList();
                var lst_tables = (from d in db.REPORTS_TABLES_ASSIGNMENT
                                  join t in db.TEMPLATES_TABLES
                                  on d.TemplateTableKey equals t.dKey
                                  where d.ReportHeaderKey == ReportHeaderKey
                                  orderby t.ItemCode
                                  select new
                                  {
                                      AssignedKey = d.dKey,
                                      d.TemplateTableKey,
                                      t.ItemCode,
                                      t.ItemName,
                                      d.AssignPerson,
                                      t.Sorted
                                  }).ToList();
                if (!GrantAccess("REPORTS_HEADERS_APPROVE"))
                    throw new Exception(NOT_PERMISSION);
                StringBuilder builder = new StringBuilder();
                string nl = Environment.NewLine;
                if (lst_values.Count > 0)
                {
                    builder.Append("<div class=\"tab\">" + nl);
                    builder.Append("<input type=\"radio\" name=\"css-tabs\" id=\"valuesconst\" checked class=\"tab-switch\" data-id=\"0\">" + nl);
                    builder.Append("<label for=\"valuesconst\" class=\"tab-label\">Trường dữ liệu</label>" + nl);
                    builder.Append("<div class=\"tab-content\">" + nl);
                    builder.Append("<div class=\"hot-container hot-scrollable\">" + nl);
                    builder.Append("<div id=\"CntPL_valuesconst\"></div>" + nl);
                    builder.Append("</div>" + nl);
                    builder.Append("</div>" + nl);
                    builder.Append("</div>" + nl);
                }

                if (lst_tables.Count > 0)
                {
                    bool hasValues = lst_values.Count > 0;
                    int _count = 0;

                    var group_tables = lst_tables.GroupBy(p => p.TemplateTableKey).ToList();
                    group_tables.ForEach(group =>
                    {
                        var lst_person_assign = group.ToList();
                        string template_table_name = lst_person_assign.FirstOrDefault().ItemName;
                        int? sorted = lst_person_assign.FirstOrDefault().Sorted;

                        var lst_person_key = lst_person_assign.Select(p => p.AssignPerson).ToList();

                        builder.Append("<div class=\"tab\">" + nl);
                        builder.Append(string.Format("<input type=\"radio\" name=\"css-tabs\" id=\"PL_{0}\"{1} class=\"tab-switch\" data-id=\"{0}\">", group.Key, !hasValues && _count == 0 ? "checked" : "") + nl);
                        builder.Append(string.Format("<label for=\"PL_{0}\" class=\"tab-label\">PL{1}</label>", group.Key, sorted) + nl);
                        builder.Append("<div class=\"tab-content\">" + nl);
                        builder.Append(string.Format("<h3>{0}</h3>", template_table_name) + nl);
                        builder.Append("<div class=\"hot-container hot-scrollable\">" + nl);
                        builder.Append(string.Format("<div id=\"CntPL_{0}\"></div>", group.Key) + nl);
                        builder.Append("</div>" + nl);
                        builder.Append(string.Format("<h3>Người nhập: <b>{0}</b></h3>", string.Join(";", db.CAT_EMPLOYEES.Where(p => lst_person_key.Contains(p.dKey)).Select(p => p.ItemName).ToArray())) + nl);
                        builder.Append("</div>" + nl);
                        builder.Append("</div>" + nl);
                        _count++;
                    });

                }
                result.Data = builder.ToString();
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListValuesForInput(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "dKey", "Tên Trường", "Giá trị" };
                table.ColumnsHidden = new int[] { 0 };

                var query = (from d in db.REPORTS_VALUECONST_VALUES
                             join v in db.TEMPLATES_VALUES_CONST
                                on d.TemplateValueConstKey equals v.dKey
                             where d.ReportHeaderKey == ReportHeaderKey
                             orderby v.Sorted
                             select new
                             {
                                 d.dKey,
                                 v.ItemName,
                                 d.ValueInput,
                                 d.AssignPerson
                             }).ToList();

                if (query.Count > 0)
                {
                    var data = query.Select(p => new
                    {
                        p.dKey,
                        p.ItemName,
                        p.ValueInput

                    }).ToList();

                    data.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "" });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListTableValuesForInput(int? TableAssignmentKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                var itemAssignment = db.REPORTS_TABLES_ASSIGNMENT.Find(TableAssignmentKey);
                var lst_template_columns = (from t in db.TEMPLATES_TABLES_COLUMNS
                                            where t.TableTemplateKey == itemAssignment.TemplateTableKey
                                            orderby t.Sorted
                                            select t).ToList();

                HsonTable table = new HsonTable();
                var lst_caption = lst_template_columns.Select(p => p.ColumnCaption).ToList();
                //lst_caption.Insert(0, "Người Nhập");
                table.HeadersCaption = lst_caption.ToArray();

                //var lst_width = lst_template_columns.Select(p => p.WidthInPx ?? 10).ToList();
                //table.HeadersWidth = lst_width.ToArray();

                var me_id = MyAccount.Model.EmployeeKey;
                var lst_columns_key = lst_template_columns.Select(p => p.dKey).ToList();
                var query = (from d in db.REPORTS_TABLES_COLUMNS_VALUES
                             join t in db.TEMPLATES_TABLES_COLUMNS
                             on d.TemplateColumnKey equals t.dKey
                             where d.ReportTableAssignedKey == TableAssignmentKey && lst_columns_key.Contains(d.TemplateColumnKey)
                             && d.AssignPerson == me_id
                             orderby d.AssignPerson, d.RowIndex, t.Sorted
                             select d).ToList();

                if (query.Count > 0)
                {
                    var datagroup = query.GroupBy(p => new { p.AssignPerson, p.RowIndex }).ToList();
                    foreach (var group in datagroup)
                    {
                        var data_row_value = group.Select(p => p.ValueInput).ToList();
                        //data_row_value.Insert(0, db.CAT_EMPLOYEES.Find(group.Key.AssignPerson).ItemName);
                        table.Source.Add(data_row_value.ToArray());
                    }
                }
                else
                {
                    List<string> dataNull = new List<string>();
                    lst_template_columns.ForEach(p => { dataNull.Add(""); });
                    table.Source.AddRange(new object[] { dataNull.ToArray() });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListValuesReview(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "dKey", "Tên Trường", "Giá trị" };
                table.ColumnsHidden = new int[] { 0 };

                var query = (from d in db.REPORTS_VALUECONST_VALUES
                             join v in db.TEMPLATES_VALUES_CONST
                                on d.TemplateValueConstKey equals v.dKey
                             where d.ReportHeaderKey == ReportHeaderKey
                             orderby v.Sorted
                             select new
                             {
                                 d.dKey,
                                 v.ItemName,
                                 d.ValueInput,
                                 d.AssignPerson
                             }).ToList();

                if (query.Count > 0)
                {
                    var data = query.Select(p => new
                    {
                        p.dKey,
                        p.ItemName,
                        p.ValueInput

                    }).ToList();

                    data.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "" });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListTableValuesReview(int? ReportHeaderKey, int? TableTemplateKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                //table template schema
                var lst_template_columns = (from t in db.TEMPLATES_TABLES_COLUMNS
                                            where t.TableTemplateKey == TableTemplateKey
                                            orderby t.Sorted
                                            select t).ToList();

                HsonTable table = new HsonTable();
                var lst_caption = lst_template_columns.Select(p => p.ColumnCaption).ToList();
                //lst_caption.Insert(0, "Người Nhập");
                table.HeadersCaption = lst_caption.ToArray();

                //var lst_width = lst_template_columns.Select(p => p.WidthInPx ?? 10).ToList();
                //table.HeadersWidth = lst_width.ToArray();

                var lst_item_table_assign_key = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == ReportHeaderKey && p.TemplateTableKey == TableTemplateKey).Select(p => p.dKey).ToList();
                var lst_columns_key = lst_template_columns.Select(p => p.dKey).ToList();
                var query = (from d in db.REPORTS_TABLES_COLUMNS_VALUES
                             join t in db.TEMPLATES_TABLES_COLUMNS
                             on d.TemplateColumnKey equals t.dKey
                             where lst_item_table_assign_key.Contains(d.ReportTableAssignedKey) && lst_columns_key.Contains(d.TemplateColumnKey)
                             orderby d.AssignPerson, d.RowIndex, t.Sorted
                             select d).ToList();

                if (query.Count > 0)
                {
                    var datagroup = query.GroupBy(p => new { p.AssignPerson, p.RowIndex }).ToList();
                    foreach (var group in datagroup)
                    {
                        var data_row_value = group.Select(p => p.ValueInput).ToList();
                        //data_row_value.Insert(0, db.CAT_EMPLOYEES.Find(group.Key.AssignPerson).ItemName);
                        table.Source.Add(data_row_value.ToArray());
                    }
                }
                else
                {
                    List<string> dataNull = new List<string>();
                    lst_template_columns.ForEach(p => { dataNull.Add(""); });
                    table.Source.AddRange(new object[] { dataNull.ToArray() });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult ApproveReport(int? id)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess("REPORTS_HEADERS_APPROVE"))
                    throw new Exception(NOT_PERMISSION);

                REPORTS_HEADERS item = db.REPORTS_HEADERS.Find(id);
                item.ApproveDate = Now;
                item.ApproveStatus = 5;
                item.ModifiedBy = MyAccount.Model.dKey.ToString();
                item.ModifiedOn = Now;
                db.Entry(item).State = EntityState.Modified;

                var lst_tables = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == item.dKey).ToList();
                lst_tables.ForEach(tbl =>
                {
                    tbl.ModifiedBy = item.ModifiedBy;
                    tbl.ModifiedOn = item.ModifiedOn;
                    tbl.InputState = 5;//approve
                    db.Entry(tbl).State = EntityState.Modified;
                });

                var lst_values = db.REPORTS_VALUECONST_VALUES.Where(p => p.ReportHeaderKey == item.dKey).ToList();
                lst_tables.ForEach(vconst =>
                {
                    vconst.ModifiedBy = item.ModifiedBy;
                    vconst.ModifiedOn = item.ModifiedOn;
                    vconst.InputState = 5;//approve
                    db.Entry(vconst).State = EntityState.Modified;
                });
                db.SaveChanges();

                result.RedirectTo = string.Format("/{0}/Index", m_ControllerName);
                result.Result_Status = 1;

                result.Result_Message = UPDATE_SUCCESS;
                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    item.dKey.ToString(),
                    item.ItemCode,
                    item.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult DeclineReport(int? ReportHeaderKey, List<string[]> lstTables, List<string[]> lstValues)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess("REPORTS_HEADERS_APPROVE"))
                    throw new Exception(NOT_PERMISSION);

                REPORTS_HEADERS item = db.REPORTS_HEADERS.Find(ReportHeaderKey);
                item.ApproveStatus = 3;//trả về trạng thái đang nhập liệu
                item.ModifiedBy = MyAccount.Model.dKey.ToString();
                item.ModifiedOn = Now;
                db.Entry(item).State = EntityState.Modified;

                foreach (string[] arr in lstTables)
                {
                    if (string.IsNullOrEmpty(arr[0])) continue;
                    if (arr[1] == "false") continue;

                    int dkey = Convert.ToInt32(arr[0]);
                    var itemTable = db.REPORTS_TABLES_ASSIGNMENT.Find(dkey);
                    if (itemTable == null) continue;

                    itemTable.InputState = 3;//trả về trạng thái đang nhập liệu
                    itemTable.ModifiedBy = item.ModifiedBy;
                    itemTable.ModifiedOn = item.ModifiedOn;
                    db.Entry(itemTable).State = EntityState.Modified;
                }

                foreach (string[] arr in lstValues)
                {
                    if (string.IsNullOrEmpty(arr[2])) continue;
                    if (arr[0] == "false") continue;

                    int assign_person_key = Convert.ToInt32(arr[2]);

                    var values_assigned = db.REPORTS_VALUECONST_VALUES.Where(p => p.ReportHeaderKey == ReportHeaderKey && p.AssignPerson == assign_person_key).ToList();
                    values_assigned.ForEach(p =>
                    {
                        p.InputState = 3;
                        p.ModifiedBy = item.ModifiedBy;
                        p.ModifiedOn = item.ModifiedOn;
                        db.Entry(p).State = EntityState.Modified;
                    });
                }

                db.SaveChanges();

                result.RedirectTo = string.Format("/{0}/ReviewData/" + ReportHeaderKey, m_ControllerName);
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListPersonInputValues_Decline(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "Chọn", "Người Nhập", "AssignPerson" };
                table.ColumnsHidden = new int[] { 2 };
                table.HeadersWidth = new int[] { 50, 250 };
                var query = (from d in db.REPORTS_VALUECONST_VALUES
                             join v in db.TEMPLATES_VALUES_CONST
                                on d.TemplateValueConstKey equals v.dKey
                             where d.ReportHeaderKey == ReportHeaderKey && d.InputState == 4
                             select new
                             {
                                 d.dKey,
                                 v.ItemName,
                                 d.ValueInput,
                                 d.AssignPerson
                             }).ToList();

                if (query.Count > 0)
                {
                    var datagroup = query.GroupBy(p => p.AssignPerson).ToList();
                    datagroup.ForEach(item =>
                    {
                        table.Source.Add(new string[] { "false", db.CAT_EMPLOYEES.Find(item.Key.Value).ItemName, Convert.ToString(item.Key) });
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "false", "", "" });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListPersonInputTables_Decline(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "dKey", "Chọn", "Phụ Lục", "Người Nhập" };
                table.ColumnsHidden = new int[] { 0 };
                var query = (from d in db.REPORTS_TABLES_ASSIGNMENT
                             join t in db.TEMPLATES_TABLES
                             on d.TemplateTableKey equals t.dKey
                             join e in db.CAT_EMPLOYEES
                             on d.AssignPerson equals e.dKey
                             where d.ReportHeaderKey == ReportHeaderKey && d.InputState == 4
                             orderby t.Sorted
                             select new
                             {
                                 d.dKey,
                                 Selected = "false",
                                 TemplateTableName = t.ItemName,
                                 PersonAssignName = e.ItemName
                             }).ToList();

                if (query.Count > 0)
                {
                    query.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "false", "", "" });
                }

                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListApproved(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                string StrFromDate = Request.QueryString["StrFromDate"] ?? "";
                string StrToDate = Request.QueryString["StrToDate"] ?? "";

                DateTime? FromDate = null;
                if (!string.IsNullOrEmpty(StrFromDate))
                    FromDate = DateTime.ParseExact(StrFromDate + " 00:00:00", TextUtil.DateLocalFormat + " hh:mm:ss", null);
                DateTime? ToDate = null;
                if (!string.IsNullOrEmpty(StrToDate))
                    ToDate = DateTime.ParseExact(StrToDate + " 23:59:59", TextUtil.DateLocalFormat + " HH:mm:ss", null);

                //giới hạn số báo cáo, theo nhân viên của phòng ban
                var lstRestricted = GetListTemplateRestricted().Select(p => p.TemplateHeaderKey).ToList();

                page = page == 0 ? 1 : page;
                var lstItems = (from d in db.REPORTS_HEADERS
                                where d.RecordStatus == 1 && d.ApproveStatus == 5 &&
                                lstRestricted.Contains(d.TemplateKey) &&
                                (FromDate == null || (FromDate != null && d.ApproveDate >= FromDate)) &&
                                (ToDate == null || (ToDate != null && d.ApproveDate <= ToDate))
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.CreatedOn,
                                    d.ApproveDate
                                }).ToList();

                if (!GrantAccess("REPORTS_HEADERS_EXPORT"))
                    lstItems.Clear();

                TableModel table = new TableModel()
                {
                    Selectable = false
                };
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemName", Caption = "Tên Báo Cáo", Width = 60, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "CreatedOn", Caption = "Ngày tạo", Width = 10, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ApproveDate", Caption = "Ngày duyệt", Width = 10, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "btnActions", Caption = "Hành động", Width = 10, Filtered = false });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemName,
                    CreatedOn = p.CreatedOn == null ? "-" : p.CreatedOn.Value.ToString(TextUtil.DateLocalFormat),
                    ApproveDate = p.ApproveDate == null ? "-" : p.ApproveDate.Value.ToString(TextUtil.DateLocalFormat),
                    BtnDownload = string.Format("<a class=\"btn btn-default btn100-action\" data-cmd=\"download\" data-id=\"{0}\" data-toggle=\"tooltip\" title=\"Tải Về\"><i class=\"mi-2x mi-cloud-download at-right\"></i><a/>", p.dKey) +
                    (db.REPORTS_HEADERS_FILESATTACHED.Where(d => d.ReportHeaderKey == p.dKey).Count() == 0 ? "" : string.Format("<a class=\"btn btn-default btn100-action\" data-id=\"{0}\" data-cmd=\"filesattach\" data-toggle=\"tooltip\" title=\"File Đính Kèm\"><i class=\"mi-2x mi-attachment at-right\"></i><a/>", p.dKey))
                }).ToList();

                table.CalcTotalPage(q1.Count);
                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult ExportWord_OpenXml(int? ReportHeaderKey)
        {
            AjaxResult zResult = new AjaxResult();
            try
            {
                if (!GrantAccess("REPORTS_HEADERS_EXPORT"))
                    throw new Exception(NOT_PERMISSION);

                if (ReportHeaderKey == null)
                    throw new Exception("không tìm thấy thông tin");

                var itemReportHeader = db.REPORTS_HEADERS.Find(ReportHeaderKey);
                if (itemReportHeader == null)
                    throw new Exception("không tìm thấy thông tin báo cáo");

                var itemTemplate = db.TEMPLATES_HEADERS.Find(itemReportHeader.TemplateKey);
                if (itemTemplate == null)
                    throw new Exception("không tìm thấy thông tin mẫu báo cáo");
                if (!System.IO.File.Exists(Server.MapPath(itemTemplate.CurrentFile)))
                    throw new Exception("không tìm thấy file dữ liệu mẫu");

                DateTime _now = DateTime.Now;
                string file_sample = Server.MapPath(itemTemplate.CurrentFile);
                string file_path = Server.MapPath(Path_RptHeader_Donwload);
                string filename = string.Format("{0}_{1}", Now.Ticks, itemTemplate.CurrentName);

                //multi-export: copy ra 1 file mới để edit & save lại
                System.IO.File.Copy(file_sample, file_path + filename);

                #region lấy dữ liệu

                var query_tables_valueconst = (from v in db.REPORTS_VALUECONST_VALUES
                                               join tv in db.TEMPLATES_VALUES_CONST
                                               on v.TemplateValueConstKey equals tv.dKey
                                               where v.ReportHeaderKey == ReportHeaderKey
                                               orderby tv.Sorted
                                               select new
                                               {
                                                   tv.ItemCode,
                                                   tv.ItemName,
                                                   v.ValueInput,
                                                   tv.Sorted
                                               }).ToList();

                var query_tables_lst = (from template in db.TEMPLATES_HEADERS_TABLES
                                        join tbl in db.TEMPLATES_TABLES on template.TemplateTableKey equals tbl.dKey
                                        where template.TemplateHeaderKey == itemTemplate.dKey
                                        select new
                                        {
                                            template.TemplateHeaderKey,
                                            template.TemplateTableKey,
                                            tbl.ItemCode,
                                            tbl.ItemName
                                        }).ToList();
                #endregion

                using (DocumentFormat.OpenXml.Packaging.WordprocessingDocument doc = DocumentFormat.OpenXml.Packaging.WordprocessingDocument.Open(file_path + filename, true))
                {
                    //var codeList = query_tables_valueconst.ToList().Select(p => p.CodeID).ToList();

                    var body = doc.MainDocumentPart.Document.Body;
                    //var paragraphs = body.Elements<DocumentFormat.OpenXml.Wordprocessing.Paragraph>();
                    var tables = body.Elements<DocumentFormat.OpenXml.Wordprocessing.Table>();


                    #region thay trường dữ liệu

                    //int _pos = -1;
                    //bool _markstart_detected = false;
                    List<int> lst_pos = new List<int>();

                    var text_paragraphs = body.Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>().ToList();

                    #region mark expression

                    string markstart = "<#";
                    string markend = "#>";
                    int paragraph_pos = 0;

                    DataTable dtMatches = new DataTable();
                    dtMatches.Columns.Add("id", typeof(int));
                    dtMatches.Columns.Add("pindex", typeof(int));
                    dtMatches.Columns.Add("expression", typeof(string));
                    dtMatches.Columns.Add("markstartindex", typeof(int));
                    dtMatches.Columns.Add("markendindex", typeof(int));

                    foreach (var text_p in text_paragraphs)
                    {
                        MatchCollection mx_start = Regex.Matches(text_p.Text, markstart);
                        if (mx_start.Count > 0)
                        {
                            for (int i = 0; i < mx_start.Count; i++)
                            {
                                dtMatches.Rows.Add(new object[]
                                {
                                    dtMatches.Rows.Count + 1,
                                    paragraph_pos,
                                    text_p.Text,
                                    mx_start[i].Index,
                                    DBNull.Value
                                });
                            }
                        }

                        MatchCollection mx_end = Regex.Matches(text_p.Text, markend);
                        if (mx_end.Count > 0)
                        {
                            for (int i = 0; i < mx_end.Count; i++)
                            {
                                dtMatches.Rows.Add(new object[]
                                {
                                    dtMatches.Rows.Count + 1,
                                    paragraph_pos,
                                    text_p.Text,
                                    DBNull.Value,
                                    mx_end[i].Index
                                });
                            }
                        }

                        paragraph_pos++;
                    }

                    #endregion

                    #region analyze

                    dtMatches.DefaultView.RowFilter = "markstartindex IS NOT NULL";
                    DataTable dtMxStarts = dtMatches.DefaultView.ToTable();

                    dtMatches.DefaultView.RowFilter = "markendindex IS NOT NULL";
                    DataTable dtMxEnds = dtMatches.DefaultView.ToTable();

                    if (dtMxStarts.Rows.Count == dtMxEnds.Rows.Count)
                    {
                        for (int i = 0; i < dtMxStarts.Rows.Count; i++)
                        {
                            DataRow drStart = dtMxStarts.Rows[i];
                            DataRow drEnd = dtMxEnds.Rows[i];

                            string expr_start = Convert.ToString(drStart["expression"]);
                            int pindex_start = Convert.ToInt32(drStart["pindex"]);
                            int pmark_start = Convert.ToInt32(drStart["markstartindex"]);

                            string expr_end = Convert.ToString(drEnd["expression"]);
                            int pindex_end = Convert.ToInt32(drEnd["pindex"]);
                            int pmark_end = Convert.ToInt32(drEnd["markendindex"]);

                            string expr_all = string.Empty;
                            string temp = "";

                            if (pindex_start == pindex_end)
                            {
                                //mark (start-end) trên cùng 1 paragraph
                                temp = expr_start.Substring(pmark_start, pmark_end + 2 - pmark_start);
                                expr_all = temp;

                                temp = temp.Replace(markstart, "");
                                temp = temp.Replace(markend, "");
                                var item_const_value = query_tables_valueconst.Where(p => p.ItemCode == temp).FirstOrDefault();
                                if (item_const_value != null)
                                {
                                    text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(expr_all, item_const_value.ValueInput);
                                    ApplyFontStyle(text_paragraphs[pindex_start].Parent as DocumentFormat.OpenXml.Wordprocessing.Run);
                                }
                            }
                            else if (pindex_start < pindex_end)
                            {
                                //mark end ở paragraph sau
                                //nối các expr lại thành 1

                                if (pindex_start == pindex_end - 1)
                                {
                                    temp = expr_start.Substring(pmark_start) + expr_end.Substring(0, pmark_end + 2);
                                    expr_all += temp;

                                    temp = temp.Replace(markstart, "");
                                    temp = temp.Replace(markend, "");
                                    var item_const_value = query_tables_valueconst.Where(p => p.ItemCode == temp).FirstOrDefault();
                                    if (item_const_value != null)
                                    {
                                        text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(expr_start.Substring(pmark_start), item_const_value.ValueInput);
                                        text_paragraphs[pindex_end].Text = text_paragraphs[pindex_end].Text.Replace(expr_end.Substring(0, pmark_end + 2), "");

                                        ApplyFontStyle(text_paragraphs[pindex_start].Parent as DocumentFormat.OpenXml.Wordprocessing.Run);
                                    }
                                }
                                else
                                {
                                    for (int ex_index = pindex_start; ex_index <= pindex_end; ex_index++)
                                    {
                                        if (ex_index == pindex_start)
                                        {
                                            temp = text_paragraphs[ex_index].Text.Substring(pmark_start);
                                            expr_all += temp;
                                        }
                                        else if (ex_index == pindex_end)
                                        {
                                            temp = text_paragraphs[ex_index].Text.Substring(0, pmark_end + 2);
                                            expr_all += temp;
                                            text_paragraphs[ex_index].Text = text_paragraphs[ex_index].Text.Replace(temp, "");

                                            if (text_paragraphs[ex_index].Text.Contains(markstart))
                                            {
                                                // still has mark start of next one
                                                //change the start-point
                                                if (i < dtMxStarts.Rows.Count - 1)
                                                    dtMxStarts.Rows[i + 1]["markstartindex"] = text_paragraphs[ex_index].Text.IndexOf(markstart);
                                            }
                                        }
                                        else
                                        {
                                            expr_all += text_paragraphs[ex_index].Text;
                                            text_paragraphs[ex_index].Text = "";
                                        }
                                    }

                                    temp = text_paragraphs[pindex_start].Text.Substring(pmark_start);
                                    var item_const_value = query_tables_valueconst.Where(p => p.ItemCode == temp).FirstOrDefault();
                                    if (item_const_value != null)
                                    {
                                        text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(temp, item_const_value.ValueInput);
                                        ApplyFontStyle(text_paragraphs[pindex_start].Parent as DocumentFormat.OpenXml.Wordprocessing.Run);
                                    }
                                }
                            }
                            else
                            {
                                //do nothing
                            }
                        }
                    }

                    #endregion

                    #endregion

                    #region điền bảng phụ lục

                    //duyệt các bảng phụ lục của report header
                    foreach (var tbl_tableindex in query_tables_lst)
                    {
                        //duyệt qua từng table trong file word
                        foreach (var wtbl in tables.ToList())
                        {
                            //lấy toàn bộ Rows trong table
                            var wtbl_rows = wtbl.Elements<DocumentFormat.OpenXml.Wordprocessing.TableRow>().ToList();
                            if (wtbl_rows.Count == 0)
                                continue;

                            //lấy toàn bộ Cells của first-row
                            var wtbl_cells = wtbl_rows[0].Elements<DocumentFormat.OpenXml.Wordprocessing.TableCell>().ToList();
                            if (wtbl_cells.Count == 0)
                                continue;

                            //lấy toàn bộ paragraphs.Text của first-cell
                            var wtbl_paragraphs = wtbl_cells[0].Elements<DocumentFormat.OpenXml.Wordprocessing.Paragraph>()
                                .SelectMany(p => p.Elements<DocumentFormat.OpenXml.Wordprocessing.Run>())
                                .SelectMany(p => p.Elements<DocumentFormat.OpenXml.Wordprocessing.Text>())
                                .ToList();
                            if (wtbl_paragraphs.Count == 0)
                                continue;

                            /*
                             * wtbl_paragraphs.Count có thể nhiều hơn 1 do table bị qua trang hoặc do Enter-New Row,
                             * nên cộng các paragraph lại thành 1 chuỗi
                             */
                            string wtbl_tableindex_id = string.Join("", wtbl_paragraphs.Select(p => p.Text.Trim()).ToArray());
                            wtbl_tableindex_id = wtbl_tableindex_id.Trim();

                            if (tbl_tableindex.ItemCode == wtbl_tableindex_id)
                            {
                                var data_rows = GetTableData(itemReportHeader.dKey, tbl_tableindex.TemplateTableKey.Value);

                                #region điền dữ liệu vào phụ lục đã đánh dấu trong file word 

                                int count = 0;
                                data_rows.ForEach(row =>
                                {
                                    wtbl.Append(new DocumentFormat.OpenXml.Wordprocessing.TableRow());
                                    var last_row = wtbl.Elements<DocumentFormat.OpenXml.Wordprocessing.TableRow>().ToList().Last();
                                    for (int i = 0; i < row.Length; i++)
                                        last_row.Append(new DocumentFormat.OpenXml.Wordprocessing.TableCell());

                                    if (count == 0)
                                    {
                                        /*
                                         * luôn luôn có 1 dòng chừa trống sẵn để làm mẫu
                                         * khi bắt đầu điền thì tạo thêm 1 dòng phía dưới dòng mẫu để tựu định dạng độ rộng cột theo dòng mẫu
                                         * sau đó sẽ xóa dòng mẫu
                                         */
                                        var rowlist = wtbl.Elements<DocumentFormat.OpenXml.Wordprocessing.TableRow>().ToList();
                                        var sample_row = rowlist[rowlist.Count - 2];
                                        sample_row.Remove();
                                    }

                                    var currentrow = wtbl.Elements<DocumentFormat.OpenXml.Wordprocessing.TableRow>().ToList().Last();
                                    var lst_cells = currentrow.Elements<DocumentFormat.OpenXml.Wordprocessing.TableCell>().ToList();

                                    for (int i = 0; i < row.Length; i++)
                                    {
                                        lst_cells[i].Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(
                                        new DocumentFormat.OpenXml.Wordprocessing.Run(
                                            new DocumentFormat.OpenXml.Wordprocessing.Text(row[i]))));
                                    }
                                    count++;
                                });

                                #endregion

                                #region xóa dòng đầu tiên, dòng đánh dấu phụ lục

                                var first_row = wtbl.Elements<DocumentFormat.OpenXml.Wordprocessing.TableRow>().ToList().First();
                                first_row.Remove();

                                #endregion
                            }

                        }
                    }

                    #endregion

                    if (DocumentFormat.OpenXml.Packaging.OpenXmlPackage.CanSave)
                        doc.Save();
                }

                //throw new Exception("Code ended by user due to test");

                zResult.Result_Status = 1;
                zResult.Result_Message = "Đang kết xuất file...";
                zResult.Data = filename;

                zResult.Result_Message = UPDATE_SUCCESS;
                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    zResult.Result_Status.ToString(),
                    zResult.Result_Message,
                    itemReportHeader.dKey.ToString(),
                    itemReportHeader.ItemCode,
                    itemReportHeader.ItemName
                    );
            }
            catch (Exception ex)
            {
                zResult.Result_Status = 0;
                zResult.Result_Message = ex.Message;
                zResult.Data = null;
            }

            return new CustomJsonResult { Data = (zResult) };
        }

        private List<string[]> GetTableData(int ReportHeaderKey, int TemplateTableKey)
        {
            List<string[]> lstResult = new List<string[]>();
            string msg = string.Empty;
            try
            {
                //lấy danh sách columns template theo thứ tự
                var lst_columns_titles = (from c in db.TEMPLATES_TABLES_COLUMNS
                                          where c.TableTemplateKey == TemplateTableKey
                                          orderby c.Sorted
                                          select new
                                          {
                                              c.dKey,
                                              c.ColumnCode,
                                              c.ColumnCaption,
                                              c.Sorted
                                          }).ToList();
                var lst_col_ids = lst_columns_titles.Select(p => p.dKey).ToList();

                //lấy danh sách assign: có thể assign 1 table cho nhiều người nhập
                var lst_tbl_assigned_key = (from d in db.REPORTS_TABLES_ASSIGNMENT
                                            where d.ReportHeaderKey == ReportHeaderKey && d.TemplateTableKey == TemplateTableKey
                                            select d).Select(p => p.dKey).ToList();

                //lấy danh sách dữ liệu đã nhập (có thể từ nhiều người nhập)
                var lstData = (from d in db.REPORTS_TABLES_COLUMNS_VALUES
                               where lst_tbl_assigned_key.Contains(d.ReportTableAssignedKey) && lst_col_ids.Contains(d.TemplateColumnKey)
                               select d).ToList();
                //lstData = lstData.Where(p => lst_col_ids.Contains(p.TemplateColumnKey)).ToList();

                //phân dữ liệu theo từng người nhập riêng biệt
                var group_data_person = lstData.GroupBy(p => p.CreatedBy).ToList();
                group_data_person.ForEach(group_person =>
                {
                    var group_data_row = group_person.OrderBy(p => p.RowIndex).GroupBy(p => p.RowIndex).ToList();
                    group_data_row.ForEach(group_row =>
                    {
                        var group_row_data = group_row.ToList();
                        var datarow = (from c in lst_columns_titles
                                       join d in group_row_data
                                       on c.dKey equals d.TemplateColumnKey
                                       orderby c.Sorted
                                       select new
                                       {
                                           c.ColumnCode,
                                           c.ColumnCaption,
                                           c.Sorted,
                                           d.ValueInput
                                       }).ToList();
                        List<string> data_array = new List<string>();
                        datarow.ForEach(row =>
                        {
                            data_array.Add(row.ValueInput);
                        });
                        lstResult.Add(data_array.ToArray());
                    });
                });
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return lstResult;
        }

        private void ApplyFontStyle(DocumentFormat.OpenXml.Wordprocessing.Run pRun_Parent)
        {
            if (pRun_Parent != null)
            {
                var runProp = pRun_Parent.Elements<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().FirstOrDefault();
                if (runProp == null)
                {
                    runProp = new DocumentFormat.OpenXml.Wordprocessing.RunProperties();
                    runProp.Append(new DocumentFormat.OpenXml.Wordprocessing.RunFonts() { Ascii = "Times New Roman" });
                    runProp.Append(new DocumentFormat.OpenXml.Wordprocessing.FontSize() { Val = new DocumentFormat.OpenXml.StringValue("26") });//26 half-point font size ==> Font Size in real-set = 13
                    pRun_Parent.RunProperties = runProp;
                }
                else
                {
                    runProp.PrependChild(new DocumentFormat.OpenXml.Wordprocessing.RunFonts() { Ascii = "Times New Roman" });
                    runProp.PrependChild(new DocumentFormat.OpenXml.Wordprocessing.FontSize() { Val = new DocumentFormat.OpenXml.StringValue("26") });//26 half-point font size ==> Font Size in real-set = 13
                }
            }
        }

        public ActionResult DownloadFileExport(string downloadfilename)
        {
            string file_path = Server.MapPath(Path_RptHeader_Donwload + downloadfilename);//filepathfull: no use
            return File(file_path, "application/force-download", downloadfilename);
        }

        public ActionResult GetListAttachFileDetail(int page, int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;

                //danh sách report đang trong trạng thái phân công/đang nhập/nhập liệu xong
                var lstItems = (from d in db.REPORTS_HEADERS_FILESATTACHED
                                where d.ReportHeaderKey == ReportHeaderKey
                                select new
                                {
                                    d.dKey,
                                    d.FileName,
                                    d.CreatedBy,
                                    d.CreatedOn
                                }).ToList();
                TableModel table = new TableModel()
                {
                    Selectable = false
                };
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "", Caption = "Tên File", Width = 50, Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "StatusDesc", Caption = "Người Đính Kèm", Width = 15, Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "FilesCount", Caption = "Ngày Đính Kèm", Width = 15, Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "btnActions", Caption = "Hành Động", Width = 10, Filtered = false });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.FileName,
                    CreatedBy = GetCreatorName(p.CreatedBy),
                    CreatedOn = p.CreatedOn.Value.ToString(TextUtil.DateLocalFormat),
                    BtnActions = string.Format("<a class=\"btn btn-default btn100-action\" data-key=\"{0}\"><i class=\"mi-2x mi-cloud-download at-right\"></i></a>", p.dKey)
                }).ToList();
                table.CalcTotalPage(q1.Count);

                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetFilesAttach(List<int> lstfiles)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (lstfiles != null && lstfiles.Count > 0)
                {
                    var lstDownloadLink = (from d in db.REPORTS_HEADERS_FILESATTACHED
                                           where lstfiles.Contains(d.dKey)
                                           select new
                                           {
                                               d.FileName,
                                               d.FileReference
                                           }).ToList();
                    result.Data = lstDownloadLink;
                }
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult DownloadFile(string filepath, string downloadfilename)
        {
            string server_path = Server.MapPath(filepath);//filepathfull: no use
            return File(server_path, "application/force-download", downloadfilename);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}