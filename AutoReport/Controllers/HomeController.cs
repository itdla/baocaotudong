﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;

namespace AutoReport.Controllers
{
    public class HomeController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();
        private string m_ControllerName = "Home";

        public ActionResult Index()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult WorkDeskMain()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult WorkDoc()
        {
            ViewBag.ControllerName = m_ControllerName;
            ViewBag.FileDoc = "#";
            DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath(PathDoc_UserGuide));
            var fi = dirInfo.GetFiles("*.docx", SearchOption.TopDirectoryOnly).OrderByDescending(p => p.Name).FirstOrDefault();
            if (fi != null)
            {
                ViewBag.FileDoc = PathDoc_UserGuide + fi.Name;
            }
            return View();
        }

        public ActionResult NotPermission()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult InProgress()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult LoadList(int page, int pageSize, string Rpt_ID, string Rpt_Name, int? Rpt_Month, int? Rpt_Year)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */

            Rpt_ID = Rpt_ID ?? "";
            Rpt_Name = Rpt_Name ?? "";
            Rpt_Month = Rpt_Month ?? 0;
            Rpt_Year = Rpt_Year ?? 0;

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter

            var searchQuery = (from f in db.RPT_REPORTHEADER
                               where f.RecordStatus == 1
                               orderby f.Rpt_ID, f.Rpt_Year descending, f.Rpt_Month descending
                               select f).ToList();

            if (MyAccount.IsDefaultUser != null && !MyAccount.IsDefaultUser.Value)
            {
                //org account
                var emp = db.CAT_EMPLOYEES.Find(MyAccount.EmployeeKey);
                if (emp != null)
                {
                    Guid? departmentkey = emp.DepartmentKey;

                    var query_template = db.RPT_TEMPLATES_DEPARTMENT.Where(p => p.DepartmentKey == departmentkey).ToList()
                        .Select(p => p.Rpt_TemplatesKey).ToList();

                    searchQuery = searchQuery.Where(p => query_template.Contains(p.Rpt_TemplatesKey)).ToList();
                }
                else
                {
                    searchQuery.Clear();
                }
            }

            var fq1 = searchQuery.ToList().Select(p => new
            {
                p.Rpt_ReportHeaderKey,
                p.Rpt_ID,
                p.Rpt_Name,
                p.Rpt_Month,
                p.Rpt_Year,
                p.RptStatus
            });

            if (!string.IsNullOrEmpty(Rpt_ID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_ID.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_ID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(Rpt_Name))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_Name.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_Name.Trim().ToLower()))).ToList();
            if (Rpt_Month != 0)
                fq1 = fq1.Where(p => p.Rpt_Month == Rpt_Month).ToList();
            if (Rpt_Year != 0)
                fq1 = fq1.Where(p => p.Rpt_Year == Rpt_Year).ToList();

            int _index = 1;
            var fq2 = fq1.ToList().Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                STT = _index++,
                d.Rpt_ID,
                d.Rpt_Name,
                d.Rpt_Month,
                d.Rpt_Year
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 8, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Báo Cáo", IsShow = true, Format = "", Width = 21, Width_Measure = "%", FieldName = "Rpt_ID" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Báo Cáo", IsShow = true, Format = "", Width = 40, Width_Measure = "%", FieldName = "Rpt_Name" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tháng", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Month", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Năm", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Year", ColumnAlignment = (int)DataContentAlignment.Center });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportHeaderKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadList_RptInput_OnWork(int page, int pageSize, string Searchkey)
        {

            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();

            Guid userkey = MyAccount.UserID;
            Guid rpt_emp = MyAccount.EmployeeKey == null ? Guid.Empty : MyAccount.EmployeeKey.Value;
            int rpt_data_type = Convert.ToInt32(eRptDataType.DataValue);
            List<int?> rptstatus_allow = new List<int?>();
            rptstatus_allow.Add(Convert.ToInt32(eRptHeaderStatus.Created));
            rptstatus_allow.Add(Convert.ToInt32(eRptHeaderStatus.Declined));

            //lấy danh sách report create/return
            var searchQuery = (from f in db.RPT_REPORTHEADER
                               where f.RecordStatus == 1 &&
                               rptstatus_allow.Contains(f.RptStatus)
                               orderby f.Rpt_Year descending, f.Rpt_Month descending
                               select f).ToList();
            var lst_rptkey = searchQuery.Select(p => p.Rpt_ReportHeaderKey).ToList();

            //lấy danh sách các report có phân công nhập trường dữ liệu cho nhân sự
            Guid empkey = MyAccount.EmployeeKey == null ? Guid.Empty : MyAccount.EmployeeKey.Value;
            var lst_value_enlist = (from p in db.RPT_REPORTVALUES_EMPLOYEES_ENLISTED
                                    join d in db.RPT_REPORTDETAIL_VALUECONST on p.Rpt_ReportDetail_ValueConstKey equals d.Rpt_ReportDetail_ValueConstKey
                                    where lst_rptkey.Contains(d.Rpt_ReportHeaderKey) &&
                                    p.EmployeeKey == empkey
                                    select d.Rpt_ReportHeaderKey).ToList();
            //lấy danh sách các report có phân công nhập phụ lục cho nhân sự
            var lst_tables_enlist = (from et in db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                                     join rt in db.RPT_REPORTDETAIL_TABLEINDEX on et.Rpt_ReportDetail_TableIndexKey equals rt.Rpt_ReportDetail_TableIndexKey
                                     where et.EmployeeKey == empkey
                                     select rt.Rpt_ReportHeaderKey).ToList();
            //filter lại 
            searchQuery = searchQuery.Where(p => lst_value_enlist.Contains(p.Rpt_ReportHeaderKey) || lst_tables_enlist.Contains(p.Rpt_ReportHeaderKey)).ToList();
            
            var fq1 = searchQuery.Select(p => new
            {
                p.Rpt_ReportHeaderKey,
                p.Rpt_ID,
                p.Rpt_Name,
                p.Rpt_Month,
                p.Rpt_Year,
                DateEnd = p.DateEnd == null ? "" : p.DateEnd.Value.ToString("dd/MM/yyyy"),
                TinhTrang = GenDateStatus(p.DateEnd),
                Sort = p.DateEnd == null ? 999999 : (Convert.ToInt32((p.DateEnd.Value.Date - DateTime.Now.Date).TotalDays) < 0 ? -1 : Convert.ToInt32((p.DateEnd.Value.Date - DateTime.Now.Date).TotalDays))
            }).ToList();

            int _index = 1;
            var fq2 = fq1.OrderBy(p => p.Sort).Select(p => new
            {
                p.Rpt_ReportHeaderKey,
                STT = _index++,
                p.Rpt_ID,
                p.Rpt_Name,
                p.Rpt_Month,
                p.Rpt_Year,
                p.DateEnd,
                p.TinhTrang
            }).ToList();
            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Báo Cáo", IsShow = true, Format = "", Width = 25, Width_Measure = "%", FieldName = "Rpt_ID" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Báo Cáo", IsShow = true, Format = "", Width = 35, Width_Measure = "%", FieldName = "Rpt_Name" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tháng", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Month", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Năm", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Year", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Ngày hết hạn", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "DateEnd", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Tình trạng", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "TinhTrang", ColumnAlignment = (int)DataContentAlignment.Center });
            //

            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportHeaderKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadList_RptHdr_OnWork(int page, int pageSize, string Searchkey)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            List<int?> rptstatus_allow = new List<int?>();
            rptstatus_allow.Add(Convert.ToInt32(eRptHeaderStatus.Created));
            rptstatus_allow.Add(Convert.ToInt32(eRptHeaderStatus.Declined));

            var searchQuery = (from f in db.RPT_REPORTHEADER
                               where f.RecordStatus == 1 && 
                               rptstatus_allow.Contains(f.RptStatus)
                               orderby f.Rpt_ID, f.Rpt_Year descending, f.Rpt_Month descending
                               select f).ToList();

            if (MyAccount.IsDefaultUser != null && !MyAccount.IsDefaultUser.Value)
            {
                //org account
                var emp = db.CAT_EMPLOYEES.Find(MyAccount.EmployeeKey);
                if (emp != null)
                {
                    Guid? departmentkey = emp.DepartmentKey;

                    var query_template = db.RPT_TEMPLATES_DEPARTMENT.Where(p => p.DepartmentKey == departmentkey).ToList()
                        .Select(p => p.Rpt_TemplatesKey).ToList();

                    searchQuery = searchQuery.Where(p => query_template.Contains(p.Rpt_TemplatesKey)).ToList();

                    var lst_rptkey = searchQuery.Select(p => p.Rpt_ReportHeaderKey).ToList();
                    //lấy danh sách các report có phân công nhập trường dữ liệu cho nhân sự
                    Guid empkey = MyAccount.EmployeeKey == null ? Guid.Empty : MyAccount.EmployeeKey.Value;
                    var lst_value_enlist = (from p in db.RPT_REPORTVALUES_EMPLOYEES_ENLISTED
                                            join d in db.RPT_REPORTDETAIL_VALUECONST on p.Rpt_ReportDetail_ValueConstKey equals d.Rpt_ReportDetail_ValueConstKey
                                            where lst_rptkey.Contains(d.Rpt_ReportHeaderKey) &&
                                            p.EmployeeKey == empkey
                                            select d.Rpt_ReportHeaderKey).ToList();
                    //lấy danh sách các report có phân công nhập phụ lục cho nhân sự
                    var lst_tables_enlist = (from et in db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                                             join rt in db.RPT_REPORTDETAIL_TABLEINDEX on et.Rpt_ReportDetail_TableIndexKey equals rt.Rpt_ReportDetail_TableIndexKey
                                             where et.EmployeeKey == empkey
                                             select rt.Rpt_ReportHeaderKey).ToList();

                    //filter lại 
                    searchQuery = searchQuery.Where(p => lst_value_enlist.Contains(p.Rpt_ReportHeaderKey) || lst_tables_enlist.Contains(p.Rpt_ReportHeaderKey)).ToList();

                }
                else
                {
                    searchQuery.Clear();
                }
            }

            int _index = 1;
            var fq1 = searchQuery.ToList().Select(p => new
            {
                p.Rpt_ReportHeaderKey,
                STT = _index++,
                p.Rpt_ID,
                p.Rpt_Name,
                p.Rpt_Month,
                p.Rpt_Year,
                DateEnd = p.DateEnd == null ? "" : p.DateEnd.Value.ToString("dd/MM/yyyy")
            }).ToList();

            table.intTotalPage = GetTotalPage(fq1.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq1.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Báo Cáo", IsShow = true, Format = "", Width = 25, Width_Measure = "%", FieldName = "Rpt_ID" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Báo Cáo", IsShow = true, Format = "", Width = 45, Width_Measure = "%", FieldName = "Rpt_Name" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tháng", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Month", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Năm", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Year", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Ngày hết hạn", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "DateEnd", ColumnAlignment = (int)DataContentAlignment.Center });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportHeaderKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        private string GenDateStatus(DateTime? dt)
        {
            string result = "-";
            if (dt == null) return result;

            int datediffday = Convert.ToInt32((dt.Value.Date - DateTime.Now.Date).TotalDays);
            if (datediffday < 0)
                result = "Đã quá hạn";
            else if (datediffday == 0)
                result = "Hạn chót hôm nay";
            else if (datediffday < 15)
                result = string.Format("Còn {0} ngày", datediffday + 1);
            else
                result = "Chưa đến hạn";
            return result;
        }
    }
}