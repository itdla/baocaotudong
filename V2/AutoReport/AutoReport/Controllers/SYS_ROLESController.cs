﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using System.Transactions;
using AutoReport.Utils;
using System.Reflection;

namespace AutoReport.Controllers
{
    public class SYS_ROLESController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "CAT_POSITIONS";

        // GET: SYS_ROLES
        public ActionResult Index()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: SYS_ROLES/Create
        public ActionResult Create()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        [HttpPost]
        public ActionResult Create(SYS_ROLES sYS_ROLES)
        {
            if (ModelState.IsValid)
            {
                db.SYS_ROLES.Add(sYS_ROLES);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sYS_ROLES);
        }

        // GET: SYS_ROLES/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.ControllerName = m_ControllerName;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SYS_ROLES sYS_ROLES = db.SYS_ROLES.Find(id);
            if (sYS_ROLES == null)
            {
                return HttpNotFound();
            }
            return View(sYS_ROLES);
        }

        // POST: SYS_ROLES/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "dKey,ItemCode,ItemName,ParentKey,Sorted,LanguageKey,ShowOnMenu,UrlReference,FaIcon,RegularIcon")] SYS_ROLES sYS_ROLES)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sYS_ROLES).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sYS_ROLES);
        }

        // GET: SYS_ROLES/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SYS_ROLES sYS_ROLES = db.SYS_ROLES.Find(id);
            if (sYS_ROLES == null)
            {
                return HttpNotFound();
            }
            return View(sYS_ROLES);
        }

        public ActionResult GetList(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                var lstItems = (from d in db.SYS_ROLES
                                orderby d.ParentKey, d.Sorted
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.Sorted,
                                    d.UrlReference
                                }).ToList();
                if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                    lstItems.Clear();

                TableModel table = new TableModel();
                table.HeaderColumns.Add(new TableHeaderColumns() { Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { Caption = "STT", Width = 9, Alignment = "center" });
                table.HeaderColumns.Add(new TableHeaderColumns() { Caption = "Code", Width = 30 });
                table.HeaderColumns.Add(new TableHeaderColumns() { Caption = "Tên", Width = 60 });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemCode,
                    p.ItemName,
                }).ToList();

                #region manual filter

                string key;
                string filter_expr;
                foreach (TableHeaderColumns tcol in table.HeaderColumns.Where(p => p.Filtered).ToList())
                {
                    key = tcol.FieldName;
                    filter_expr = Request.QueryString[key];

                    if (!string.IsNullOrEmpty(filter_expr))
                    {
                        filter_expr = TextUtil.RemoveVni(Convert.ToString(filter_expr)).ToLower();

                        q1 = q1.Where(p => TextUtil.RemoveVni(Convert.ToString(p.GetType().GetProperty(key).GetValue(p, null)).ToLower()).Contains(filter_expr)).ToList();

                        table.FilterValues.Add(key, filter_expr ?? "");
                    }
                }

                #endregion

                table.CalcTotalPage(q1.Count);

                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
