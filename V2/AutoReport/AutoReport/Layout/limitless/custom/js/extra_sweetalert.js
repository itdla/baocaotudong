﻿/* ------------------------------------------------------------------------------
 *
 *  # Sweet Alert component
 *
 *  Demo JS code for extra_sweetalert.html page
 *
 * ---------------------------------------------------------------------------- */

function swal_success(caption, msg, confirm, delay) {
    swal({
        title: caption,
        text: msg,
        type: 'success',
        showConfirmButton: confirm,
        timer: delay
    });
}

function swal_success_redirect(caption, msg, confirm, delay, url) {
    swal({
        title: caption,
        text: msg,
        type: 'success',
        showConfirmButton: confirm,
        timer: delay
    }).then(function (r1) {
        if (r1.dismiss == "timer") {
            window.location.href = url;
        }
    });
}

function swal_error(caption, msg, confirm, delay) {
    swal({
        title: caption,
        text: msg,
        type: 'error',
        showConfirmButton: confirm,
        timer: delay
    });
}

function swal_warning(caption, msg, confirm, delay) {
    swal({
        title: caption,
        text: msg,
        type: 'warning',
        showConfirmButton: confirm,
        timer: delay
    });
}

function swal_delete(caption, msg, urlaction) {
    swal({
        title: caption,
        text: msg,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Xóa!',
        cancelButtonText: 'Trở lại!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
    }).then(function (r1) {
        //if cancel ==> r1.dismiss: Dismiss can be 'cancel', 'overlay', 'close', and 'timer'
        if (r1.value == true) {
            $.get(urlaction, function (ajresult) {
                if (ajresult.Result_Status == 1) {
                    swal({
                        title: 'Xóa thành công!',
                        text: 'Dữ liệu đã bị xóa!',
                        type: 'success',
                        showConfirmButton: false,
                        confirmButtonText: 'Đóng',
                        confirmButtonClass: 'btn btn-success',
                        timer: 1800
                    }).then(function (r2) {
                        if (r2.dismiss == "timer") {
                            if (ajresult.RedirectTo == null || ajresult.RedirectTo == "") {
                                //do something
                            }
                            else {
                                window.location.href = ajresult.RedirectTo;
                            }

                        }
                    });
                }
                else {
                    swal({
                        title: 'Đã xãy ra lỗi!',
                        text: ajresult.Result_Message,
                        type: 'error',
                        showConfirmButton: true,
                        confirmButtonText: 'Đóng',
                        confirmButtonClass: 'btn btn-success',
                    });
                }
            });
        }
    });
}

