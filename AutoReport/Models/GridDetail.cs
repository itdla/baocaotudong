﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoReport.Models
{
    public class GridDetail
    {
        public bool ShowAction { get; set; }
        public List<object> Data { get; set; }
        public List<HeaderColumns> Header { get; set; }

        public GridDetail()
        {
            ShowAction = true;
            Data = new List<object>();
            Header = new List<HeaderColumns>();
        }
        public class HeaderColumns
        {
            public HeaderColumns()
            {
                DataType = eDataType.StringType;
                Align = eAlign.None;
                PlaceHolderText = string.Empty;
                Enable = true;
                IsShow = true;
            }
            public string Field { get; set; }

            public string Caption { get; set; }
            public string PlaceHolderText { get; set; }
            public string Width { get; set; }
            public eDataType DataType { get; set; }
            public eAlign Align { get; set; }
            public eLayoutType LayoutType { get; set; }

            public List<SelectListItem> DataSource { get; set; }

            public bool Enable { get; set; }
            public bool IsShow { get; set; }
        }

        public enum eDataType
        {
            StringType = 0,
            IntegerType = 1,
            DecimalType = 2,
            Date = 3,
            Time = 4,
            DateTime = 5,
            Image = 6,
        }

        public enum eAlign
        {
            Left = 1,
            Right = 2,
            Center = 3,
            None = 0
        }

        public enum eLayoutType
        {
            STT = 1,
            DropdownList = 2,
            Hide = 3
        }
    }
}