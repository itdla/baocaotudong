﻿using AutoReport.Models;
using AutoReport.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AutoReport.Controllers
{
    public class HomeController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "Home";

        public ActionResult Index()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult DocData()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }
        public ActionResult NotPermission()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }
        public ActionResult DataRestricted()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }
        public ActionResult NotFound()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }
        public ActionResult BaseHeader()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        public ActionResult GetSummaryChartColumns()
        {
            AjaxResult result = new AjaxResult();
            try
            {
                DateTime StartDateYear = new DateTime(Now.Year, 1, 1, 0, 0, 0);
                DateTime EndDateYear = new DateTime(Now.Year, 12, 31, 23, 59, 59);

                var lstItems = (from d in db.REPORTS_HEADERS
                                where d.RecordStatus == 1 && (StartDateYear == null || (StartDateYear != null && d.CreatedOn >= StartDateYear)) &&
                                (EndDateYear == null || (EndDateYear != null && d.CreatedOn <= EndDateYear))
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.ApproveStatus,
                                    d.DateEnd,
                                    d.CreatedOn
                                }).ToList();

                List<object> datasource = new List<object>();
                //Random rnd = new Random();
                for (int i = 1; i < 13; i++)
                {
                    DateTime FromDate = new DateTime(Now.Year, i, 1, 0, 0, 0);
                    DateTime ToDate = DateTime.ParseExact(FromDate.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy") + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);

                    var lstInMonth = lstItems.Where(p => p.CreatedOn.Value >= FromDate && p.CreatedOn <= ToDate).ToList();

                    //hoàn thành
                    int count_status = lstInMonth.Where(p => p.ApproveStatus == 5).Count();
                    //count_status = rnd.Next(7, 18);//test
                    datasource.Add(new { x = "Tháng " + i, y = count_status, group = "c" });

                    //chưa hoàn thành
                    count_status = lstInMonth.Where(p => p.ApproveStatus != 5).Count();
                    //count_status = rnd.Next(0, 7);//test
                    datasource.Add(new { x = "Tháng " + i, y = count_status, group = "u" });
                }

                result.Data = datasource;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListDocuments(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;

                TableModel table = new TableModel()
                {
                    Selectable = false
                };
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "DocName", Caption = "Tài liệu", Width = 80, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "Action", Caption = "Thao Tác", Width = 10, Filtered = true });

                table.CalcTotalPage(1);
                table.DataSource.Add(new TableRowsData()
                {
                    DataKey = 0,
                    DataArray = new string[] { "0", "1", "Hướng Dẫn Sử Dụng", "<a class=\"btn btn-default btn100-action\" href=\"/Doc/userguide_V1_1.docx\"><i class=\"mi-2x mi-cloud-download at-right\"></i></a>" }
                });
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }
    }
}