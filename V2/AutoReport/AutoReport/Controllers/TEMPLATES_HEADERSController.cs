﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using System.Transactions;
using AutoReport.Utils;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace AutoReport.Controllers
{
    public class TEMPLATES_HEADERSController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "TEMPLATES_HEADERS";

        // GET: TEMPLATES_HEADERS/Index
        public ActionResult Index()
        {
            if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: TEMPLATES_HEADERS/Create
        public ActionResult Create()
        {
            if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                return B_NotPermission();

            TEMPLATES_HEADERS item = new TEMPLATES_HEADERS();
            ViewBag.ControllerName = m_ControllerName;
            return View(item);
        }

        [HttpPost]
        public ActionResult Create(TEMPLATES_HEADERS ItemModel, List<string[]> data_tables, List<string[]> data_values, string SaveType)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, true);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);

                using (TransactionScope scope = new TransactionScope())
                {
                    ItemModel.CreatedBy = MyAccount.Model.dKey.ToString();
                    ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                    ItemModel.CreatedOn = Now;
                    ItemModel.ModifiedOn = Now;
                    ItemModel.RecordStatus = 1;

                    db.TEMPLATES_HEADERS.Add(ItemModel);
                    db.SaveChanges();

                    #region tables template selected
                    int marked = 0;
                    if (data_tables != null)
                    {
                        foreach (string[] arr in data_tables)
                        {
                            if (arr.Where(p => p.Trim() == "").Count() == arr.Length)
                                continue;
                            if (string.IsNullOrEmpty(arr[0])) continue;
                            TEMPLATES_HEADERS_TABLES tbl = new TEMPLATES_HEADERS_TABLES();
                            tbl.TemplateHeaderKey = ItemModel.dKey;
                            tbl.TemplateTableKey = Convert.ToInt32(arr[2]);

                            db.TEMPLATES_HEADERS_TABLES.Add(tbl);
                            marked++;
                        }
                        if (marked > 0)
                            db.SaveChanges();
                    }

                    #endregion

                    #region values const

                    marked = 0;
                    if (data_values != null)
                    {
                        int _index = 1;
                        foreach (string[] arr in data_values)
                        {
                            if (arr.Where(p => p.Trim() == "").Count() == arr.Length)
                                continue;
                            if (string.IsNullOrEmpty(arr[0])) continue;
                            TEMPLATES_VALUES_CONST vcon = new TEMPLATES_VALUES_CONST();
                            vcon.TemplateHeaderKey = ItemModel.dKey;
                            vcon.ItemCode = arr[1].Trim();
                            vcon.ItemName = arr[2].Trim();
                            vcon.Sorted = _index++;

                            db.TEMPLATES_VALUES_CONST.Add(vcon);
                            marked++;
                        }
                        if (marked > 0)
                            db.SaveChanges();
                    }

                    #endregion

                    scope.Complete();
                }
                result.Data = ItemModel;
                result.Result_Status = 1;
                result.Result_Message = CREATE_SUCCESS;
                if (SaveType == "savenew")
                    result.RedirectTo = string.Format("/{0}/Create", m_ControllerName);
                else
                    result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        // GET: TEMPLATES_HEADERS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            TEMPLATES_HEADERS item = db.TEMPLATES_HEADERS.Find(id);
            if (item == null)
                return B_NotFound();
            if (item.RecordStatus == 0)
                return B_NotFound();

            item.CreatedBy = GetCreatorName(item.CreatedBy);
            item.ModifiedBy = GetCreatorName(item.ModifiedBy);
            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(TEMPLATES_HEADERS ItemModel, List<string[]> data_tables, List<string[]> data_values, List<string> tables_del, List<string> values_del)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, false);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);

                TEMPLATES_HEADERS oldItem = db.TEMPLATES_HEADERS.Find(ItemModel.dKey);
                if (oldItem == null)
                    throw new Exception(NOT_FOUND);

                //save model
                string[] arr_model = { "ItemCode", "ItemName", "Sorted", "Remarks" };
                foreach (string pName in arr_model)
                {
                    PropertyInfo pInfo = oldItem.GetType().GetProperty(pName);
                    object v_to_set = ItemModel.GetType().GetProperty(pName).GetValue(ItemModel, null);
                    pInfo.SetValue(oldItem, v_to_set);
                }
                oldItem.ModifiedBy = MyAccount.Model.dKey.ToString();
                oldItem.ModifiedOn = Now;
                db.Entry(oldItem).State = EntityState.Modified;

                //kiểm tra mẫu này có đang sử dụng, nếu có, không cho thay đổi phụ lục hoặc trường dữ liệu
                if (db.REPORTS_HEADERS.Where(p => p.TemplateKey == ItemModel.dKey).Count() == 0)
                {
                    #region tables template selected

                    if (data_tables != null)
                    {
                        foreach (string[] arr in data_tables)
                        {
                            if (arr.Where(p => p.Trim() == "").Count() == arr.Length)
                                continue;
                            string keyStr = arr[0];
                            if (keyStr == "" || keyStr == "0")
                            {
                                //add new or del only, no edit
                                TEMPLATES_HEADERS_TABLES tbl = new TEMPLATES_HEADERS_TABLES();
                                tbl.TemplateHeaderKey = ItemModel.dKey;
                                tbl.TemplateTableKey = Convert.ToInt32(arr[2]);
                                db.TEMPLATES_HEADERS_TABLES.Add(tbl);
                            }
                        }
                    }

                    //delete marked tables
                    if (tables_del != null)
                    {
                        foreach (string keyStr in tables_del)
                        {
                            TEMPLATES_HEADERS_TABLES tbl = db.TEMPLATES_HEADERS_TABLES.Find(Convert.ToInt32(keyStr));
                            if (tbl != null)
                                db.Entry(tbl).State = EntityState.Deleted;
                        }
                    }

                    #endregion

                    #region values const

                    if (data_values != null)
                    {
                        int _index = 1;
                        foreach (string[] arr in data_values)
                        {
                            if (arr.Where(p => p.Trim() == "").Count() == arr.Length)
                                continue;
                            string objKey = Convert.ToString(arr[0]).Trim();
                            string objstate = Convert.ToString(arr[1]).Trim().ToLower();
                            if (string.IsNullOrEmpty(objKey) || objKey == "0")
                            {
                                TEMPLATES_VALUES_CONST vcon = new TEMPLATES_VALUES_CONST();
                                vcon.TemplateHeaderKey = ItemModel.dKey;
                                vcon.ItemCode = arr[2].Trim();
                                vcon.ItemName = arr[3].Trim();
                                vcon.Sorted = _index++;

                                db.TEMPLATES_VALUES_CONST.Add(vcon);
                            }
                            else
                            {
                                if (objstate != "e") continue;// no changed row

                                TEMPLATES_VALUES_CONST vcon = db.TEMPLATES_VALUES_CONST.Find(Convert.ToInt32(objKey));
                                vcon.TemplateHeaderKey = ItemModel.dKey;
                                vcon.ItemCode = arr[2].Trim();
                                vcon.ItemName = arr[3].Trim();
                                db.Entry(vcon).State = EntityState.Modified;
                            }
                        }
                    }

                    if (values_del != null)
                    {
                        foreach (string keyStr in values_del)
                        {
                            TEMPLATES_VALUES_CONST vcon = db.TEMPLATES_VALUES_CONST.Find(Convert.ToInt32(keyStr));
                            if (vcon != null)
                                db.Entry(vcon).State = EntityState.Deleted;
                        }
                    }

                    #endregion

                    result.Result_Status = 1;
                }
                else
                {
                    result.Result_Status = -1;
                    result.Result_Message = "Mẫu hiện đang được sử dụng, trường dữ liệu, các bảng phụ lục và file mẫu sẽ không được cập nhật";
                }

                db.SaveChanges();
                result.Result_Message = UPDATE_SUCCESS;
                result.Data = ItemModel;
                result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        private string CheckBeforeSave(TEMPLATES_HEADERS ItemModel, bool isNew)
        {
            if (string.IsNullOrEmpty(ItemModel.ItemCode))
                return "Mã biểu mẫu không được để trống";
            if (string.IsNullOrEmpty(ItemModel.ItemName))
                return "Tên biểu mẫu không được để trống";

            ItemModel.ItemCode = ItemModel.ItemCode.Trim();
            var itemdub = db.TEMPLATES_HEADERS.Where(p => p.ItemCode == ItemModel.ItemCode).ToList();
            if (isNew)
            {
                if (itemdub.Count != 0)
                    return "Mã biểu mẫu đã tồn tại";
            }
            else
            {
                if (itemdub.Where(p => p.dKey != ItemModel.dKey).Count() != 0)
                    return "Mã biểu mẫu đã tồn tại";

                //var lst_ReportHeader = db.REPORTS_HEADERS.Where(p => p.TemplateKey == ItemModel.dKey).Count();
                //if (lst_ReportHeader > 0)
                //    return "Biểu mẫu hiện đang được sử dụng. Vui lòng sao chép sang mẫu mới để chỉnh sửa";
            }
            return string.Empty;
        }

        public ActionResult UploadFileTemplate(HttpPostedFileBase file)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                int id = Request["id"] == null ? 0 : Convert.ToInt32(Request["id"]);
                TEMPLATES_HEADERS ItemModel = db.TEMPLATES_HEADERS.Find(id);
                if (ItemModel == null)
                    throw new Exception(NOT_FOUND);

                if (file != null)
                {
                    if (!string.IsNullOrEmpty(ItemModel.CurrentFile))
                        DeleteTemplateMain_CurrentFile(ItemModel.CurrentFile);

                    string revision_name = string.Format("{0}_{1}", Now_Full_String(), file.FileName);
                    string revision_file = string.Format("{0}{1}", Path_Template_Revision, revision_name);

                    ItemModel.CurrentName = file.FileName;
                    ItemModel.CurrentFile = Path_Template_Main + file.FileName;
                    ItemModel.RevisionName = revision_name;
                    ItemModel.RevisionFile = revision_file;
                    db.Entry(ItemModel).State = EntityState.Modified;

                    TEMPLATES_REVISIONS revision = new TEMPLATES_REVISIONS();
                    revision.TemplateKey = ItemModel.dKey;
                    revision.CurrentName = ItemModel.CurrentName;
                    revision.CurrentFile = ItemModel.CurrentFile;
                    revision.RevisionName = ItemModel.RevisionName;
                    revision.RevisionFile = ItemModel.RevisionFile;
                    revision.CreatedBy = MyAccount.Model.dKey.ToString();
                    revision.ModifiedBy = MyAccount.Model.dKey.ToString();
                    revision.CreatedOn = Now;
                    revision.ModifiedOn = Now;
                    db.TEMPLATES_REVISIONS.Add(revision);

                    file.SaveAs(Server.MapPath(ItemModel.CurrentFile));
                    file.SaveAs(Server.MapPath(ItemModel.RevisionFile));

                    string msg = string.Empty;
                    Dictionary<string, string> lstFields = GenerateFieldsFromDocx(Server.MapPath(ItemModel.CurrentFile), out msg);
                    if (!string.IsNullOrEmpty(msg))
                        throw new Exception(msg);

                    int sort = 1;
                    foreach (KeyValuePair<string, string> field in lstFields)
                    {
                        TEMPLATES_VALUES_CONST itemvalue = new TEMPLATES_VALUES_CONST();
                        itemvalue.TemplateHeaderKey = ItemModel.dKey;
                        itemvalue.ItemCode = field.Key.Trim();
                        itemvalue.ItemName = field.Value.Trim();
                        itemvalue.Sorted = sort++;
                        db.TEMPLATES_VALUES_CONST.Add(itemvalue);
                    }

                    db.SaveChanges();
                }
                result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        private void DeleteTemplateMain_CurrentFile(string path)
        {
            try
            {
                if (System.IO.File.Exists(Server.MapPath(path)))
                    System.IO.File.Delete(path);
            }
            catch
            {

            }
        }

        private Dictionary<string, string> GenerateFieldsFromDocx(string filedocx, out string msg)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            msg = string.Empty;
            try
            {
                using (DocumentFormat.OpenXml.Packaging.WordprocessingDocument doc = DocumentFormat.OpenXml.Packaging.WordprocessingDocument.Open(filedocx, true))
                {
                    var body = doc.MainDocumentPart.Document.Body;

                    string markstart = "<#";
                    string markend = "#>";
                    int paragraph_pos = 0;

                    DataTable dtMatches = new DataTable();
                    dtMatches.Columns.Add("id", typeof(int));
                    dtMatches.Columns.Add("pindex", typeof(int));
                    dtMatches.Columns.Add("expression", typeof(string));
                    dtMatches.Columns.Add("markstartindex", typeof(int));
                    dtMatches.Columns.Add("markendindex", typeof(int));

                    #region lấy mark expression

                    var text_paragraphs = body.Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>().ToList();

                    foreach (var text_p in text_paragraphs)
                    {
                        MatchCollection mx_start = Regex.Matches(text_p.Text, markstart);
                        if (mx_start.Count > 0)
                        {
                            for (int i = 0; i < mx_start.Count; i++)
                            {
                                dtMatches.Rows.Add(new object[]
                                {
                                    dtMatches.Rows.Count + 1,
                                    paragraph_pos,
                                    text_p.Text,
                                    mx_start[i].Index,
                                    DBNull.Value
                                });
                            }
                        }

                        MatchCollection mx_end = Regex.Matches(text_p.Text, markend);
                        if (mx_end.Count > 0)
                        {
                            for (int i = 0; i < mx_end.Count; i++)
                            {
                                dtMatches.Rows.Add(new object[]
                                {
                                    dtMatches.Rows.Count + 1,
                                    paragraph_pos,
                                    text_p.Text,
                                    DBNull.Value,
                                    mx_end[i].Index
                                });
                            }
                        }

                        paragraph_pos++;
                    }

                    #endregion

                    #region analyze

                    dtMatches.DefaultView.RowFilter = "markstartindex IS NOT NULL";
                    DataTable dtMxStarts = dtMatches.DefaultView.ToTable();

                    dtMatches.DefaultView.RowFilter = "markendindex IS NOT NULL";
                    DataTable dtMxEnds = dtMatches.DefaultView.ToTable();

                    //dtMxStarts.Rows.Count == dtMxEnds.Rows.Count
                    //data mở và đóng phải bằng nhau
                    if (dtMxStarts.Rows.Count == dtMxEnds.Rows.Count)
                    {
                        for (int i = 0; i < dtMxStarts.Rows.Count; i++)
                        {
                            DataRow drStart = dtMxStarts.Rows[i];
                            DataRow drEnd = dtMxEnds.Rows[i];

                            string expr_start = Convert.ToString(drStart["expression"]);
                            int pindex_start = Convert.ToInt32(drStart["pindex"]);
                            int pmark_start = Convert.ToInt32(drStart["markstartindex"]);

                            string expr_end = Convert.ToString(drEnd["expression"]);
                            int pindex_end = Convert.ToInt32(drEnd["pindex"]);
                            int pmark_end = Convert.ToInt32(drEnd["markendindex"]);

                            string expr_all = string.Empty;
                            string temp = "";

                            if (pindex_start == pindex_end)
                            {
                                //mark (start-end) trên cùng 1 paragraph
                                temp = expr_start.Substring(pmark_start, pmark_end + 2 - pmark_start);
                                expr_all = temp;

                                string[] arr_expr = StandardizeExpression(expr_all, markstart, markend, '|');
                                //0: code
                                //1: description
                                if (result.Where(p => p.Key == arr_expr[0]).Count() == 0)
                                    result.Add(arr_expr[0], arr_expr[1]);

                                text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(expr_all, markstart + arr_expr[0] + markend);
                            }
                            else if (pindex_start < pindex_end)
                            {
                                //mark end ở paragraph sau
                                //nối các expr lại thành 1

                                if (pindex_start == pindex_end - 1)
                                {
                                    temp = expr_start.Substring(pmark_start) + expr_end.Substring(0, pmark_end + 2);
                                    expr_all += temp;

                                    string[] arr_expr = StandardizeExpression(expr_all, markstart, markend, '|');
                                    //0: code
                                    //1: description
                                    if (result.Where(p => p.Key == arr_expr[0]).Count() == 0)
                                        result.Add(arr_expr[0], arr_expr[1]);

                                    text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(expr_start.Substring(pmark_start), markstart + arr_expr[0] + markend);
                                    text_paragraphs[pindex_end].Text = text_paragraphs[pindex_end].Text.Replace(expr_end.Substring(0, pmark_end + 2), "");
                                }
                                else
                                {

                                    for (int ex_index = pindex_start; ex_index <= pindex_end; ex_index++)
                                    {
                                        if (ex_index == pindex_start)
                                        {
                                            temp = text_paragraphs[ex_index].Text.Substring(pmark_start);
                                            expr_all += temp;
                                            //text_paragraphs[ex_index].Text = text_paragraphs[ex_index].Text.Replace(temp, "[the start-point string]");
                                        }
                                        else if (ex_index == pindex_end)
                                        {
                                            temp = text_paragraphs[ex_index].Text.Substring(0, pmark_end + 2);
                                            expr_all += temp;
                                            text_paragraphs[ex_index].Text = text_paragraphs[ex_index].Text.Replace(temp, "");

                                            if (text_paragraphs[ex_index].Text.Contains(markstart))
                                            {
                                                // still has mark start of next one
                                                //change the start-point
                                                if (i < dtMxStarts.Rows.Count - 1)
                                                    dtMxStarts.Rows[i + 1]["markstartindex"] = text_paragraphs[ex_index].Text.IndexOf(markstart);
                                            }
                                        }
                                        else
                                        {
                                            expr_all += text_paragraphs[ex_index].Text;
                                            text_paragraphs[ex_index].Text = "";
                                        }
                                    }

                                    string[] arr_expr = StandardizeExpression(expr_all, markstart, markend, '|');
                                    //0: code
                                    //1: description
                                    if (result.Where(p => p.Key == arr_expr[0]).Count() == 0)
                                        result.Add(arr_expr[0], arr_expr[1]);

                                    temp = text_paragraphs[pindex_start].Text.Substring(pmark_start);
                                    text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(temp, markstart + arr_expr[0] + markend);
                                }
                            }
                            else
                            {
                                //do nothing
                            }
                        }
                    }
                    else
                    {
                        msg = "Dữ liệu định dạng không hợp lệ. Vui lòng xem lại file mẫu";
                    }
                    #endregion

                    if (string.IsNullOrEmpty(msg))
                    {
                        if (DocumentFormat.OpenXml.Packaging.OpenXmlPackage.CanSave)
                            doc.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return result;
        }

        private string[] StandardizeExpression(string expression, string mark_start, string mark_end, char marksplit)
        {
            if (!string.IsNullOrEmpty(mark_start))
                expression = expression.Replace(mark_start, "");
            if (!string.IsNullOrEmpty(mark_end))
                expression = expression.Replace(mark_end, "");

            return expression.Split(marksplit);
        }

        // GET: TEMPLATES_HEADERS/Delete/5
        public ActionResult Delete(int? id)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_DELETE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                TEMPLATES_HEADERS model = db.TEMPLATES_HEADERS.Find(id);
                if (model == null)
                    throw new Exception(NOT_FOUND);

                model.ModifiedBy = MyAccount.Model.dKey.ToString();
                model.ModifiedOn = Now;
                model.RecordStatus = 0;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                result.Result_Message = DELETE_SUCCESS;
                result.RedirectTo = string.Format("/{0}/Index", m_ControllerName);
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    model.dKey.ToString(),
                    model.ItemCode,
                    model.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetList(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                var lstItems = (from d in db.TEMPLATES_HEADERS
                                where d.RecordStatus == 1
                                orderby d.ItemName
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.RevisionName,
                                    d.RevisionFile,
                                    d.Remarks
                                }).ToList();

                if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                    lstItems.Clear();

                TableModel table = new TableModel();
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemCode", Caption = "Mã Báo Cáo", Width = 15, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemName", Caption = "Tên Báo cáo", Width = 35, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "", Caption = "File Mẫu", Width = 10, Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "TablesCount", Caption = "Phụ lục", Width = 5, Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "FieldsCount", Caption = "Trường", Width = 5, Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "Remarks", Caption = "Ghi Chú", Width = 20, Filtered = true });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemCode,
                    p.ItemName,
                    ReportFile = string.IsNullOrEmpty(p.RevisionFile) ? "" : string.Format("<a class=\"btn btn-primary linkfile\" href=\"#\">Tải Về</a>", p.RevisionFile),
                    TablesCount = db.TEMPLATES_HEADERS_TABLES.Where(t => t.TemplateHeaderKey == p.dKey).Count(),
                    FieldsCount = db.TEMPLATES_VALUES_CONST.Where(t => t.TemplateHeaderKey == p.dKey).Count(),
                    p.Remarks
                }).ToList();

                #region manual filter

                string key;
                string filter_expr;
                foreach (TableHeaderColumns tcol in table.HeaderColumns.Where(p => p.Filtered).ToList())
                {
                    key = tcol.FieldName;
                    filter_expr = Request.QueryString[key];

                    if (!string.IsNullOrEmpty(filter_expr))
                    {
                        filter_expr = TextUtil.RemoveVni(Convert.ToString(filter_expr)).ToLower();

                        q1 = q1.Where(p => TextUtil.RemoveVni(Convert.ToString(p.GetType().GetProperty(key).GetValue(p, null)).ToLower()).Contains(filter_expr)).ToList();

                        table.FilterValues.Add(key, filter_expr ?? "");
                    }
                }

                #endregion

                table.CalcTotalPage(q1.Count);

                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetTablesTemplate(int? headertemplatekey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "Key", "State", "Table Template Key", "Bảng Phụ Lục" };
                table.ColumnsHidden = new int[] { 0, 1, 2 };

                var query = (from h in db.TEMPLATES_HEADERS_TABLES
                             join t in db.TEMPLATES_TABLES
                             on h.TemplateTableKey equals t.dKey
                             where h.TemplateHeaderKey == headertemplatekey
                             orderby t.Sorted
                             select new
                             {
                                 h.dKey,
                                 State = "",
                                 h.TemplateTableKey,
                                 ItemName = t.ItemCode + " - " + t.ItemName
                             }).ToList();
                if (query.Count > 0)
                {
                    query.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "", "" });
                }
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetValuesConst(int? headertemplatekey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "Key", "State", "Mã Nhận Dạng", "Mô Tả" };
                table.ColumnsHidden = new int[] { 0, 1 };

                var query = (from v in db.TEMPLATES_VALUES_CONST
                             where v.TemplateHeaderKey == headertemplatekey
                             select new
                             {
                                 v.dKey,
                                 State = "",
                                 v.ItemCode,
                                 v.ItemName
                             }).ToList();

                if (query.Count > 0)
                {
                    query.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "", "" });
                }
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        public ActionResult CopyToNew(int? id)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_COPY", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                TEMPLATES_HEADERS model = db.TEMPLATES_HEADERS.Find(id);
                if (model == null)
                    throw new Exception(NOT_FOUND);

                var lst_columns = (from d in db.TEMPLATES_TABLES_COLUMNS
                                   where d.TableTemplateKey == model.dKey
                                   orderby d.Sorted
                                   select d).ToList();

                TEMPLATES_HEADERS model_copy = new TEMPLATES_HEADERS();
                foreach (PropertyInfo p_model in model_copy.GetType().GetProperties())
                {
                    p_model.SetValue(model_copy, model.GetType().GetProperty(p_model.Name).GetValue(model, null));
                }
                model_copy.dKey = 0;
                model_copy.ItemCode = "(copy) - " + model_copy.ItemCode;
                model_copy.ItemName = "(copy) - " + model_copy.ItemName;
                model_copy.CurrentFile = null;
                model_copy.CurrentName = null;
                model_copy.RevisionFile = null;
                model_copy.RevisionName = null;
                model_copy.RecordStatus = 1;
                model_copy.CreatedBy = MyAccount.Model.dKey.ToString();
                model_copy.ModifiedBy = MyAccount.Model.dKey.ToString();
                model_copy.CreatedOn = Now;
                model_copy.ModifiedOn = Now;
                db.TEMPLATES_HEADERS.Add(model_copy);
                db.SaveChanges();

                result.Result_Caption = "Sao chép thành công";
                result.Result_Message = "Đã sao chép mẫu báo cáo";
                result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, model_copy.dKey);
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetFileDownload(int? id)
        {
            TEMPLATES_HEADERS model = db.TEMPLATES_HEADERS.Find(id);
            return File(Server.MapPath(model.RevisionFile), "application/force-download", model.CurrentName);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult LoadListTablesTemplateSelection(int? headertemplatekey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                var query_tables = (from d in db.TEMPLATES_TABLES
                                    orderby d.ReportRef, d.ItemCode
                                    select new
                                    {
                                        d.dKey,
                                        d.ItemCode,
                                        d.ItemName,
                                        d.ReportRef
                                    }).ToList();

                var lst_hdr_tblskey = db.TEMPLATES_HEADERS_TABLES.Where(p => p.TemplateHeaderKey == headertemplatekey).Select(p => p.TemplateTableKey).ToList();

                StringBuilder builder = new StringBuilder();
                string nl = Environment.NewLine;

                builder.Append("<div class=\"col-md-12\">");
                builder.Append("<ul class=\"ultree\">" + nl);

                var datagroup = query_tables.GroupBy(p => p.ReportRef).ToList();
                int group_index = 1;
                int child_index = 1;
                foreach (var group in datagroup)
                {
                    var lstTbls = group.ToList();
                    string chk_parent = lstTbls.Where(p => lst_hdr_tblskey.Contains(p.dKey)).Count() > 0 ? " checked" : "";
                    builder.Append("<li aria-expanded=\"true\">" + nl);
                    builder.Append(string.Format("<input type=\"checkbox\" id=\"gri{0}\" class=\"fancytree-checkbox\"{1}/>", group_index, chk_parent) + nl);
                    builder.Append(string.Format("<label for=\"gri{0}\" class=\"fancytree-title\">{1}</label>", group_index, string.IsNullOrEmpty(group.Key) ? "(Chưa phân nhóm)" : group.Key) + nl);

                    builder.Append("<ul class=\"ultree\">" + nl);
                    foreach (var tbl in lstTbls)
                    {
                        builder.Append("<li aria-expanded=\"true\">" + nl);
                        string chk_child = lst_hdr_tblskey.IndexOf(tbl.dKey) != -1 ? "checked" : "";
                        builder.Append(string.Format("<input type=\"checkbox\" id=\"tbl{0}\" data-parent=\"gri{1}\" data-key=\"{2}\" data-id=\"{3}\" data-name=\"{4}\" class=\"fancytree-checkbox\"{5}/>",
                            child_index, group_index, tbl.dKey, tbl.ItemCode, tbl.ItemName, chk_child) + nl);
                        builder.Append(string.Format("<label for=\"tbl{0}\" class=\"fancytree-title\">{1} - {2}</label>", child_index, tbl.ItemCode, tbl.ItemName) + nl);
                        builder.Append("</li>");
                        child_index++;
                    }
                    builder.Append("</ul>");
                    builder.Append("</li>");

                    group_index++;
                }
                builder.Append("</ul>");
                builder.Append("</div>");

                result.Result_Status = 1;
                result.Data = builder.ToString();
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

    }
}
