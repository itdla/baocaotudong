﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Models
{
    public class Data_StackBarVMChart
    {
        /// <summary>
        /// Stacked Column Charts
        /// </summary>
        public Data_StackBarVMChart(int x, int y, string label, string group)
        {
            XValue = x;
            YValue = y;
            LabelCaption = label;
            DataGroup = group;
        }

        public int XValue
        {
            get;
            set;
        }

        public int YValue
        {
            get;
            set;
        }

        public string LabelCaption
        {
            get;
            set;
        }

        public string DataGroup
        {
            get;
            set;
        }
    }
}