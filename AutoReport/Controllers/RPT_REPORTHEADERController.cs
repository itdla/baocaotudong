﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using Spire.Doc;
using Spire.Doc.Documents;

namespace AutoReport.Controllers
{
    public class RPT_REPORTHEADERController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();

        private string m_ControllerName = "RPT_REPORTHEADER";

        // GET: RPT_REPORTHEADER/Index
        public ActionResult Index()
        {
            if (!CheckPermission("REPORTS_INIT"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult EnlistUsersTableIndex(Guid? id)
        {
            RPT_REPORTHEADER item = db.RPT_REPORTHEADER.Find(id);
            ViewBag.ControllerName = m_ControllerName;
            ViewBag.BackLink = string.Format("/{0}/Edit/{1}", m_ControllerName, id);
            return View(item);
        }

        public ActionResult EnlistUsersValueConst(Guid? id)
        {
            RPT_REPORTHEADER item = db.RPT_REPORTHEADER.Find(id);
            ViewBag.ControllerName = m_ControllerName;
            ViewBag.BackLink = string.Format("/{0}/Edit/{1}", m_ControllerName, id);

            #region query data

            var query_emp = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).ToList().Select(p => new
            {
                p.EmployeeKey,
                p.EmployeeName,
                Department = db.CAT_DEPARTMENTS.Find(p.DepartmentKey)?.DepartmentName ?? ""
            }).ToList();

            var emp_list = query_emp.OrderBy(p => p.Department).ThenBy(p => p.EmployeeName).Select(p => new
            {
                p.EmployeeKey,
                ED_Name = (p.Department == "" ? "" : p.Department + " > ") + p.EmployeeName
            }).ToList();

            #endregion

            ViewBag.EmployeeList = new SelectList(emp_list, "EmployeeKey", "ED_Name");
            return View(item);
        }

        public ActionResult RptReportCopyNew()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult RptAddTableIndex()
        {
            ViewBag.TemplatesList = new SelectList(db.RPT_TEMPLATES.Where(p => p.RecordStatus == 1).ToList().OrderBy(p => p.Templates_Name).ToList(), "Rpt_TemplatesKey", "Templates_Name");
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult RptFileAttachedIndex()
        {
            if (!CheckPermission("REPORTS_INIT"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult RptTableIndex_EnlistUsers(Guid? id)
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult RptFileAttachedEdit(Guid? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            RPT_REPORTHEADER iteminfo = db.RPT_REPORTHEADER.Find(id);
            if (iteminfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.ControllerName = m_ControllerName;
            return View(iteminfo);
        }

        public ActionResult LoadList(int page, int pageSize, string Rpt_ID, string Rpt_Name, int? Rpt_Month, int? Rpt_Year)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */

            Rpt_ID = Rpt_ID ?? "";
            Rpt_Name = Rpt_Name ?? "";
            Rpt_Month = Rpt_Month ?? 0;
            Rpt_Year = Rpt_Year ?? 0;

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(Rpt_ID))
                table.KeysSearch.Add("Rpt_ID", Rpt_ID);
            if (!string.IsNullOrEmpty(Rpt_Name))
                table.KeysSearch.Add("Rpt_Name", Rpt_Name);
            if (Rpt_Month != 0)
                table.KeysSearch.Add("Rpt_Month", Rpt_Month.ToString());
            if (Rpt_Year != 0)
                table.KeysSearch.Add("Rpt_Year", Rpt_Year.ToString());

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter

            var searchQuery = (from f in db.RPT_REPORTHEADER
                               where f.RecordStatus == 1
                               orderby f.Rpt_ID, f.Rpt_Year descending, f.Rpt_Month descending
                               select f).ToList();

            if (MyAccount.IsDefaultUser != null && !MyAccount.IsDefaultUser.Value)
            {
                //org account
                var emp = db.CAT_EMPLOYEES.Find(MyAccount.EmployeeKey);
                if (emp != null)
                {
                    Guid? departmentkey = emp.DepartmentKey;

                    var query_template = db.RPT_TEMPLATES_DEPARTMENT.Where(p => p.DepartmentKey == departmentkey).ToList()
                        .Select(p => p.Rpt_TemplatesKey).ToList();

                    searchQuery = searchQuery.Where(p => query_template.Contains(p.Rpt_TemplatesKey)).ToList();
                }
                else
                {
                    searchQuery.Clear();
                }
            }

            var fq1 = searchQuery.ToList().Select(p => new
            {
                p.Rpt_ReportHeaderKey,
                p.Rpt_ID,
                p.Rpt_Name,
                p.Rpt_Month,
                p.Rpt_Year,
                p.RptStatus
            });

            if (!string.IsNullOrEmpty(Rpt_ID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_ID.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_ID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(Rpt_Name))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_Name.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_Name.Trim().ToLower()))).ToList();
            if (Rpt_Month != 0)
                fq1 = fq1.Where(p => p.Rpt_Month == Rpt_Month).ToList();
            if (Rpt_Year != 0)
                fq1 = fq1.Where(p => p.Rpt_Year == Rpt_Year).ToList();

            int _index = 1;
            var fq2 = fq1.ToList().Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                STT = _index++,
                d.Rpt_ID,
                d.Rpt_Name,
                d.Rpt_Month,
                d.Rpt_Year,
                HanhDong =
                "<a type=\"button\" class=\"btn btn-primary\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/Detail/" + d.Rpt_ReportHeaderKey + "'>Xem</a>" +
                (d.RptStatus == 3 ? "" :
                "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/Edit/" + d.Rpt_ReportHeaderKey + "'>Sửa</a>" +
                "<button type=\"button\" class=\"btn btn-danger btn_xoa\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.Rpt_ReportHeaderKey + "'>Xóa </button>")
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 8, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Báo Cáo", IsShow = true, Format = "", Width = 21, Width_Measure = "%", FieldName = "Rpt_ID" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Báo Cáo", IsShow = true, Format = "", Width = 40, Width_Measure = "%", FieldName = "Rpt_Name" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tháng", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Month", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Năm", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Year", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 18, Width_Measure = "%" });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportHeaderKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadList_ReportFileAttached(int page, int pageSize, string Rpt_ID, string Rpt_Name, int? Rpt_Month, int? Rpt_Year)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */

            Rpt_ID = Rpt_ID ?? "";
            Rpt_Name = Rpt_Name ?? "";
            Rpt_Month = Rpt_Month ?? 0;
            Rpt_Year = Rpt_Year ?? 0;

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(Rpt_ID))
                table.KeysSearch.Add("Rpt_ID", Rpt_ID);
            if (!string.IsNullOrEmpty(Rpt_Name))
                table.KeysSearch.Add("Rpt_Name", Rpt_Name);
            if (Rpt_Month != 0)
                table.KeysSearch.Add("Rpt_Month", Rpt_Month.ToString());
            if (Rpt_Year != 0)
                table.KeysSearch.Add("Rpt_Year", Rpt_Year.ToString());

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter

            List<int?> lst_status_allowed = new List<int?>();
            lst_status_allowed.Add(Convert.ToInt32(eRptHeaderStatus.Created));
            lst_status_allowed.Add(Convert.ToInt32(eRptHeaderStatus.Declined));

            var searchQuery = (from f in db.RPT_REPORTHEADER
                               where f.RecordStatus == 1 && lst_status_allowed.Contains(f.RptStatus)
                               orderby f.Rpt_ID, f.Rpt_Year descending, f.Rpt_Month descending
                               select f).ToList();

            if (MyAccount.IsDefaultUser != null && !MyAccount.IsDefaultUser.Value)
            {
                //org account
                var emp = db.CAT_EMPLOYEES.Find(MyAccount.EmployeeKey);
                if (emp != null)
                {
                    Guid? departmentkey = emp.DepartmentKey;

                    var query_template = db.RPT_TEMPLATES_DEPARTMENT.Where(p => p.DepartmentKey == departmentkey).ToList()
                        .Select(p => p.Rpt_TemplatesKey).ToList();

                    searchQuery = searchQuery.Where(p => query_template.Contains(p.Rpt_TemplatesKey)).ToList();
                }
                else
                {
                    searchQuery.Clear();
                }
            }

            var fq1 = searchQuery.ToList().Select(p => new
            {
                p.Rpt_ReportHeaderKey,
                p.Rpt_ID,
                p.Rpt_Name,
                p.Rpt_Month,
                p.Rpt_Year,
                p.RptStatus
            });

            if (!string.IsNullOrEmpty(Rpt_ID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_ID.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_ID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(Rpt_Name))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_Name.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_Name.Trim().ToLower()))).ToList();
            if (Rpt_Month != 0)
                fq1 = fq1.Where(p => p.Rpt_Month == Rpt_Month).ToList();
            if (Rpt_Year != 0)
                fq1 = fq1.Where(p => p.Rpt_Year == Rpt_Year).ToList();

            int _index = 1;
            var fq2 = fq1.ToList().Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                STT = _index++,
                d.Rpt_ID,
                d.Rpt_Name,
                d.Rpt_Month,
                d.Rpt_Year,
                FilesAttached = string.Format("{0} đính kèm", db.RPT_REPORTFILESATTACH.Where(p => p.Rpt_ReportHeaderKey == d.Rpt_ReportHeaderKey).Count()),
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/RptFileAttachedEdit/" + d.Rpt_ReportHeaderKey + "'>Quản Lý</a>"
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 8, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Báo Cáo", IsShow = true, Format = "", Width = 21, Width_Measure = "%", FieldName = "Rpt_ID" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Báo Cáo", IsShow = true, Format = "", Width = 40, Width_Measure = "%", FieldName = "Rpt_Name" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tháng", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Month", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Năm", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Year", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Files", IsShow = true, Format = "", Width = 10, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 8, Width_Measure = "%" });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportHeaderKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadList_FileAttached(int page, int pageSize, string SearchKey)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            Guid rptKey = string.IsNullOrEmpty(SearchKey) ? Guid.Empty : Guid.Parse(SearchKey);

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter

            List<int?> lst_status_allowed = new List<int?>();
            lst_status_allowed.Add(Convert.ToInt32(eRptHeaderStatus.Created));
            lst_status_allowed.Add(Convert.ToInt32(eRptHeaderStatus.Declined));

            var searchQuery = db.RPT_REPORTFILESATTACH.Where(p => p.RecordStatus == 1 && p.Rpt_ReportHeaderKey == rptKey).ToList();

            int _index = 1;
            var fq2 = searchQuery.Select(d => new
            {
                d.Rpt_ReportFileAttachedKey,
                STT = _index++,
                d.AttachedFileName,
                d.Note,
                LinkTai = string.Format("<a href='{0}?Time={1}' target='_blank'>Tải File</a>", d.FileReference, Now.Ticks),
                HanhDong = string.Format("<a class=\"btn btn-danger btndel\" data-id=\"{0}\" style=\"font-size: 10px;margin-right: 5px;\">Xóa</a>", d.Rpt_ReportFileAttachedKey)
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên file", IsShow = true, Format = "", Width = 38, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Ghi Chú", IsShow = true, Format = "", Width = 35, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tải về", IsShow = true, Format = "", Width = 10, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 10, Width_Measure = "%" });
            //

            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportFileAttachedKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        /// <summary>
        /// Sau khi tạo báo cáo xong sẽ phân công bảng phụ lục cho nhân sự nhập liệu
        /// Chọn 1 phụ lục từ danh sách, nhân nút phân công,
        /// popup danh sách nhân viên lên,
        /// chọn nhân viên và nhấn Save, sẽ lưu vào db
        /// </summary>
        public ActionResult LoadList_EnlistUsersTables(int page, int pageSize, string SearchKey)
        {
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            table.KeysSearch = new Dictionary<string, string>();
            //Filter
            Guid rptHdrKey = string.IsNullOrEmpty(SearchKey) ? Guid.Empty : Guid.Parse(SearchKey);
            var searchQuery = (from r in db.RPT_REPORTDETAIL_TABLEINDEX
                               join t in db.RPT_TEMPLATES_TABLENAME on r.Rpt_Templates_TableNameKey equals t.Rpt_Templates_TableNameKey
                               where r.Rpt_ReportHeaderKey == rptHdrKey
                               select new
                               {
                                   r.Rpt_ReportDetail_TableIndexKey,
                                   t.TableID,
                                   t.TableName
                               }).ToList();

            var lst_rpt_tbls = searchQuery.Select(p => p.Rpt_ReportDetail_TableIndexKey).ToList();
            var emp_enlist = (from r in db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                              join e in db.CAT_EMPLOYEES on r.EmployeeKey equals e.EmployeeKey
                              where lst_rpt_tbls.Contains(r.Rpt_ReportDetail_TableIndexKey)
                              select new
                              {
                                  r.Rpt_ReportTables_Employee_EnlistedKey,
                                  r.Rpt_ReportDetail_TableIndexKey,
                                  e.EmployeeName
                              }).ToList();

            var fq1 = searchQuery.Select(d => new
            {
                d.Rpt_ReportDetail_TableIndexKey,
                d.TableID,
                d.TableName,
                EmpList = emp_enlist.Where(p => p.Rpt_ReportDetail_TableIndexKey == d.Rpt_ReportDetail_TableIndexKey).Count() == 0 ? "-" :
                    emp_enlist.Where(p => p.Rpt_ReportDetail_TableIndexKey == d.Rpt_ReportDetail_TableIndexKey).OrderBy(p => p.EmployeeName).ToList().Select(p => p.EmployeeName).Aggregate((x, y) => x + "<br >" + y),
                HanhDong = "<a type=\"button\" class=\"btn btn-success btnadduser\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.Rpt_ReportDetail_TableIndexKey + "'>Phân Công</a>"
            }).ToList();

            var data = fq1.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            table.intTotalPage = GetTotalPage(fq1.Count, pageSize);

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Phụ Lục", IsShow = true, Format = "", Width = 200 });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Phụ Lục", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Nhân Sự", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành Động", IsShow = true, Format = "" });
            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportDetail_TableIndexKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }


            return new CustomJsonResult { Data = table };
        }

        /// <summary>
        /// Sau khi tạo báo cáo xong sẽ phân công trường dữ liệu cho nhân sự nhập liệu
        /// </summary>
        public ActionResult LoadList_EnlistUsersValues_View(int page, int pageSize, string SearchKey)
        {
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            table.KeysSearch = new Dictionary<string, string>();
            //Filter
            Guid rptHdrKey = string.IsNullOrEmpty(SearchKey) ? Guid.Empty : Guid.Parse(SearchKey);
            var searchQuery = (from r in db.RPT_REPORTDETAIL_VALUECONST
                               where r.Rpt_ReportHeaderKey == rptHdrKey
                               orderby r.Sort
                               select new
                               {
                                   r.Rpt_ReportDetail_ValueConstKey,
                                   r.CodeID,
                                   r.CodeName
                               }).ToList();
            var lst_rpt_values = searchQuery.Select(p => p.Rpt_ReportDetail_ValueConstKey).ToList();
            var emp_enlist = (from r in db.RPT_REPORTVALUES_EMPLOYEES_ENLISTED
                              join e in db.CAT_EMPLOYEES on r.EmployeeKey equals e.EmployeeKey
                              where lst_rpt_values.Contains(r.Rpt_ReportDetail_ValueConstKey)
                              select new
                              {
                                  r.Rpt_ReportValues_Employees_EnlistedKey,
                                  r.Rpt_ReportDetail_ValueConstKey,
                                  e.EmployeeName
                              }).ToList();

            var fq1 = searchQuery.Select(d => new
            {
                d.Rpt_ReportDetail_ValueConstKey,
                d.CodeID,
                d.CodeName,
                EmpList = emp_enlist.Where(p => p.Rpt_ReportDetail_ValueConstKey == d.Rpt_ReportDetail_ValueConstKey).Count() == 0 ? "-" :
                    emp_enlist.Where(p => p.Rpt_ReportDetail_ValueConstKey == d.Rpt_ReportDetail_ValueConstKey).OrderBy(p => p.EmployeeName).ToList().Select(p => p.EmployeeName).Aggregate((x, y) => x + "<br >" + y)
            }).ToList();

            var data = fq1.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            table.intTotalPage = GetTotalPage(fq1.Count, pageSize);

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Trường", IsShow = true, Format = "", Width = 200 });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Trường", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Nhân Sự", IsShow = true, Format = "" });
            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportDetail_ValueConstKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }
            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadList_EnlistUsersValues_Till(Guid? rptkey, Guid? empkeyselected)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            StringBuilder htmlStr = new StringBuilder();

            try
            {
                Guid hdrkey = rptkey == null ? Guid.Empty : rptkey.Value;
                Guid empkey = empkeyselected == null ? Guid.Empty : empkeyselected.Value;

                //danh sách các trường nhập liệu
                var vlue_const_list = db.RPT_REPORTDETAIL_VALUECONST.Where(p => p.Rpt_ReportHeaderKey == hdrkey).ToList();
                var key_list = vlue_const_list.Select(p => p.Rpt_ReportDetail_ValueConstKey).ToList();// key nhập liệu

                //danh sách các trường đã phân công nhập liệu
                var vlue_enlist = db.RPT_REPORTVALUES_EMPLOYEES_ENLISTED.Where(p => key_list.Contains(p.Rpt_ReportDetail_ValueConstKey)).ToList();

                //danh sách các trường phân công cho nhân viên đã chọn
                var vlue_emp_enlist = vlue_enlist.Where(p => p.EmployeeKey == empkey).ToList();

                var data = vlue_const_list.OrderBy(p => p.Sort).ToList();
                foreach (var item in data)
                {
                    string chk_state = "";
                    if (vlue_emp_enlist.Where(p => p.Rpt_ReportDetail_ValueConstKey == item.Rpt_ReportDetail_ValueConstKey).Count() > 0)
                        chk_state = "checked";// nếu đã dc phân công cho nhân viên này
                    else if (vlue_enlist.Where(p => p.Rpt_ReportDetail_ValueConstKey == item.Rpt_ReportDetail_ValueConstKey).Count() > 0)
                        chk_state = "disabled checked";// nếu dc phân công cho nhân viên khác
                    else
                        chk_state = "";//nếu chưa được phân công

                    htmlStr.Append(string.Format("<div><input type=\"checkbox\" id=\"{0}\" name=\"{0}\" class=\"chklocal\" {2}/>&nbsp;&nbsp;<span>{1} </span></div>", item.Rpt_ReportDetail_ValueConstKey, item.CodeName, chk_state));
                }
                result.refstate = data.Count == vlue_emp_enlist.Count ? "all" : "";

                result.Data = htmlStr.ToString();
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = result };
        }

        public ActionResult LoadList_TblTemplate_EnlistUser(int page, int pageSize, string SearchKey)
        {
            //SearchKey: Rpt_ReportTables_Employee_EnlistedKey
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();

            Guid rpt_tbl_key = string.IsNullOrEmpty(SearchKey) ? Guid.Empty : Guid.Parse(SearchKey);

            var lst_emp_enlisted = db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED.Where(p => p.Rpt_ReportDetail_TableIndexKey == rpt_tbl_key).ToList()
                .Select(p => p.EmployeeKey).ToList();

            var query_emp = (from e in db.CAT_EMPLOYEES
                             where e.RecordStatus == 1
                             select new
                             {
                                 e.EmployeeKey,
                                 e.EmployeeID,
                                 e.EmployeeName,
                                 e.DepartmentKey
                             }).ToList();

            var data = query_emp.ToList().Select(d => new
            {
                d.EmployeeKey,
                HanhDong = "<input type=\"checkbox\" id=\"" + d.EmployeeKey.ToString() + "\"" + (lst_emp_enlisted.IndexOf(d.EmployeeKey) == -1 ? "" : " checked=\"true\"") + " style=\"width: 24px; height: 24px;\">",
                d.EmployeeName,
                Department = db.CAT_DEPARTMENTS.Find(d.DepartmentKey)?.DepartmentName,
                IsSelected = lst_emp_enlisted.IndexOf(d.EmployeeKey) != -1
            }).ToList();

            data = data.OrderByDescending(p => p.IsSelected)
                .ThenBy(p => p.Department)
                .ThenBy(p => p.EmployeeName)
                .ToList();

            table.intTotalPage = 1;

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Chọn", IsShow = true, Format = "", Width = 8, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Nhân Viên", IsShow = true, Format = "", Width = 30, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Phòng Ban", IsShow = true, Format = "", Width = 60, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Selected", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].EmployeeKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult SaveList_TblTemplate_EnlistUser(Guid? tblindexkey, string[] arr_employees)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;

            try
            {
                Guid key = tblindexkey == null ? Guid.Empty : tblindexkey.Value;
                var emp_enlisted = db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED.Where(p => p.Rpt_ReportDetail_TableIndexKey == key).ToList();
                emp_enlisted.ForEach(p => p.Activate = false);

                if (arr_employees != null)
                {
                    foreach (string emp in arr_employees)
                    {
                        Guid empkey = Guid.Parse(emp);
                        var emp_enlist = emp_enlisted.Where(p => p.EmployeeKey == empkey).FirstOrDefault();
                        if (emp_enlist != null)
                            emp_enlist.Activate = true;
                        else
                        {
                            var itemnew = new RPT_REPORTTABLES_EMPLOYEES_ENLISTED();
                            itemnew.Rpt_ReportTables_Employee_EnlistedKey = Guid.NewGuid();
                            itemnew.Rpt_ReportDetail_TableIndexKey = key;
                            itemnew.EmployeeKey = empkey;
                            db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED.Add(itemnew);
                        }
                    }
                }

                foreach (var emp_update in emp_enlisted)
                {
                    db.Entry(emp_update).State = emp_update.Activate ? EntityState.Modified : EntityState.Deleted;
                }
                db.SaveChanges();
                result.strMess = "Đã phân công";
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult SaveList_ValuesConst_EnlistUser(Guid? rptkey, Guid? employeekey, string[] lstvalues)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;

            try
            {
                if (rptkey == null)
                    throw new Exception("Báo cáo chưa xác định");
                if (employeekey == null)
                    throw new Exception("Chưa chọn nhân sự phân công");

                Guid hdrkey = rptkey.Value;
                Guid empKey = employeekey.Value;

                //lấy danh sách trường dữ liệu của report
                var rpt_valueslst = db.RPT_REPORTDETAIL_VALUECONST.Where(p => p.Rpt_ReportHeaderKey == hdrkey).ToList();
                var lst_valuedetailkey = rpt_valueslst.Select(p => p.Rpt_ReportDetail_ValueConstKey).ToList();

                //lấy danh sách các trường đã được phân công cho nhân viên
                var emp_enlisted = db.RPT_REPORTVALUES_EMPLOYEES_ENLISTED.Where(p => p.EmployeeKey == empKey && lst_valuedetailkey.Contains(p.Rpt_ReportDetail_ValueConstKey)).ToList();
                emp_enlisted.ForEach(p => p.Activate = false);

                if (lstvalues != null)
                {
                    foreach (string v in lstvalues)
                    {
                        Guid valueconstkey = Guid.Parse(v);
                        var emp_enlist = emp_enlisted.Where(p => p.Rpt_ReportDetail_ValueConstKey == valueconstkey).FirstOrDefault();
                        if (emp_enlist != null)
                            emp_enlist.Activate = true;
                        else
                        {
                            var itemnew = new RPT_REPORTVALUES_EMPLOYEES_ENLISTED();
                            itemnew.Rpt_ReportValues_Employees_EnlistedKey = Guid.NewGuid();
                            itemnew.Rpt_ReportDetail_ValueConstKey = valueconstkey;
                            itemnew.EmployeeKey = empKey;
                            itemnew.Activate = true;
                            db.RPT_REPORTVALUES_EMPLOYEES_ENLISTED.Add(itemnew);
                        }
                    }
                }

                foreach (var emp_update in emp_enlisted)
                {
                    db.Entry(emp_update).State = emp_update.Activate ? EntityState.Modified : EntityState.Deleted;
                }
                db.SaveChanges();
                result.strMess = "Đã phân công";
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult LoadList_TableIndexToAdd_Edit(int page, int pageSize, string SearchKey)
        {
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            Guid RptTemplateKey = string.IsNullOrEmpty(SearchKey) ? Guid.Empty : Guid.Parse(SearchKey);
            var searchQuery = (from t in db.RPT_TEMPLATES_DETAIL_TABLEINDEX
                               join tbl in db.RPT_TEMPLATES_TABLENAME
                                  on t.Rpt_Templates_TableNameKey equals tbl.Rpt_Templates_TableNameKey
                               orderby tbl.TableID
                               where t.Rpt_TemplatesKey == RptTemplateKey
                               select new
                               {
                                   t.Rpt_Templates_Detail_TableIndexKey,
                                   tbl.TableID,
                                   tbl.TableName,
                                   t.Rpt_Templates_TableNameKey
                               }).ToList();

            var data = searchQuery.ToList().Select(d => new
            {
                d.Rpt_Templates_Detail_TableIndexKey,
                ItemCheck = "<input type=\"checkbox\" style=\"width: 20px; height: 20px;\" id=\"" + d.Rpt_Templates_TableNameKey + "\" />",
                d.TableID,
                d.TableName
            }).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            table.intTotalPage = GetTotalPage(searchQuery.Count(), pageSize);

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Chọn", IsShow = true, Format = "", Width = 50 });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Phụ Lục", IsShow = true, Format = "", Width = 150 });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Phụ Lục", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã TblTemplate", IsShow = false, Format = "" });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_Templates_Detail_TableIndexKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadValueList_New(Guid? TemplatesKey)
        {
            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();
            grid.ShowAction = true;

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Templates Key",
                Field = "Rpt_Templates_Detail_ValueConstKey",
                DataType = GridDetail.eDataType.StringType,
                Width = "2%",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Mã Trường",
                Field = "CodeID",
                DataType = GridDetail.eDataType.StringType,
                Width = "26%",
                Enable = true,
                IsShow = true,
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tên Trường",
                Field = "CodeName",
                DataType = GridDetail.eDataType.StringType,
                Width = "65%",
                Enable = true,
                IsShow = true
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "From Template",
                DataType = GridDetail.eDataType.StringType,
                Width = "2%",
                Enable = true,
                IsShow = true,
                LayoutType = GridDetail.eLayoutType.Hide
            });

            var zDetails = (from v in db.RPT_TEMPLATES_DETAIL_VALUECONST
                            orderby v.CodeID
                            where v.Rpt_TemplatesKey == TemplatesKey
                            orderby v.Sort
                            select new
                            {
                                v.Rpt_Templates_Detail_ValueConstKey,
                                v.CodeID,
                                v.CodeName,
                                FromTemplate = "yes"
                            }).ToList();
            if (zDetails.Count > 0)
            {
                for (int i = 0; i < zDetails.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                        zDetails[i].Rpt_Templates_Detail_ValueConstKey,
                        zDetails[i].CodeID,
                        zDetails[i].CodeName,
                        zDetails[i].FromTemplate
                    });
                }
            }
            else
            {
                arrBody.Add(new object[grid.Header.Count]);
            }
            grid.Data = arrBody;

            var itemTemplate = db.RPT_TEMPLATES.Find(TemplatesKey);

            Dictionary<string, object> result = new Dictionary<string, object>();
            result.Add("datagrid", grid);
            result.Add("templateid", itemTemplate == null ? "" : itemTemplate.Templates_ID + " - " + Now.Year);
            result.Add("templatename", itemTemplate == null ? "" : itemTemplate.Templates_Name + " - " + Now.Year);
            return new CustomJsonResult { Data = result };
        }

        public ActionResult LoadTableList_New(int page, int pageSize, string SearchKey)
        {
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            Guid Rpt_TemplatesKey = string.IsNullOrEmpty(SearchKey) ? Guid.Empty : Guid.Parse(SearchKey);
            var searchQuery = (from tl in db.RPT_TEMPLATES
                               join a in db.RPT_TEMPLATES_DETAIL_TABLEINDEX on tl.Rpt_TemplatesKey equals a.Rpt_TemplatesKey
                               join tbl in db.RPT_TEMPLATES_TABLENAME on a.Rpt_Templates_TableNameKey equals tbl.Rpt_Templates_TableNameKey
                               where tl.Rpt_TemplatesKey == Rpt_TemplatesKey
                               orderby tbl.TableID
                               select new
                               {
                                   a.Rpt_Templates_Detail_TableIndexKey,
                                   tbl.TableID,
                                   tbl.TableName,
                                   tbl.Rpt_Templates_TableNameKey
                               }).ToList();

            var data = searchQuery.ToList().Select(d => new
            {
                d.Rpt_Templates_Detail_TableIndexKey,
                d.TableID,
                d.TableName,
                d.Rpt_Templates_TableNameKey
            }).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            table.intTotalPage = GetTotalPage(searchQuery.Count(), pageSize);

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Phụ Lục", IsShow = true, Format = "", Width = 200 });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Phụ Lục", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã TblTemplate", IsShow = false, Format = "" });
            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_Templates_Detail_TableIndexKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }


            return new CustomJsonResult { Data = table };
        }

        // GET: RPT_REPORTHEADER/Create
        public ActionResult Create()
        {
            if (!CheckPermission("REPORTS_INIT_ADD"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            RPT_REPORTHEADER item = new RPT_REPORTHEADER();
            item.Rpt_Month = DateTime.Now.Month;
            item.Rpt_Year = DateTime.Now.Year;
            item.DateStart = DateTime.Now;
            item.DateEnd = new DateTime(DateTime.Now.Year, 12, 31);

            #region query data

            var queryTemplate = (from p in db.RPT_TEMPLATES
                                 where p.RecordStatus == 1
                                 orderby p.Templates_ID
                                 select new
                                 {
                                     p.Rpt_TemplatesKey,
                                     Templates_Name = p.Templates_ID + " - " + p.Templates_Name
                                 }).ToList();

            if (MyAccount.IsDefaultUser != null && !MyAccount.IsDefaultUser.Value)
            {
                var depkey = db.CAT_EMPLOYEES.Find(MyAccount.EmployeeKey)?.DepartmentKey;

                var templates_restricted = (from d in db.RPT_TEMPLATES_DEPARTMENT
                                            where d.DepartmentKey == depkey
                                            select d).ToList().Select(p => p.Rpt_TemplatesKey).ToList();

                queryTemplate = queryTemplate.Where(p => templates_restricted.Contains(p.Rpt_TemplatesKey)).ToList();
            }

            #endregion

            ViewBag.TemplatesList = new SelectList(queryTemplate, "Rpt_TemplatesKey", "Templates_Name");
            return View(item);
        }

        public ActionResult CreateWizard()
        {
            if (!CheckPermission("REPORTS_INIT_ADD"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            RPT_REPORTHEADER item = new RPT_REPORTHEADER();
            item.Rpt_Month = DateTime.Now.Month;
            item.Rpt_Year = DateTime.Now.Year;
            item.DateStart = DateTime.Now;
            item.DateEnd = new DateTime(DateTime.Now.Year, 12, 31);

            #region query data

            var queryTemplate = (from p in db.RPT_TEMPLATES
                                 where p.RecordStatus == 1
                                 orderby p.Templates_ID
                                 select new
                                 {
                                     p.Rpt_TemplatesKey,
                                     Templates_Name = p.Templates_ID + " - " + p.Templates_Name
                                 }).ToList();

            if (MyAccount.IsDefaultUser != null && !MyAccount.IsDefaultUser.Value)
            {
                var depkey = db.CAT_EMPLOYEES.Find(MyAccount.EmployeeKey)?.DepartmentKey;

                var templates_restricted = (from d in db.RPT_TEMPLATES_DEPARTMENT
                                            where d.DepartmentKey == depkey
                                            select d).ToList().Select(p => p.Rpt_TemplatesKey).ToList();

                queryTemplate = queryTemplate.Where(p => templates_restricted.Contains(p.Rpt_TemplatesKey)).ToList();
            }


            var queryEmployee = (from p in db.CAT_EMPLOYEES
                                 where p.RecordStatus == 1
                                 select new
                                 {
                                     p.EmployeeKey,
                                     p.EmployeeName,
                                     p.DepartmentKey
                                 }).ToList();
            var emp_q1 = queryEmployee.Select(p => new
            {
                p.EmployeeKey,
                p.EmployeeName,
                DepartmentName = (db.CAT_DEPARTMENTS.Find(p.DepartmentKey)?.DepartmentName) ?? ""
            }).OrderBy(p => p.DepartmentName).ThenBy(p => p.EmployeeName).ToList();

            var empList = emp_q1.Select(p => new
            {
                p.EmployeeKey,
                EmpName = p.EmployeeName
                //EmpName = (string.IsNullOrEmpty(p.DepartmentName) ? "" : p.DepartmentName + " > ") + p.EmployeeName
            });
            #endregion

            ViewBag.TemplatesList = new SelectList(queryTemplate, "Rpt_TemplatesKey", "Templates_Name");
            ViewBag.EmployeeList = new SelectList(empList, "EmployeeKey", "EmpName");
            return View(item);
        }

        public ActionResult Wizard_Step_Changed(RPT_REPORTHEADER itemrpt, string stepname)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 0;

            StringBuilder htmlStr = new StringBuilder();
            string step_perform = "reset";
            string step_next = string.Empty;
            try
            {
                result.strMess = string.Empty;
                switch (stepname)
                {
                    case "1n":
                        {
                            step_perform = "next";
                            step_next = "2n";
                            if (itemrpt.Rpt_TemplatesKey == Guid.Empty)
                                result.strMess = "Vui lòng chọn mẫu";
                            else
                            {
                                var template = db.RPT_TEMPLATES.Find(itemrpt.Rpt_TemplatesKey);
                                itemrpt.Rpt_ID = string.Format("{0} - {1}", template.Templates_ID, Now.Year);
                                itemrpt.Rpt_Name = string.Format("{0} - {1}", template.Templates_Name, Now.Year);
                                itemrpt.Rpt_Month = Now.Month;
                                itemrpt.Rpt_Year = Now.Year;
                                result.intStatus = 1;
                            }
                        }
                        break;
                    case "2n":
                        {
                            step_perform = "next";
                            step_next = "3n";
                            if (string.IsNullOrEmpty(itemrpt.Rpt_ID))
                                result.strMess = "Mã báo cáo không được để trống";
                            else if (string.IsNullOrEmpty(itemrpt.Rpt_Name))
                                result.strMess = "Tên báo cáo không được để trống";
                            else if (itemrpt.Rpt_Month == 0)
                                result.strMess = "Chưa chọn tháng";
                            else if (itemrpt.Rpt_Year == 0)
                                result.strMess = "Chưa chọn năm";
                            else if (itemrpt.DateStart == null)
                                result.strMess = "Chưa chọn ngày bắt đầu";
                            else if (itemrpt.DateEnd == null)
                                result.strMess = "Chưa chọn ngày kết thúc";
                            else
                            {
                                result.intStatus = 1;

                                var lst_values = db.RPT_TEMPLATES_DETAIL_VALUECONST.Where(p => p.Rpt_TemplatesKey == itemrpt.Rpt_TemplatesKey).OrderBy(p => p.Sort).ToList();
                                foreach (RPT_TEMPLATES_DETAIL_VALUECONST itemvalue in lst_values)
                                {
                                    htmlStr.Append(string.Format("<div><input type=\"checkbox\" data-id=\"{0}\" data-code=\"{1}\" data-desc=\"{2}\" class=\"chklocal\" />&nbsp;&nbsp;<span>'{1}' - {2} </span></div>",
                                        itemvalue.Rpt_Templates_Detail_ValueConstKey,
                                        itemvalue.CodeID,
                                        itemvalue.CodeName));
                                }
                            }

                        }
                        break;
                    case "2p":
                        {
                            step_perform = "prev";
                            step_next = "1n";
                            result.intStatus = 1;
                        }
                        break;
                    case "3n":
                        {
                            step_perform = "next";
                            var queryEmp = (from p in db.CAT_EMPLOYEES
                                            where p.RecordStatus == 1
                                            orderby p.EmployeeName
                                            select p).ToList();

                            Guid templatekey = itemrpt.Rpt_TemplatesKey;
                            var queryTblIndex = (from d in db.RPT_TEMPLATES_DETAIL_TABLEINDEX
                                                 join t in db.RPT_TEMPLATES_TABLENAME
                                                 on d.Rpt_Templates_TableNameKey equals t.Rpt_Templates_TableNameKey
                                                 where d.Rpt_TemplatesKey == templatekey
                                                 orderby t.Sorted
                                                 select t).ToList();

                            //combobox chọn nhân sự nhập liệu
                            StringBuilder code_drop_multi = new StringBuilder();
                            code_drop_multi.Append("<select multiple>");
                            foreach (CAT_EMPLOYEES emp in queryEmp)
                            {
                                code_drop_multi.Append(string.Format("<option value=\"{0}\">{1}</option>", emp.EmployeeKey, emp.EmployeeName));
                            }
                            code_drop_multi.Append("</select>");

                            //các dòng dữ liệu
                            foreach (RPT_TEMPLATES_TABLENAME itemTbl in queryTblIndex)
                            {
                                htmlStr.Append("<tr>");
                                htmlStr.Append("<td width=\"1%\" style=\"display: none;\">" + itemTbl.Rpt_Templates_TableNameKey.ToString() + "</td>");
                                htmlStr.Append("<td width=\"50%\">" + itemTbl.TableName + "</td>");
                                htmlStr.Append("<td width=\"49%\">" + code_drop_multi.ToString() + "</td>");
                                htmlStr.Append("</tr>");
                            }

                            step_next = "4n";
                            result.intStatus = 1;
                        }
                        break;
                    case "3p":
                        {
                            step_perform = "prev";
                            step_next = "2n";
                            result.intStatus = 1;
                        }
                        break;
                    case "4n":
                        {
                            step_perform = "next";
                            step_next = "5n";
                            result.intStatus = 1;
                        }
                        break;
                    case "4p":
                        {
                            step_perform = "prev";
                            step_next = "3n";
                            result.intStatus = 1;
                        }
                        break;
                    case "5n":
                        {
                            step_perform = "OK";
                            step_next = "fn";
                            result.strMess = "Bạn đã hoàn thành cấu hình báo cáo";
                            result.intStatus = 1;
                        }
                        break;
                    case "5p":
                        {
                            step_perform = "prev";
                            step_next = "4n";
                            result.intStatus = 1;
                        }
                        break;
                    default:

                        break;
                }

                result.Data = itemrpt;
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            Dictionary<string, object> dic_result = new Dictionary<string, object>();
            dic_result.Add("status", result.intStatus);
            dic_result.Add("message", result.strMess);
            dic_result.Add("model", itemrpt);
            dic_result.Add("stepcurrent", stepname);
            dic_result.Add("stepnext", step_next);
            dic_result.Add("stepperform", step_perform);
            dic_result.Add("codehtml", htmlStr.ToString());

            return new CustomJsonResult { Data = dic_result };
        }

        public ActionResult Wizard_Save_Data(RPT_REPORTHEADER ItemModel, List<string[]> enlisted_values, List<string[]> enlisted_tbl)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                result.strMess = CheckBeforeSave(ItemModel, true);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                ItemModel.Rpt_ReportHeaderKey = Guid.NewGuid();
                ItemModel.RecordStatus = Convert.ToInt32(eRecordStatus.Active);
                ItemModel.RptStatus = Convert.ToInt32(eRptHeaderStatus.Created);
                ItemModel.CreatedOn = Now;
                ItemModel.CreatedBy = MyAccount.UserID;
                ItemModel.ModifiedOn = Now;
                ItemModel.ModifiedBy = MyAccount.UserID;

                RPT_TEMPLATES template = db.RPT_TEMPLATES.Find(ItemModel.Rpt_TemplatesKey);
                if (template.FileReference != null)
                {
                    ItemModel.FileName = template.FileName;
                    ItemModel.FileReference = string.Format("{0}{1}_{2}", PathUpload_HeaderOrg, Now_Full_String(), template.FileName);
                }

                db.RPT_REPORTHEADER.Add(ItemModel);

                int sorted = 1;

                #region lưu giá trị trường dữ liệu
                if (enlisted_values != null)
                {
                    foreach (string[] arr in enlisted_values)
                    {
                        RPT_REPORTDETAIL_VALUECONST item = new RPT_REPORTDETAIL_VALUECONST();
                        item.Rpt_ReportDetail_ValueConstKey = Guid.NewGuid();
                        item.Rpt_ReportHeaderKey = ItemModel.Rpt_ReportHeaderKey;
                        item.Rpt_Templates_Detail_ValueConstKey = Guid.Parse(arr[0]);
                        item.CodeID = arr[1];
                        item.CodeName = arr[2];
                        item.ContentValue = string.Empty;
                        item.Sort = sorted;

                        item.CreatedOn = ItemModel.CreatedOn;
                        item.CreatedBy = ItemModel.CreatedBy;
                        item.ModifiedOn = ItemModel.ModifiedOn;
                        item.ModifiedBy = ItemModel.ModifiedBy;

                        db.RPT_REPORTDETAIL_VALUECONST.Add(item);

                        //phân quyền nhập liệu
                        RPT_REPORTVALUES_EMPLOYEES_ENLISTED item_enlisted = new RPT_REPORTVALUES_EMPLOYEES_ENLISTED();
                        item_enlisted.Rpt_ReportValues_Employees_EnlistedKey = Guid.NewGuid();
                        item_enlisted.Rpt_ReportDetail_ValueConstKey = item.Rpt_ReportDetail_ValueConstKey;
                        item_enlisted.EmployeeKey = Guid.Parse(arr[4]);
                        item_enlisted.Activate = true;
                        db.RPT_REPORTVALUES_EMPLOYEES_ENLISTED.Add(item_enlisted);

                        sorted++;
                    }
                }
                #endregion

                #region lưu giá trị bảng phụ lục

                sorted = 1;
                if (enlisted_tbl != null)
                {
                    foreach (string[] arr in enlisted_tbl)
                    {
                        RPT_REPORTDETAIL_TABLEINDEX itemtbl = new RPT_REPORTDETAIL_TABLEINDEX();
                        itemtbl.Rpt_ReportDetail_TableIndexKey = Guid.NewGuid();
                        itemtbl.Rpt_ReportHeaderKey = ItemModel.Rpt_ReportHeaderKey;
                        itemtbl.Rpt_Templates_TableNameKey = Guid.Parse(arr[0]);
                        //itemtbl.EmployeeKey = Guid.Parse(arr[1]);
                        db.RPT_REPORTDETAIL_TABLEINDEX.Add(itemtbl);

                        //table columns
                        var lst_columns = db.RPT_TEMPLATES_TABLECOLUMN.Where(p =>
                        p.Rpt_Templates_TableNameKey == itemtbl.Rpt_Templates_TableNameKey).ToList();
                        foreach (RPT_TEMPLATES_TABLECOLUMN col in lst_columns)
                        {
                            RPT_REPORTDETAIL_TABLECOLUMN itemcol = new RPT_REPORTDETAIL_TABLECOLUMN();
                            itemcol.Rpt_ReportDetail_TableColumnKey = Guid.NewGuid();
                            itemcol.Rpt_ReportDetail_TableIndexKey = itemtbl.Rpt_ReportDetail_TableIndexKey;
                            itemcol.Rpt_Templates_TableColumnKey = col.Rpt_Templates_TableColumnKey;

                            db.RPT_REPORTDETAIL_TABLECOLUMN.Add(itemcol);
                        }

                        //phân quyền nhập liệu
                        RPT_REPORTTABLES_EMPLOYEES_ENLISTED item_enlisted = new RPT_REPORTTABLES_EMPLOYEES_ENLISTED();
                        item_enlisted.Rpt_ReportTables_Employee_EnlistedKey = Guid.NewGuid();
                        item_enlisted.Rpt_ReportDetail_TableIndexKey = itemtbl.Rpt_ReportDetail_TableIndexKey;
                        item_enlisted.EmployeeKey = Guid.Parse(arr[1]);
                        item_enlisted.Activate = true;
                        db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED.Add(item_enlisted);
                        sorted++;
                    }
                }
                #endregion

                db.SaveChanges();

                if (ItemModel.FileReference != null)
                {
                    //copy to org
                    System.IO.File.Copy(Server.MapPath(template.FileReference), Server.MapPath(ItemModel.FileReference));
                }

                result.redirectlink = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.Rpt_ReportHeaderKey);
                result.strMess = "Đã lưu dữ liệu";
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = result };
        }

        // POST: RPT_REPORTHEADER/Create
        [HttpPost]
        public ActionResult Create(RPT_REPORTHEADER iteminfo, List<string[]> lst_item_const)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;

            try
            {
                result.strMess = CheckBeforeSave(iteminfo, true);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                iteminfo.Rpt_ReportHeaderKey = Guid.NewGuid();
                iteminfo.Rpt_ID = iteminfo.Rpt_ID.Trim();
                iteminfo.RecordStatus = 1;
                iteminfo.RptStatus = 1;
                RPT_TEMPLATES itemTemplate = db.RPT_TEMPLATES.Find(iteminfo.Rpt_TemplatesKey);
                if (itemTemplate == null)
                {
                    throw new Exception("không tìm thấy thông tin file mẫu");
                }
                if (!string.IsNullOrEmpty(itemTemplate.FileReference))
                {
                    iteminfo.FileName = itemTemplate.FileName;
                    iteminfo.FileReference = string.Format("{0}{1}_{2}", PathUpload_HeaderOrg, Now_Full_String(), itemTemplate.FileName);
                }

                iteminfo.Rpt_ID = iteminfo.Rpt_ID.Trim();
                iteminfo.CreatedOn = DateTime.Now;
                iteminfo.ModifiedOn = DateTime.Now;
                iteminfo.CreatedBy = MyAccount.UserID;
                iteminfo.ModifiedBy = MyAccount.UserID;
                db.RPT_REPORTHEADER.Add(iteminfo);

                #region save list value const ro report

                for (int i = 0; i < lst_item_const.Count; i++)
                {
                    string[] data = lst_item_const[i];
                    int empty_cols = data.Where(p => string.IsNullOrEmpty(p)).Count();
                    if (empty_cols == data.Length)
                        continue;

                    RPT_REPORTDETAIL_VALUECONST item_rptconst = new RPT_REPORTDETAIL_VALUECONST();
                    item_rptconst.Rpt_ReportDetail_ValueConstKey = Guid.NewGuid();
                    item_rptconst.Rpt_ReportHeaderKey = iteminfo.Rpt_ReportHeaderKey;
                    item_rptconst.Rpt_Templates_Detail_ValueConstKey = string.IsNullOrEmpty(data[0]) ? Guid.Empty : Guid.Parse(data[0]);
                    item_rptconst.CodeID = data[1];
                    item_rptconst.CodeName = data[2];
                    item_rptconst.Sort = i;
                    item_rptconst.ContentValue = data[3];
                    item_rptconst.CreatedOn = DateTime.Now;
                    item_rptconst.ModifiedOn = DateTime.Now;
                    item_rptconst.CreatedBy = MyAccount.UserID;
                    item_rptconst.ModifiedBy = MyAccount.UserID;
                    db.RPT_REPORTDETAIL_VALUECONST.Add(item_rptconst);
                }

                #endregion

                #region save table list to report

                var lst_templatetbl = db.RPT_TEMPLATES_DETAIL_TABLEINDEX
                    .Where(p => p.Rpt_TemplatesKey == iteminfo.Rpt_TemplatesKey)
                    .ToList();

                foreach (RPT_TEMPLATES_DETAIL_TABLEINDEX itemtbl_template in lst_templatetbl)
                {
                    RPT_REPORTDETAIL_TABLEINDEX item_tblrpt = new RPT_REPORTDETAIL_TABLEINDEX();
                    item_tblrpt.Rpt_ReportDetail_TableIndexKey = Guid.NewGuid();
                    item_tblrpt.Rpt_Templates_TableNameKey = itemtbl_template.Rpt_Templates_TableNameKey;
                    item_tblrpt.Rpt_ReportHeaderKey = iteminfo.Rpt_ReportHeaderKey;
                    db.RPT_REPORTDETAIL_TABLEINDEX.Add(item_tblrpt);

                    #region add columns of table

                    var lst_tblcolumns_template = db.RPT_TEMPLATES_TABLECOLUMN
                        .Where(p => p.Rpt_Templates_TableNameKey == itemtbl_template.Rpt_Templates_TableNameKey)
                        .ToList();
                    foreach (RPT_TEMPLATES_TABLECOLUMN item_coltemplate in lst_tblcolumns_template)
                    {
                        RPT_REPORTDETAIL_TABLECOLUMN item_colrpt = new RPT_REPORTDETAIL_TABLECOLUMN();
                        item_colrpt.Rpt_ReportDetail_TableColumnKey = Guid.NewGuid();
                        item_colrpt.Rpt_ReportDetail_TableIndexKey = item_tblrpt.Rpt_ReportDetail_TableIndexKey;
                        item_colrpt.Rpt_Templates_TableColumnKey = item_coltemplate.Rpt_Templates_TableColumnKey;
                        item_colrpt.HiddenOnGUI = item_coltemplate.HiddenOnGUI;

                        db.RPT_REPORTDETAIL_TABLECOLUMN.Add(item_colrpt);
                    }
                    #endregion
                }

                #endregion

                db.SaveChanges();
                if (!string.IsNullOrEmpty(iteminfo.FileReference))
                {
                    //copy to org
                    System.IO.File.Copy(Server.MapPath(itemTemplate.FileReference), Server.MapPath(iteminfo.FileReference));
                }
                result.Data = iteminfo;
                result.strMess = "Đã lưu thông tin";
                CreateLog(m_ControllerName, "Create", iteminfo.Rpt_ReportHeaderKey.ToString(), iteminfo.Rpt_ID, iteminfo.Rpt_Name, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        private string CheckBeforeSave(RPT_REPORTHEADER iteminfo, bool isNew)
        {
            if (string.IsNullOrEmpty(iteminfo.Rpt_ID))
                return "Vui lòng điền mã báo cáo";
            if (string.IsNullOrEmpty(iteminfo.Rpt_Name))
                return "Vui lòng điền tên báo cáo";
            if (iteminfo.Rpt_Month <= 0 || iteminfo.Rpt_Month > 12)
                return "Tháng báo cáo không hợp lệ";
            if (iteminfo.Rpt_Year < 1900)
                return "Năm báo cáo không hợp lệ";
            if (iteminfo.Rpt_TemplatesKey == null)
                return "Vui lòng chọn mẫu báo cáo";
            return string.Empty;
        }

        // GET: RPT_REPORTHEADER/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (!CheckPermission("REPORTS_INIT_EDIT"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            if (id == null)
            {
                return HttpNotFound();
            }
            RPT_REPORTHEADER iteminfo = db.RPT_REPORTHEADER.Find(id);
            if (iteminfo == null)
            {
                return HttpNotFound();
            }
            #region query data

            var queryEmp = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).ToList().Select(p => new
            {
                p.EmployeeKey,
                p.DepartmentKey,
                ED_Name = (p.DepartmentKey == null ? "" : db.CAT_DEPARTMENTS.Find(p.DepartmentKey).DepartmentName + " - ") + p.EmployeeName
            }).OrderBy(p => p.ED_Name).ToList();

            var queryTemplate = (from p in db.RPT_TEMPLATES
                                 where p.RecordStatus == 1
                                 orderby p.Templates_ID
                                 select new
                                 {
                                     p.Rpt_TemplatesKey,
                                     Templates_Name = p.Templates_ID + " - " + p.Templates_Name
                                 }).ToList();

            if (MyAccount.IsDefaultUser != null && !MyAccount.IsDefaultUser.Value)
            {
                var depkey = db.CAT_EMPLOYEES.Find(MyAccount.EmployeeKey)?.DepartmentKey;

                var templates_restricted = (from d in db.RPT_TEMPLATES_DEPARTMENT
                                            where d.DepartmentKey == depkey
                                            select d).ToList().Select(p => p.Rpt_TemplatesKey).ToList();

                queryTemplate = queryTemplate.Where(p => templates_restricted.Contains(p.Rpt_TemplatesKey)).ToList();
                queryEmp = queryEmp.Where(p => MyAccount.IsDefaultUser.Value || p.DepartmentKey == depkey).ToList();
            }

            #endregion

            ViewBag.FileRaw = string.Format("{0}?Time={1}", iteminfo.FileReference, DateTime.Now.Ticks);
            ViewBag.EmployessList = new SelectList(queryEmp, "EmployeeKey", "ED_Name");
            ViewBag.TemplatesList = new SelectList(queryTemplate, "Rpt_TemplatesKey", "Templates_Name");

            return View(iteminfo);
        }

        public ActionResult UploadNewFile(HttpPostedFileBase file)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                if (file != null)
                {
                    Guid id = new Guid(Request["id"]);
                    RPT_REPORTHEADER rptHdr = db.RPT_REPORTHEADER.Find(id);
                    if (rptHdr == null)
                        throw new Exception("Không tìm thấy thông tin.");
                    if (!string.IsNullOrEmpty(rptHdr.FileReference))
                        DeleteFileUploaded(Server.MapPath(rptHdr.FileReference));
                    rptHdr.FileReference = string.Format("{0}{1}_{2}", PathUpload_HeaderOrg, Now_Full_String(), file.FileName);
                    file.SaveAs(Server.MapPath(rptHdr.FileReference));

                    db.Entry(rptHdr).State = EntityState.Modified;
                    db.SaveChanges();
                    result.strMess = "Đã lưu thông tin";
                }
                else
                {
                    result.intStatus = 2;
                }
            }
            catch (Exception ex)
            {
                result.strMess = ex.Message;
                result.intStatus = 0;
            }

            return new CustomJsonResult { Data = (result) };
        }

        // POST: RPT_REPORTHEADER/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(RPT_REPORTHEADER iteminfo, List<string[]> lst_item_const, string[] lst_item_const_id_deleted)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;

            try
            {
                result.strMess = CheckBeforeSave(iteminfo, false);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                #region save info

                var oldItem = db.RPT_REPORTHEADER.Find(iteminfo.Rpt_ReportHeaderKey);
                if (iteminfo == null)
                {
                    throw new Exception("không tìm thấy thông tin báo cáo");
                }
                iteminfo.Rpt_ID = iteminfo.Rpt_ID.Trim();
                string[] prop_set_list = { "Rpt_ID", "Rpt_Name", "Rpt_Year", "DateStart", "DateEnd", "Rpt_Month", "Remark" };

                foreach (string propname in prop_set_list)
                {
                    PropertyInfo pNew = iteminfo.GetType().GetProperty(propname);
                    if (pNew == null) continue;
                    object pNew_Value = pNew.GetValue(iteminfo);
                    oldItem.GetType().GetProperty(propname).SetValue(oldItem, pNew_Value, null);
                }

                oldItem.Rpt_ID = iteminfo.Rpt_ID.Trim();
                oldItem.ModifiedBy = MyAccount.UserID;
                oldItem.ModifiedOn = DateTime.Now;

                #endregion

                #region save items const

                for (int i = 0; i < lst_item_const.Count; i++)
                {
                    string[] data = lst_item_const[i];
                    if (string.IsNullOrEmpty(data[0]))
                    {
                        int empty_cols = data.Where(p => string.IsNullOrEmpty(p)).Count();
                        if (empty_cols == data.Length)
                            continue;

                        RPT_REPORTDETAIL_VALUECONST itemnew = new RPT_REPORTDETAIL_VALUECONST();
                        itemnew.Rpt_ReportDetail_ValueConstKey = Guid.NewGuid();
                        itemnew.Rpt_ReportHeaderKey = iteminfo.Rpt_ReportHeaderKey;
                        itemnew.Rpt_Templates_Detail_ValueConstKey = string.IsNullOrEmpty(data[0]) ? Guid.Empty : Guid.Parse(data[0]);
                        itemnew.CodeID = Convert.ToString(data[1]).Trim();
                        itemnew.CodeName = data[2];
                        itemnew.Sort = i;
                        itemnew.ContentValue = data[3];
                        itemnew.CreatedOn = DateTime.Now;
                        itemnew.ModifiedOn = DateTime.Now;
                        itemnew.CreatedBy = MyAccount.UserID;
                        itemnew.ModifiedBy = MyAccount.UserID;
                        db.RPT_REPORTDETAIL_VALUECONST.Add(itemnew);
                    }
                    else
                    {
                        Guid key = Guid.Parse(data[0]);
                        RPT_REPORTDETAIL_VALUECONST itemedit = db.RPT_REPORTDETAIL_VALUECONST.Find(key);
                        if (itemedit == null) continue;
                        itemedit.CodeID = data[1];
                        itemedit.CodeName = data[2];
                        itemedit.ContentValue = data[3];
                        itemedit.Sort = i;
                        itemedit.ModifiedOn = DateTime.Now;
                        itemedit.ModifiedBy = MyAccount.UserID;
                        db.Entry(itemedit).State = EntityState.Modified;
                    }
                }

                #endregion

                #region delete item const

                if (lst_item_const_id_deleted != null)
                {
                    foreach (string id in lst_item_const_id_deleted)
                    {
                        RPT_REPORTDETAIL_VALUECONST itemdel = db.RPT_REPORTDETAIL_VALUECONST.Find(Guid.Parse(id));
                        if (itemdel != null)
                            db.Entry(itemdel).State = EntityState.Deleted;
                    }
                }

                #endregion

                db.Entry(oldItem).State = EntityState.Modified;
                db.SaveChanges();
                result.Data = iteminfo;
                result.strMess = "Đã cập nhật thông tin";
                CreateLog(m_ControllerName, "Update", iteminfo.Rpt_ReportHeaderKey.ToString(), iteminfo.Rpt_ID, iteminfo.Rpt_Name, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult Detail(Guid? id)
        {
            if (!CheckPermission("REPORTS_INIT_EDIT"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            if (id == null)
            {
                return HttpNotFound();
            }
            RPT_REPORTHEADER iteminfo = db.RPT_REPORTHEADER.Find(id);
            if (iteminfo == null)
            {
                return HttpNotFound();
            }

            #region query data

            var queryEmp = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).ToList().Select(p => new
            {
                p.EmployeeKey,
                ED_Name = (p.DepartmentKey == null ? "" : db.CAT_DEPARTMENTS.Find(p.DepartmentKey).DepartmentName + " - ") + p.EmployeeName
            }).OrderBy(p => p.ED_Name).ToList();


            #endregion

            ViewBag.EmployessList = new SelectList(queryEmp.ToList(), "EmployeeKey", "ED_Name");
            ViewBag.TemplatesList = new SelectList(db.RPT_TEMPLATES.Where(p => p.RecordStatus == 1).ToList().OrderBy(p => p.Templates_Name).ToList(), "Rpt_TemplatesKey", "Templates_Name");

            return View(iteminfo);
        }

        public ActionResult LoadValueList_Edit(Guid? RptHeaderKey)
        {
            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();
            grid.ShowAction = false;

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "field key",
                Field = "Rpt_ReportDetail_ValueConstKey",
                DataType = GridDetail.eDataType.StringType,
                Width = "2%",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Mã Trường",
                Field = "CodeID",
                DataType = GridDetail.eDataType.StringType,
                Width = "26%",
                Enable = false,
                IsShow = true,
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tên Trường",
                Field = "CodeName",
                DataType = GridDetail.eDataType.StringType,
                Width = "65%",
                Enable = false,
                IsShow = true
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Dữ Liệu",
                Field = "ContentValue",
                DataType = GridDetail.eDataType.StringType,
                Width = "2%",
                Enable = true,
                IsShow = true,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            var zDetails = (from v in db.RPT_REPORTDETAIL_VALUECONST
                            orderby v.CodeID
                            where v.Rpt_ReportHeaderKey == RptHeaderKey
                            orderby v.Sort
                            select new
                            {
                                v.Rpt_ReportDetail_ValueConstKey,
                                v.CodeID,
                                v.CodeName,
                                v.ContentValue
                            }).ToList();
            if (zDetails.Count > 0)
            {
                for (int i = 0; i < zDetails.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                        zDetails[i].Rpt_ReportDetail_ValueConstKey.ToString(),
                        zDetails[i].CodeID,
                        zDetails[i].CodeName,
                        zDetails[i].ContentValue
                    });
                }
            }
            else
            {
                arrBody.Add(new object[grid.Header.Count]);
            }
            grid.Data = arrBody;
            //var data = new { header = arrHDR, body = arrBody };
            return new CustomJsonResult { Data = grid };
        }

        public ActionResult LoadTableList_Edit(int page, int pageSize, string SearchKey)
        {
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            Guid RptHeaderKey = string.IsNullOrEmpty(SearchKey) ? Guid.Empty : Guid.Parse(SearchKey);
            var searchQuery = (from t in db.RPT_REPORTDETAIL_TABLEINDEX
                               join tbl in db.RPT_TEMPLATES_TABLENAME
                                  on t.Rpt_Templates_TableNameKey equals tbl.Rpt_Templates_TableNameKey
                               orderby tbl.TableID
                               where t.Rpt_ReportHeaderKey == RptHeaderKey
                               select new
                               {
                                   t.Rpt_ReportDetail_TableIndexKey,
                                   tbl.TableID,
                                   tbl.TableName,
                                   t.Rpt_Templates_TableNameKey
                               }).ToList();
            var lst_rpt_tbls = searchQuery.Select(p => p.Rpt_ReportDetail_TableIndexKey).ToList();
            var emp_enlist = (from r in db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                              join e in db.CAT_EMPLOYEES on r.EmployeeKey equals e.EmployeeKey
                              where lst_rpt_tbls.Contains(r.Rpt_ReportDetail_TableIndexKey)
                              select new
                              {
                                  r.Rpt_ReportTables_Employee_EnlistedKey,
                                  r.Rpt_ReportDetail_TableIndexKey,
                                  e.EmployeeName
                              }).ToList();

            var data = searchQuery.ToList().Select(d => new
            {
                d.Rpt_ReportDetail_TableIndexKey,
                d.TableID,
                d.TableName,
                d.Rpt_Templates_TableNameKey,
                EmpList = emp_enlist.Where(p => p.Rpt_ReportDetail_TableIndexKey == d.Rpt_ReportDetail_TableIndexKey).Count() == 0 ? "-" :
                    emp_enlist.Where(p => p.Rpt_ReportDetail_TableIndexKey == d.Rpt_ReportDetail_TableIndexKey).OrderBy(p => p.EmployeeName).ToList().Select(p => p.EmployeeName).Aggregate((x, y) => x + "<br >" + y)
                //HanhDong = "<a type=\"button\" class=\"btn btn-danger btndeltbl\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.Rpt_ReportDetail_TableIndexKey + "'>Xóa PL</a>"
            }).ToList();

            table.intTotalPage = 1;

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Phụ Lục", IsShow = true, Format = "", Width = 150 });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Phụ Lục", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã TblTemplate", IsShow = false, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Nhân Sự", IsShow = true, Format = "" });
            //table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 100 });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportDetail_TableIndexKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadTableList_View(int page, int pageSize, string SearchKey)
        {
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            Guid RptHeaderKey = string.IsNullOrEmpty(SearchKey) ? Guid.Empty : Guid.Parse(SearchKey);
            var searchQuery = (from t in db.RPT_REPORTDETAIL_TABLEINDEX
                               join tbl in db.RPT_TEMPLATES_TABLENAME
                                  on t.Rpt_Templates_TableNameKey equals tbl.Rpt_Templates_TableNameKey
                               orderby tbl.TableID
                               where t.Rpt_ReportHeaderKey == RptHeaderKey
                               select new
                               {
                                   t.Rpt_ReportDetail_TableIndexKey,
                                   tbl.TableID,
                                   tbl.TableName,
                                   t.Rpt_Templates_TableNameKey
                               }).ToList();

            //get employee input list name
            var lst_tblindex_key = searchQuery.Select(p => p.Rpt_ReportDetail_TableIndexKey).ToList();
            var query_nguoinhap = (from p in db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                                   join e in db.CAT_EMPLOYEES
                                       on p.EmployeeKey equals e.EmployeeKey
                                   where lst_tblindex_key.Contains(p.Rpt_ReportDetail_TableIndexKey)
                                   select new
                                   {
                                       p.Rpt_ReportDetail_TableIndexKey,
                                       e.EmployeeName
                                   }).ToList();

            var data = searchQuery.ToList().Select(d => new
            {
                d.Rpt_ReportDetail_TableIndexKey,
                d.TableID,
                d.TableName,
                NguoiNhap = query_nguoinhap.Where(p => p.Rpt_ReportDetail_TableIndexKey == d.Rpt_ReportDetail_TableIndexKey).Count() == 0 ? "" :
                query_nguoinhap.Where(p => p.Rpt_ReportDetail_TableIndexKey == d.Rpt_ReportDetail_TableIndexKey)
                .Select(p => p.EmployeeName).ToList().Aggregate((x, y) => x + ";" + y)
            }).ToList();

            table.intTotalPage = 1;

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Phụ Lục", IsShow = true, Format = "", Width = 14, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Phụ Lục", IsShow = true, Format = "", Width = 60, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Người nhập", IsShow = true, Format = "", Width = 25, Width_Measure = "%" });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportDetail_TableIndexKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadTableUsersList_Edit(Guid? RptHeaderKey)
        {
            var lst_users = (from tbl in db.RPT_REPORTDETAIL_TABLEINDEX
                             join u in db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                                 on tbl.Rpt_ReportDetail_TableIndexKey equals u.Rpt_ReportDetail_TableIndexKey
                             where tbl.Rpt_ReportHeaderKey == RptHeaderKey
                             select new
                             {
                                 tbl.Rpt_ReportDetail_TableIndexKey,
                                 u.EmployeeKey,
                                 u.Activate
                             }).ToList();

            List<string> lst_tbl = new List<string>();
            foreach (var item in lst_users)
            {
                lst_tbl.Add(item.Rpt_ReportDetail_TableIndexKey.ToString());
                lst_tbl.Add(item.EmployeeKey.ToString());
                lst_tbl.Add(item.Activate == true ? "1" : "0");
            }
            Dictionary<string, object> lst_result = new Dictionary<string, object>();
            return new CustomJsonResult { Data = lst_tbl };
        }

        // GET: RPT_REPORTHEADER/Delete/5
        public ActionResult Delete(Guid? id)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            result.strMess = "Đã xóa thông tin";
            RPT_REPORTHEADER item = db.RPT_REPORTHEADER.Find(id);

            try
            {
                if (!CheckPermission("REPORTS_INIT_DELETE"))
                    throw new Exception(NO_PERMISSION);

                if (item == null)
                    throw new Exception("Không tìm thấy thông tin!");

                item.RecordStatus = 0;
                item.ModifiedBy = MyAccount.UserID;
                item.ModifiedOn = DateTime.Now;

                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                CreateLog(m_ControllerName, "Delete", item.Rpt_ReportHeaderKey.ToString(), item.Rpt_ID, item.Rpt_Name, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult CopyToNew(Guid? id, bool copyall)
        {
            ViewBag.ControllerName = m_ControllerName;
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                if (id == null)
                    throw new Exception("Mã báo cáo chưa xác định");

                RPT_REPORTHEADER itemHDR_Old = db.RPT_REPORTHEADER.Find(id);
                if (itemHDR_Old == null)
                    throw new Exception("Không tìm thấy thông tin báo cáo để sao chép");

                RPT_REPORTHEADER itemHDR_New = new RPT_REPORTHEADER();
                itemHDR_New.Rpt_ReportHeaderKey = Guid.NewGuid();
                itemHDR_New.Rpt_ID = "(Copy) " + itemHDR_Old.Rpt_ID;
                itemHDR_New.Rpt_Name = "(Copy) " + itemHDR_Old.Rpt_Name;
                itemHDR_New.Rpt_Month = Now.Month;
                itemHDR_New.Rpt_Year = Now.Year;
                itemHDR_New.DateStart = Now;
                itemHDR_New.DateEnd = new DateTime(Now.Year, 12, 31);
                itemHDR_New.Rpt_TemplatesKey = itemHDR_Old.Rpt_TemplatesKey;
                itemHDR_New.FileReference = itemHDR_Old.FileReference;
                itemHDR_New.RecordStatus = 1;
                itemHDR_New.RptStatus = Convert.ToInt32(eRptHeaderStatus.Created);
                itemHDR_New.CreatedOn = Now;
                itemHDR_New.ModifiedOn = Now;
                itemHDR_New.CreatedBy = MyAccount.UserID;
                itemHDR_New.ModifiedBy = MyAccount.UserID;
                itemHDR_New.Person_InputValues = itemHDR_Old.Person_InputValues;

                if (!string.IsNullOrEmpty(itemHDR_Old.FileReference))
                {
                    itemHDR_New.FileReference = string.Format("{0}{1}_{2}", PathUpload_HeaderOrg, Now_Full_String(), itemHDR_Old.FileName);
                    itemHDR_New.FileName = itemHDR_Old.FileName;
                }
                db.RPT_REPORTHEADER.Add(itemHDR_New);

                #region sao chéo trường dữ liệu

                var lst_valueconst = db.RPT_REPORTDETAIL_VALUECONST.Where(p => p.Rpt_ReportHeaderKey == id).ToList();
                foreach (RPT_REPORTDETAIL_VALUECONST itemconst in lst_valueconst)
                {
                    RPT_REPORTDETAIL_VALUECONST item_new = new RPT_REPORTDETAIL_VALUECONST();
                    item_new.Rpt_ReportDetail_ValueConstKey = Guid.NewGuid();
                    item_new.Rpt_ReportHeaderKey = itemHDR_New.Rpt_ReportHeaderKey;
                    item_new.Rpt_Templates_Detail_ValueConstKey = itemconst.Rpt_Templates_Detail_ValueConstKey;
                    item_new.CodeID = itemconst.CodeID;
                    item_new.CodeName = itemconst.CodeName;
                    if (copyall)
                        item_new.ContentValue = itemconst.ContentValue;
                    item_new.Sort = itemconst.Sort;
                    item_new.ItemReferenceKey = itemconst.ItemReferenceKey;
                    item_new.CreatedBy = itemHDR_New.CreatedBy;
                    item_new.ModifiedBy = itemHDR_New.ModifiedBy;
                    item_new.CreatedOn = itemHDR_New.CreatedOn;
                    item_new.ModifiedOn = itemHDR_New.ModifiedOn;

                    db.RPT_REPORTDETAIL_VALUECONST.Add(item_new);
                }

                #endregion

                #region sao chép bảng phụ lục

                var lst_tbls = db.RPT_REPORTDETAIL_TABLEINDEX.Where(p => p.Rpt_ReportHeaderKey == id).ToList();

                foreach (RPT_REPORTDETAIL_TABLEINDEX itemtbl in lst_tbls)
                {
                    Guid tblkey_Old = itemtbl.Rpt_ReportDetail_TableIndexKey;

                    //sao chép bảng phụ lục
                    RPT_REPORTDETAIL_TABLEINDEX itemtbl_New = new RPT_REPORTDETAIL_TABLEINDEX();
                    itemtbl_New.Rpt_ReportDetail_TableIndexKey = Guid.NewGuid();
                    itemtbl_New.Rpt_ReportHeaderKey = itemHDR_New.Rpt_ReportHeaderKey;
                    itemtbl_New.Rpt_Templates_TableNameKey = itemtbl.Rpt_Templates_TableNameKey;
                    db.RPT_REPORTDETAIL_TABLEINDEX.Add(itemtbl_New);

                    //sao chép cột phụ lục
                    var lst_cols = db.RPT_REPORTDETAIL_TABLECOLUMN.Where(p => p.Rpt_ReportDetail_TableIndexKey == tblkey_Old).ToList();
                    foreach (RPT_REPORTDETAIL_TABLECOLUMN col in lst_cols)
                    {
                        Guid col_value_key_old = col.Rpt_ReportDetail_TableColumnKey;
                        RPT_REPORTDETAIL_TABLECOLUMN col_new = new RPT_REPORTDETAIL_TABLECOLUMN();
                        col_new.Rpt_ReportDetail_TableColumnKey = Guid.NewGuid();
                        col_new.Rpt_ReportDetail_TableIndexKey = itemtbl_New.Rpt_ReportDetail_TableIndexKey;
                        col_new.Rpt_Templates_TableColumnKey = col.Rpt_Templates_TableColumnKey;
                        col_new.HiddenOnGUI = col.HiddenOnGUI;
                        db.RPT_REPORTDETAIL_TABLECOLUMN.Add(col_new);

                        if (copyall)
                        {
                            #region sao chép giá trị dữ liệu phụ lục

                            var lst_col_values = db.RPT_REPORTDETAIL_TABLECOLUMN_VALUE.Where(p => p.Rpt_ReportDetail_TableColumnKey == col_value_key_old).ToList();
                            foreach (RPT_REPORTDETAIL_TABLECOLUMN_VALUE col_val in lst_col_values)
                            {
                                RPT_REPORTDETAIL_TABLECOLUMN_VALUE col_val_new = new RPT_REPORTDETAIL_TABLECOLUMN_VALUE();
                                col_val_new.Rpt_ReportDetail_TableColumn_ValueKey = Guid.NewGuid();
                                col_val_new.Rpt_ReportDetail_TableColumnKey = col_new.Rpt_ReportDetail_TableColumnKey;
                                col_val_new.RowIndex = col_val.RowIndex;
                                col_val_new.ContentValue = col_val.ContentValue;
                                col_val_new.RefContentKey = col_val.RefContentKey;
                                col_val_new.CreatedBy = itemHDR_New.CreatedBy;
                                col_val_new.ModifiedBy = itemHDR_New.ModifiedBy;
                                col_val_new.CreatedOn = itemHDR_New.CreatedOn;
                                col_val_new.ModifiedOn = itemHDR_New.ModifiedOn;

                                db.RPT_REPORTDETAIL_TABLECOLUMN_VALUE.Add(col_val_new);
                            }

                            #endregion
                        }
                    }

                    #region sao chép người dùng nhập liệu

                    var lst_employees = db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED.Where(p => p.Rpt_ReportDetail_TableIndexKey == tblkey_Old && p.Activate == true).ToList();
                    foreach (RPT_REPORTTABLES_EMPLOYEES_ENLISTED emp in lst_employees)
                    {
                        RPT_REPORTTABLES_EMPLOYEES_ENLISTED user_new = new RPT_REPORTTABLES_EMPLOYEES_ENLISTED();
                        user_new.Rpt_ReportTables_Employee_EnlistedKey = Guid.NewGuid();
                        user_new.Rpt_ReportDetail_TableIndexKey = itemtbl_New.Rpt_ReportDetail_TableIndexKey;
                        user_new.EmployeeKey = emp.EmployeeKey;
                        user_new.Activate = emp.Activate;

                        db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED.Add(user_new);
                    }

                    #endregion
                }

                #endregion

                #region copy các file đính kèm ra file mới

                var lst_files_attached = db.RPT_REPORTFILESATTACH.Where(p => p.Rpt_ReportHeaderKey == itemHDR_Old.Rpt_ReportHeaderKey).ToList();
                foreach (RPT_REPORTFILESATTACH file_attached in lst_files_attached)
                {
                    RPT_REPORTFILESATTACH file_att_new = new RPT_REPORTFILESATTACH();
                    file_att_new.Rpt_ReportFileAttachedKey = Guid.NewGuid();
                    file_att_new.Rpt_ReportHeaderKey = itemHDR_New.Rpt_ReportHeaderKey;
                    file_att_new.AttachedFileID = itemHDR_New.Rpt_ID;
                    file_att_new.AttachedFileName = file_attached.AttachedFileName;
                    file_att_new.FileReference = string.Format("{0}{1}_{2}_{3}", PathUpload_FileAttached, itemHDR_Old.Rpt_ID, Now_Full_String(), file_attached.AttachedFileName);
                    file_att_new.Note = "Copy";
                    file_att_new.Active = true;
                    file_att_new.RecordStatus = Convert.ToInt32(eRecordStatus.Active);
                    file_att_new.CreatedOn = Now;
                    file_att_new.CreatedBy = MyAccount.UserID;
                    file_att_new.ModifiedOn = Now;
                    file_att_new.ModifiedBy = MyAccount.UserID;
                    db.RPT_REPORTFILESATTACH.Add(file_att_new);

                    System.IO.File.Copy(Server.MapPath(file_attached.FileReference), Server.MapPath(file_att_new.FileReference));
                }

                #endregion

                db.SaveChanges();
                if (!string.IsNullOrEmpty(itemHDR_New.FileReference))
                {
                    //copy org
                    System.IO.File.Copy(Server.MapPath(itemHDR_Old.FileReference), Server.MapPath(itemHDR_New.FileReference));
                }

                result.strMess = "Sao chép thành công";
                result.Data = itemHDR_New;
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult AddNew_PL_OnEdit(Guid? rpt_headerkey, string[] lst_tables_template)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                if (rpt_headerkey == null)
                    throw new Exception("Không tìm thấy thông tin báo cáo");

                int row_existed = 0;
                int row_added = 0;

                foreach (string tbl_template_key in lst_tables_template)
                {
                    Guid key = Guid.Parse(tbl_template_key);
                    int item_existed_count = db.RPT_REPORTDETAIL_TABLEINDEX
                        .Where(p => p.Rpt_ReportHeaderKey == rpt_headerkey && p.Rpt_Templates_TableNameKey == key)
                        .ToList().Count;

                    if (item_existed_count > 0)
                        row_existed++;
                    else
                    {
                        //add table template
                        RPT_REPORTDETAIL_TABLEINDEX itemtbl = new RPT_REPORTDETAIL_TABLEINDEX();
                        itemtbl.Rpt_ReportDetail_TableIndexKey = Guid.NewGuid();
                        itemtbl.Rpt_ReportHeaderKey = rpt_headerkey.Value;
                        itemtbl.Rpt_Templates_TableNameKey = key;
                        db.RPT_REPORTDETAIL_TABLEINDEX.Add(itemtbl);
                        row_added++;

                        //addcolumn template
                        var lst_cols = db.RPT_TEMPLATES_TABLECOLUMN.Where(p => p.Rpt_Templates_TableNameKey == key).ToList();
                        foreach (RPT_TEMPLATES_TABLECOLUMN itemcol in lst_cols)
                        {
                            RPT_REPORTDETAIL_TABLECOLUMN itemRptCol = new RPT_REPORTDETAIL_TABLECOLUMN();
                            itemRptCol.Rpt_ReportDetail_TableColumnKey = Guid.NewGuid();
                            itemRptCol.Rpt_ReportDetail_TableIndexKey = itemtbl.Rpt_ReportDetail_TableIndexKey;
                            itemRptCol.Rpt_Templates_TableColumnKey = itemcol.Rpt_Templates_TableColumnKey;
                            itemRptCol.HiddenOnGUI = itemcol.HiddenOnGUI;
                            db.RPT_REPORTDETAIL_TABLECOLUMN.Add(itemRptCol);
                        }
                    }
                }
                if (row_added > 0)
                {
                    db.SaveChanges();
                    result.intStatus = 1;
                    result.strMess += string.Format("Đã thêm mới {0} phụ lục vào báo cáo", row_added);
                }
                if (row_existed > 0)
                    result.strMess += (string.IsNullOrEmpty(result.strMess) ? "" : "<br />")
                        + string.Format("Bỏ qua {0} phụ lục (đã có trong báo cáo)", row_existed);

            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult Delete_PL_OnEdit(Guid? Report_TableIndex_Key)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                RPT_REPORTDETAIL_TABLEINDEX item = db.RPT_REPORTDETAIL_TABLEINDEX.Find(Report_TableIndex_Key);
                if (item == null)
                    throw new Exception("Không tìm thấy thông tin!");

                //xóa column
                var lst_cols = db.RPT_REPORTDETAIL_TABLECOLUMN.Where(p => p.Rpt_ReportDetail_TableIndexKey == Report_TableIndex_Key).ToList();
                foreach (RPT_REPORTDETAIL_TABLECOLUMN col in lst_cols)
                {
                    Guid col_key = col.Rpt_ReportDetail_TableColumnKey;
                    var lst_values = db.RPT_REPORTDETAIL_TABLECOLUMN_VALUE.Where(p => p.Rpt_ReportDetail_TableColumnKey == col_key).ToList();
                    foreach (RPT_REPORTDETAIL_TABLECOLUMN_VALUE col_val in lst_values)
                    {
                        db.Entry(col_val).State = EntityState.Deleted;
                    }
                    db.Entry(col).State = EntityState.Deleted;
                }

                db.Entry(item).State = EntityState.Deleted;
                db.SaveChanges();
                result.intStatus = 1;
                result.strMess = "Đã xóa phụ lục";
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult FileAttached_SaveFile(List<HttpPostedFileBase> FileUpload)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                Guid rptHdrkey = Guid.Parse(Request.Form["Rpt_ReportHeaderKey"]);
                RPT_REPORTHEADER itemHdr = db.RPT_REPORTHEADER.Find(rptHdrkey);

                foreach (HttpPostedFileBase f in FileUpload)
                {
                    if (f == null) continue;

                    RPT_REPORTFILESATTACH item = new RPT_REPORTFILESATTACH();
                    item.Rpt_ReportFileAttachedKey = Guid.NewGuid();
                    item.Rpt_ReportHeaderKey = rptHdrkey;
                    item.AttachedFileID = itemHdr.Rpt_ID;
                    item.AttachedFileName = f.FileName;
                    item.FileReference = string.Format("{0}{1}_{2}_{3}", PathUpload_FileAttached, itemHdr.Rpt_ID, Now_Full_String(), f.FileName);
                    item.RecordStatus = Convert.ToInt32(eRecordStatus.Active);
                    item.Note = Request.Form["Note"];
                    item.Active = true;
                    item.CreatedOn = Now;
                    item.CreatedBy = MyAccount.UserID;
                    item.ModifiedOn = Now;
                    item.ModifiedBy = MyAccount.UserID;
                    db.RPT_REPORTFILESATTACH.Add(item);
                    f.SaveAs(Server.MapPath(item.FileReference));
                }
                db.SaveChanges();
                result.strMess = "Thêm thành công";
                result.redirectlink = string.Format("/{0}/RptFileAttachedEdit/{1}", m_ControllerName, rptHdrkey);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult FileAttached_DeleteFile(Guid? id)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                RPT_REPORTFILESATTACH item = db.RPT_REPORTFILESATTACH.Find(id);
                if (item == null)
                    throw new Exception("Không tìm thấy thông tin");
                System.IO.File.Delete(Server.MapPath(item.FileReference));
                db.Entry(item).State = EntityState.Deleted;
                db.SaveChanges();

                result.strMess = "Xóa thành công";
                result.redirectlink = string.Format("/{0}/RptFileAttachedEdit/{1}", m_ControllerName, item.Rpt_ReportHeaderKey);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
