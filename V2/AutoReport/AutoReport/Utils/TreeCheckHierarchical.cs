﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Utils
{
    public class TreeCheckHierarchical
    {
        public TreeCheckHierarchical()
        {
            IsTopMostParent = false;
            Sorted = 0;
            IsChecked = false;
            IsLeaf = false;
        }

        public bool IsTopMostParent { get; set; }

        public bool IsLeaf { get; set; }

        public string Node_Id { get; set; }

        public string Node_Text { get; set; }

        public string NodeParent_Id { get; set; }

        public string NodeParent_Text { get; set; }

        public int Sorted { get; set; }

        public bool IsChecked { get; set; }
    }
}