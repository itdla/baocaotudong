﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
namespace AutoReport.Controllers
{
    public class UsersController : BaseController
    {
        // GET: ItemString
        private AutoReportEntities db = new AutoReportEntities();
        private string m_ControllerName = "Users";

        public ActionResult Index()
        {
            if (!CheckPermission("USERS"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            ViewBag.ControllerName = m_ControllerName;

            return View();
        }

        public ActionResult Create()
        {
            if (!CheckPermission("USERS_ADD"))
            {
                return RedirectToAction("NotPermission", "Home");
            }

            ViewBag.ControllerName = m_ControllerName;

            #region query data

            var queryEmpSearch = (from e in db.CAT_EMPLOYEES
                                  where e.RecordStatus == 1
                                  select new
                                  {
                                      e.EmployeeKey,
                                      e.EmployeeName,
                                      e.PositionKey,
                                      e.DepartmentKey
                                  }).ToList();

            var fq1_Emp = queryEmpSearch.Select(p => new
            {
                p.EmployeeKey,
                p.EmployeeName,
                DepartmentName = db.CAT_DEPARTMENTS.Find(p.DepartmentKey)?.DepartmentName ?? "",
                PositionName = db.CAT_POSITIONS.Find(p.PositionKey)?.PositionName ?? ""
            }).OrderBy(p => p.DepartmentName).ThenBy(p => p.EmployeeName).ToList();

            var fq2_Emp = fq1_Emp.Select(p => new
            {
                p.EmployeeKey,
                ED_Name = string.Format("{0}{1}{2}", p.DepartmentName == "" ? "" : p.DepartmentName + " > ",
                    p.EmployeeName,
                    p.PositionName == "" ? "" : " > " + p.PositionName)
            }).ToList();

            #endregion

            ViewBag.EmployessList = new SelectList(fq2_Emp.ToList(), "EmployeeKey", "ED_Name"); SYS_Users Users = new SYS_Users();
            Users.Active = true;
            return View(Users);
        }

        [HttpPost]
        public ActionResult Create(SYS_Users iteminfo)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                result.strMess = CheckBeforeSave(iteminfo, true);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                iteminfo.IsDefaultUser = false;//true for admin only and cannot be deleted
                iteminfo.UserID = Guid.NewGuid();
                iteminfo.RecordStatus = 1;
                iteminfo.CreatedBy = MyAccount.UserID;
                iteminfo.ModifiedBy = MyAccount.UserID;
                iteminfo.CreatedOn = DateTime.Now;
                iteminfo.ModifiedOn = DateTime.Now;

                db.SYS_Users.Add(iteminfo);
                db.SaveChanges();
                result.Data = iteminfo;
                result.strMess = "Đã lưu thông tin";
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            CreateLog(m_ControllerName, "Create", iteminfo.UserID.ToString(), iteminfo.UserName, iteminfo.UserName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult Edit(Guid? id)
        {
            if (!CheckPermission("USERS_EDIT"))
            {
                return RedirectToAction("NotPermission", "Home");
            }

            ViewBag.ControllerName = m_ControllerName;

            #region query data

            var queryEmpSearch = (from e in db.CAT_EMPLOYEES
                                  where e.RecordStatus == 1
                                  select new
                                  {
                                      e.EmployeeKey,
                                      e.EmployeeName,
                                      e.PositionKey,
                                      e.DepartmentKey
                                  }).ToList();

            var fq1_Emp = queryEmpSearch.Select(p => new
            {
                p.EmployeeKey,
                p.EmployeeName,
                DepartmentName = db.CAT_DEPARTMENTS.Find(p.DepartmentKey)?.DepartmentName ?? "",
                PositionName = db.CAT_POSITIONS.Find(p.PositionKey)?.PositionName ?? ""
            }).OrderBy(p => p.DepartmentName).ThenBy(p => p.EmployeeName).ToList();

            var fq2_Emp = fq1_Emp.Select(p => new
            {
                p.EmployeeKey,
                ED_Name = string.Format("{0}{1}{2}", p.DepartmentName == "" ? "" : p.DepartmentName + " > ",
                    p.EmployeeName,
                    p.PositionName == "" ? "" : " > " + p.PositionName)
            }).ToList();

            #endregion

            ViewBag.EmployessList = new SelectList(fq2_Emp, "EmployeeKey", "ED_Name"); SYS_Users Users = new SYS_Users();
            SYS_Users item = db.SYS_Users.Find(id);
            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(SYS_Users iteminfo)
        {
            //Utils.CreateMD5(iteminfo.Password);
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                result.strMess = CheckBeforeSave(iteminfo, false);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                SYS_Users oldItem = db.SYS_Users.Find(iteminfo.UserID);
                if (oldItem == null)
                    throw new Exception("Không tìm thấy thông tin");

                if (iteminfo.UserPassword != oldItem.UserPassword)
                {
                    //if password has changed
                    iteminfo.UserPassword = Utils.CreateMD5(iteminfo.UserPassword);
                }

                string[] prop_set_list = { "UserName", "UserPassword", "Active", "EmployeeKey", "Note" };

                foreach (string propname in prop_set_list)
                {
                    PropertyInfo pNew = iteminfo.GetType().GetProperty(propname);
                    if (pNew == null) continue;
                    object pNew_Value = pNew.GetValue(iteminfo);
                    oldItem.GetType().GetProperty(propname).SetValue(oldItem, pNew_Value, null);
                }

                oldItem.ModifiedBy = MyAccount.UserID;
                oldItem.ModifiedOn = DateTime.Now;
                db.Entry(oldItem).State = EntityState.Modified;

                db.SaveChanges();
                result.Data = iteminfo;
                result.strMess = "Đã lưu thông tin";
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            CreateLog(m_ControllerName, "Edit", iteminfo.UserID.ToString(), iteminfo.UserName, iteminfo.UserName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult Delete(Guid id)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            result.strMess = "Đã xóa thông tin";
            SYS_Users item = db.SYS_Users.Find(id);
            try
            {
                if (!CheckPermission("USERS_DELETE"))
                    throw new Exception(NO_PERMISSION);

                if (item == null)
                    throw new Exception("Không tìm thấy thông tin!");

                if (Convert.ToBoolean(item.IsDefaultUser ?? false))
                    throw new Exception("Tài khoản mặc định của hệ thống, không thể xóa.");

                item.RecordStatus = 0;
                item.ModifiedBy = MyAccount.UserID;
                item.ModifiedOn = DateTime.Now;

                result.strMess = "Người dùng đã bị xóa!";
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            CreateLog(m_ControllerName, "Delete", item.UserID.ToString(), item.UserName, item.UserName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            return new CustomJsonResult { Data = (result) };
        }

        private string CheckBeforeSave(SYS_Users iteminfo, bool isNew)
        {
            if (string.IsNullOrEmpty(iteminfo.UserName))
                return "Tên đăng nhập không được để trống";
            if (string.IsNullOrEmpty(iteminfo.UserPassword))
                return "Mật khẩu không được để trống";
            return string.Empty;
        }

        public JsonResult LoadList(int page, int pageSize, string UserName, string EmployeeName)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            UserName = UserName ?? "";
            EmployeeName = EmployeeName ?? "";

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(UserName))
                table.KeysSearch.Add("UserName", UserName);
            if (!string.IsNullOrEmpty(EmployeeName))
                table.KeysSearch.Add("EmployeeName", EmployeeName);

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            var searchQuery = from f in db.SYS_Users
                              where f.RecordStatus == 1
                              select new
                              {
                                  f.UserID,
                                  f.UserName,
                                  f.EmployeeKey,
                                  f.GroupKey
                              };

            var fq1 = searchQuery.ToList().Select(p => new
            {
                p.UserID,
                p.UserName,
                EmployeeName = db.CAT_EMPLOYEES.Find(p.EmployeeKey)?.EmployeeName ?? ""
            }).OrderBy(p => p.UserName).ToList();

            //lọc theo điều kiện
            if (!string.IsNullOrEmpty(UserName))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.UserName.Trim().ToLower()).Contains(Utils.RemoveVni(UserName.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(EmployeeName))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.EmployeeName.Trim().ToLower()).Contains(Utils.RemoveVni(EmployeeName.Trim().ToLower()))).ToList();

            int _index = 1;
            var fq2 = fq1.Select(d => new
            {
                d.UserID,
                STT = _index++,
                d.UserName,
                d.EmployeeName,
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/Edit/" + d.UserID + "'>Sửa</a>" +
                "<a href=\"/Group/Edit?UserID=" + d.UserID + "\" class=\"btn btn-primary\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.UserID + "'>Quyền</a>" +
                "<a type=\"button\" class=\"btn btn-danger btn_xoa\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.UserID + "'>Xóa </a>",
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 7, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Tài khoản", IsShow = true, Format = "", FieldName = "UserName", Width = 22, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Nhân Viên", IsShow = true, Format = "", FieldName = "EmployeeName", Width = 50, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 20, Width_Measure = "%" });
            //

            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].UserID.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }
    }
}