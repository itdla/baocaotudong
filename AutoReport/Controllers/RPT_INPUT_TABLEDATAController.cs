﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using Spire.Doc;
using Spire.Doc.Documents;

namespace AutoReport.Controllers
{
    public class RPT_INPUT_TABLEDATAController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();

        private string m_ControllerName = "RPT_INPUT_TABLEDATA";

        public ActionResult RptDataInput_Index()
        {
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult RptDataInput_Edit(Guid? id)
        {
            ViewBag.ControllerName = m_ControllerName;
            RPT_REPORTHEADER item = db.RPT_REPORTHEADER.Find(id);

            Guid rptkey = id.Value;
            Guid userkey = MyAccount.UserID;
            RPT_REPORT_SENT_APPROVED rpt_inputcomplete = db.RPT_REPORT_SENT_APPROVED.Where(p => p.Rpt_ReportHeaderKey == rptkey && p.Rpt_DataType == 1 && p.CreatedBy == userkey).FirstOrDefault();
            ViewBag.InputCompleted = rpt_inputcomplete == null ? "no" : (rpt_inputcomplete.InputStatus == Convert.ToInt32(eRptInputStatus.Done) ? "yes" : "no");

            ViewBag.BackToIndexLink = "/RPT_INPUT_PREVIEW/RptInputListIndex";
            return View(item);
        }

        public ActionResult LoadList_TableIndexInput_Index(int page, int pageSize, string Rpt_ID, string Rpt_Name, int? Rpt_Month, int? Rpt_Year, string DateEnd, string TinhTrang)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            Rpt_ID = Rpt_ID ?? "";
            Rpt_Name = Rpt_Name ?? "";
            Rpt_Month = Rpt_Month ?? 0;
            Rpt_Year = Rpt_Year ?? 0;
            DateEnd = DateEnd ?? "";
            TinhTrang = TinhTrang ?? "";

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(Rpt_ID))
                table.KeysSearch.Add("Rpt_ID", Rpt_ID);
            if (!string.IsNullOrEmpty(Rpt_Name))
                table.KeysSearch.Add("Rpt_Name", Rpt_Name);
            if (Rpt_Month != 0)
                table.KeysSearch.Add("Rpt_Month", Rpt_Month.ToString());
            if (Rpt_Year != 0)
                table.KeysSearch.Add("Rpt_Year", Rpt_Year.ToString());
            if (!string.IsNullOrEmpty(DateEnd))
                table.KeysSearch.Add("DateEnd", DateEnd);
            if (!string.IsNullOrEmpty(TinhTrang))
                table.KeysSearch.Add("TinhTrang", TinhTrang);

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();

            Guid userkey = MyAccount.UserID;
            Guid? rpt_emp = MyAccount.EmployeeKey;
            int rpt_data_type = Convert.ToInt32(eRptDataType.DataTable);
            int rpt_header_status = Convert.ToInt32(eRptHeaderStatus.Created);

            var query_tbl_allowed = db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                .Where(p => p.EmployeeKey == rpt_emp)
                .Select(p => p.Rpt_ReportDetail_TableIndexKey).ToList();

            var query_rpt_allowed = db.RPT_REPORTDETAIL_TABLEINDEX
                .Where(p => query_tbl_allowed.Contains(p.Rpt_ReportDetail_TableIndexKey))
                .Select(p => p.Rpt_ReportHeaderKey).ToList();

            //Filter
            var searchQuery = (from f in db.RPT_REPORTHEADER
                               where f.RecordStatus == 1 &&
                               (query_rpt_allowed.Contains(f.Rpt_ReportHeaderKey)) &&
                               f.RptStatus == rpt_header_status
                               orderby f.Rpt_Year descending, f.Rpt_Month descending
                               select f).ToList();

            var lst_rptkey = searchQuery.Select(p => p.Rpt_ReportHeaderKey).ToList();

            var lst_completed = (from d in db.RPT_REPORT_SENT_APPROVED
                                 where lst_rptkey.Contains(d.Rpt_ReportHeaderKey)
                                 && d.Rpt_DataType == rpt_data_type
                                 select new
                                 {
                                     d.Rpt_Report_Sent_ApprovedKey,
                                     d.Rpt_ReportHeaderKey,
                                     d.InputStatus,
                                     d.CreatedBy
                                 }).ToList();

            var fq1 = searchQuery.Select(p => new
            {
                p.Rpt_ReportHeaderKey,
                p.Rpt_ID,
                p.Rpt_Name,
                p.Rpt_Month,
                p.Rpt_Year,
                DateEnd = p.DateEnd == null ? "-" : p.DateEnd.Value.ToString("dd/MM/yyyy"),
                TinhTrang = (lst_completed.Where(d => d.Rpt_ReportHeaderKey == p.Rpt_ReportHeaderKey && d.CreatedBy == userkey).FirstOrDefault()?.InputStatus) ?? 0
            });

            if (!string.IsNullOrEmpty(Rpt_ID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_ID.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_ID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(Rpt_Name))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_Name.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_Name.Trim().ToLower()))).ToList();
            if (Rpt_Month != 0)
                fq1 = fq1.Where(p => p.Rpt_Month == Rpt_Month).ToList();
            if (Rpt_Year != 0)
                fq1 = fq1.Where(p => p.Rpt_Year == Rpt_Year).ToList();
            if (!string.IsNullOrEmpty(DateEnd))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.DateEnd.Trim().ToLower()).Contains(Utils.RemoveVni(DateEnd.Trim().ToLower()))).ToList();

            var fq2 = fq1.Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                d.Rpt_ID,
                d.Rpt_Name,
                d.Rpt_Month,
                d.Rpt_Year,
                d.DateEnd,
                TinhTrang = EnumUtil.GetEnumDescText((eRptInputStatus)d.TinhTrang),
                HanhDong = "<a type=\"button\" class=\"btn btn-" + (d.TinhTrang != 1 ? "success" : "primary") + "\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/RptDataInput_Edit/" + d.Rpt_ReportHeaderKey + "'>" + (d.TinhTrang != 1 ? "Nhập liệu" : "Xem") + "</a>"
            }).ToList();

            if (!string.IsNullOrEmpty(TinhTrang))
                fq2 = fq2.Where(p => Utils.RemoveVni(p.TinhTrang.Trim().ToLower()).Contains(Utils.RemoveVni(TinhTrang.Trim().ToLower()))).ToList();

            int _index = 1;
            var fq3 = fq2.Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                STT = _index++,
                d.Rpt_ID,
                d.Rpt_Name,
                d.Rpt_Month,
                d.Rpt_Year,
                d.DateEnd,
                d.TinhTrang,
                d.HanhDong
            }).ToList();

            table.intTotalPage = GetTotalPage(fq3.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq3.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Báo Cáo", IsShow = true, Format = "", Width = 20, Width_Measure = "%", FieldName = "Rpt_ID" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Báo Cáo", IsShow = true, Format = "", Width = 33, Width_Measure = "%", FieldName = "Rpt_Name" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tháng", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Month", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Năm", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Year", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Ngày hết hạn", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "DateEnd", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Tình trạng", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "TinhTrang", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 12, Width_Measure = "%" });
            //

            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportHeaderKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult RptDataInput_OnRptHeaderKeyChanged(Guid? RptHeaderKey, string inputcompleted)
        {
            //inputcompleted: yes: completed (unable to edit); no: not completed (editible)
            inputcompleted = string.IsNullOrEmpty(inputcompleted) ? "no" : inputcompleted;

            Guid current_employee = MyAccount.EmployeeKey ?? Guid.Empty;
            RPT_REPORTHEADER itemHdr = db.RPT_REPORTHEADER.Find(RptHeaderKey);

            var lst_tbl_allow = db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                .Where(p => p.EmployeeKey == current_employee)
                .Select(p => p.Rpt_ReportDetail_TableIndexKey).ToList();

            var query = (from rpt_tbls in db.RPT_REPORTDETAIL_TABLEINDEX
                         join template_tbls in db.RPT_TEMPLATES_TABLENAME
                             on rpt_tbls.Rpt_Templates_TableNameKey equals template_tbls.Rpt_Templates_TableNameKey
                         where lst_tbl_allow.Contains(rpt_tbls.Rpt_ReportDetail_TableIndexKey) &&
                         rpt_tbls.Rpt_ReportHeaderKey == RptHeaderKey
                         orderby template_tbls.TableID
                         select new
                         {
                             rpt_tbls.Rpt_ReportDetail_TableIndexKey,
                             template_tbls.TableID,
                             template_tbls.TableName
                         }).ToList();

            List<ActionResult> lst_tbl = new List<ActionResult>();
            foreach (var item in query)
            {
                string div_btn_id = "btn_section_tbl_" + item.TableID;
                string div_tbl_id = "div_section_tbl_" + item.TableID;
                lst_tbl.Add(LoadTableList_Input(item.Rpt_ReportDetail_TableIndexKey, div_btn_id, div_tbl_id, inputcompleted == "no"));
            }
            Dictionary<string, object> lst_result = new Dictionary<string, object>();
            lst_result.Add("TablesList", query);
            lst_result.Add("TablesData", lst_tbl);
            return new CustomJsonResult { Data = lst_result };
        }

        public ActionResult LoadTableList_Input(Guid? Rpt_ReportDetail_TableIndexKey, string div_btn_id, string div_tbl_id, bool editible)
        {
            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();
            grid.ShowAction = editible;

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = Convert.ToString(Rpt_ReportDetail_TableIndexKey),
                Field = "Rpt_ReportDetail_TableIndexKey",
                DataType = GridDetail.eDataType.StringType,
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });

            #region create table source

            var zColumns = (from rpt_col in db.RPT_REPORTDETAIL_TABLECOLUMN
                            join template_col in db.RPT_TEMPLATES_TABLECOLUMN
                                on rpt_col.Rpt_Templates_TableColumnKey equals template_col.Rpt_Templates_TableColumnKey
                            where rpt_col.Rpt_ReportDetail_TableIndexKey == Rpt_ReportDetail_TableIndexKey
                            orderby template_col.ColumnOrder
                            select new
                            {
                                rpt_col.Rpt_ReportDetail_TableColumnKey,
                                template_col.ColumnName,
                                template_col.ColumnCaption
                            }).ToList();

            for (int i = 0; i < zColumns.Count; i++)
            {
                grid.Header.Add(new GridDetail.HeaderColumns()
                {
                    Caption = zColumns[i].ColumnCaption,
                    Field = zColumns[i].ColumnName,
                    DataType = GridDetail.eDataType.StringType,
                    Enable = grid.ShowAction,
                    IsShow = true
                });
            }

            #endregion

            #region create data source

            Guid current_account = MyAccount.UserID;
            List<Guid> lst_tblcol_key = zColumns.Select(p => p.Rpt_ReportDetail_TableColumnKey).ToList();

            var zDetails = (from colv in db.RPT_REPORTDETAIL_TABLECOLUMN_VALUE
                            where lst_tblcol_key.Contains(colv.Rpt_ReportDetail_TableColumnKey) &&
                            (colv.CreatedBy == current_account)
                            orderby colv.RowIndex
                            select new
                            {
                                colv.Rpt_ReportDetail_TableColumnKey,
                                colv.RowIndex,
                                colv.ContentValue,
                                colv.CreatedOn,
                                colv.CreatedBy,
                                colv.ModifiedOn,
                                colv.ModifiedBy
                            }).ToList();

            if (zDetails.Count > 0)
            {
                int row_start = zDetails.Min(p => p.RowIndex).Value;
                int row_end = zDetails.Max(p => p.RowIndex).Value;
                while (row_start <= row_end)
                {
                    string[] arr_itemval = new string[lst_tblcol_key.Count + 1];
                    arr_itemval[0] = string.Empty;//Convert.ToString(Rpt_ReportDetail_TableIndexKey);
                    for (int i = 1; i <= lst_tblcol_key.Count; i++)
                    {
                        var itemval = zDetails.Where(p => p.RowIndex == row_start && p.Rpt_ReportDetail_TableColumnKey == lst_tblcol_key[i - 1]).FirstOrDefault();
                        arr_itemval[i] = itemval == null ? "" : itemval.ContentValue;
                    }
                    arrBody.Add(arr_itemval);
                    row_start++;
                }
            }
            else
            {
                arrBody.Add(new string[lst_tblcol_key.Count + 1]);
            }

            #endregion

            grid.Data = arrBody;

            Dictionary<string, object> lst_result = new Dictionary<string, object>();
            lst_result.Add("result", zColumns.Count > 0 ? "" : "Chưa có");
            lst_result.Add("msg", zColumns.Count > 0 ? "" : "Phụ lục chưa được khởi tạo");
            lst_result.Add("div_btn_id", div_btn_id);
            lst_result.Add("div_tbl_id", div_tbl_id);
            lst_result.Add("div_tbl_key", Rpt_ReportDetail_TableIndexKey.Value);
            lst_result.Add("gridlayout", grid);
            return new CustomJsonResult { Data = lst_result };
        }

        public ActionResult SaveData_Input(Guid? RptHeaderKey, List<Guid> lst_tblkey, List<string> lst_tblname, List<List<string>> lst_tbls_hdr, List<List<string[]>> lst_data, List<string[]> lst_rows_deleted)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            RPT_REPORTHEADER itemRpt = db.RPT_REPORTHEADER.Find(RptHeaderKey);

            try
            {
                Guid userkey = MyAccount.UserID;

                if (itemRpt == null)
                    throw new Exception("Không tìm thấy thông tin");

                for (int i = 0; i < lst_tblkey.Count; i++)
                {
                    Guid tbl_index_key = lst_tblkey[i];

                    #region table column bảng phụ lục

                    //lấy danh sách column trong rpt_column của table đang duyệt
                    var query_columns_tbl = (from c in db.RPT_REPORTDETAIL_TABLECOLUMN
                                             join col in db.RPT_TEMPLATES_TABLECOLUMN
                                                 on c.Rpt_Templates_TableColumnKey equals col.Rpt_Templates_TableColumnKey
                                             where c.Rpt_ReportDetail_TableIndexKey == tbl_index_key
                                             orderby col.ColumnOrder
                                             select new
                                             {
                                                 c.Rpt_ReportDetail_TableColumnKey,
                                                 col.ColumnName,
                                                 col.ColumnCaption,
                                                 col.ColumnOrder,
                                                 col.ColumnDataType
                                             }).ToList();

                    #endregion

                    if (lst_rows_deleted != null)
                    {
                        List<Guid> lst_cols = query_columns_tbl.Select(p => p.Rpt_ReportDetail_TableColumnKey).ToList();
                        for (int row_del = 0; row_del < lst_rows_deleted.Count; row_del++)
                        {
                            #region xóa các cột bị xóa trên grid, trước khi lưu dữ liệu

                            //pos_0: table index key
                            //pos_1: row index
                            string[] data_del = lst_rows_deleted[row_del];
                            if (Guid.Parse(data_del[0]) == tbl_index_key)
                            {
                                int row_del_index = Convert.ToInt32(data_del[1]);
                                var lst_items_del = db.RPT_REPORTDETAIL_TABLECOLUMN_VALUE
                                        .Where(p => lst_cols.Contains(p.Rpt_ReportDetail_TableColumnKey) && p.RowIndex >= row_del_index && p.CreatedBy == userkey)
                                        .ToList();
                                foreach (RPT_REPORTDETAIL_TABLECOLUMN_VALUE itemdel in lst_items_del)
                                    db.Entry(itemdel).State = EntityState.Deleted;
                            }

                            #endregion
                        }
                    }

                    for (int row = 0; row < lst_data[i].Count; row++)
                    {
                        #region lưu giá trị các cột của từng bảng vào db

                        var lstTemp = lst_data[i][row].ToList();
                        lstTemp.ForEach(p => p.Trim());

                        string[] datasource = lstTemp.ToArray();
                        if (datasource.Length - 1 != query_columns_tbl.Count)
                        {
                            //cột đầu tiên đánh dấu TableIndexKey dùng cho xóa Rows, nên sẽ dài hơn 1 cột
                            throw new Exception("Dữ liệu không trùng khớp với số cột trong bảng phụ lục " + lst_tblname[i]);
                        }

                        //nếu là dòng rỗng toàn bộ==> bỏ qua
                        int empty_cols = datasource.Where(p => string.IsNullOrEmpty(p)).Count();
                        if (empty_cols == datasource.Length)
                            continue;

                        for (int col = 1; col < datasource.Length; col++)
                        {
                            Guid current_col = query_columns_tbl[col - 1].Rpt_ReportDetail_TableColumnKey;
                            RPT_REPORTDETAIL_TABLECOLUMN_VALUE item_val = null;
                            if (RptHeaderKey != null)
                                item_val = db.RPT_REPORTDETAIL_TABLECOLUMN_VALUE
                                .Where(p => p.Rpt_ReportDetail_TableColumnKey == current_col && p.RowIndex == row && p.CreatedBy == userkey)
                                .FirstOrDefault();

                            //newvalue
                            if (item_val == null)
                            {
                                item_val = new RPT_REPORTDETAIL_TABLECOLUMN_VALUE();
                                item_val.Rpt_ReportDetail_TableColumn_ValueKey = Guid.NewGuid();
                                item_val.Rpt_ReportDetail_TableColumnKey = current_col;
                                item_val.RowIndex = row;
                                item_val.ContentValue = datasource[col].Trim();
                                //item_val.RefContentKey = Guid.Empty;
                                item_val.CreatedBy = MyAccount.UserID;
                                item_val.ModifiedBy = MyAccount.UserID;
                                item_val.CreatedOn = DateTime.Now;
                                item_val.ModifiedOn = DateTime.Now;
                                db.RPT_REPORTDETAIL_TABLECOLUMN_VALUE.Add(item_val);
                            }
                            else
                            {
                                item_val.ContentValue = datasource[col].Trim();
                                //item_val.RefContentKey = Guid.Empty;
                                item_val.ModifiedBy = MyAccount.UserID;
                                item_val.ModifiedOn = DateTime.Now;
                                db.Entry(item_val).State = EntityState.Modified;
                            }
                        }

                        #endregion
                    }
                }

                db.SaveChanges();
                result.Data = RptHeaderKey;
                result.strMess = "Lưu dữ liệu thành công";
                CreateLog(m_ControllerName, "Save-Data", itemRpt.Rpt_ReportHeaderKey.ToString(), itemRpt.Rpt_ID, itemRpt.Rpt_Name, System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult Update_InputTableIndex_Complete(Guid? Report_TableIndex_Key)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            var rptHeader = db.RPT_REPORTHEADER.Find(Report_TableIndex_Key);
            try
            {
                if (Report_TableIndex_Key == null)
                    throw new Exception("Không tìm thấy thông tin!");
                Guid rptkey = Report_TableIndex_Key.Value;
                Guid userkey = MyAccount.UserID;

                int rptType = Convert.ToInt32(eRptDataType.DataTable);

                bool _isNew = false;
                RPT_REPORT_SENT_APPROVED item = db.RPT_REPORT_SENT_APPROVED
                    .Where(p => p.Rpt_ReportHeaderKey == rptkey && p.Rpt_DataType == rptType && p.CreatedBy == userkey)
                    .FirstOrDefault();
                if (item == null)
                {
                    _isNew = true;
                    item = new RPT_REPORT_SENT_APPROVED();
                    item.EmployeeKey = MyAccount.EmployeeKey.Value;
                    item.CreatedBy = userkey;
                    item.CreatedOn = DateTime.Now;
                }
               
                item.Rpt_ReportHeaderKey = rptkey;
                item.Rpt_DataType = rptType;
                item.InputStatus = Convert.ToInt32(eRptInputStatus.Done);
                item.ModifiedBy = userkey;
                item.ModifiedOn = DateTime.Now;

                if (_isNew)
                    db.RPT_REPORT_SENT_APPROVED.Add(item);
                else
                    db.Entry(item).State = EntityState.Modified;

                db.SaveChanges();
                result.Data = rptkey;
                result.strMess = "Bạn đã hoàn thành nhập liệu";
                CreateLog(m_ControllerName, "Update-Status", rptHeader.Rpt_ReportHeaderKey.ToString(), rptHeader.Rpt_ID, rptHeader.Rpt_Name, System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
