﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;

namespace AutoReport.Controllers
{
    public class CAT_DEPARTMENTSController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();
        private string m_ControllerName = "CAT_DEPARTMENTS";

        // GET: CAT_DEPARTMENTS
        public ActionResult Index()
        {
            if (!CheckPermission("DEPARTMENTS"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: CAT_DEPARTMENTS/Create
        public ActionResult Create()
        {
            if (!CheckPermission("DEPARTMENTS_ADD"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            CAT_DEPARTMENTS item = new CAT_DEPARTMENTS();
            item.DepartmentID = "(Tự động phát sinh)";

            ViewBag.EmployeeList = new SelectList(db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).OrderBy(p => p.EmployeeName).ToList(), "EmployeeKey", "EmployeeName");
            ViewBag.DepartmentParentList = new SelectList(db.CAT_DEPARTMENTS.Where(p => p.RecordStatus == 1).OrderBy(p => p.DepartmentName).ToList(), "DepartmentKey", "DepartmentName");
            ViewBag.ControllerName = m_ControllerName;
            return View(item);
        }

        // POST: CAT_DEPARTMENTS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(CAT_DEPARTMENTS iteminfo)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                result.strMess = CheckBeforeSave(iteminfo, true);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                string prefix = "PB";
                var lastitem = db.CAT_DEPARTMENTS.Where(p => p.DepartmentID.Contains(prefix))
                    .OrderBy(p => p.DepartmentID).ToList().LastOrDefault();
                if (lastitem == null)
                    iteminfo.DepartmentID = prefix + "000001";
                else
                {
                    int lastnum = Convert.ToInt32(lastitem.DepartmentID.Replace(prefix, ""));
                    int nextnum = lastnum + 1;
                    if (nextnum.ToString().Length > 6)
                        iteminfo.DepartmentID = prefix + nextnum.ToString();
                    else
                        iteminfo.DepartmentID = prefix + nextnum.ToString("000000");
                }
                iteminfo.DepartmentKey = Guid.NewGuid();
                iteminfo.Active = true;
                iteminfo.RecordStatus = 1;
                iteminfo.CreatedBy = MyAccount.UserID;
                iteminfo.ModifiedBy = MyAccount.UserID;
                iteminfo.CreatedOn = DateTime.Now;
                iteminfo.ModifiedOn = DateTime.Now;

                db.CAT_DEPARTMENTS.Add(iteminfo);

                db.SaveChanges();
                result.Data = iteminfo;
                result.strMess = "Đã lưu thông tin";
                CreateLog(m_ControllerName, "Create", iteminfo.DepartmentKey.ToString(), iteminfo.DepartmentID, iteminfo.DepartmentName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        // GET: CAT_DEPARTMENTS/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (!CheckPermission("DEPARTMENTS_EDIT"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            if (id == null)
            {
                return HttpNotFound();
            }
            CAT_DEPARTMENTS iteminfo = db.CAT_DEPARTMENTS.Find(id);
            if (iteminfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmployeeList = new SelectList(db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).OrderBy(p => p.EmployeeName).ToList(), "EmployeeKey", "EmployeeName");
            ViewBag.DepartmentParentList = new SelectList(db.CAT_DEPARTMENTS.Where(p => p.RecordStatus == 1).OrderBy(p => p.DepartmentName).ToList(), "DepartmentKey", "DepartmentName");

            return View(iteminfo);
        }

        // POST: CAT_DEPARTMENTS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(CAT_DEPARTMENTS iteminfo)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                result.strMess = CheckBeforeSave(iteminfo, false);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                CAT_DEPARTMENTS oldItem = db.CAT_DEPARTMENTS.Find(iteminfo.DepartmentKey);
                if (oldItem == null)
                    throw new Exception("Không tìm thấy thông tin");

                string[] prop_set_list = { "DepartmentName", "Abbr", "ParentKey", "EmployeeKey", "Note" };

                foreach (string propname in prop_set_list)
                {
                    PropertyInfo pNew = iteminfo.GetType().GetProperty(propname);
                    if (pNew == null) continue;
                    object pNew_Value = pNew.GetValue(iteminfo);
                    oldItem.GetType().GetProperty(propname).SetValue(oldItem, pNew_Value, null);
                }

                oldItem.ModifiedBy = MyAccount.UserID;
                oldItem.ModifiedOn = DateTime.Now;
                db.Entry(oldItem).State = EntityState.Modified;

                db.SaveChanges();
                result.Data = iteminfo;
                result.strMess = "Đã lưu thông tin";
                CreateLog(m_ControllerName, "Edit", iteminfo.DepartmentKey.ToString(), iteminfo.DepartmentID, iteminfo.DepartmentName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        private string CheckBeforeSave(CAT_DEPARTMENTS iteminfo, bool isNew)
        {
            if (string.IsNullOrEmpty(iteminfo.DepartmentID))
                return "Mã phòng ban không được để trống";
            if (string.IsNullOrEmpty(iteminfo.DepartmentName))
                return "Tên phòng ban không được để trống";
            return string.Empty;
        }

        // GET: CAT_DEPARTMENTS/Delete/5
        public ActionResult Delete(Guid? id)
        {
            
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            result.strMess = "Đã xóa thông tin";
            CAT_DEPARTMENTS item = db.CAT_DEPARTMENTS.Find(id);

            try
            {
                if (!CheckPermission("DEPARTMENTS_DELETE"))
                    throw new Exception(NO_PERMISSION);

                if (item == null)
                    throw new Exception("Không tìm thấy thông tin!");

                item.RecordStatus = 0;
                item.ModifiedBy = MyAccount.UserID;
                item.ModifiedOn = DateTime.Now;

                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                CreateLog(m_ControllerName, "Delete", item.DepartmentKey.ToString(), item.DepartmentID, item.DepartmentName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult LoadList(int page, int pageSize, string DepartmentID, string DepartmentName, string Abbr, string Manager)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            //Guid _id = new Guid(this.Request["id"]);
            DepartmentID = string.IsNullOrEmpty(DepartmentID) ? "" : DepartmentID;
            DepartmentName = string.IsNullOrEmpty(DepartmentName) ? "" : DepartmentName;
            Abbr = string.IsNullOrEmpty(Abbr) ? "" : Abbr;
            Manager = string.IsNullOrEmpty(Manager) ? "" : Manager;

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(DepartmentID))
                table.KeysSearch.Add("DepartmentID", DepartmentID);
            if (!string.IsNullOrEmpty(DepartmentName))
                table.KeysSearch.Add("DepartmentName", DepartmentName);
            if (!string.IsNullOrEmpty(Abbr))
                table.KeysSearch.Add("Abbr", Abbr);
            if (!string.IsNullOrEmpty(Manager))
                table.KeysSearch.Add("Manager", Manager);

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            var searchQuery = from d in db.CAT_DEPARTMENTS
                              orderby d.DepartmentName
                              where d.RecordStatus == 1
                              select d;

            var fq1 = searchQuery.ToList().Select(d => new
            {
                d.DepartmentKey,
                d.DepartmentID,
                d.DepartmentName,
                d.Abbr,
                Manager = db.CAT_EMPLOYEES.Find(d.EmployeeKey)?.EmployeeName ?? ""
            }).OrderBy(p => p.DepartmentID).ToList();

            //lọc theo điều kiện
            if (!string.IsNullOrEmpty(DepartmentID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.DepartmentID.Trim().ToLower()).Contains(Utils.RemoveVni(DepartmentID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(DepartmentName))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.DepartmentName.Trim().ToLower()).Contains(Utils.RemoveVni(DepartmentName.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(Abbr))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Abbr.Trim().ToLower()).Contains(Utils.RemoveVni(Abbr.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(Manager))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Manager.Trim().ToLower()).Contains(Utils.RemoveVni(Manager.Trim().ToLower()))).ToList();

            int _index = 1;
            var fq2 = fq1.Select(p => new
            {
                p.DepartmentKey,
                STT = _index++,
                p.DepartmentID,
                p.DepartmentName,
                p.Abbr,
                p.Manager,
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/Edit/" + p.DepartmentKey + "'>Sửa</a>" +
                "<button type=\"button\" class=\"btn btn-danger btn_xoa\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + p.DepartmentKey + "'>Xóa </button>"
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Phòng Ban", IsShow = true, Format = "", FieldName = "DepartmentID", Width = 13, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Phòng Ban", IsShow = true, Format = "", FieldName = "DepartmentName", Width = 34, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Viết Tắt", IsShow = true, Format = "", FieldName = "Abbr", Width = 14, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Trưởng Phòng", IsShow = true, Format = "", FieldName = "Manager", Width = 20, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 12, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Right });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].DepartmentKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
