﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;

namespace AutoReport.Controllers
{
    public class CAT_POSITIONSController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();
        private string m_ControllerName = "CAT_POSITIONS";

        // GET: CAT_POSITIONS/Index
        public ActionResult Index()
        {
            if (!CheckPermission("POSITIONS"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: CAT_POSITIONS/Create
        public ActionResult Create()
        {
            if (!CheckPermission("POSITIONS_ADD"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            CAT_POSITIONS iteminfo = new CAT_POSITIONS();
            iteminfo.PositionID = "(Tự động phát sinh)";
            ViewBag.ControllerName = m_ControllerName;
            return View(iteminfo);
        }

        // POST: CAT_POSITIONS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(CAT_POSITIONS iteminfo)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                result.strMess = CheckBeforeSave(iteminfo, true);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                string prefix = "CV";

                var lastitem = db.CAT_POSITIONS.Where(p => p.PositionID.Contains(prefix))
                    .OrderBy(p => p.PositionID).ToList().LastOrDefault();
                if (lastitem == null)
                    iteminfo.PositionID = prefix + "000001";
                else
                {
                    int lastnum = Convert.ToInt32(lastitem.PositionID.Replace(prefix, ""));
                    int nextnum = lastnum + 1;
                    if (nextnum.ToString().Length > 6)
                        iteminfo.PositionID = prefix + nextnum.ToString();
                    else
                        iteminfo.PositionID = prefix + nextnum.ToString("000000");
                }

                iteminfo.PositionKey = Guid.NewGuid();
                iteminfo.Active = true;
                iteminfo.RecordStatus = 1;
                iteminfo.CreatedBy = MyAccount.UserID;
                iteminfo.ModifiedBy = MyAccount.UserID;
                iteminfo.CreatedOn = DateTime.Now;
                iteminfo.ModifiedOn = DateTime.Now;

                db.CAT_POSITIONS.Add(iteminfo);
                db.SaveChanges();
                result.Data = iteminfo;
                result.strMess = "Đã lưu thông tin";
                CreateLog(m_ControllerName, "Create", iteminfo.PositionKey.ToString(), iteminfo.PositionID, iteminfo.PositionName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        // GET: CAT_POSITIONS/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (!CheckPermission("POSITIONS_EDIT"))
            {
                return RedirectToAction("NotPermission", "Home");
            }

            ViewBag.ControllerName = m_ControllerName;
            CAT_POSITIONS iteminfo = db.CAT_POSITIONS.Find(id);
            if (iteminfo == null)
            {
                return HttpNotFound();
            }
            return View(iteminfo);
        }

        // POST: CAT_POSITIONS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(CAT_POSITIONS iteminfo)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                result.strMess = CheckBeforeSave(iteminfo, false);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                CAT_POSITIONS oldItem = db.CAT_POSITIONS.Find(iteminfo.PositionKey);
                if (oldItem == null)
                    throw new Exception("Không tìm thấy thông tin");
                
                string[] prop_set_list = { "PositionName", "Abbr", "Note" };
                
                foreach (string propname in prop_set_list)
                {
                    PropertyInfo pNew = iteminfo.GetType().GetProperty(propname);
                    if (pNew == null) continue;
                    object pNew_Value = pNew.GetValue(iteminfo);
                    oldItem.GetType().GetProperty(propname).SetValue(oldItem, pNew_Value, null);
                }

                oldItem.ModifiedBy = MyAccount.UserID;
                oldItem.ModifiedOn = DateTime.Now;
                db.Entry(oldItem).State = EntityState.Modified;

                db.SaveChanges();
                result.Data = iteminfo;
                result.strMess = "Đã lưu thông tin";
                CreateLog(m_ControllerName, "Edit", iteminfo.PositionKey.ToString(), iteminfo.PositionID, iteminfo.PositionName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        private string CheckBeforeSave(CAT_POSITIONS iteminfo, bool isNew)
        {
            if (string.IsNullOrEmpty(iteminfo.PositionID))
                return "Mã chức vụ không được để trống";
            if (string.IsNullOrEmpty(iteminfo.PositionName))
                return "Tên chức vụ không được để trống";
            return string.Empty;
        }

        // GET: CAT_POSITIONS/Delete/5
        public ActionResult Delete(Guid? id)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            result.strMess = "Đã xóa thông tin";
            CAT_POSITIONS item = db.CAT_POSITIONS.Find(id);
            try
            {
                if (!CheckPermission("POSITIONS_DELETE"))
                    throw new Exception(NO_PERMISSION);

                if (item == null)
                    throw new Exception("Không tìm thấy thông tin!");

                item.RecordStatus = 0;
                item.ModifiedBy = MyAccount.UserID;
                item.ModifiedOn = DateTime.Now;

                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();

                CreateLog(m_ControllerName, "Delete", item.PositionKey.ToString(), item.PositionID, item.PositionName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult LoadList(int page, int pageSize, string PositionID, string PositionName, string Abbr)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            //Guid _id = new Guid(this.Request["id"]);
            PositionID = string.IsNullOrEmpty(PositionID) ? "" : PositionID;
            PositionName = string.IsNullOrEmpty(PositionName) ? "" : PositionName;
            Abbr = string.IsNullOrEmpty(Abbr) ? "" : Abbr;

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(PositionID))
                table.KeysSearch.Add("PositionID", PositionID);
            if (!string.IsNullOrEmpty(PositionName))
                table.KeysSearch.Add("PositionName", PositionName);
            if (!string.IsNullOrEmpty(Abbr))
                table.KeysSearch.Add("Abbr", Abbr);

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            var searchQuery = from d in db.CAT_POSITIONS
                              orderby d.PositionName
                              where d.RecordStatus == 1
                              select d;

            var fq1 = searchQuery.ToList().Select(d => new
            {
                d.PositionKey,
                d.PositionID,
                d.PositionName,
                d.Abbr
            }).OrderBy(p => p.PositionID).ToList();

            //lọc theo điều kiện
            if(!string.IsNullOrEmpty(PositionID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.PositionID.Trim().ToLower()).Contains(Utils.RemoveVni(PositionID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(PositionName))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.PositionName.Trim().ToLower()).Contains(Utils.RemoveVni(PositionName.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(Abbr))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Abbr.Trim().ToLower()).Contains(Utils.RemoveVni(Abbr.Trim().ToLower()))).ToList();

            int _index = 1;
            var fq2 = fq1.Select(p => new
            {
                p.PositionKey,
                STT = _index++,
                p.PositionID,
                p.PositionName,
                p.Abbr,
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/Edit/" + p.PositionKey + "'>Sửa</a>" +
                "<button type=\"button\" class=\"btn btn-danger btn_xoa\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + p.PositionKey + "'>Xóa </button>"
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Chức Vụ", IsShow = true, Format = "", FieldName = "PositionID", Width = 18, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Chức Vụ", IsShow = true, Format = "", FieldName = "PositionName", Width = 50, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Viết Tắt", IsShow = true, Format = "", FieldName = "Abbr", Width = 13, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 12, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Right });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].PositionKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
