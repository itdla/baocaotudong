﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Utils
{
    public class HsonTable
    {
        public HsonTable()
        {
            Source = new List<object>();
        }

        public string[] HeadersCaption { get; set; }

        public int[] HeadersWidth { get; set; }

        //list index of columns hidden
        public int[] ColumnsHidden { get; set; }

        public List<object> Source { get; set; }
    }
}