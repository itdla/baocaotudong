﻿function UpdateMask() {
    $(".int").inputmask("integer", { radixPoint: ",", autoGroup: true, groupSeparator: ".", groupSize: 3 });
    //$(".int").format({ format: "#.###", locale: "vi-VN" });
    $(".dec").inputmask("decimal", { radixPoint: ",", autoGroup: true, groupSeparator: ".", groupSize: 3 });

    $(".date").inputmask("mask", "'99/99/9999'");// { radixPoint: ",", autoGroup: true, groupSeparator: ".", groupSize: 3 });

    autosize($('textarea.auto-growth'));

    //CKEDITOR.replace('.rich');
    //CKEDITOR.config.height = 300;

    //Datetimepicker plugin
    $('.datetime').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY HH:mm',
        clearButton: true,
        weekStart: 1
    });
    // $('#ReportStatus').iCheck();
    $('input:checkbox').iCheck({ checkboxClass: 'icheckbox_flat-green', radioClass: 'iradio_flat-green' });

    //$('.date').bootstrapMaterialDatePicker({
    //    format: 'DD/MM/YYYY',
    //    clearButton: true,
    //    weekStart: 1,
    //    time: false
    //});

    $('.time').bootstrapMaterialDatePicker({
        format: 'HH:mm',
        clearButton: true,
        date: false
    });
    $('select').selectpicker({

    });

}
function LoadTable(divid, url, page) {
    if ($("#" + divid).length == 0) {
        return;
    }

    var search = $("#form_search").serialize();
    var pagesize = 20;
    if (page == null) {
        if ($("#" + divid).attr('data-page')) {
            page = $("#" + divid).attr('data-page');
        } else {
            page = 1;
        }
    }

    var htmlLoading = 'Đang tải dữ liệu....';//'<div class="preloader" style="margin-left: 50%"> <div class="spinner-layer pl-green"> <div class="circle-clipper left"> <div class="circle"></div> </div> <div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>';

    $("#" + divid).html(htmlLoading);
    var session = $("#DLA_Session").val();
    //$("#" + divid).LoadingOverlay("show");

    if (url.indexOf("?") == -1) {
        url += '?';
    } else {
        //url += '&';
    }

    $.post(url + '&page=' + page + '&pagesize=' + pagesize + (search == null ? '' : '&' + search) + '&session=' + (session == null ? "" : session), function (data) {
        //$(".result").html(data);
        if (data.intStatus != 1) {
            showError(data.strMess);
            $("#" + divid).html(data.strMess);
            $("#" + divid).LoadingOverlay("hide");
        } else {
            var contents = "";

            contents = '<table  style="line-height: 1.4;"  class="col-sm-12 table table-hover table-striped bulk_action" id="no-more-tables" width="100%">';
            if (data.lstHeader.length > 0) {
                contents += '<thead><tr>';
                for (var i = 0; i < data.lstHeader.length; i++) {
                    var header = data.lstHeader[i];
                    if (header.IsShow) {
                        contents += '<th align="center" ' + (header.Width == 0 ? '' : 'width="' + header.Width + header.Width_Measure + '"') + '>' + header.Caption + '</th>';
                    }
                }

                if (data.isShowAction) {
                    contents += '<th width="100" style="float: right;">Thao tác</th>';
                }

                contents += '</tr></thead>';
            }

            contents += '<tbody>';
            if (data.EnableSearch) {
                contents += '<tr>';
                var manual_index = 1;

                var url_base = "";
                var sp = url.indexOf('?');
                if (sp > -1) {
                    url_base = url.substring(0, sp);
                }

                for (var i = 0; i < data.lstHeader.length; i++) {
                    if (data.lstHeader[i].IsShow) {
                        contents += '<td ' + (data.lstHeader[i].Width == 0 ? '' : 'width="' + data.lstHeader[i].Width + '"') + '>';
                        var code = '';
                        if (data.lstHeader[i].FieldName == "") {
                            code = '<input readonly type="text" class="seachmanual seachmanual_' + manual_index + '" style="width:100%" onkeypress="return SearchTable(event, ' + divid + ', \'' + url_base + '\')" />';
                        }
                        else {
                            var text_search = '';
                            if (data.KeysSearch != null)
                                text_search = data.KeysSearch[data.lstHeader[i].FieldName];
                            code = '<input id="searchbox_' + data.lstHeader[i].FieldName + '" type="text" class="seachmanual seachmanual_' + manual_index + '" style="width:100%" onkeypress="return SearchTable(event, ' + divid + ', \'' + url_base + '\')" ' + (text_search == null || text_search == "" ? "" : "value=\"" + text_search + "\"") + '/>';
                        }
                        contents += code;
                        contents += '</td>';
                        manual_index++;
                    }

                }
                contents += '<td></td>';
                contents += '</tr>';
            }

            if (data.lstData.length == 0) {
                var column = data.lstHeader.length;
                if (data.isShowAction) {
                    column++;
                }

                contents += '<tr><td align="center" colspan="' + column + '"><h5>Không có dữ liệu</h5></td></tr>';
            } else {

                for (var i = 0; i < data.lstData.length; i++) {
                    contents += '<tr>';
                    var objData = data.lstData[i];
                    var col = 0;
                    $.each(objData.objData, function (index, value) {
                        var header = data.lstHeader[col];
                        col++;
                        if (header.IsShow) {
                            var data_align = "left";
                            if (header.ColumnAlignment == 1)
                                data_align = "left";
                            else if (header.ColumnAlignment == 2)
                                data_align = "center";
                            else if (header.ColumnAlignment == 3)
                                data_align = "right";

                            if (header.Format != '') {
                                if (header.Type == 2) {
                                    //Date
                                    //var date = new Date(value);
                                    if (value != null) {
                                        contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + moment(value).format(header.Format) + '</td>';
                                    } else {
                                        contents += '<td align="' + data_align + '" data-title="' + header.Caption + '"> &nbsp;</td>';
                                    }

                                } else if (header.Type == 3) {

                                    //Time
                                    if (value == null) {
                                        contents += '<td align="' + data_align + '" data-title="' + header.Caption + '"> &nbsp; </td>';
                                    } else {
                                        var time = new Date('2016/11/2 ' + value);
                                        contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + moment(time).format(header.Format) + '</td>';
                                    }
                                } else if (header.Type == 4) {
                                    //Boolean
                                    var split = header.Format.split("/");
                                    if (value == null) {
                                        contents += '<td align="' + data_align + '" data-title="' + header.Caption + '"> &nbsp; </td>';
                                    } else {
                                        contents += '<td align="' + data_align + '" data-title="' + header.Caption + '"> ' + (value ? split[0] : split[1]) + ' </td>';
                                    }
                                } else if (header.Type == 5) {
                                    //Integer
                                    contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")) + '</td>';
                                } else if (header.Type == 6) {
                                    //Decimal
                                    contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';
                                } else if (header.Type == 7) {
                                    //Images
                                    contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + (value == null ? '<img  width="50"  src="/Sys_File/ImageView/00000000-0000-0000-0000-000000000000"/>' : '<img width="70" src="/Sys_File/ImageView/' + value + '"/>') + '</td>';
                                }
                                else if (header.Type == 98) {
                                    if (header.DataArray[value]) {
                                        value = header.DataArray[value];
                                    } else {
                                        value = null;
                                    }
                                    contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';
                                } else {
                                    contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';
                                }
                            } else {
                                if (header.Type == 5) {
                                    //Integer
                                    contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")) + '</td>';
                                } else if (header.Type == 6) {
                                    //Decimal
                                    contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';
                                } else if (header.Type == 7) {
                                    //Images
                                    contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + (value == null ? '<img width="70"  src="/Sys_File/ImageView/00000000-0000-0000-0000-000000000000"/>' : '<img width="70" src="/Sys_File/ImageView/' + value + '"/>') + '</td>';
                                }
                                else if (header.Type == 98) {
                                    if (header.DataArray[value]) {
                                        value = header.DataArray[value];
                                    } else {
                                        value = null;
                                    }
                                    contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';

                                } else {
                                    contents += '<td align="' + data_align + '" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';
                                }

                            }
                        }
                    });

                    if (data.isShowAction) {
                        contents += '<td style="padding: 5px; float: right;">';

                        if (data.LstExColumn.length > 0) {
                            for (var j = 0; j < objData.ExButtonShow.length; j++) {
                                var button = data.LstExColumn[j];
                                if (objData.ExButtonShow[j]) {
                                    contents += '&nbsp;<a title="' + button.Title + '" data-id="' + objData.ID + '"class="btn _' + button.ID + ' bg-' + button.Color + '  waves-effect waves-circle waves-float" ' + (button.strURL == null ? "" : 'href="' + button.strURL + '/' + objData.ID + '"') + '><i class="material-icons">' + button.ICon + '</i></a>';
                                }
                            }
                        }


                        if (objData.isShowDetail) {
                            contents += '&nbsp;<a style="width: 55px;  padding: 0px;" data-id="' + objData.ID + '"class="btn ' + divid + '_btnDetail bg-orange  waves-effect waves-circle waves-float" ' + (data.strURLDetail == null ? "" : 'href="' + data.strURLDetail + '/' + objData.ID + (objData.ExURLDetail == null ? '' : '&' + objData.ExURLDetail) + '"') + '>Chi tiết</a>';
                        }
                        if (objData.isShowEdit) {
                            if (objData.IsEditModal) {
                                contents += '&nbsp;<a style="width: 35px; padding: 0px;"  data-id="' + objData.ID + '" href="javascript:ModalURL(\'' + data.strURLEdit + '/' + objData.ID + '?&Session=' + data.Session + '&' + objData.ExURLEdit + '\')" class="btn ' + divid + '_btnEdit bg-blue waves-effect waves-circle waves-float" >Sửa</a>';
                            } else {
                                contents += '&nbsp;<a style="width: 35px; padding: 0px;"  data-id="' + objData.ID + '"class="btn ' + divid + '_btnEdit bg-blue  waves-effect waves-circle waves-float"   ' + (data.strURLEdit == null ? "" : 'href="' + data.strURLEdit + '/' + objData.ID + '?&Session=' + data.Session + '&' + objData.ExURLEdit + '"') + '>Sửa</a>';
                            }

                        }
                        if (objData.isShowDelete) {
                            contents += '&nbsp;<a style="width: 30px; padding: 0px;" data-id="' + objData.ID + '" href="javascript:Delete(\'' + data.strURLDelete + '\',\'' + objData.ID + '\', \'' + divid + '\',\'' + url + '\',null)"  class="btn bg-red  waves-effect waves-circle waves-float" data-url="' + data.strURLEdit + '" data-id="' + objData.ID + '">Xóa</a>';
                        }

                        contents += '</td>';
                    }

                    contents += '</tr>';
                }
            }

            contents += '</tbody>';
            contents += '</table>';

            if (data.intTotalPage > 1) {
                contents += '<nav><ul class="pagination pull-right" style="margin-right: 20px;">';

                var start = data.intCurrentPage - 5 > 0 ? data.intCurrentPage - 5 : 1;

                for (var i = start; i < data.intCurrentPage; i++) {
                    contents += '<li><a href="javascript:LoadTable(\'' + divid + '\',\'' + url + '\',\'' + i + '\');" class="waves-effect"> ' + i + ' </a></li>';
                }
                contents += '<li><a class="waves-effect"> <b>' + data.intCurrentPage + '</b> </a></li>';
                for (var i = data.intCurrentPage + 1; i < data.intCurrentPage + 5 && i <= data.intTotalPage; i++) {
                    contents += '<li><a href="javascript:LoadTable(\'' + divid + '\',\'' + url + '\',\'' + i + '\');" class="waves-effect"> ' + i + ' </a></li>';
                }
                contents += '</ul></nav>';
            }

            $("#" + divid).html(contents);

            $("#" + divid).attr('data-page', data.intCurrentPage);
        }
    });
}

function SearchTable(e, divid, url_base) {
    if (e.keyCode != 13)
        return;

    var searchquery = "";
    var table_div = divid.childNodes[0];
    var table_tbody = table_div.childNodes[1].childNodes[0];//<tr> search row, see debug
    //return;
    for (var i = 0; i < table_tbody.childNodes.length; i++) {
        if (table_tbody.childNodes[i].childNodes.length == 0)
            continue;
        var ctrlid = table_tbody.childNodes[i].childNodes[0].id;
        if (ctrlid != "") {
            if (searchquery == "")
                searchquery += "?" + ctrlid.replace("searchbox_", "") + "=" + table_tbody.childNodes[i].childNodes[0].value;
            else
                searchquery += "&" + ctrlid.replace("searchbox_", "") + "=" + table_tbody.childNodes[i].childNodes[0].value;
        }
    }

    if (searchquery == "")
        return;
    LoadTable(divid.id, url_base + searchquery, null);

}
function LoadTableDetail(divid, url, page, showaction) {
    var search = null;
    search = $("#form_search").serialize();
    var pagesize = 25;
    if (page == null) {
        if ($("#" + divid).attr('data-page')) {
            page = $("#" + divid).attr('data-page');
        } else {
            page = 1;
        }
    }

    var htmlLoading = '<div class="preloader" style="margin-left: 50%"> <div class="spinner-layer pl-green"> <div class="circle-clipper left"> <div class="circle"></div> </div> <div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>';

    $("#" + divid).html(htmlLoading);
    var session = $("#HHT_Session").val();
    //$("#" + divid).LoadingOverlay("show");

    //var urldata = 'page=' + page + '&pagesize=' + pagesize + (search == null ? '' : '&' + search) + '&session=' + (session == null ? "" : session);

    $.post(url + '&page=' + page + '&pagesize=' + pagesize, function (data) {
        //$(".result").html(data);
        if (data.intStatus != 1) {
            showError(data.strMess);
            $("#" + divid).html(data.strMess);
            $("#" + divid).LoadingOverlay("hide");
        } else {
            var contents = '<table  style="line-height: 1.4;"  class="col-sm-12 table-hover table-striped table-condensed" id="no-more-tables" width="100%">';
            if (data.lstHeader.length > 0) {
                contents += '<thead><tr>';
                for (var i = 0; i < data.lstHeader.length; i++) {
                    var header = data.lstHeader[i];
                    if (header.IsShow) {
                        contents += '<th align="center">' + header.Caption + '</th>';
                    }
                }

                if (data.isShowAction && showaction) {
                    contents += '<th width="150">Thao tác</th>';
                }

                contents += '</tr></thead>';
            }
            contents += '<tbody>';
            if (data.lstData.length == 0) {
                var column = data.lstHeader.length;
                if (data.isShowAction && showaction) {
                    column++;
                }

                contents += '<tr><td align="center" colspan="' + column + '"><h5>Không có dữ liệu</h5></td></tr>';
            } else {


                for (var i = 0; i < data.lstData.length; i++) {
                    contents += '<tr>';
                    var objData = data.lstData[i];
                    var col = 0;
                    $.each(objData.objData, function (index, value) {
                        var header = data.lstHeader[col];
                        col++;
                        if (header.IsShow) {
                            if (header.Format != '') {
                                if (header.Type == 2) {
                                    //Date
                                    //var date = new Date(value);
                                    if (value != null) {
                                        contents += '<td align="left" data-title="' + header.Caption + '">' + moment(value).format(header.Format) + '</td>';
                                    } else {
                                        contents += '<td align="left" data-title="' + header.Caption + '"> &nbsp;</td>';
                                    }

                                } else if (header.Type == 3) {

                                    //Time
                                    if (value == null) {
                                        contents += '<td data-title="' + header.Caption + '"> &nbsp; </td>';
                                    } else {
                                        var time = new Date('2016/11/2 ' + value);
                                        contents += '<td data-title="' + header.Caption + '">' + moment(time).format(header.Format) + '</td>';
                                    }
                                } else if (header.Type == 4) {
                                    //Boolean
                                    var split = header.Format.split("/");
                                    if (value == null) {
                                        contents += '<td align="center" data-title="' + header.Caption + '"> &nbsp; </td>';
                                    } else {
                                        contents += '<td align="center" data-title="' + header.Caption + '"> ' + (value ? split[0] : split[1]) + ' </td>';
                                    }
                                } else if (header.Type == 5) {
                                    //Integer
                                    contents += '<td align="right" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';
                                } else if (header.Type == 6) {
                                    //Decimal
                                    contents += '<td align="right" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';
                                } else if (header.Type == 7) {
                                    //Images
                                    contents += '<td data-title="' + header.Caption + '">' + (value == null ? '<img  width="50"  src="/Sys_File/ImageView/00000000-0000-0000-0000-000000000000"/>' : '<img width="70" src="/Sys_File/ImageView/' + value + '"/>') + '</td>';
                                } else if (header.Type == 8) {
                                    contents += '<td data-title="' + header.Caption + '">' + (value != null ? header.Format.replace("#VALUE#", value) : '') + '</td>';
                                } else {
                                    contents += '<td data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';
                                }
                            } else {
                                if (header.Type == 5) {
                                    //Integer
                                    contents += '<td align="right" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';
                                } else if (header.Type == 6) {
                                    //Decimal
                                    contents += '<td align="right" data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';
                                } else if (header.Type == 7) {
                                    //Images
                                    contents += '<td  data-title="' + header.Caption + '">' + (value == null ? '<img width="70"  src="/Sys_File/ImageView/00000000-0000-0000-0000-000000000000"/>' : '<img width="70" src="/Sys_File/ImageView/' + value + '"/>') + '</td>';
                                } else {
                                    contents += '<td data-title="' + header.Caption + '">' + (value == null ? '&nbsp;' : value) + '</td>';
                                }

                            }
                        }
                    });

                    if (data.isShowAction && showaction) {
                        contents += '<td style="padding: 5px;">';

                        if (data.LstExColumn.length > 0) {
                            for (var j = 0; j < objData.ExButtonShow.length; j++) {
                                var button = data.LstExColumn[j];
                                if (objData.ExButtonShow[j]) {
                                    contents += '&nbsp;<a target="' + button.target + '" title="' + button.Title + '" data-id="' + objData.ID + '"class="btn _' + button.ID + ' bg-' + button.Color + ' btn-circle waves-effect waves-circle waves-float" ' + (button.strURL == null ? "" : 'href="' + button.strURL + objData.ID + '"') + '><i class="material-icons">' + button.ICon + '</i></a>';
                                }
                            }
                        }



                        if (objData.isShowEdit) {
                            contents += '&nbsp;<a  data-id="' + objData.ID + '" class="btn ' + divid + '_btnEdit btn bg-green btn-circle waves-effect waves-circle waves-float" ><i class="material-icons">edit</i></a>';
                        }
                        if (objData.isShowDelete) {
                            contents += '&nbsp;<a data-id="' + objData.ID + '"   class="btn ' + divid + '_btnDelete btn bg-red btn-circle waves-effect waves-circle waves-float"  data-id="' + objData.ID + '"><i class="material-icons">delete</i></a>';
                        }

                        contents += '</td>';
                    }

                    contents += '</tr>';
                }
            }
            contents += '</tbody></table>';

            if (data.intTotalPage > 1) {
                contents += '<nav> <ul class="pagination pull-right" style="margin-right: 20px;">';

                var start = data.intCurrentPage - 5 > 0 ? data.intCurrentPage - 5 : 1;

                for (var i = start; i < data.intCurrentPage; i++) {
                    contents += '<li><a href="javascript:LoadTable(\'' + divid + '\',\'' + url + '\',\'' + i + '\');" class="waves-effect"> ' + i + ' </a></li>';
                }
                contents += '<li><a class="waves-effect"> <b>' + data.intCurrentPage + '</b> </a></li>';
                for (var i = data.intCurrentPage + 1; i < data.intCurrentPage + 5 && i <= data.intTotalPage; i++) {
                    contents += '<li><a href="javascript:LoadTable(\'' + divid + '\',\'' + url + '\',\'' + i + '\');" class="waves-effect"> ' + i + ' </a></li>';
                }
                contents += '</ul></nav>';
            }

            $("#" + divid).html(contents);
            $("#" + divid).attr('data-page', data.intCurrentPage);

            //$("#" + divid).on("click",".btnModal", function () {
            //    ModalURL($(this).attr('data-url'));
            //});


            //$("#" + divid).LoadingOverlay("hide");
        }
    });
}
function showError(mess) {
    if (mess == '') {
        return;
    }
    var placementFrom = 'bottom';
    var placementAlign = 'right';
    var animateEnter = '';
    var animateExit = '';
    var colorName = 'bg-red';
    showNotification(colorName, mess, placementFrom, placementAlign, animateEnter, animateExit);
}
function showSuccess(mess) {
    if (mess == '') {
        return;
    }
    var placementFrom = 'bottom';
    var placementAlign = 'right';
    var animateEnter = '';
    var animateExit = '';
    var colorName = 'bg-green';
    showNotification(colorName, mess, placementFrom, placementAlign, animateEnter, animateExit);
}
function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
        message: text
    },
        {
            type: colorName,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timer: 1000,
            placement: {
                from: placementFrom,
                align: placementAlign
            },
            animate: {
                enter: animateEnter,
                exit: animateExit
            },
            template: '<div  data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
}

function showLoading() {
    $.LoadingOverlay("show");
}

function hideLoading() {
    $.LoadingOverlay("hide");
}
function ModalURL(url) {
    showLoading();
    $.get(url, function (data) {
        $("#ajaxModal").html(data);
        UpdateMask();
        $("#defaultModal").modal();
        hideLoading();
    });
}

function Delete(url, id, divid, urlx, page) {
    //showLoading();
    swal({
        title: "Bạn có chắc chắn không?",
        text: "Thao tác này không thể phục hồi lại được!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Vâng, xóa nó!",
        cancelButtonText: "Không!",
        closeOnConfirm: false
    }, function () {
        var session = $("#HHT_Session").val();
        $.post(url + '/' + id, function (data) {
            if (data.intStatus == 1) {
                LoadTable(divid, urlx, null);
                //hideLoading();
                swal("Đã xóa!", "Dữ liệu đã được xóa.", "success");
            } else {
                swal("Thất bại!", data.Messeger, "error");
            }
        });
        //swal("Deleted!", "Your imaginary file has been deleted.", "success");
    });

}

function swal_info(msg, confirm, delay) {
    if (delay > 0) {
        swal({
            position: 'center',
            type: 'info',
            title: msg,
            showConfirmButton: confirm,
            timer: delay
        });
    }
    else {
        swal({
            position: 'center',
            type: 'info',
            title: msg,
            showConfirmButton: confirm
        });
    }
    
}

function swal_success(msg, confirm, delay) {
    if (delay > 0) {
        swal({
            position: 'center',
            type: 'success',
            title: msg,
            showConfirmButton: confirm,
            timer: delay
        });
    }
    else {
        swal({
            position: 'center',
            type: 'success',
            title: msg,
            showConfirmButton: confirm
        });
    }
}

function swal_error(msg, confirm, delay) {
    if (delay > 0) {
        swal({
            position: 'center',
            type: 'error',
            title: msg,
            showConfirmButton: confirm,
            timer: delay
        });
    }
    else {
        swal({
            position: 'center',
            type: 'error',
            title: msg,
            showConfirmButton: confirm
        });
    }
}