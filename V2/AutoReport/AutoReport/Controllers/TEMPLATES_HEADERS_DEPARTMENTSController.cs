﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using System.Transactions;
using AutoReport.Utils;
using System.Reflection;
using System.Text;

namespace AutoReport.Controllers
{
    public class TEMPLATES_HEADERS_DEPARTMENTSController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "TEMPLATES_HEADERS_DEPARTMENTS";

        // GET: TEMPLATES_HEADERS_DEPARTMENTS/Index
        public ActionResult Index()
        {
            if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: TEMPLATES_HEADERS_DEPARTMENTS/Create
        public ActionResult Create(int? id)
        {
            if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                return B_NotPermission();

            CAT_DEPARTMENTS itemdepartment = db.CAT_DEPARTMENTS.Find(id);
            ViewBag.ControllerName = m_ControllerName;
            ViewBag.ItemDepartment = itemdepartment;
            return View();
        }

        [HttpPost]
        public ActionResult Create(TEMPLATES_HEADERS_DEPARTMENTS ItemModel, string SaveType)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Status = 1;
                result.Result_Message = CREATE_SUCCESS;
                if (SaveType == "savenew")
                    result.RedirectTo = string.Format("/{0}/Create", m_ControllerName);
                else
                    result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    null,
                    null
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        // GET: TEMPLATES_HEADERS_DEPARTMENTS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                return B_NotPermission();

            CAT_DEPARTMENTS itemdepartment = db.CAT_DEPARTMENTS.Find(id);
            if (itemdepartment == null)
                return B_NotFound();

            ViewBag.ControllerName = m_ControllerName;
            ViewBag.ItemDepartment = itemdepartment;
            return View();
        }

        [HttpPost]
        public ActionResult Edit(int? DepartmentKey, List<string[]> lstTemplates, List<string> lstDelKeys)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                int sorted = 0;
                foreach (string[] arr in lstTemplates)
                {
                    if (arr.Where(p => string.IsNullOrEmpty(p)).Count() == arr.Length)
                        continue;
                    sorted++;
                    int detail_key = Convert.ToInt32(arr[0]);
                    if (detail_key == 0)
                    {
                        //add & delete only, no edit
                        TEMPLATES_HEADERS_DEPARTMENTS item = new TEMPLATES_HEADERS_DEPARTMENTS();
                        item.DepartmentKey = DepartmentKey;
                        item.TemplateHeaderKey = Convert.ToInt32(arr[2]);
                        item.Sorted = sorted;
                        item.CreatedBy = MyAccount.Model.dKey.ToString();
                        item.ModifiedBy = MyAccount.Model.dKey.ToString();
                        item.CreatedOn = Now;
                        item.ModifiedOn = Now;
                        db.TEMPLATES_HEADERS_DEPARTMENTS.Add(item);
                    }
                    else
                    {
                        sorted = Convert.ToInt32(arr[3]);
                    }
                }

                db.SaveChanges();
                result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, DepartmentKey);
                result.Result_Message = UPDATE_SUCCESS;
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    null,
                    null,
                    null
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        // GET: TEMPLATES_HEADERS_DEPARTMENTS/Delete/5
        public ActionResult Delete(int? id)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_DELETE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                var lst_templates = db.TEMPLATES_HEADERS_DEPARTMENTS.Where(p => p.DepartmentKey == id).ToList();
                if (lst_templates.Count > 0)
                {
                    lst_templates.ForEach(item => {
                        db.Entry(item).State = EntityState.Deleted;
                    });
                    db.SaveChanges();
                }

                result.RedirectTo = string.Format("/{0}/Index", m_ControllerName);
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetList(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                var lstItems = (from d in db.CAT_DEPARTMENTS
                                orderby d.ItemName
                                select new
                                {
                                    d.dKey,
                                    d.ItemName
                                }).ToList();

                if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                    lstItems.Clear();

                TableModel table = new TableModel();
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemName", Caption = "Tên Phòng Ban", Width = 70, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "TemplateCount", Caption = "Tổng", Width = 20 });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemName,
                    TemplateCount = string.Format("{0} báo cáo", db.TEMPLATES_HEADERS_DEPARTMENTS.Where(t => t.DepartmentKey == p.dKey).Count())
                }).ToList();

                #region manual filter

                string key;
                string filter_expr;
                foreach (TableHeaderColumns tcol in table.HeaderColumns.Where(p => p.Filtered).ToList())
                {
                    key = tcol.FieldName;
                    filter_expr = Request.QueryString[key];

                    if (!string.IsNullOrEmpty(filter_expr))
                    {
                        filter_expr = TextUtil.RemoveVni(Convert.ToString(filter_expr)).ToLower();

                        q1 = q1.Where(p => TextUtil.RemoveVni(Convert.ToString(p.GetType().GetProperty(key).GetValue(p, null)).ToLower()).Contains(filter_expr)).ToList();

                        table.FilterValues.Add(key, filter_expr ?? "");
                    }
                }

                #endregion

                table.CalcTotalPage(q1.Count);
                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetTemplates(int? DepartmentKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "Key", "State", "TemplateHeaderKey", "Sorted", "Báo cáo" };
                table.ColumnsHidden = new int[] { 0, 1, 2, 3 };

                //fields use in afterChange event
                var query = (from c in db.TEMPLATES_HEADERS_DEPARTMENTS
                             where c.DepartmentKey == DepartmentKey
                             select new
                             {
                                 c.dKey,
                                 c.TemplateHeaderKey,
                                 c.Sorted
                             }).ToList();

                var data = query.Select(p => new
                {
                    p.dKey,
                    State = "",
                    p.TemplateHeaderKey,
                    p.Sorted,
                    TemplateName = db.TEMPLATES_HEADERS.Find(p.TemplateHeaderKey)?.ItemName
                }).ToList();

                if (data.Count > 0)
                {
                    data.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "", "", "" });
                }
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult LoadListHeaderTemplateSelection(int? DepartmentKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                var query_headers = (from d in db.TEMPLATES_HEADERS
                                     orderby d.ItemName
                                     select new
                                     {
                                         d.dKey,
                                         d.ItemCode,
                                         d.ItemName
                                     }).ToList();

                var query_header_tables = (from d in db.TEMPLATES_HEADERS_DEPARTMENTS
                                           where d.DepartmentKey == DepartmentKey
                                           select d).ToList();

                StringBuilder builder = new StringBuilder();
                string nl = Environment.NewLine;

                builder.Append("<div class=\"col-md-12\">");
                foreach (var hdr in query_headers)
                {
                    string setchecked = query_header_tables.Where(p => p.TemplateHeaderKey == hdr.dKey).ToList().Count > 0 ? "checked" : "";

                    builder.Append("<div class=\"row clearfix\">");
                    builder.Append(string.Format("<input type=\"checkbox\" data-key=\"{0}\" data-id=\"{1}\" data-name=\"{2}\" class=\"fancytree-checkbox\" {3}/>",
                        hdr.dKey, hdr.ItemCode, hdr.ItemName, setchecked) + nl);

                    builder.Append(string.Format("<span class=\"fancytree-title\">{0}</span>", hdr.ItemName) + nl);

                    builder.Append("</div>");
                }
                builder.Append("</div>");

                result.Result_Status = 1;
                result.Data = builder.ToString();
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
