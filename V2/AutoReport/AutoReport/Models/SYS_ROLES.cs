//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutoReport.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SYS_ROLES
    {
        public int dKey { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public Nullable<int> ParentKey { get; set; }
        public Nullable<int> Sorted { get; set; }
        public Nullable<int> LanguageKey { get; set; }
        public Nullable<bool> ShowOnMenu { get; set; }
        public string UrlReference { get; set; }
        public string FaIcon { get; set; }
        public string RegularIcon { get; set; }
    }
}
