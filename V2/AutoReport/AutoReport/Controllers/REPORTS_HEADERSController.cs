﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using System.Transactions;
using AutoReport.Utils;
using System.Reflection;
using System.Text;

namespace AutoReport.Controllers
{
    public class REPORTS_HEADERSController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "REPORTS_HEADERS";

        // GET: REPORTS_HEADERS/Index
        public ActionResult Index()
        {
            if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult AssignIndex()
        {
            if (!GrantAccess(string.Format("{0}_ASSIGN", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult AssignEditTables(int? id)
        {
            REPORTS_HEADERS item = db.REPORTS_HEADERS.Find(id);
            if (item == null)
                return B_NotFound();
            if (item.RecordStatus == 0)
                return B_NotFound();
            //danh sách phụ lục theo mẫu bc
            var lst_tables_template = db.TEMPLATES_HEADERS_TABLES.Where(p => p.TemplateHeaderKey == item.TemplateKey).ToList();
            var lst_tables_assigned = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == id).ToList();

            var state_count = lst_tables_assigned.Where(p => p.InputState == 0).Select(p => p.TemplateTableKey).Distinct().Count();
            ViewBag.State = 0;
            if (state_count > 0)
            {
                //vẫn còn phụ lục chưa được phân công
                ViewBag.State = 0;
            }
            else
            {
                state_count = lst_tables_assigned.Where(p => p.InputState == 1).Select(p => p.TemplateTableKey).Distinct().Count();
                if(state_count == lst_tables_template.Count)
                {
                    //phụ lục đã được phân công cho nhân sự, nhưng state chưa kết thúc phân công: hiện nút hoàn thành phân công
                    ViewBag.State = 1;
                }
                else
                {
                    //đã hoàn thành phân công
                    state_count = lst_tables_assigned.Where(p => p.InputState >= 2).Select(p => p.TemplateTableKey).Distinct().Count();
                    if (state_count == lst_tables_template.Count)
                        ViewBag.State = 2;
                }
                
            }
            ViewBag.ControllerName = m_ControllerName;
            return View(item);
        }

        public ActionResult AssignEditValues(int? id)
        {
            REPORTS_HEADERS item = db.REPORTS_HEADERS.Find(id);
            if (item == null)
                return B_NotFound();
            if (item.RecordStatus == 0)
                return B_NotFound();

            var lst_report_values = db.REPORTS_VALUECONST_VALUES.Where(p => p.ReportHeaderKey == id).ToList();
            ViewBag.State = 0;
            int state_count = lst_report_values.Where(p => p.InputState == 0).Count();
            if(state_count > 0)
            {
                ViewBag.State = 0;
            }
            else
            {
                state_count = lst_report_values.Where(p => p.InputState == 1).Count();
                if(state_count == lst_report_values.Count)
                {
                    ViewBag.State = 1;
                }
                else
                {
                    ViewBag.State = 2;
                }
            }

            int _index = 1;
            var query_emp = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).OrderBy(p => p.ItemName).ToList()
                .Select(p => new
                {
                    p.dKey,
                    ItemName = (_index++).ToString() + ". " + p.ItemName
                }).ToList();

            ViewBag.EmployeeDropdownList = new SelectList(query_emp, "dKey", "ItemName");
            ViewBag.EmployeeKeyList = query_emp.Select(p => p.dKey).ToArray();
            ViewBag.EmployeeNameList = query_emp.Select(p => p.ItemName).ToArray();
            ViewBag.ControllerName = m_ControllerName;
            return View(item);
        }

        public ActionResult AssignLogIndex()
        {
            if (!GrantAccess(string.Format("{0}_ASSIGN", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: REPORTS_HEADERS/Create
        public ActionResult Create()
        {
            if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;

            #region query data

            //giới hạn số báo cáo, theo nhân viên của phòng ban
            var lstRestricted = GetListTemplateRestricted().Select(p => p.TemplateHeaderKey).ToList();

            var query_templates = (from t in db.TEMPLATES_HEADERS
                                   where t.RecordStatus == 1 && lstRestricted.Contains(t.dKey)
                                   select new { t.dKey, t.ItemName }).ToList();


            #endregion

            REPORTS_HEADERS item = new REPORTS_HEADERS()
            {
                Sorted = db.REPORTS_HEADERS.Count() + 1,
                RptYear = Now.Year,
                RptMonth = Now.Month,
                DateStart = Now
            };

            ViewBag.TemplateList = new SelectList(query_templates, "dKey", "ItemName");

            return View(item);
        }

        [HttpPost]
        public ActionResult Create(REPORTS_HEADERS ItemModel, List<string[]> TemplateTables, List<string[]> TemplateValues, string SaveType)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, true);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);

                using (TransactionScope scope = new TransactionScope())
                {
                    #region save Model

                    ItemModel.CreatedBy = MyAccount.Model.dKey.ToString();
                    ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                    ItemModel.CreatedOn = Now;
                    ItemModel.ModifiedOn = Now;
                    ItemModel.RecordStatus = 1;
                    ItemModel.ApproveStatus = 0;
                    db.REPORTS_HEADERS.Add(ItemModel);
                    db.SaveChanges();

                    #endregion

                    #region save tables

                    bool _hasRows = false;
                    //foreach (string[] arr in TemplateTables)
                    //{
                    //    int tblkey = Convert.ToInt32(arr[2]);// see TemplateEditValueChanged_Tables
                    //    REPORTS_TABLES_ASSIGNMENT tblassign = new REPORTS_TABLES_ASSIGNMENT()
                    //    {
                    //        ReportHeaderKey = ItemModel.dKey,
                    //        TemplateTableKey = tblkey,
                    //        InputState = 0,
                    //        CreatedOn = ItemModel.CreatedOn,
                    //        CreatedBy = ItemModel.CreatedBy,
                    //        ModifiedOn = ItemModel.ModifiedOn,
                    //        ModifiedBy = ItemModel.ModifiedBy
                    //    };
                    //    db.REPORTS_TABLES_ASSIGNMENT.Add(tblassign);
                    //}

                    #endregion

                    #region save values

                    foreach (string[] arr in TemplateValues)
                    {
                        if (arr.Where(p => string.IsNullOrEmpty(p)).Count() == arr.Length)
                            continue;

                        int valuekey = Convert.ToInt32(arr[0]);//see TemplateEditValueChanged_Values

                        REPORTS_VALUECONST_VALUES rpt_valueconst = new REPORTS_VALUECONST_VALUES()
                        {
                            ReportHeaderKey = ItemModel.dKey,
                            TemplateValueConstKey = valuekey,
                            InputState = 0,
                            CreatedOn = ItemModel.CreatedOn,
                            CreatedBy = ItemModel.CreatedBy,
                            ModifiedOn = ItemModel.ModifiedOn,
                            ModifiedBy = ItemModel.ModifiedBy
                        };
                        db.REPORTS_VALUECONST_VALUES.Add(rpt_valueconst);

                        _hasRows = true;
                    }

                    #endregion

                    if (_hasRows)
                        db.SaveChanges();

                    scope.Complete();
                }
                result.Result_Status = 1;
                result.Result_Message = CREATE_SUCCESS;
                switch (SaveType)
                {
                    case "save_edit":
                        result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);
                        break;
                    case "save_new":
                        result.RedirectTo = string.Format("/{0}/Create", m_ControllerName);
                        break;
                    default: break;
                }

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        // GET: REPORTS_HEADERS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            REPORTS_HEADERS item = db.REPORTS_HEADERS.Find(id);
            if (item == null)
                return B_NotFound();
            if (item.RecordStatus == 0)
                return B_NotFound();

            item.CreatedBy = GetCreatorName(item.CreatedBy);
            item.ModifiedBy = GetCreatorName(item.ModifiedBy);

            #region query data

            //giới hạn số báo cáo, theo nhân viên của phòng ban
            var lstRestricted = GetListTemplateRestricted().Select(p => p.TemplateHeaderKey).ToList();

            var query_templates = (from t in db.TEMPLATES_HEADERS
                                   where t.RecordStatus == 1 && lstRestricted.Contains(t.dKey)
                                   select new { t.dKey, t.ItemName }).ToList();


            #endregion

            if (!lstRestricted.Contains(item.TemplateKey))
                return B_DataRestricted();

            ViewBag.TemplateList = new SelectList(query_templates, "dKey", "ItemName");
            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(REPORTS_HEADERS ItemModel)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, false);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);

                REPORTS_HEADERS oldItem = db.REPORTS_HEADERS.Find(ItemModel.dKey);
                if (oldItem == null)
                    throw new Exception(NOT_FOUND);

                //save model
                string[] arr_model = { "ItemCode", "ItemName", "RptMonth", "RptYear", "DateStart", "DateEnd", "Sorted", "Remarks" };
                foreach (string pName in arr_model)
                {
                    PropertyInfo pInfo = oldItem.GetType().GetProperty(pName);
                    object v_to_set = ItemModel.GetType().GetProperty(pName).GetValue(ItemModel, null);
                    pInfo.SetValue(oldItem, v_to_set);
                }
                oldItem.ModifiedBy = MyAccount.Model.dKey.ToString();
                oldItem.ModifiedOn = Now;
                db.Entry(oldItem).State = EntityState.Modified;

                db.SaveChanges();
                result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);
                result.Result_Message = UPDATE_SUCCESS;
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        private string CheckBeforeSave(REPORTS_HEADERS ItemModel, bool isNew)
        {
            if (isNew)
            {
                if (ItemModel.TemplateKey == 0)
                    return "Chưa chọn biểu mẫu";
            }

            if (string.IsNullOrEmpty(ItemModel.ItemCode))
                return "Mã báo cáo không được để trống";
            if (string.IsNullOrEmpty(ItemModel.ItemName))
                return "Tên báo cáo không được để trống";

            return string.Empty;
        }

        // GET: REPORTS_HEADERS/Delete/5
        public ActionResult Delete(int? id)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_DELETE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                REPORTS_HEADERS model = db.REPORTS_HEADERS.Find(id);
                if (model == null)
                    throw new Exception(NOT_FOUND);

                model.ModifiedBy = MyAccount.Model.dKey.ToString();
                model.ModifiedOn = Now;
                model.RecordStatus = 0;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                result.Result_Message = DELETE_SUCCESS;
                result.RedirectTo = string.Format("/{0}/Index", m_ControllerName);
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    model.dKey.ToString(),
                    model.ItemCode,
                    model.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveCopyHeader(int ReportHeaderKey, bool CopyData, bool CopyAssign)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_COPY", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                REPORTS_HEADERS ItemModel = db.REPORTS_HEADERS.Find(ReportHeaderKey);
                if (ItemModel == null)
                    throw new Exception(NOT_FOUND);

                using (TransactionScope scope = new TransactionScope())
                {
                    var ModelCopy = new REPORTS_HEADERS();
                    foreach (PropertyInfo p_copy in ModelCopy.GetType().GetProperties())
                    {
                        p_copy.SetValue(ModelCopy, ItemModel.GetType().GetProperty(p_copy.Name).GetValue(ItemModel, null));
                    }
                    ModelCopy.RptMonth = Now.Month;
                    ModelCopy.RptYear = Now.Year;
                    ModelCopy.ItemCode = "Copy - " + ModelCopy.ItemCode;
                    ModelCopy.ItemName = "Copy - " + ModelCopy.ItemName;
                    ModelCopy.ApproveStatus = CopyAssign ? 1 : 0;
                    ModelCopy.ApprovePerson = null;
                    ModelCopy.ApproveDate = null;
                    ModelCopy.CreatedBy = MyAccount.Model.dKey.ToString();
                    ModelCopy.ModifiedBy = ModelCopy.CreatedBy;
                    ModelCopy.CreatedOn = Now;
                    ModelCopy.ModifiedOn = ModelCopy.CreatedOn;
                    db.REPORTS_HEADERS.Add(ModelCopy);
                    db.SaveChanges();

                    #region copy values

                    var lstValues = db.REPORTS_VALUECONST_VALUES.Where(p => p.ReportHeaderKey == ItemModel.dKey).ToList();
                    foreach (var vModel in lstValues)
                    {
                        var vCopy = new REPORTS_VALUECONST_VALUES();
                        foreach (PropertyInfo p_copy in vCopy.GetType().GetProperties())
                        {
                            p_copy.SetValue(vCopy, ItemModel.GetType().GetProperty(p_copy.Name).GetValue(vModel, null));
                        }
                        if (!CopyData)
                            vCopy.ValueInput = null;
                        if (!CopyAssign)
                            vCopy.AssignPerson = null;
                        vCopy.ReportHeaderKey = ModelCopy.dKey;
                        vCopy.InputState = CopyData ? 2 : 1;//đang nhập
                        vCopy.CreatedBy = ModelCopy.CreatedBy;
                        vCopy.ModifiedBy = ModelCopy.ModifiedBy;
                        vCopy.CreatedOn = ModelCopy.CreatedOn;
                        vCopy.ModifiedOn = ModelCopy.ModifiedOn;
                        db.REPORTS_VALUECONST_VALUES.Add(vCopy);

                        if (CopyData)
                        {
                            ModelCopy.ApproveStatus = 2;//đang nhập
                            db.Entry(ModelCopy).State = EntityState.Modified;
                        }
                    }
                    if (lstValues.Count > 0)
                        db.SaveChanges();

                    #endregion

                    #region copy tables values

                    var lstTables_Assigned = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == ItemModel.dKey).ToList();

                    if (CopyAssign)
                    {
                        foreach (var tModel in lstTables_Assigned)
                        {
                            var tCopy = new REPORTS_TABLES_ASSIGNMENT();
                            foreach (PropertyInfo p_copy in tCopy.GetType().GetProperties())
                            {
                                p_copy.SetValue(tCopy, ItemModel.GetType().GetProperty(p_copy.Name).GetValue(tModel, null));
                            }
                            tCopy.ReportHeaderKey = ModelCopy.dKey;
                            tCopy.InputState = 1;//phân công xong
                            tCopy.CreatedBy = MyAccount.Model.dKey.ToString();
                            tCopy.ModifiedBy = MyAccount.Model.dKey.ToString();
                            tCopy.CreatedOn = ModelCopy.CreatedOn;
                            tCopy.ModifiedOn = ModelCopy.ModifiedOn;
                            db.REPORTS_TABLES_ASSIGNMENT.Add(tCopy);
                            db.SaveChanges();

                            if (CopyData)
                            {
                                var lst_tblValues = db.REPORTS_TABLES_COLUMNS_VALUES.Where(p => p.ReportTableAssignedKey == tModel.dKey).ToList();
                                foreach (var tvModel in lst_tblValues)
                                {
                                    var tvCopy = new REPORTS_TABLES_COLUMNS_VALUES();
                                    foreach (PropertyInfo p_copy in tvCopy.GetType().GetProperties())
                                    {
                                        p_copy.SetValue(tvCopy, ItemModel.GetType().GetProperty(p_copy.Name).GetValue(tvModel, null));
                                    }
                                    tvCopy.ReportTableAssignedKey = tCopy.dKey;
                                    tvCopy.CreatedBy = MyAccount.Model.dKey.ToString();
                                    tvCopy.ModifiedBy = MyAccount.Model.dKey.ToString();
                                    tvCopy.CreatedOn = ModelCopy.CreatedOn;
                                    tvCopy.ModifiedOn = ModelCopy.ModifiedOn;
                                    db.REPORTS_TABLES_COLUMNS_VALUES.Add(tvCopy);

                                    ModelCopy.ApproveStatus = 2;//đang nhập
                                    db.Entry(ModelCopy).State = EntityState.Modified;
                                }

                                db.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    scope.Complete();
                }

                result.Result_Message = "Sao chép thành công";
                result.RedirectTo = string.Format("/{0}/Index", m_ControllerName);
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetList(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                var lstRestricted = GetListTemplateRestricted().Select(p => p.TemplateHeaderKey).ToList();

                var lstItems = (from d in db.REPORTS_HEADERS
                                where d.RecordStatus == 1 && lstRestricted.Contains(d.TemplateKey)
                                orderby d.CreatedOn descending
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.DateEnd,
                                    d.ApproveStatus
                                }).ToList();

                if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                    lstItems.Clear();

                TableModel table = new TableModel();
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemName", Caption = "Tên Báo Cáo", Width = 60, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "DateEnd", Caption = "Hạn Hoàn Thành", Width = 15, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "StatusDesc", Caption = "Tình Trạng", Width = 15, Filtered = true });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemName,
                    DateEnd = p.DateEnd == null ? "-" : p.DateEnd.Value.ToString(TextUtil.DateLocalFormat),
                    TinhTrang = db.REPORTS_STATUS_APPROVE.Find(p.ApproveStatus)?.ItemName ?? "-"
                }).ToList();

                #region manual filter

                string key;
                string filter_expr;
                foreach (TableHeaderColumns tcol in table.HeaderColumns.Where(p => p.Filtered).ToList())
                {
                    key = tcol.FieldName;
                    filter_expr = Request.QueryString[key];

                    if (!string.IsNullOrEmpty(filter_expr))
                    {
                        filter_expr = TextUtil.RemoveVni(Convert.ToString(filter_expr)).ToLower();

                        q1 = q1.Where(p => TextUtil.RemoveVni(Convert.ToString(p.GetType().GetProperty(key).GetValue(p, null)).ToLower()).Contains(filter_expr)).ToList();

                        table.FilterValues.Add(key, filter_expr ?? "");
                    }
                }

                #endregion

                table.CalcTotalPage(q1.Count);
                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult TemplateEditValueChanged_Info(int? TemplateHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try

            {
                var template = db.TEMPLATES_HEADERS.Find(TemplateHeaderKey);
                if (template == null)
                    throw new Exception(NOT_FOUND);

                template.ItemCode = string.Format("{0}{1}{2}", template.ItemCode, Now.Year.ToString(), Now.Month.ToString("00"));
                template.ItemName = string.Format("{0} tháng {1} năm {2}", template.ItemName, Now.Month.ToString("00"), Now.Year.ToString());
                result.Data = template;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult TemplateEditValueChanged_Tables(int? TemplateHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "Key", "State", "Table Template Key", "Bảng Phụ Lục" };
                table.ColumnsHidden = new int[] { 0, 1, 2 };

                var query = (from h in db.TEMPLATES_HEADERS_TABLES
                             join t in db.TEMPLATES_TABLES
                             on h.TemplateTableKey equals t.dKey
                             where h.TemplateHeaderKey == TemplateHeaderKey
                             orderby t.Sorted
                             select new
                             {
                                 h.dKey,
                                 State = "",
                                 h.TemplateTableKey,
                                 t.ItemName
                             }).ToList();
                if (query.Count > 0)
                {
                    query.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "", "" });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult TemplateEditValueChanged_Values(int? TemplateHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "Key", "State", "Mã Nhận Dạng", "Mô Tả" };
                table.ColumnsHidden = new int[] { 0, 1 };

                var query = (from v in db.TEMPLATES_VALUES_CONST
                             where v.TemplateHeaderKey == TemplateHeaderKey
                             select new
                             {
                                 v.dKey,
                                 State = "",
                                 v.ItemCode,
                                 v.ItemName
                             }).ToList();

                if (query.Count > 0)
                {
                    query.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "", "" });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListReport_AssignIndex(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                int[] rpt_state_allow = { 0, 1, 2, 3, 4 };

                //giới hạn số báo cáo, theo nhân viên của phòng ban
                var lstRestricted = GetListTemplateRestricted().Select(p => p.TemplateHeaderKey).ToList();

                var lstItems = (from d in db.REPORTS_HEADERS
                                where d.RecordStatus == 1 &&
                                rpt_state_allow.Contains(d.ApproveStatus) &&
                                lstRestricted.Contains(d.TemplateKey)
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.DateEnd,
                                    d.ApproveStatus,
                                    d.TemplateKey
                                }).ToList();

                if (!GrantAccess(string.Format("{0}_ASSIGN", m_ControllerName)))
                    lstItems.Clear();

                TableModel table = new TableModel()
                {
                    Selectable = false
                };
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemName", Caption = "Tên Báo Cáo", Width = 50, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "DateEnd", Caption = "Hạn Hoàn Thành", Width = 15, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "StatusDesc", Caption = "Tình Trạng", Width = 15, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "btnAssignTables", Caption = "Phụ Lục", Width = 5 });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "btnAssignValues", Caption = "Dữ Liệu", Width = 5 });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemName,
                    DateEnd = p.DateEnd == null ? "-" : p.DateEnd.Value.ToString(TextUtil.DateLocalFormat),
                    TinhTrang = db.REPORTS_STATUS_APPROVE.Find(p.ApproveStatus)?.ItemName ?? "-",
                    btnAssignTables = db.TEMPLATES_HEADERS_TABLES.Where(t => t.TemplateHeaderKey == p.TemplateKey).Count() == 0 ? "" : string.Format("<a class=\"btn btn-default btn100-action\" href=\"/{0}/AssignEditTables/{1}\"><i class=\"mi-2x mi-mode-edit at-right\"></i></a>", m_ControllerName, p.dKey),
                    btnAssignValues = db.REPORTS_VALUECONST_VALUES.Where(v => v.ReportHeaderKey == p.dKey).Count() == 0 ? "" : string.Format("<a class=\"btn btn-default btn100-action\" href=\"/{0}/AssignEditValues/{1}\"><i class=\"mi-2x mi-mode-edit at-right\"></i></a>", m_ControllerName, p.dKey),
                }).ToList();

                table.CalcTotalPage(q1.Count);
                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListTablesForAssign(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "TemplateTableKey", "PersonKey", "Tên Phụ Lục", "Nhân Sự", "IsChanged" };
                table.ColumnsHidden = new int[] { 0, 1, 4 };
                REPORTS_HEADERS itemRpt = db.REPORTS_HEADERS.Find(ReportHeaderKey);

                var datagroup = (from d in db.TEMPLATES_HEADERS_TABLES
                                 where d.TemplateHeaderKey == itemRpt.TemplateKey
                                 select d).ToList();

                var lst_tables_assigned = (from tbl in db.REPORTS_TABLES_ASSIGNMENT
                                           join emp in db.CAT_EMPLOYEES on tbl.AssignPerson equals emp.dKey
                                           where tbl.ReportHeaderKey == ReportHeaderKey
                                           select new
                                           {
                                               tbl.dKey,
                                               tbl.ReportHeaderKey,
                                               tbl.TemplateTableKey,
                                               tbl.AssignPerson,
                                               EmployeeName = emp.ItemName
                                           }).ToList();

                var data = datagroup.Select(p => new
                {
                    p.TemplateTableKey,
                    Arr_PersonKey = string.Join("; ", lst_tables_assigned.Where(d => d.TemplateTableKey == p.TemplateTableKey).Select(d => d.AssignPerson.ToString()).ToArray()),
                    TableName = db.TEMPLATES_TABLES.Find(p.TemplateTableKey).ItemName,
                    Arr_PersonName = string.Join("; ", lst_tables_assigned.Where(d => d.TemplateTableKey == p.TemplateTableKey).Select(d => d.EmployeeName).ToArray()),
                    IsChanged = ""
                }).ToList();

                if (data.Count > 0)
                {
                    data.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "", "", "" });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult TreePersonAssign(int? ReportHeaderKey, int? TemplateHeaderKey, string lst_person_checked)
        {
            AjaxResult result = new AjaxResult();
            StringBuilder builder = new StringBuilder();
            string nl = Environment.NewLine;

            try
            {
                var lst_employees = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).ToList()
                    .Select(p => new
                    {
                        EmployeeKey = p.dKey,
                        EmployeeName = p.ItemName,
                        DepartmentKey = p.DepartmentKey ?? 0,
                        DepartmentName = db.CAT_DEPARTMENTS.Find(p.DepartmentKey)?.ItemName ?? "-"
                    }).ToList();

                if (lst_employees.Count > 0)
                {
                    //ds nhân sự đã phân công trong db
                    var lst_emp_assigned = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == ReportHeaderKey && p.TemplateTableKey == TemplateHeaderKey).ToList();

                    builder.Append("<ul class=\"ultree\">" + nl);
                    List<int> lst_persons_key = new List<int>();
                    if (!string.IsNullOrEmpty(lst_person_checked))
                    {
                        lst_persons_key = lst_person_checked.Split(',').ToList().Select(p => Convert.ToInt32(p)).ToList();
                    }
                    foreach (var group in lst_employees.GroupBy(p => new { p.DepartmentKey, p.DepartmentName }).ToList())
                    {
                        string dep_checked = lst_emp_assigned.Where(p => p.AssignPerson != null).Count() == 0 ? "" : " checked";
                        //trường hợp đang edit trên form, xét nhân sự này có đang check trên form không, bỏ qua dữ liệu trong db
                        dep_checked = group.ToList().Where(p => lst_persons_key.Contains(p.EmployeeKey)).Count() == 0 ? "" : " checked";

                        builder.Append("<li aria-expanded=\"true\">" + nl);
                        builder.Append(string.Format("<input type=\"checkbox\" class=\"fancytree-checkbox\" id=\"DEP_{0}\" name=\"DEP_{0}\" data-key=\"{0}\" data-parent=\"\"{1}>", group.Key.DepartmentKey, dep_checked) + nl);
                        builder.Append(string.Format("<label for\"DEP_{0}\" class=\"fancytree-title\"><b>{1}</b></label>", group.Key.DepartmentKey, group.Key.DepartmentName.ToUpper()) + nl);
                        if (group.Count() > 0)
                        {
                            builder.Append("<ul class=\"ultree\">" + nl);
                            foreach (var item in group)
                            {
                                string emp_checked = lst_emp_assigned.Where(p => p.AssignPerson == item.EmployeeKey).Count() == 0 ? "" : " checked";
                                emp_checked = lst_persons_key.Contains(item.EmployeeKey) ? " checked" : emp_checked;
                                builder.Append("<li aria-expanded=\"true\">" + nl);
                                builder.Append(string.Format("<input type=\"checkbox\" class=\"fancytree-checkbox\" id=\"EMP_{0}\" name=\"EMP_{0}\" data-key=\"{0}\" data-parent=\"DEP_{1}\" data-desc=\"{2}\"{3}>", item.EmployeeKey, group.Key.DepartmentKey, item.EmployeeName, emp_checked) + nl);
                                builder.Append(string.Format("<label for\"EMP_{0}\" class=\"fancytree-title\">{1}</label>", item.EmployeeKey, item.EmployeeName) + nl);
                                builder.Append("</li>");
                            }
                            builder.Append("</ul>");
                        }
                        builder.Append("</li>");
                    }
                    builder.Append("</ul>");
                }
                else
                {
                    builder.Append("<h3>Không có thông tin</h3>");
                }
                result.Data = builder.ToString();
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveTablesAssigned(int? ReportHeaderKey, List<string[]> DataAssigned)
        {
            //DataAssigned: see GetListTablesForAssign
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_ASSIGN", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                if (DataAssigned == null)
                    throw new Exception("Không có dữ liệu");
                REPORTS_HEADERS ItemModel = db.REPORTS_HEADERS.Find(ReportHeaderKey);

                DateTime Now = DateTime.Now;
                //từng phụ lục
                bool hasData = false;
                for (int i = 0; i < DataAssigned.Count; i++)
                {
                    string[] data_tblassgined = DataAssigned[i];
                    string row_state = data_tblassgined[data_tblassgined.Length - 1];
                    if (string.IsNullOrEmpty(row_state))
                        continue;

                    int TemplateTableKey = Convert.ToInt32(data_tblassgined[0]);
                    if (string.IsNullOrEmpty(data_tblassgined[1]))
                    {
                        var lst_person = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == ReportHeaderKey && p.TemplateTableKey == TemplateTableKey).ToList();
                        lst_person.ForEach(item =>
                        {
                            db.Entry(item).State = EntityState.Deleted;
                            hasData = true;
                        });
                    }
                    else
                    {
                        string[] arr_person_assigned = data_tblassgined[1].Split(',');
                        var lst_person_assigned = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == ReportHeaderKey && p.TemplateTableKey == TemplateTableKey).ToList();
                        if (lst_person_assigned.Where(p => p.InputState != 0).Count() > 0)
                            throw new Exception("Phụ lục đã được phân công, không thể sửa");

                        if (lst_person_assigned.Count > 0)
                        {
                            lst_person_assigned.ForEach(p => p.AssignPerson = null);
                        }

                        int _index = 0;
                        foreach (string pStrKey in arr_person_assigned)
                        {
                            //ghi đè lên dữ liệu cũ
                            if (_index < lst_person_assigned.Count)
                            {
                                REPORTS_TABLES_ASSIGNMENT itemAssign = lst_person_assigned[_index];
                                itemAssign.AssignPerson = Convert.ToInt32(pStrKey.Trim());
                                itemAssign.InputState = 1;//await
                                itemAssign.ModifiedOn = Now;
                                itemAssign.ModifiedBy = MyAccount.Model.dKey.ToString();
                                db.Entry(itemAssign).State = EntityState.Modified;
                            }
                            else
                            {
                                REPORTS_TABLES_ASSIGNMENT itemAssign = new REPORTS_TABLES_ASSIGNMENT();
                                itemAssign.ReportHeaderKey = ItemModel.dKey;
                                itemAssign.TemplateTableKey = TemplateTableKey;
                                itemAssign.AssignPerson = Convert.ToInt32(pStrKey.Trim());
                                itemAssign.InputState = 1;//await
                                itemAssign.CreatedOn = Now;
                                itemAssign.CreatedBy = MyAccount.Model.dKey.ToString();
                                itemAssign.ModifiedOn = Now;
                                itemAssign.ModifiedBy = MyAccount.Model.dKey.ToString();
                                db.REPORTS_TABLES_ASSIGNMENT.Add(itemAssign);
                            }
                            _index++;
                            hasData = true;
                        }
                        //xóa các dòng nếu dư thừa (lần sau chỉnh sửa phân công nhưng dữ liệu ít hơn)
                        lst_person_assigned.Where(p => p.AssignPerson == null).ToList().ForEach(item => db.Entry(item).State = EntityState.Deleted);
                    }
                }

                if (hasData)
                {
                    ItemModel.ApproveStatus = 1;
                    db.Entry(ItemModel).State = EntityState.Modified;

                    db.SaveChanges();
                    result.Result_Message = UPDATE_SUCCESS;
                    WriteLog(
                        m_ControllerName,
                        Request.RequestContext.RouteData.Values["action"].ToString(),
                        MethodBase.GetCurrentMethod().ToString(),
                        result.Result_Status.ToString(),
                        result.Result_Message,
                        ItemModel.dKey.ToString(),
                        ItemModel.ItemCode,
                        ItemModel.ItemName
                        );
                }
                result.RedirectTo = string.Format("/{0}/AssignEditTables/{1}", m_ControllerName, ReportHeaderKey);
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveAssignTablesComplete(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_ASSIGN", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                var lst_tbls_assigned = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == ReportHeaderKey).ToList();
                if (lst_tbls_assigned.Where(p => p.AssignPerson == null).Count() > 0)
                {
                    throw new Exception("Phụ lục vẫn chưa được phân công xong. Không thể kết thúc phân công");
                }
                lst_tbls_assigned.ForEach(item =>
                {
                    item.InputState = 2;
                    item.ModifiedBy = MyAccount.Model.dKey.ToString();
                    item.ModifiedOn = DateTime.Now;
                    db.Entry(item).State = EntityState.Modified;
                });
                REPORTS_HEADERS ItemModel = db.REPORTS_HEADERS.Find(ReportHeaderKey);

                var lst_values_assign = db.REPORTS_VALUECONST_VALUES.Where(p => p.ReportHeaderKey == ReportHeaderKey).ToList();
                if (lst_values_assign.Count == 0)
                {
                    //không có dữ liệu
                    ItemModel.ApproveStatus = 2;
                    ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                    ItemModel.ModifiedOn = DateTime.Now;
                    db.Entry(ItemModel).State = EntityState.Modified;
                }
                else
                {
                    if (lst_values_assign.Where(p => p.InputState == 2).Count() > 0)
                    {
                        //đã hoàn thành nhập liệu giá trị
                        ItemModel.ApproveStatus = 2;
                        ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                        ItemModel.ModifiedOn = DateTime.Now;
                        db.Entry(ItemModel).State = EntityState.Modified;
                    }
                }

                //lưu nhật ký phân công
                var tblGroup = lst_tbls_assigned.GroupBy(p => p.TemplateTableKey).ToList();
                tblGroup.ForEach(group =>
                {
                    var templateTbl = db.TEMPLATES_TABLES.Find(group.Key);
                    var lstPerson = group.ToList().GroupBy(p => p.AssignPerson).ToList();

                    lstPerson.ForEach(person =>
                    {
                        var itemLog = new LOG_REPORT_ASSIGNMENT();
                        itemLog.DateCreated = DateTime.Now;
                        itemLog.ReportHeaderKey = ItemModel.dKey;
                        itemLog.AssignPerson = MyAccount.Model.EmployeeKey;
                        itemLog.ReceivedPerson = person.Key.Value;
                        itemLog.AssignContent = string.Format("Phụ lục: {0}-{1}", templateTbl.ItemCode, templateTbl.ItemName);
                        db.LOG_REPORT_ASSIGNMENT.Add(itemLog);
                    });

                });
                db.SaveChanges();

                result.Result_Message = UPDATE_SUCCESS;
                result.RedirectTo = string.Format("/{0}/AssignEditTables/{1}", m_ControllerName, ReportHeaderKey);
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveAssignTablesUnlock(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_ASSIGN", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                REPORTS_HEADERS ItemModel = db.REPORTS_HEADERS.Find(ReportHeaderKey);
                ItemModel.ApproveStatus = 1;
                ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                ItemModel.ModifiedOn = DateTime.Now;

                var lst_tbls_assigned = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == ReportHeaderKey).ToList();
                lst_tbls_assigned.ForEach(item =>
                {
                    item.InputState = 1;
                    item.ModifiedBy = ItemModel.ModifiedBy;
                    item.ModifiedOn = ItemModel.ModifiedOn;
                    db.Entry(item).State = EntityState.Modified;
                });

                db.Entry(ItemModel).State = EntityState.Modified;
                db.SaveChanges();

                result.RedirectTo = string.Format("/{0}/AssignEditTables/" + ItemModel.dKey, m_ControllerName);
                result.Result_Status = 1;

                result.Result_Message = UPDATE_SUCCESS;
                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListValuesForAssign(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "dKey", "TemplateValueConstKey", "Tên Trường", "Nhân Sự", "AssignPerson", "IsChanged" };
                table.ColumnsHidden = new int[] { 0, 1, 4, 5 };
                REPORTS_HEADERS itemRpt = db.REPORTS_HEADERS.Find(ReportHeaderKey);

                var query = (from d in db.REPORTS_VALUECONST_VALUES
                             join v in db.TEMPLATES_VALUES_CONST
                                on d.TemplateValueConstKey equals v.dKey
                             where d.ReportHeaderKey == ReportHeaderKey
                             orderby v.Sorted
                             select new
                             {
                                 d.dKey,
                                 d.TemplateValueConstKey,
                                 v.ItemName,
                                 d.AssignPerson
                             }).ToList();

                int _index = 1;
                var query_emp = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).OrderBy(p => p.ItemName).ToList()
                    .Select(p => new
                    {
                        p.dKey,
                        ItemName = (_index++).ToString() + ". " + p.ItemName
                    }).ToList();

                var data = query.Select(p => new
                {
                    p.dKey,
                    p.TemplateValueConstKey,
                    p.ItemName,
                    PersonName = p.AssignPerson == null ? "(chọn)" : query_emp.Where(d => d.dKey == p.AssignPerson).FirstOrDefault()?.ItemName ?? "",
                    p.AssignPerson,
                    IsChanged = "",
                }).ToList();
                if (data.Count > 0)
                {
                    data.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "", "", "", "" });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveValuesAssigned(int? ReportHeaderKey, List<string[]> DataAssigned)
        {
            //DataAssigned: see GetListTablesForAssign
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_ASSIGN", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                if (DataAssigned == null)
                    throw new Exception("Không có dữ liệu");

                REPORTS_HEADERS ItemModel = db.REPORTS_HEADERS.Find(ReportHeaderKey);

                DateTime Now = DateTime.Now;
                bool hasData = false;
                for (int i = 0; i < DataAssigned.Count; i++)
                {
                    string[] arrData = DataAssigned[i];
                    if (arrData[arrData.Length - 1] == "")
                        continue;//no change made

                    int dkey = Convert.ToInt32(arrData[0]);
                    var item = db.REPORTS_VALUECONST_VALUES.Find(dkey);

                    if (!string.IsNullOrEmpty(arrData[4]) || arrData[4] != "0")
                    {
                        item.InputState = 1;
                        item.AssignPerson = Convert.ToInt32(arrData[4]);
                    }
                    else
                    {
                        item.InputState = 0;
                        item.AssignPerson = null;
                    }
                    item.ModifiedOn = Now;
                    item.ModifiedBy = MyAccount.Model.dKey.ToString();
                    db.Entry(item).State = EntityState.Modified;
                    hasData = true;
                }
                if (hasData)
                {
                    ItemModel.ApproveStatus = 1;
                    db.Entry(ItemModel).State = EntityState.Modified;

                    db.SaveChanges();
                    result.Result_Message = UPDATE_SUCCESS;
                    WriteLog(
                        m_ControllerName,
                        Request.RequestContext.RouteData.Values["action"].ToString(),
                        MethodBase.GetCurrentMethod().ToString(),
                        result.Result_Status.ToString(),
                        result.Result_Message,
                        ItemModel.dKey.ToString(),
                        ItemModel.ItemCode,
                        ItemModel.ItemName
                        );
                }

                result.RedirectTo = string.Format("/{0}/AssignEditValues/{1}", m_ControllerName, ReportHeaderKey);
                result.Result_Status = 1;


            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveAssignValuesComplete(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_ASSIGN", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                var lst_values_assigned = db.REPORTS_VALUECONST_VALUES.Where(p => p.ReportHeaderKey == ReportHeaderKey).ToList();
                if (lst_values_assigned.Where(p => p.AssignPerson == null).Count() > 0)
                    throw new Exception("Dữ liệu vẫn chưa được phân công xong. Không thể kết thúc phân công");

                REPORTS_HEADERS ItemModel = db.REPORTS_HEADERS.Find(ReportHeaderKey);
                lst_values_assigned.ForEach(item =>
                {
                    item.InputState = 2;
                    item.ModifiedBy = MyAccount.Model.dKey.ToString();
                    item.ModifiedOn = DateTime.Now;
                    db.Entry(item).State = EntityState.Modified;
                });

                var lst_tables_assigned = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == ReportHeaderKey).ToList();
                if (lst_tables_assigned.Count == 0)
                {
                    //không có dữ liệu
                    var lst_tables_Template = db.TEMPLATES_HEADERS_TABLES.Where(p => p.TemplateHeaderKey == ItemModel.TemplateKey).ToList();
                    if (lst_tables_Template.Count == 0)
                    {
                        //mẫu báo cáo không có bảng phụ lục nào
                        ItemModel.ApproveStatus = 2;
                        ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                        ItemModel.ModifiedOn = DateTime.Now;
                        db.Entry(ItemModel).State = EntityState.Modified;
                    }
                }
                else
                {
                    if (lst_tables_assigned.Where(p => p.InputState == 2).Count() > 0)
                    {
                        //đã hoàn thành nhập liệu giá trị
                        ItemModel.ApproveStatus = 2;
                        ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                        ItemModel.ModifiedOn = DateTime.Now;
                        db.Entry(ItemModel).State = EntityState.Modified;
                    }
                }

                //lưu nhật ký phân công
                var valueGroup = lst_values_assigned.GroupBy(p => p.AssignPerson).ToList();
                valueGroup.ForEach(group =>
                {
                    var itemLog = new LOG_REPORT_ASSIGNMENT();
                    itemLog.DateCreated = DateTime.Now;
                    itemLog.ReportHeaderKey = ItemModel.dKey;
                    itemLog.AssignPerson = MyAccount.Model.EmployeeKey;
                    itemLog.ReceivedPerson = group.Key.Value;
                    itemLog.AssignContent = "Trường dữ liệu: " + db.CAT_EMPLOYEES.Find(group.Key.Value)?.ItemName;
                    db.LOG_REPORT_ASSIGNMENT.Add(itemLog);

                });

                db.SaveChanges();

                result.RedirectTo = string.Format("/{0}/AssignEditValues/{1}", m_ControllerName, ReportHeaderKey);
                result.Result_Status = 1;

                result.Result_Message = UPDATE_SUCCESS;
                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveAssignValuesUnlock(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_ASSIGN", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                REPORTS_HEADERS ItemModel = db.REPORTS_HEADERS.Find(ReportHeaderKey);
                ItemModel.ApproveStatus = 1;
                ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                ItemModel.ModifiedOn = DateTime.Now;
                db.Entry(ItemModel).State = EntityState.Modified;

                var lst_values_assigned = db.REPORTS_VALUECONST_VALUES.Where(p => p.ReportHeaderKey == ReportHeaderKey).ToList();
                lst_values_assigned.ForEach(item =>
                {
                    item.InputState = 1;
                    item.ModifiedBy = ItemModel.ModifiedBy;
                    item.ModifiedOn = ItemModel.ModifiedOn;
                    db.Entry(item).State = EntityState.Modified;
                });
                db.SaveChanges();

                result.RedirectTo = string.Format("/{0}/AssignEditValues/" + ItemModel.dKey, m_ControllerName);
                result.Result_Status = 1;

                result.Result_Message = UPDATE_SUCCESS;
                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListAssignLog(string StrFromDate, string StrToDate)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                DateTime? FromDate = null;
                if (!string.IsNullOrEmpty(StrFromDate))
                    FromDate = DateTime.ParseExact(StrFromDate + " 00:00:00", TextUtil.DateLocalFormat + " hh:mm:ss", null);
                DateTime? ToDate = null;
                if (!string.IsNullOrEmpty(StrToDate))
                    ToDate = DateTime.ParseExact(StrToDate + " 23:59:59", TextUtil.DateLocalFormat + " HH:mm:ss", null);

                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "Ngày", "Báo Cáo", "Người Giao", "Người Nhận", "Nội Dung Phân Công" };

                var query = (from l in db.LOG_REPORT_ASSIGNMENT
                             join r in db.REPORTS_HEADERS on l.ReportHeaderKey equals r.dKey
                             where (FromDate == null || (FromDate != null && l.DateCreated >= FromDate)) &&
                             (ToDate == null || (ToDate != null && l.DateCreated <= ToDate))
                             orderby l.dKey descending
                             select new
                             {
                                 l.DateCreated,
                                 l.ReportHeaderKey,
                                 l.AssignPerson,
                                 l.ReceivedPerson,
                                 l.AssignContent
                             }).ToList();

                var data = query.Select(p => new
                {
                    DateCreated = p.DateCreated.ToString(TextUtil.DateFormat_L),
                    ReportName = db.REPORTS_HEADERS.Find(p.ReportHeaderKey)?.ItemName,
                    AssignPersonName = db.CAT_EMPLOYEES.Find(p.AssignPerson)?.ItemName,
                    ReceivedPersonName = db.CAT_EMPLOYEES.Find(p.ReceivedPerson)?.ItemName,
                    p.AssignContent
                }).ToList();

                if (data.Count > 0)
                {
                    data.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "", "", "" });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }
    }
}
