﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AutoReport
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinders.Binders.Add(typeof(DateTime), new MyDatetimeBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new MyDatetimeBinder());
        }
        public class MyDatetimeBinder : DefaultModelBinder
        {
            public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                string key = bindingContext.ModelName;
                if (bindingContext.ValueProvider.GetValue(key) == null)
                {
                    return base.BindModel(controllerContext, bindingContext);
                }
                var v = bindingContext.ValueProvider.GetValue(key).RawValue.ToString();
                if (bindingContext.ValueProvider.GetValue(key).RawValue is string[])
                {
                    v = ((string[])bindingContext.ValueProvider.GetValue(key).RawValue)[0].ToString();
                }
                DateTime outPut;
                if (DateTime.TryParseExact(v, new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss", "HH:mm" }, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out outPut))
                    return outPut;

                return base.BindModel(controllerContext, bindingContext);

            }
        }
    }
}
