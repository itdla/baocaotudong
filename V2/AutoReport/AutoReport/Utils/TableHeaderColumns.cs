﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Utils
{
    public class TableHeaderColumns
    {
        private string[] ColumnAlignment = { "left", "center", "justify", "right", "char" };

        public TableHeaderColumns()
        {
            FieldName = string.Empty;
            Caption = "N/A";
            Width = 1;
            Width_Unit = "%";
            ColumnVisible = true;
            Alignment = ColumnAlignment[0];
            Filtered = false;
        }

        /// <summary>
        /// Required for filter, works only if Filtered enable
        /// </summary>
        public string FieldName
        {
            get;
            set;
        }

        public string Caption
        {
            get;
            set;
        }

        public int Width
        {
            get;
            set;
        }

        /// <summary>
        /// Default value: percentage (%) otherwise: pixel (px)
        /// </summary>
        public string Width_Unit
        {
            get;
            set;
        }

        /// <summary>
        /// if false, Width = 1
        /// </summary>
        public bool ColumnVisible
        {
            get;
            set;
        }

        /// <summary>
        /// center, justify, left, right, char
        /// </summary>
        public string Alignment
        {
            get;
            set;
        }

        public bool Filtered
        {
            get;
            set;
        }
    }
}