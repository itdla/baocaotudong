﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Models
{
    public class Data_DoughnutChart
    {
        public Data_DoughnutChart(int y, string label)
        {
            YValue = y;
            LabelCaption = label;
        }

        public int YValue
        {
            get;
            set;
        }

        public string LabelCaption
        {
            get;
            set;
        }
    }
}