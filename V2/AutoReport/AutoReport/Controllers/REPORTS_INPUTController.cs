﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using System.Transactions;
using AutoReport.Utils;
using System.Reflection;
using System.Text;

namespace AutoReport.Controllers
{
    public class REPORTS_INPUTController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "REPORTS_INPUT";

        // GET: REPORTS_INPUT/Index
        public ActionResult Index()
        {
            if (!GrantAccess("REPORTS_HEADERS_INPUT"))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult AttachFileIndex()
        {
            if (!GrantAccess("REPORTS_HEADERS_INPUT"))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult AttachFileDetail(int? id)
        {
            if (!GrantAccess("REPORTS_HEADERS_INPUT"))
                return B_NotPermission();

            var itemHeader = db.REPORTS_HEADERS.Find(id);
            if (itemHeader == null)
                return B_NotFound();

            ViewBag.ControllerName = m_ControllerName;
            return View(itemHeader);
        }

        public ActionResult InputValuesConst(int? id)
        {
            REPORTS_HEADERS item = db.REPORTS_HEADERS.Find(id);
            ViewBag.ControllerName = m_ControllerName;

            int me_id = MyAccount.Model.EmployeeKey;
            var lst_values = db.REPORTS_VALUECONST_VALUES.Where(p => p.ReportHeaderKey == id && p.AssignPerson == me_id).ToList();
            int state = 0;//chưa nhập
            if (lst_values.Where(p => p.InputState == 3).Count() > 0)
                state = 1;//đang nhập: hiện nút kết thúc nhập liệu
            else if (lst_values.Where(p => p.InputState == 2).Count() > 0)
                state = 2;//mới phân công xong, chỉ hiện nút lưu
            ViewBag.InputState = state;
            return View(item);
        }

        public ActionResult InputTablesIndex(int? id)
        {
            REPORTS_HEADERS item = db.REPORTS_HEADERS.Find(id);
            ViewBag.ControllerName = m_ControllerName;
            return View(item);
        }

        public ActionResult InputTablesData(int? id)
        {
            REPORTS_TABLES_ASSIGNMENT item = db.REPORTS_TABLES_ASSIGNMENT.Find(id);
            ViewBag.TableTemplateName = db.TEMPLATES_TABLES.Find(item.TemplateTableKey).ItemName;
            ViewBag.ControllerName = m_ControllerName;
            ViewBag.InputState = 0;
            if (item.InputState == 3)
                ViewBag.InputState = 1;
            else if (item.InputState == 2)
                ViewBag.InputState = 2;
            return View(item);
        }

        public ActionResult GetListInputIndex(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                List<int> state_allow = new List<int>() { 2, 3, 4 };

                //danh sách report đang trong trạng thái phân công/đang nhập/nhập liệu xong
                var lstItems = (from d in db.REPORTS_HEADERS
                                where d.RecordStatus == 1 && state_allow.Contains(d.ApproveStatus)
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.DateEnd,
                                    d.ApproveStatus
                                }).ToList();

                //lọc danh sách nhân sự hiện tại có tham gia vào report nào trong số những report trên
                var lst_report_keys = lstItems.Select(p => p.dKey).ToList();
                var me_id = MyAccount.Model.EmployeeKey;
                //list tables assigned
                var lst_tables_assigned = (from t in db.REPORTS_TABLES_ASSIGNMENT
                                           where lst_report_keys.Contains(t.ReportHeaderKey) && t.AssignPerson == me_id
                                           select t).ToList();
                var arr_tables_rpt_keys = lst_tables_assigned.Select(p => p.ReportHeaderKey).ToList();

                //list values assigned
                var lst_values_assigned = (from t in db.REPORTS_VALUECONST_VALUES
                                           where lst_report_keys.Contains(t.ReportHeaderKey) && t.AssignPerson == me_id
                                           select t).ToList();
                var arr_values_rpt_keys = lst_values_assigned.Select(p => p.ReportHeaderKey).ToList();

                lstItems = lstItems.Where(p => arr_tables_rpt_keys.Contains(p.dKey) || arr_values_rpt_keys.Contains(p.dKey)).ToList();

                TableModel table = new TableModel()
                {
                    Selectable = false
                };
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemName", Caption = "Tên Báo Cáo", Width = 50, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "DateEnd", Caption = "Hạn Hoàn Thành", Width = 15, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "StatusDesc", Caption = "Tình Trạng", Width = 15, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "btnAssignTables", Caption = "Phụ Lục", Width = 5 });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "btnAssignValues", Caption = "Dữ Liệu", Width = 5 });


                /*
                 *  tình trạng tính theo từng nhân sự: người nào nhập xong thì hiện nhập xong
                 */

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemName,
                    DateEnd = p.DateEnd == null ? "-" : p.DateEnd.Value.ToString(TextUtil.DateLocalFormat),
                    TinhTrang = lst_tables_assigned.Where(d => d.ReportHeaderKey == p.dKey && d.AssignPerson == me_id && d.InputState != 4).Count() > 0 || lst_values_assigned.Where(d => d.ReportHeaderKey == p.dKey && d.AssignPerson == me_id && d.InputState != 4).Count() > 0 ? db.REPORTS_STATUS_INPUT.Find(p.ApproveStatus).ItemName : "Nhập liệu xong",
                    btnInputTables = !arr_tables_rpt_keys.Contains(p.dKey) ? "-" : string.Format("<a class=\"btn btn-default btn100-action\" href=\"/{0}/InputTablesIndex/{1}\"><i class=\"mi-2x mi-mode-edit at-right\"></i></a>", m_ControllerName, p.dKey),
                    btnInputValues = !arr_values_rpt_keys.Contains(p.dKey) ? "-" : string.Format("<a class=\"btn btn-default btn100-action\" href=\"/{0}/InputValuesConst/{1}\"><i class=\"mi-2x mi-mode-edit at-right\"></i></a>", m_ControllerName, p.dKey),
                }).ToList();

                table.CalcTotalPage(q1.Count);
                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListAttachFileIndex(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                List<int> state_allow = new List<int>() { 2, 3, 4 };
                //giới hạn số báo cáo, theo nhân viên của phòng ban
                var lstRestricted = GetListTemplateRestricted().Select(p => p.TemplateHeaderKey).ToList();

                //danh sách report đang trong trạng thái phân công/đang nhập/nhập liệu xong
                var lstItems = (from d in db.REPORTS_HEADERS
                                where d.RecordStatus == 1 && state_allow.Contains(d.ApproveStatus) &&
                                lstRestricted.Contains(d.TemplateKey)
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.DateEnd,
                                    d.ApproveStatus
                                }).ToList();

                TableModel table = new TableModel()
                {
                    Selectable = false
                };
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemName", Caption = "Tên Báo Cáo", Width = 55, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "StatusDesc", Caption = "Tình Trạng", Width = 15, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "FilesCount", Caption = "Số Files", Width = 10, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "btnActions", Caption = "Hành Động", Width = 10 });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemName,
                    TinhTrang = db.REPORTS_STATUS_INPUT.Find(p.ApproveStatus).ItemName,
                    FilesCount = db.REPORTS_HEADERS_FILESATTACHED.Where(d => d.ReportHeaderKey == p.dKey).Count(),
                    BtnActions = string.Format("<a class=\"btn btn-default btn100-action\" href=\"/{0}/AttachFileDetail/{1}\"><i class=\"mi-2x mi-search at-right\"></i></a>", m_ControllerName, p.dKey)
                }).ToList();
                table.CalcTotalPage(q1.Count);

                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListAttachFileDetail(int page, int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;

                //danh sách report đang trong trạng thái phân công/đang nhập/nhập liệu xong
                var lstItems = (from d in db.REPORTS_HEADERS_FILESATTACHED
                                where d.ReportHeaderKey == ReportHeaderKey
                                select new
                                {
                                    d.dKey,
                                    d.FileName,
                                    d.CreatedBy,
                                    d.CreatedOn
                                }).ToList();
                TableModel table = new TableModel()
                {
                    Selectable = false
                };
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "", Caption = "Tên File", Width = 50, Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "StatusDesc", Caption = "Người Đính Kèm", Width = 15, Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "FilesCount", Caption = "Ngày Đính Kèm", Width = 15, Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "btnActions", Caption = "Hành Động", Width = 10, Filtered = false });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.FileName,
                    CreatedBy = GetCreatorName(p.CreatedBy),
                    CreatedOn = p.CreatedOn.Value.ToString(TextUtil.DateLocalFormat),
                    BtnActions = string.Format("<a class=\"btn btn-default btn100-action\" data-key=\"{0}\"><i class=\"mi-2x mi-delete at-right\"></i></a>", p.dKey)
                }).ToList();
                table.CalcTotalPage(q1.Count);

                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListValuesForInput(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "dKey", "Tên Trường", "Giá trị", "IsChanged" };
                table.ColumnsHidden = new int[] { 0, 3 };

                var query = (from d in db.REPORTS_VALUECONST_VALUES
                             join v in db.TEMPLATES_VALUES_CONST
                                on d.TemplateValueConstKey equals v.dKey
                             where d.ReportHeaderKey == ReportHeaderKey
                             orderby v.Sorted
                             select new
                             {
                                 d.dKey,
                                 v.ItemName,
                                 d.ValueInput,
                                 IsChanged = ""
                             }).ToList();

                if (query.Count > 0)
                {
                    query.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "", "" });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveValuesInput(int? ReportHeaderKey, List<string[]> DataInput)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess("REPORTS_HEADERS_INPUT"))
                    throw new Exception(NOT_PERMISSION);
                if (DataInput == null)
                    throw new Exception("Không có dữ liệu");

                DateTime Now = DateTime.Now;

                REPORTS_HEADERS ItemModel = db.REPORTS_HEADERS.Find(ReportHeaderKey);
                ItemModel.ApproveStatus = 3;//đang nhập liệu
                ItemModel.ModifiedOn = Now;
                db.Entry(ItemModel).State = EntityState.Modified;

                for (int i = 0; i < DataInput.Count; i++)
                {
                    string[] arrData = DataInput[i];
                    if (arrData[arrData.Length - 1] == "")
                        continue;//no change made

                    int dkey = Convert.ToInt32(arrData[0]);
                    var item = db.REPORTS_VALUECONST_VALUES.Find(dkey);
                    item.InputState = 3;//đang nhập
                    item.ValueInput = arrData[2].Trim();
                    item.ModifiedOn = Now;
                    item.ModifiedBy = MyAccount.Model.dKey.ToString();
                    db.Entry(item).State = EntityState.Modified;
                }
                db.SaveChanges();

                result.Result_Message = UPDATE_SUCCESS;
                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );

                result.RedirectTo = string.Format("/{0}/InputValuesConst/{1}", m_ControllerName, ReportHeaderKey);
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveValuesComplete(int? ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess("REPORTS_HEADERS_INPUT"))
                    throw new Exception(NOT_PERMISSION);

                DateTime Now = DateTime.Now;
                int me_id = MyAccount.Model.EmployeeKey;
                var lstItems = db.REPORTS_VALUECONST_VALUES.Where(p => p.ReportHeaderKey == ReportHeaderKey && p.AssignPerson == me_id).ToList();
                lstItems.ForEach(item =>
                {
                    item.InputState = 4;//đang nhập
                    item.ModifiedOn = Now;
                    item.ModifiedBy = MyAccount.Model.dKey.ToString();
                    db.Entry(item).State = EntityState.Modified;
                });

                REPORTS_HEADERS ItemModel = db.REPORTS_HEADERS.Find(ReportHeaderKey);
                ItemModel.ApproveStatus = 3;

                #region kiem tra xem da nhap xong het ==> hoan thanh nhap lieu

                var lst_tables_input_complete_all = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == ReportHeaderKey && p.InputState != 4).Count();
                if (lst_tables_input_complete_all == 0)
                {
                    //tất cả đã hoàn thành nhập liệu, set status = nhập xong
                    ItemModel.ApproveStatus = 4;
                    ItemModel.ModifiedOn = Now;
                }
                db.Entry(ItemModel).State = EntityState.Modified;

                #endregion

                db.SaveChanges();
                result.RedirectTo = string.Format("/{0}/InputValuesConst/{1}", m_ControllerName, ReportHeaderKey);
                result.Result_Status = 1;

                result.Result_Message = UPDATE_SUCCESS;
                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListTableInputIndex(int page, int ReportHeaderKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                var me_id = MyAccount.Model.EmployeeKey;

                List<int> state_allow = new List<int>() { 1, 2, 3 };
                var lstItems = (from d in db.REPORTS_TABLES_ASSIGNMENT
                                join t in db.TEMPLATES_TABLES
                                on d.TemplateTableKey equals t.dKey
                                where d.ReportHeaderKey == ReportHeaderKey && d.AssignPerson == me_id
                                orderby t.Sorted
                                select new
                                {
                                    d.dKey,
                                    t.ItemName,
                                    d.InputState
                                }).ToList();

                TableModel table = new TableModel();
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemName", Caption = "Tên Phụ Lục", Width = 75, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "StatusDesc", Caption = "Tình Trạng", Width = 15, Filtered = true });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemName,
                    StatusDesc = db.REPORTS_STATUS_INPUT.Find(p.InputState)?.ItemName
                }).ToList();

                table.CalcTotalPage(q1.Count);
                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetListTableValuesForInput(int? TableAssignmentKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                var itemAssignment = db.REPORTS_TABLES_ASSIGNMENT.Find(TableAssignmentKey);
                var lst_template_columns = (from t in db.TEMPLATES_TABLES_COLUMNS
                                            where t.TableTemplateKey == itemAssignment.TemplateTableKey
                                            orderby t.Sorted
                                            select t).ToList();

                HsonTable table = new HsonTable();
                var lst_caption = lst_template_columns.Select(p => p.ColumnCaption).ToList();
                var lst_width = lst_template_columns.Select(p => p.WidthInPx ?? 10).ToList();
                table.HeadersCaption = lst_caption.ToArray();
                table.HeadersWidth = lst_width.ToArray();

                var me_id = MyAccount.Model.EmployeeKey;
                var lst_columns_key = lst_template_columns.Select(p => p.dKey).ToList();
                var query = (from d in db.REPORTS_TABLES_COLUMNS_VALUES
                             join t in db.TEMPLATES_TABLES_COLUMNS
                             on d.TemplateColumnKey equals t.dKey
                             where d.ReportTableAssignedKey == TableAssignmentKey && lst_columns_key.Contains(d.TemplateColumnKey)
                             && d.AssignPerson == me_id
                             orderby d.RowIndex, t.Sorted
                             select d).ToList();

                if (query.Count > 0)
                {
                    var datagroup = query.GroupBy(p => p.RowIndex).ToList();
                    foreach (var group in datagroup)
                    {
                        var data_row_value = group.Select(p => p.ValueInput).ToArray();
                        table.Source.Add(data_row_value);
                    }
                }
                else
                {
                    List<string> dataNull = new List<string>();
                    lst_template_columns.ForEach(p => { dataNull.Add(""); });
                    table.Source.AddRange(new object[] { dataNull.ToArray() });
                }
                result.Result_Status = 1;
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveTableValuesInput(int? TableAssignmentKey, List<string[]> DataInput)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess("REPORTS_HEADERS_INPUT"))
                    throw new Exception(NOT_PERMISSION);
                if (DataInput == null)
                    throw new Exception("Không có dữ liệu");

                DateTime Now = DateTime.Now;

                REPORTS_TABLES_ASSIGNMENT itemTblAssign = db.REPORTS_TABLES_ASSIGNMENT.Find(TableAssignmentKey);
                itemTblAssign.InputState = 3;//đang nhập liệu
                itemTblAssign.ModifiedOn = Now;
                db.Entry(itemTblAssign).State = EntityState.Modified;

                REPORTS_HEADERS itemHeader = db.REPORTS_HEADERS.Find(itemTblAssign.ReportHeaderKey);
                itemHeader.ApproveStatus = 3;
                itemHeader.ModifiedOn = Now;
                db.Entry(itemHeader).State = EntityState.Modified;

                //get schema
                var lst_template_columns = (from t in db.TEMPLATES_TABLES_COLUMNS
                                            where t.TableTemplateKey == itemTblAssign.TemplateTableKey
                                            orderby t.Sorted
                                            select t).ToList();
                var lst_columns_key = lst_template_columns.Select(p => p.dKey).ToList();

                //get data
                var me_id = MyAccount.Model.EmployeeKey;
                var data_tbl_input = (from d in db.REPORTS_TABLES_COLUMNS_VALUES
                                      join t in db.TEMPLATES_TABLES_COLUMNS
                                      on d.TemplateColumnKey equals t.dKey
                                      where d.ReportTableAssignedKey == TableAssignmentKey
                                      && d.AssignPerson == me_id
                                      && lst_columns_key.Contains(d.TemplateColumnKey)
                                      orderby d.RowIndex, t.Sorted
                                      select d).ToList();
                data_tbl_input.ForEach(p =>
                {
                    p.ValueInput = null;
                });
                var lst_datagroup = data_tbl_input.GroupBy(p => p.RowIndex).ToList();
                bool hasData = false;

                for (int i = 0; i < DataInput.Count; i++)
                {
                    string[] arrData = DataInput[i];
                    if (arrData.Where(p => string.IsNullOrEmpty(p)).Count() == arrData.Length)
                        continue;
                    hasData = true;

                    if (lst_datagroup.Count > 0)
                    {
                        //ghi đè lên dữ liệu cũ nếu số dòng trong phạm vi dữ liệu cũ
                        if (i < lst_datagroup.Count)
                        {
                            int col_index = 0;
                            foreach (var item in lst_datagroup[i])
                            {
                                item.AssignPerson = me_id;
                                item.ValueInput = arrData[col_index].Trim();
                                item.RowIndex = i;
                                item.ModifiedOn = Now;
                                item.ModifiedBy = MyAccount.Model.dKey.ToString();
                                db.Entry(item).State = EntityState.Modified;
                                col_index++;
                            }
                        }
                        else
                        {
                            int col_index = 0;
                            foreach (int column_key in lst_columns_key)
                            {
                                REPORTS_TABLES_COLUMNS_VALUES itemTblValue = new REPORTS_TABLES_COLUMNS_VALUES();
                                itemTblValue.ReportTableAssignedKey = itemTblAssign.dKey;
                                itemTblValue.TemplateColumnKey = column_key;
                                itemTblValue.AssignPerson = me_id;
                                itemTblValue.ValueInput = arrData[col_index];
                                itemTblValue.CreatedOn = Now;
                                itemTblValue.CreatedBy = MyAccount.Model.dKey.ToString();
                                itemTblValue.ModifiedOn = Now;
                                itemTblValue.ModifiedBy = MyAccount.Model.dKey.ToString();
                                itemTblValue.RowIndex = i;
                                db.REPORTS_TABLES_COLUMNS_VALUES.Add(itemTblValue);
                                col_index++;
                            }
                        }
                    }
                    else
                    {
                        int col_index = 0;
                        foreach (int column_key in lst_columns_key)
                        {
                            REPORTS_TABLES_COLUMNS_VALUES itemTblValue = new REPORTS_TABLES_COLUMNS_VALUES();
                            itemTblValue.ReportTableAssignedKey = itemTblAssign.dKey;
                            itemTblValue.TemplateColumnKey = column_key;
                            itemTblValue.AssignPerson = me_id;
                            itemTblValue.ValueInput = arrData[col_index];
                            itemTblValue.CreatedOn = Now;
                            itemTblValue.CreatedBy = MyAccount.Model.dKey.ToString();
                            itemTblValue.ModifiedOn = Now;
                            itemTblValue.ModifiedBy = MyAccount.Model.dKey.ToString();
                            itemTblValue.RowIndex = i;
                            db.REPORTS_TABLES_COLUMNS_VALUES.Add(itemTblValue);
                            col_index++;
                        }
                    }
                }

                if (hasData)
                {
                    if (DataInput.Count < lst_datagroup.Count)
                    {
                        //trường hợp edit, xóa bớt dòng dữ liệu ==> xóa dữ liệu trong db
                        for (int i = DataInput.Count; i < lst_datagroup.Count; i++)
                        {
                            lst_datagroup[i].ToList().ForEach(item => db.Entry(item).State = EntityState.Deleted);
                        }
                    }

                    db.SaveChanges();
                    result.Result_Message = UPDATE_SUCCESS;
                    WriteLog(
                        m_ControllerName,
                        Request.RequestContext.RouteData.Values["action"].ToString(),
                        MethodBase.GetCurrentMethod().ToString(),
                        result.Result_Status.ToString(),
                        result.Result_Message,
                        itemTblAssign.dKey.ToString(),
                        null,
                        null
                        );
                }
                result.Result_Status = 1;
                result.RedirectTo = string.Format("/{0}/InputTablesData/{1}", m_ControllerName, TableAssignmentKey);
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveTableValuesComplete(int? TableAssignmentKey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess("REPORTS_HEADERS_INPUT"))
                    throw new Exception(NOT_PERMISSION);

                DateTime Now = DateTime.Now;
                var item = db.REPORTS_TABLES_ASSIGNMENT.Find(TableAssignmentKey);
                item.InputState = 4;
                item.ModifiedBy = MyAccount.Model.dKey.ToString();
                item.ModifiedOn = Now;
                db.Entry(item).State = EntityState.Modified;

                REPORTS_HEADERS itemHeader = db.REPORTS_HEADERS.Find(item.ReportHeaderKey);
                itemHeader.ApproveStatus = 3;

                #region kiem tra xem cac phu luc khac nhap xong het ==> hoan thanh nhap lieu

                var lst_valuesAll = db.REPORTS_VALUECONST_VALUES.Where(p => p.ReportHeaderKey == item.ReportHeaderKey && p.InputState != 4).ToList();
                var lst_tablesAll = db.REPORTS_TABLES_ASSIGNMENT.Where(p => p.ReportHeaderKey == item.ReportHeaderKey && p.InputState != 4 && p.dKey != TableAssignmentKey).ToList();

                if (lst_valuesAll.Count == 0 && lst_tablesAll.Count == 0)
                {
                    //tất cả đã hoàn thành nhập liệu, set status = nhập xong
                    itemHeader.ApproveStatus = 4;
                    itemHeader.ModifiedOn = Now;
                }
                db.Entry(itemHeader).State = EntityState.Modified;

                #endregion

                db.SaveChanges();
                result.RedirectTo = string.Format("/{0}/InputTablesData/{1}", m_ControllerName, TableAssignmentKey);
                result.Result_Status = 1;

                result.Result_Message = UPDATE_SUCCESS;
                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    item.dKey.ToString(),
                    null,
                    null
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult SaveFileAttached(HttpPostedFileBase[] files)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess("REPORTS_HEADERS_INPUT"))
                    throw new Exception(NOT_PERMISSION);

                int reportkey = string.IsNullOrEmpty(Request["ReportHeaderKey"]) ? 0 : Convert.ToInt32(Request["ReportHeaderKey"]);
                if (files.Length > 0)
                {
                    DateTime Now = DateTime.Now;

                    foreach (var file in files)
                    {
                        REPORTS_HEADERS_FILESATTACHED item = new REPORTS_HEADERS_FILESATTACHED();
                        item.ReportHeaderKey = reportkey;
                        item.FileName = file.FileName;
                        item.FileReference = string.Format("{0}{1}", Path_RptHeader_Attached, Now_Full_String() + file.FileName);
                        item.CreatedOn = Now;
                        item.CreatedBy = MyAccount.Model.dKey.ToString();
                        db.REPORTS_HEADERS_FILESATTACHED.Add(item);

                        file.SaveAs(Server.MapPath(item.FileReference));
                    }
                    db.SaveChanges();

                    result.Result_Message = UPDATE_SUCCESS;
                    WriteLog(
                        m_ControllerName,
                        Request.RequestContext.RouteData.Values["action"].ToString(),
                        MethodBase.GetCurrentMethod().ToString(),
                        result.Result_Status.ToString(),
                        result.Result_Message,
                        reportkey.ToString(),
                        null,
                        null
                        );
                }
                result.Result_Status = 1;
                result.RedirectTo = string.Format("/{0}/AttachFileDetail/{1}", m_ControllerName, reportkey);
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult DeleteFileAttach(int? id)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess("REPORTS_HEADERS_INPUT"))
                    throw new Exception(NOT_PERMISSION);
                
                var model = db.REPORTS_HEADERS_FILESATTACHED.Find(id);
                if (model == null)
                    throw new Exception(NOT_FOUND);

                if(!string.IsNullOrEmpty(model.CreatedBy))
                {
                    int acccount_key = Convert.ToInt32(model.CreatedBy);
                    if (acccount_key != MyAccount.Model.dKey)
                        throw new Exception("Không thể xóa file đính kèm của người khác tải lên");
                }
                int ReportHeaderKey = model.ReportHeaderKey.Value;

                System.IO.File.Delete(Server.MapPath(model.FileReference));
                db.Entry(model).State = EntityState.Deleted;
                db.SaveChanges();

                result.Result_Message = DELETE_SUCCESS;
                result.RedirectTo = string.Format("/{0}/AttachFileDetail/{1}", m_ControllerName, ReportHeaderKey);
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    model.dKey.ToString(),
                    model.FileName,
                    model.FileReference
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }
        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}