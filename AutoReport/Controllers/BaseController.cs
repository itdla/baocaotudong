﻿using AutoReport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoReport.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        public static readonly int PageSize = 30;
        public string CREATE_SAVE_SUCCESS = "Tạo dữ liệu thành công";
        public string UPDATE_SAVE_SUCCESS = "Cập nhật dữ liệu thành công";
        public string DELETE_SAVE_SUCCESS = "Xóa dữ liệu thành công";
        public string NO_PERMISSION = "Không có quyền truy cập";
        public string NOT_FOUND = "Không tìm thấy dữ liệu này";
        public int zInActive = -1;
        public string SuperUserName = "su";
        public string SuperUserPassword = "E4DC02DD368986D682F4CE9C60CA97E1";
        public static bool IsSuperUser = false;

        protected string PathUpload_TemplateOrg = "/Uploads/templates/org/";
        protected string PathUpload_TemplateVersion = "/Uploads/templates/versions/";

        protected string PathUpload_HeaderOrg = "/Uploads/headers/org/";
        protected string PathUpload_HeaderVersion = "/Uploads/headers/versions/";
        protected string PathUpload_HeaderExportTemp = "/Uploads/headers/exports/";
        protected string PathUpload_FileAttached = "/Uploads/attached/";
        protected string PathDoc_UserGuide = "/Doc/";
        public int GetTotalPage(int count, int? pageSize)
        {
            int size = PageSize;
            if (pageSize != null)
            {
                size = pageSize.Value;
            }
            int totalPage = count % size > 0 ? count / size + 1 : count / size;
            return totalPage;
        }

        public SYS_Users MyAccount
        {
            get
            {
                return (Session["_MyAccount"] == null ? null : (SYS_Users)Session["_MyAccount"]);
            }
            set
            {
                Session["_MyAccount"] = value;
            }
        }
        public DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }

        public string Now_Full_String()
        {
            return string.Format("{0}{1}{2}{3}{4}{5}", new object[]
                {
                    Now.Year.ToString(),
                    Now.Month.ToString("00"),
                    Now.Day.ToString("00"),
                    Now.Hour.ToString("00"),
                    Now.Minute.ToString("00"),
                    Now.Second.ToString("00")
                });
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.Url.ToString().Contains("Login") && !filterContext.HttpContext.Request.Url.ToString().Contains("System"))
            {
                if (this.MyAccount == null)
                {
                    filterContext.Result = new RedirectResult("/System/Login?url=" + filterContext.HttpContext.Request.RawUrl);
                    return;
                }
            }

        }

        public bool CheckPermission(string RoleCode)
        {
            if (IsSuperUser) return true;

            AutoReportEntities zDB = new AutoReportEntities();
            try
            {
                DateTime zNow = Now;
                var zMyAuth = MyAccount;

                var zUser = MyAccount;
                if (zUser == null)
                {
                    return false;
                }

                if (string.IsNullOrEmpty(RoleCode))
                {
                    return true;
                }
                else
                {
                    var zRole = zDB.SYS_Role.Where(p => p.Code == RoleCode).FirstOrDefault();
                    if (zRole != null)
                    {
                        var zRoleAccess = zDB.SYS_User_Access.Where(p => p.RoleID == zRole.RoleID && p.UserID == zUser.UserID && p.IsAccess).FirstOrDefault();
                        if (zRoleAccess != null)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                zDB.Dispose();
            }
        }

        /// <summary>
        /// Create a log action of current user in current scenario
        /// </summary>
        /// <param name="controllername">Current Controller Name</param>
        /// <param name="user_action">User action name. 'Add', 'Edit', 'Delete' etc.</param>
        /// <param name="methodname">Current Method Name. Call System.Reflection.MethodBase.GetCurrentMethod().Name from within the method.</param>
        /// <param name="method_exec_result">Result of method. Success, Error, others...</param>
        /// <param name="method_exec_message">Message to note.</param>
        /// <param name="userkey">Current user login</param>
        public void CreateLog(string controllername, string user_action, string datakey, string dataid, string dataname, string methodname, string method_exec_result, string method_exec_message)
        {
            string errmsg = string.Empty;
            AutoReportEntities db = new AutoReportEntities();
            Sys_Log_Action itemlog = new Sys_Log_Action();

            try
            {
                string client_device_name = System.Net.Dns.GetHostEntry(Request.ServerVariables["REMOTE_HOST"]).HostName;

                itemlog.LogControllerName = controllername;
                itemlog.LogAction = user_action;
                itemlog.LogMethodRef = methodname;
                itemlog.LogResult = method_exec_result;
                itemlog.LogMessage = method_exec_message;
                itemlog.DeviceIP = Request.UserHostAddress;
                itemlog.DeviceHostName = client_device_name;
                itemlog.UserLogin = MyAccount.UserID;
                itemlog.DateCreated = Now;
                itemlog.DataKey = datakey;
                itemlog.DataID = dataid;
                itemlog.DataName = dataname;
                db.Sys_Log_Action.Add(itemlog);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                errmsg = ex.Message;
            }
        }

        protected void DeleteFileUploaded(string path)
        {
            try
            {
                System.IO.File.Delete(path);
            }
            catch
            {

            }
        }
    }
}