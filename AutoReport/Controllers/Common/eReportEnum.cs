﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AutoReport.Controllers
{

    #region enumerable

    public enum eRptHeaderStatus
    {
        [Description("Không")]
        None = 0,

        [Description("Mới")]
        Created = 1,

        [Description("Chờ duyệt")]
        InProcess = 2,

        [Description("Đã duyệt")]
        Approved = 3,

        [Description("Trả về")]
        Declined = 4
    }

    public enum eRptInputStatus
    {
        [Description("Chưa phân công")]
        NotApplied = -1,

        [Description("Đang nhập")]
        Inputting = 0,

        [Description("Nhập xong")]
        Done = 1
    }

    public enum eRptDataType
    {
        [Description("Trường dữ liệu")]
        DataValue = 0,

        [Description("Bảng phụ lục")]
        DataTable = 1
    }

    public enum eRecordStatus
    {
        [Description("Đã bị xóa")]
        InActive = 0,

        [Description("Đang hoạt động")]
        Active = 1
    }

    public enum eWorkStatus
    {
        [Description("Không")]
        None = 0,

        [Description("Thành công")]
        Success = 1,

        [Description("Lỗi")]
        Error = 2
    }
    #endregion

    #region enum extract

    public class EnumUtil
    {
        public EnumUtil() { }

        public static string GetEnumDescText(Enum item)
        {
            string msg = string.Empty;
            string result = string.Empty;

            try
            {
                FieldInfo fi = item.GetType().GetField(item.ToString());
                DescriptionAttribute attr_desc = fi.GetCustomAttribute(typeof(DescriptionAttribute), false) as DescriptionAttribute;
                if (attr_desc != null)
                    result = attr_desc.Description;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                result = "N/A";
            }
            return result;
        }

        public static T GetValueFromCategory<T>(string category)
        {
            var type = typeof(T);
            if (!type.IsEnum)
                throw new Exception("Type is not an enum type");

            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(CategoryAttribute)) as CategoryAttribute;
                if (attribute != null)
                {
                    if (attribute.Category == category)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == category)
                        return (T)field.GetValue(null);
                }
            }
            return default(T);
        }
    }

    #endregion
}