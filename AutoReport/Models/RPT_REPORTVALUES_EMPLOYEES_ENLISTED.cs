//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutoReport.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RPT_REPORTVALUES_EMPLOYEES_ENLISTED
    {
        public System.Guid Rpt_ReportValues_Employees_EnlistedKey { get; set; }
        public System.Guid Rpt_ReportDetail_ValueConstKey { get; set; }
        public System.Guid EmployeeKey { get; set; }
        public bool Activate { get; set; }
    }
}
