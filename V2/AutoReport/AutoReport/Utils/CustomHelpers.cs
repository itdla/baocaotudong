﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace System.Web.Mvc.Html
{
    public static class CustomHelpers
    {
        public static MvcHtmlString DLA_DateFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            var name = metaData.PropertyName;
            DateTime? dateTime = null;
            DateTime flag;
            if ((metaData.Model != null) && DateTime.TryParse(metaData.Model.ToString(), out flag))
            {
                dateTime = new DateTime?(flag);
            }
            // create your html string, you could defer to DisplayFor to render a span or
            // use the TagBuilder class to create a span and add your attributes to it

            string html = "<input type='text' name='" + name + "' id='" + name + "' class='form-control date' value='" + (dateTime == null ? "" : dateTime.Value.ToString("dd/MM/yyyy")) + "' data-inputmask=\"'mask': '99/99/9999'\">";
            return new MvcHtmlString(html);
        }
        public static MvcHtmlString DLA_CheckBoxFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            var name = metaData.PropertyName;
            bool? value = null;
            bool flag;
            if ((metaData.Model != null) && bool.TryParse(metaData.Model.ToString(), out flag))
            {
                value = flag;
            }
            // create your html string, you could defer to DisplayFor to render a span or
            // use the TagBuilder class to create a span and add your attributes to it

            string html = "<input type='checkbox' name='" + name + "' id='" + name + "' " + (value == null ? "" : value.Value ? "checked" : "") + " value='true' class='flat'><label  for='" + name + "'></label>";
            return new MvcHtmlString(html);
        }
    }
}