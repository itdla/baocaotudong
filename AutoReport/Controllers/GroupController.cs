﻿using AutoReport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AutoReport.Controllers
{
    public class GroupController : BaseController
    {
        // GET: Group
        private string m_ControllerName = "Group";
        AutoReportEntities db = new AutoReportEntities();

        public ActionResult Edit(Guid? UserID)
        {
            if (!CheckPermission("USERS_AUTH"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            List<string> zResult = new List<string>();
            var zMyUser = db.SYS_Users.Find(UserID);
            var emp = db.CAT_EMPLOYEES.Find(zMyUser.EmployeeKey);

            if (zMyUser != null)
            {
                zResult = (from roleaccess in db.SYS_User_Access.Where(p => p.UserID == zMyUser.UserID)
                           join role in db.SYS_Role
                           on roleaccess.RoleID equals role.RoleID
                           where roleaccess.IsAccess == true
                           select role.Code).ToList();
            }

            var list = db.SYS_User_Access.Where(p => p.UserID == zMyUser.UserID && p.IsAccess == true && p.UserID != null && p.Status != zInActive).ToList();
            var ListRole = db.SYS_Role.Where(p => p.Status != zInActive).ToList();
            var listid = list.Select(p => p.RoleID).ToList();

            StringBuilder strValue = new StringBuilder();
            ViewBag.JsTree = "<ul class=\"dlaul\">" + ShowList(null, ListRole, listid) + "</ul>";

            ViewBag.EmployeeTitle = string.Format("{0} ({1})", zMyUser.UserName, emp == null ? "Tài khoản chưa được giao cho nhân viên" : emp.EmployeeName);
            return View(zResult);
        }

        [HttpPost]
        public ActionResult Edit(Guid UserID, FormCollection form)
        {
            AjaxResult zResult = new AjaxResult();
            var zListGuid = new List<Guid>();
            foreach (var item in form)
            {
                if (item.ToString().Trim().Length == 36)
                {
                    zListGuid.Add(Guid.Parse(item.ToString()));
                }
            }
            List<SYS_User_Access> zListUserAccess = new List<SYS_User_Access>();
            var zRemoveUserAccessResult = DeletePhysicalAccessByUserID(UserID);
            for (int i = 0; i < zListGuid.Count(); i++)
            {
                SYS_User_Access sra = new SYS_User_Access();
                sra.AccessID = Guid.NewGuid();
                sra.UserID = UserID;
                sra.RoleID = zListGuid[i];
                sra.IsAccess = true;
                sra.CreatedBy = "SYS";
                sra.ModifiedBy = "SYS";
                sra.CreatedDateTime = DateTime.Now;
                sra.ModifiedDateTime = DateTime.Now;
                zListUserAccess.Add(sra);

            }
            var zAddGroupAccessResult = AddGroupAccess(zListUserAccess);
            if (zAddGroupAccessResult)
            {
                var zListRole = new List<SYS_Role>();
                foreach (var Id in zListGuid)
                {
                    var zRole = db.SYS_Role.Where(p => p.RoleID == Id).FirstOrDefault();
                    if (zRole != null)
                    {
                        zListRole.Add(zRole);
                    }
                }
                // zResult.ListRole = zListRole;
                zResult.intStatus = 1;
                zResult.strMess = "Chỉnh sửa thành công!";
            }
            else
            {
                zResult.intStatus = 2;
                zResult.strMess = "Chỉnh sửa thất bại!";
            }
            var zUser = db.SYS_Users.Find(UserID);
            CreateLog(m_ControllerName, "Update-Auth", zUser.UserID.ToString(), zUser.UserName, zUser.UserName, System.Reflection.MethodBase.GetCurrentMethod().Name, zResult.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), zResult.strMess);
            return new CustomJsonResult { Data = (zResult) };
        }

        public string ShowList(Guid? parent, List<SYS_Role> alllistPage, List<Guid> access)
        {
            if (access == null)
            {
                access = new List<Guid>();
            }

            string list = "";
            var zListPage = new List<SYS_Role>();
            if (parent == null)
            {
                zListPage = alllistPage.Where(p => p.IDParent == null).OrderBy(p => p.Sort).ToList();
            }
            else
            {
                zListPage = alllistPage.Where(p => p.IDParent == parent.Value).OrderBy(p => p.Sort).ToList();
            }
            for (int i = 0; i < zListPage.Count; i++)
            {
                string ch = access.Contains(zListPage[i].RoleID) ? "checked" : "";
                list += "<li class=\"dlali\">";
                list += "<div class=\"dk-li01\"><input " + ch + "  name=\"" + zListPage[i].RoleID.ToString() + "\" class='ckex' parentID=\""
                    + zListPage[i].IDParent.ToString() + "\" type=\"checkbox\" id=\""
                    + zListPage[i].RoleID.ToString() + "\"style = \"opacity: 1.0;position: inherit;" + "\">&nbsp;"
                    + "<span style=\"margin-right: 10px\">" + zListPage[i].Name + "</span></div>"/*"<label class=\"control-label\" >" + listPage[i].strName + "</label></div>"*/;
                var listChildID = alllistPage.Where(p => p.IDParent == zListPage[i].RoleID).OrderBy(p => p.Sort).Select(p => p.RoleID).ToList();
                var listChild = alllistPage.Where(p => p.IDParent != null).Where(p => listChildID.Contains(p.IDParent.Value)).ToList();
                if (listChild.Count() == 0)
                {
                    for (int j = 0; j < listChildID.Count(); j++)
                    {
                        var item = alllistPage.Where(p => p.RoleID == listChildID[j]).FirstOrDefault();
                        if (item != null)
                        {
                            ch = access.Contains(item.RoleID) ? "checked" : "";
                            list += "<div class=\"dk-li02\"><input " + ch + " name=\"" + item.RoleID.ToString()
                                + "\" class='ckex' parentID=\"" + item.IDParent + "\" type=\"checkbox\" id=\""
                                + item.RoleID.ToString() + "\"style = \"opacity: 1.0;position: inherit;"
                                + "\">&nbsp;" + "<span style=\"margin-right: 10px\">" + item.Name + "</span></div>";
                        }
                    }
                }
                else
                {
                    if (alllistPage.Where(p => p.IDParent == zListPage[i].RoleID).Count() > 0)
                    {
                        list += "<ul class=\"dlaul\">";
                        list += ShowList(zListPage[i].RoleID, alllistPage, access);
                        list += "</ul>";
                    }
                }
                list += "</li>";
            }
            return list;
        }

        public bool DeletePhysicalAccessByUserID(Guid UserID)
        {
            try
            {
                var zListGroupAccess = db.SYS_User_Access.Where(p => p.UserID == UserID).ToList();
                db.SYS_User_Access.RemoveRange(zListGroupAccess);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AddGroupAccess(List<SYS_User_Access> zLstAccess)
        {
            try
            {
                db.SYS_User_Access.AddRange(zLstAccess);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}