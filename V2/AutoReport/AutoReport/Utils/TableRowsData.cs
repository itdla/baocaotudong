﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Utils
{
    public class TableRowsData
    {
        public TableRowsData()
        {
        }

        public TableRowsData(object key, object data_arr)
        {
            DataKey = key;
            DataArray = data_arr;
        }

        public object DataKey
        {
            get;
            set;
        }

        public object DataArray
        {
            get;
            set;
        }

    }
}