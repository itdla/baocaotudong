﻿function showloading() {
    $('body').block({
        message: '<i class="icon-spinner6 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });
}

function hideloading() {
    $('body').unblock();
}

//extra Jgrowl
function show_pnotify(type, note) {
    if (type == 'error')
        type = 'danger';
    var arr_types = ['none', 'info', 'success', 'warning', 'danger'];
    var arr_header = ['Thông Tin', 'Thông Tin', 'Thành Công', 'Cảnh Báo', 'Lỗi'];

    var _typeindex = arr_types.indexOf(type);
    if (_typeindex == -1)
        _typeindex = 0;

    $.jGrowl.defaults.closer = false;

    var pTheme = 'bg-' + arr_types[_typeindex];
    var pHeader = arr_header[_typeindex];

    // Bottom right
    $.jGrowl(note, {
        position: 'bottom-right',
        theme: pTheme,
        header: pHeader,
        timeout: 2000
    });
}