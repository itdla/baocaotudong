﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;


namespace AutoReport.Controllers
{
    public class RPT_TEMPLATES_DEPARTMENTController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();
        private string m_ControllerName = "RPT_TEMPLATES_DEPARTMENT";

        // GET: RPT_TEMPLATES_DEPARTMENT/Index
        public ActionResult Index()
        {
            if (!CheckPermission("TEMPLATES_DEPARTMENT"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: RPT_TEMPLATES_DEPARTMENT/Create
        public ActionResult Create()
        {
            if (!CheckPermission("TEMPLATES_DEPARTMENT_ADD"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            ViewBag.DepartmentList = new SelectList(db.CAT_DEPARTMENTS.Where(p => p.RecordStatus == 1).OrderBy(p => p.DepartmentName).ToList(), "DepartmentKey", "DepartmentName");

            return View();
        }

        // GET: RPT_TEMPLATES_DEPARTMENT/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (!CheckPermission("TEMPLATES_DEPARTMENT_EDIT"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            ViewBag.DepartmentList = new SelectList(db.CAT_DEPARTMENTS.Where(p => p.RecordStatus == 1).OrderBy(p => p.DepartmentName).ToList(), "DepartmentKey", "DepartmentName", id);

            return View();
        }

        public ActionResult SaveTemplateSelected(Guid? departmentkey, List<string[]> templatesselected, string action)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            CAT_DEPARTMENTS department = db.CAT_DEPARTMENTS.Find(departmentkey);

            try
            {
                if (department == null)
                    throw new Exception("Chưa chọn phòng ban");

                if (action.ToLower() == "edit")
                {
                    var lst_templates = db.RPT_TEMPLATES_DEPARTMENT.Where(p => p.DepartmentKey == departmentkey).ToList();
                    lst_templates.ForEach(p => db.Entry(p).State = EntityState.Deleted);
                }
                if (templatesselected != null)
                {
                    foreach (string[] arr in templatesselected)
                    {
                        RPT_TEMPLATES_DEPARTMENT itemtempate = new RPT_TEMPLATES_DEPARTMENT();
                        itemtempate.Rpt_Templates_DepartmentKey = Guid.NewGuid();
                        itemtempate.DepartmentKey = department.DepartmentKey;
                        itemtempate.Rpt_TemplatesKey = Guid.Parse(arr[0]);
                        db.RPT_TEMPLATES_DEPARTMENT.Add(itemtempate);
                    }
                }
                db.SaveChanges();
                result.strMess = "Đã lưu thông tin";
                CreateLog(m_ControllerName, action, department.DepartmentKey.ToString(), department.DepartmentID, department.DepartmentName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult Delete(Guid? id)
        {

            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            result.strMess = "Đã xóa thông tin";
            CAT_DEPARTMENTS department = db.CAT_DEPARTMENTS.Find(id);

            try
            {
                var lst_templates = db.RPT_TEMPLATES_DEPARTMENT.Where(p => p.DepartmentKey == id).ToList();
                lst_templates.ForEach(p => db.Entry(p).State = EntityState.Deleted);

                db.SaveChanges();
                CreateLog(m_ControllerName, "Delete", department.DepartmentKey.ToString(), department.DepartmentID, department.DepartmentName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);

            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult LoadList(int page, int pageSize, string DepartmentID, string DepartmentName)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            //Guid _id = new Guid(this.Request["id"]);
            DepartmentID = string.IsNullOrEmpty(DepartmentID) ? "" : DepartmentID;
            DepartmentName = string.IsNullOrEmpty(DepartmentName) ? "" : DepartmentName;

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(DepartmentID))
                table.KeysSearch.Add("DepartmentID", DepartmentID);
            if (!string.IsNullOrEmpty(DepartmentName))
                table.KeysSearch.Add("DepartmentName", DepartmentName);

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();

            //Filter
            var dep_list = db.RPT_TEMPLATES_DEPARTMENT.GroupBy(p => p.DepartmentKey).ToList().Select(p => p.Key).ToList();

            var searchQuery = (from d in db.CAT_DEPARTMENTS
                               where dep_list.Contains(d.DepartmentKey)
                               orderby d.DepartmentName
                               select d).ToList();

            var fq1 = searchQuery.Select(p => new
            {
                p.DepartmentKey,
                p.DepartmentID,
                p.DepartmentName,
                TemplateCount = db.RPT_TEMPLATES_DEPARTMENT.Where(t => t.DepartmentKey == p.DepartmentKey).Count()
            }).ToList();

            //lọc theo điều kiện
            if (!string.IsNullOrEmpty(DepartmentID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.DepartmentID.Trim().ToLower()).Contains(Utils.RemoveVni(DepartmentID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(DepartmentName))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.DepartmentName.Trim().ToLower()).Contains(Utils.RemoveVni(DepartmentName.Trim().ToLower()))).ToList();

            int _index = 1;
            var fq2 = fq1.Select(p => new
            {
                p.DepartmentKey,
                STT = _index++,
                p.DepartmentID,
                p.DepartmentName,
                TemplateCount = db.RPT_TEMPLATES_DEPARTMENT.Where(t => t.DepartmentKey == p.DepartmentKey).Count(),
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/Edit/" + p.DepartmentKey + "'>Sửa</a>" +
                "<button type=\"button\" class=\"btn btn-danger btn_xoa\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + p.DepartmentKey + "'>Xóa </button>"
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Phòng ban", IsShow = true, Format = "", FieldName = "DepartmentID", Width = 25, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Phòng ban", IsShow = true, Format = "", FieldName = "DepartmentName", Width = 45, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Số Báo Cáo", IsShow = true, Format = "", Width = 11, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 12, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Right });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].DepartmentKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadTemplatesList(Guid? departmentkey)
        {
            string nl = Environment.NewLine;
            StringBuilder htmlStr = new StringBuilder();
            Dictionary<string, object> result = new Dictionary<string, object>();
            result.Add("checkall", false);
            result.Add("codehtml", "");

            try
            {
                var lst_templates_department = db.RPT_TEMPLATES_DEPARTMENT.Where(p => p.DepartmentKey == departmentkey).ToList();
                var lst_templates = (from p in db.RPT_TEMPLATES
                                     where p.RecordStatus == 1
                                     orderby p.Templates_Name
                                     select p).ToList();
                foreach (var item in lst_templates)
                {
                    bool chek = lst_templates_department.Where(p => p.Rpt_TemplatesKey == item.Rpt_TemplatesKey).ToList().Count > 0;
                    htmlStr.Append(nl);
                    htmlStr.Append(string.Format("<div><input type=\"checkbox\" id=\"{0}\" name=\"{0}\" class=\"chklocal\" {2}/>&nbsp;&nbsp;<span>{1} </span></div>", item.Rpt_TemplatesKey, item.Templates_Name, chek ? "checked" : ""));
                }
                result["checkall"] = lst_templates_department.Count == lst_templates.Count;
                result["codehtml"] = htmlStr.ToString();
            }
            catch (Exception ex)
            {
                htmlStr.Clear();
                htmlStr.Append(ex.Message);
            }

            return new CustomJsonResult { Data = result };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
