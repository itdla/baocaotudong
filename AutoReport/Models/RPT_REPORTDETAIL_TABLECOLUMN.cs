//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutoReport.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RPT_REPORTDETAIL_TABLECOLUMN
    {
        public System.Guid Rpt_ReportDetail_TableColumnKey { get; set; }
        public System.Guid Rpt_ReportDetail_TableIndexKey { get; set; }
        public System.Guid Rpt_Templates_TableColumnKey { get; set; }
        public Nullable<bool> HiddenOnGUI { get; set; }
    }
}
