﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Models
{
    public class CanvasChart
    {
        private string m_ChartTitle;
        private bool m_AnimationEnable;
        private string m_ChartType;
        private string m_AxisX_Title;
        private string m_AxisY_Title;
        private List<Data_DoughnutChart> m_DataList_Donut;
        private List<Data_StackBarVMChart> m_DataList_StackBarVM;

        public CanvasChart()
        {
            m_ChartTitle = "New Chart Title";
            m_ChartType = "doughnut";
            m_AxisX_Title = string.Empty;
            m_AxisY_Title = string.Empty;
            m_AnimationEnable = true;
            m_DataList_Donut = new List<Data_DoughnutChart>();
            m_DataList_StackBarVM = new List<Data_StackBarVMChart>();
        }

        public string ChartTitle { get => m_ChartTitle; set => m_ChartTitle = value; }
        public bool AnimationEnable { get => m_AnimationEnable; set => m_AnimationEnable = value; }
        public string ChartType { get => m_ChartType; set => m_ChartType = value; }
        public List<Data_DoughnutChart> DataList_Donut { get => m_DataList_Donut; set => m_DataList_Donut = value; }
        public List<Data_StackBarVMChart> DataList_StackBarVM { get => m_DataList_StackBarVM; set => m_DataList_StackBarVM = value; }
        public string AxisX_Title { get => m_AxisX_Title; set => m_AxisX_Title = value; }
        public string AxisY_Title { get => m_AxisY_Title; set => m_AxisY_Title = value; }
    }
}