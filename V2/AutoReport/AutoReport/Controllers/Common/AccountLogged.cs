﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoReport.Models;

namespace AutoReport.Controllers
{
    public class AccountLogged
    {
        public AccountLogged()
        {
            Model = new SYS_USERS();
        }

        public SYS_USERS Model
        {
            get;
            set;
        }

        public bool GrantAccess(string Code, int userkey, bool isdefault)
        {
            string error = string.Empty;
            bool result = false;
            try
            {
                if (isdefault)
                    return true;

                using (AutoReport_V3Entities db = new AutoReport_V3Entities())
                {
                    var query = (from ra in db.SYS_ROLES_ACCESS
                                 join rm in db.SYS_ROLES
                                 on ra.RoleKey equals rm.dKey
                                 where ra.UserKey == userkey && rm.ItemCode == Code && ra.IsAccess == true
                                 select new
                                 {
                                     rm.ItemName,
                                     ra.IsAccess
                                 }).ToList();
                    result = query.Count() > 0;
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            return result;
        }
    }
}