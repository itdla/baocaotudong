﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Utils
{
    public class TableCellsFormat
    {
        public TableCellsFormat()
        {

        }

        public bool Editable
        {
            get;
            set;
        }

        public string EditType
        {
            get;
            set;
        }

        /// <summary>
        /// Order: ID | Raw | Class | Style. Style will overwrite class css
        /// </summary>
        public string Html_IDElement
        {
            get;
            set;
        }

        /// <summary>
        /// Order: ID | Raw | Class | Style. Style will overwrite class css
        /// </summary>
        public string Html_RawElements
        {
            get;
            set;
        }

        /// <summary>
        /// Order: ID | Raw | Class | Style. Style will overwrite class css
        /// </summary>
        public string Html_ClassElements
        {
            get;
            set;
        }

        /// <summary>
        /// Order: ID | Raw | Class | Style. Style will overwrite class css
        /// </summary>
        public string Html_StyleElements
        {
            get;
            set;
        }

    }
}