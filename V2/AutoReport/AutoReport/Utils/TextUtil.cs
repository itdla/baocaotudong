﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace AutoReport.Utils
{
    public static class TextUtil
    {
        public static string ComboBox_NullText = "------Chọn------";
        public static string DateFormat_L = "dd/MM/yyyy HH:mm";
        public static string Notify_Error = "Đã xảy ra lỗi thực thi";
        public static string DateLocalFormat = "dd/MM/yyyy";

        public static string RemoveVni(string expression)
        {
            string result = string.Empty;
            if (string.IsNullOrEmpty(expression))
                return result;
            string uCharsL = "à á ả ã ạ ă ằ ắ ẳ ẵ ặ â ầ ấ ẩ ẫ ậ đ è é ẻ ẽ ẹ ê ề ế ể ễ ệ ì í ỉ ĩ ị ò ó ỏ õ ọ ô ồ ố ổ ỗ ộ ơ ờ ớ ở ỡ ợ ù ú ủ ũ ụ ư ừ ứ ử ữ ự ỳ ý ỷ ỹ ỵ";
            string uCharsU = "À Á Ả Ã Ạ Ă Ằ Ắ Ẳ Ẵ Ặ Â Ầ Ấ Ẩ Ẫ Ậ Đ È É Ẻ Ẽ Ẹ Ê Ề Ế Ể Ễ Ệ Ì Í Ỉ Ĩ Ị Ò Ó Ỏ Õ Ọ Ô Ồ Ố Ổ Ỗ Ộ Ơ Ờ Ớ Ở Ỡ Ợ Ù Ú Ủ Ũ Ụ Ư Ừ Ứ Ử Ữ Ự Ỳ Ý Ỷ Ỹ Ỵ";
            string nCharsN = "a a a a a a a a a a a a a a a a a d e e e e e e e e e e e i i i i i o o o o o o o o o o o o o o o o o u u u u u u u u u u u y y y y y";

            string[] arrCharsL = uCharsL.Split(' ');
            string[] arrCharsU = uCharsU.Split(' ');
            string[] arrCharsN = nCharsN.Split(' ');

            foreach (char c in expression)
            {
                if (!char.IsLetter(c))
                {
                    result += c;
                    continue;
                }

                int _index = char.IsUpper(c) ? Array.IndexOf(arrCharsU, c.ToString()) : Array.IndexOf(arrCharsL, c.ToString());
                if (_index != -1)
                    result = char.IsUpper(c) ? result + arrCharsN[_index].ToUpper() : result + arrCharsN[_index];
                else
                    result += c;
            }
            return result;
        }

        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        /// <summary>
        /// Convert a date to string with formated by 'dd/MM/yyyy'
        /// </summary>
        /// <param name="dt">Input date, return empty if leave it null</param>
        /// <param name="separate_symbol">default is '/'</param>
        /// <returns>A formated string of date</returns>
        public static string ToTextFormat(DateTime? dt, string separate_symbol)
        {
            if (dt == null)
                return string.Empty;
            if (string.IsNullOrEmpty(separate_symbol))
                separate_symbol = "/";

            return dt.Value.Day.ToString("00") + separate_symbol + dt.Value.Month.ToString("00") + separate_symbol + dt.Value.Year.ToString();
        }
    }
}