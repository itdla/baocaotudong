﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using System.Transactions;
using AutoReport.Utils;
using System.Reflection;

namespace AutoReport.Controllers
{
    public class CAT_DEPARTMENTSController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "CAT_DEPARTMENTS";

        // GET: CAT_DEPARTMENTS/Index
        public ActionResult Index()
        {
            if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: CAT_DEPARTMENTS/Create
        public ActionResult Create()
        {
            if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;

            #region query data

            var query_emp = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).OrderBy(p => p.ItemName).ToList();

            var query_department = db.CAT_DEPARTMENTS.Where(p => p.RecordStatus == 1).OrderBy(p => p.ItemName).ToList();

            #endregion

            var lst_items = db.CAT_DEPARTMENTS.Where(p => p.RecordStatus == 1).ToList();
            CAT_DEPARTMENTS item = new CAT_DEPARTMENTS()
            {
                ItemCode = "(Tự động phát sinh)",
                Sorted = lst_items.Count == 0 ? 1 : lst_items[lst_items.Count - 1].Sorted + 1
            };

            ViewBag.EmployeeList = new SelectList(query_emp, "dKey", "ItemName");
            ViewBag.DepartmentList = new SelectList(query_department, "dKey", "ItemName");
            return View(item);
        }

        [HttpPost]
        public ActionResult Create(CAT_DEPARTMENTS ItemModel, string SaveType)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, true);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);

                using (TransactionScope scope = new TransactionScope())
                {
                    //auto-gen new item code
                    var lst_items = db.CAT_DEPARTMENTS.ToList();
                    if (lst_items.Count > 0)
                    {
                        ItemModel.ItemCode = lst_items[lst_items.Count - 1].ItemCode;
                        ItemModel.ItemCode = ItemModel.ItemCode.Replace("PB", "");
                        ItemModel.ItemCode = (Convert.ToInt32(ItemModel.ItemCode) + 1).ToString();

                        if (ItemModel.ItemCode.Length < 6)
                            ItemModel.ItemCode = Convert.ToInt32(ItemModel.ItemCode).ToString("000000");
                    }
                    else
                    {
                        ItemModel.ItemCode = "PB000001";
                    }

                    //save master
                    ItemModel.CreatedBy = MyAccount.Model.dKey.ToString();
                    ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                    ItemModel.CreatedOn = Now;
                    ItemModel.ModifiedOn = Now;
                    ItemModel.RecordStatus = 1;
                    db.CAT_DEPARTMENTS.Add(ItemModel);
                    db.SaveChanges();

                    scope.Complete();
                }
                result.Result_Status = 1;
                result.Result_Message = CREATE_SUCCESS;
                if (SaveType == "savenew")
                    result.RedirectTo = string.Format("/{0}/Create", m_ControllerName);
                else
                    result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        // GET: CAT_DEPARTMENTS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            CAT_DEPARTMENTS item = db.CAT_DEPARTMENTS.Find(id);
            if (item == null)
                return B_NotFound();
            if (item.RecordStatus == 0)
                return B_NotFound();

            item.CreatedBy = GetCreatorName(item.CreatedBy);
            item.ModifiedBy = GetCreatorName(item.ModifiedBy);

            #region query data

            var query_emp = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).OrderBy(p => p.ItemName).ToList();

            var query_department = db.CAT_DEPARTMENTS.Where(p => p.RecordStatus == 1).OrderBy(p => p.ItemName).ToList();

            #endregion

            ViewBag.EmployeeList = new SelectList(query_emp, "dKey", "ItemName");
            ViewBag.DepartmentList = new SelectList(query_department, "dKey", "ItemName");
            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(CAT_DEPARTMENTS ItemModel)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, false);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);

                CAT_DEPARTMENTS oldItem = db.CAT_DEPARTMENTS.Find(ItemModel.dKey);
                if (oldItem == null)
                    throw new Exception(NOT_FOUND);

                //save model
                string[] arr_model = { "ItemName", "ParentKey", "EmployeeKey", "Abbr", "Sorted", "Remarks" };
                foreach (string pName in arr_model)
                {
                    PropertyInfo pInfo = oldItem.GetType().GetProperty(pName);
                    object v_to_set = ItemModel.GetType().GetProperty(pName).GetValue(ItemModel, null);
                    pInfo.SetValue(oldItem, v_to_set);
                }
                oldItem.ModifiedBy = MyAccount.Model.dKey.ToString();
                oldItem.ModifiedOn = Now;
                db.Entry(oldItem).State = EntityState.Modified;

                db.SaveChanges();
                result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);
                result.Result_Message = UPDATE_SUCCESS;
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        private string CheckBeforeSave(CAT_DEPARTMENTS ItemModel, bool isNew)
        {
            if (string.IsNullOrEmpty(ItemModel.ItemCode))
                return "Mã phòng ban không được để trống";
            if (string.IsNullOrEmpty(ItemModel.ItemName))
                return "Tên phòng ban không được để trống";

            return string.Empty;
        }

        // GET: CAT_DEPARTMENTS/Delete/5
        public ActionResult Delete(int? id)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_DELETE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                CAT_DEPARTMENTS model = db.CAT_DEPARTMENTS.Find(id);
                if (model == null)
                    throw new Exception(NOT_FOUND);

                model.ModifiedBy = MyAccount.Model.dKey.ToString();
                model.ModifiedOn = Now;
                model.RecordStatus = 0;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                result.Result_Message = DELETE_SUCCESS;
                result.RedirectTo = string.Format("/{0}/Index", m_ControllerName);
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    model.dKey.ToString(),
                    model.ItemCode,
                    model.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetList(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                var lstItems = (from d in db.CAT_DEPARTMENTS
                                where d.RecordStatus == 1
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.Abbr
                                }).ToList();

                if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                    lstItems.Clear();

                TableModel table = new TableModel();
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemCode", Caption = "Mã Phòng Ban", Width = 20, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemName", Caption = "Tên Phòng Ban", Width = 40, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "Abbr", Caption = "Ký Hiệu", Width = 20, Filtered = true });
                
                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemCode,
                    p.ItemName,
                    p.Abbr
                }).ToList();

                #region manual filter

                string key;
                string filter_expr;
                foreach (TableHeaderColumns tcol in table.HeaderColumns.Where(p => p.Filtered).ToList())
                {
                    key = tcol.FieldName;
                    filter_expr = Request.QueryString[key];

                    if (!string.IsNullOrEmpty(filter_expr))
                    {
                        filter_expr = TextUtil.RemoveVni(Convert.ToString(filter_expr)).ToLower();

                        q1 = q1.Where(p => TextUtil.RemoveVni(Convert.ToString(p.GetType().GetProperty(key).GetValue(p, null)).ToLower()).Contains(filter_expr)).ToList();

                        table.FilterValues.Add(key, filter_expr ?? "");
                    }
                }

                #endregion

                table.CalcTotalPage(q1.Count);
                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
