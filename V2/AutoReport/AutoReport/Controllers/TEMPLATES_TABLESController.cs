﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using System.Transactions;
using AutoReport.Utils;
using System.Reflection;

namespace AutoReport.Controllers
{
    public class TEMPLATES_TABLESController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "TEMPLATES_TABLES";

        // GET: TEMPLATES_TABLES/Index
        public ActionResult Index()
        {
            if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: TEMPLATES_TABLES/Create
        public ActionResult Create()
        {
            if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                return B_NotPermission();
            ViewBag.ControllerName = m_ControllerName;
            TEMPLATES_TABLES item = new TEMPLATES_TABLES();
            return View(item);
        }

        // POST: TEMPLATES_TABLES/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(TEMPLATES_TABLES ItemModel, List<string[]> lstColumns, string SaveType)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, true);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);

                using (TransactionScope scope = new TransactionScope())
                {
                    //save master
                    ItemModel.CreatedBy = MyAccount.Model.dKey.ToString();
                    ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                    ItemModel.CreatedOn = Now;
                    ItemModel.ModifiedOn = Now;
                    ItemModel.RecordStatus = 1;
                    db.TEMPLATES_TABLES.Add(ItemModel);
                    db.SaveChanges();

                    if (lstColumns != null)
                    {
                        int _index = 0;
                        foreach (string[] arr in lstColumns)
                        {
                            if (arr.Where(p => p.Trim() == "").Count() == arr.Length)
                                continue;
                            _index++;
                            string _colname = arr[3] ?? string.Empty;
                            _colname = _colname.Trim();
                            string _colsort = arr[4] ?? string.Empty;
                            string _colwidth = arr[5] ?? string.Empty;
                            if (string.IsNullOrEmpty(_colname))
                                throw new Exception("Tên cột phụ lục không được để trống");

                            TEMPLATES_TABLES_COLUMNS tcol = new TEMPLATES_TABLES_COLUMNS();
                            tcol.TableTemplateKey = ItemModel.dKey;
                            tcol.ColumnCaption = _colname;

                            if (!string.IsNullOrEmpty(_colsort))
                            {
                                int ivalue = 0;
                                if (!int.TryParse(_colsort, out ivalue))
                                    throw new Exception("Thứ tự cột không phải số. Vui lòng để trống nếu bỏ qua cột này");
                                tcol.Sorted = ivalue;
                            }
                            else
                            {
                                tcol.Sorted = _index;
                            }

                            //if (!string.IsNullOrEmpty(_colwidth))
                            //{
                            //    int ivalue = 0;
                            //    if (!int.TryParse(_colwidth, out ivalue))
                            //        throw new Exception("Độ rộng cột không phải số. Vui lòng để trống nếu bỏ qua cột này");
                            //    tcol.WidthInPx = ivalue;
                            //}
                            tcol.WidthInPx = 0;
                            db.TEMPLATES_TABLES_COLUMNS.Add(tcol);
                        }
                        db.SaveChanges();
                    }

                    scope.Complete();
                }
                result.Result_Status = 1;
                result.Result_Message = CREATE_SUCCESS;
                if (SaveType == "savenew")
                    result.RedirectTo = string.Format("/{0}/Create", m_ControllerName);
                else
                    result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        // GET: TEMPLATES_TABLES/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            TEMPLATES_TABLES item = db.TEMPLATES_TABLES.Find(id);
            if (item == null)
                return B_NotFound();
            if (item.RecordStatus == 0)
                return B_NotFound();

            item.CreatedBy = GetCreatorName(item.CreatedBy);
            item.ModifiedBy = GetCreatorName(item.ModifiedBy);
            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(TEMPLATES_TABLES ItemModel, List<string[]> lstColumns, List<string> cols_deleted)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, false);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);

                TEMPLATES_TABLES oldItem = db.TEMPLATES_TABLES.Find(ItemModel.dKey);
                if (oldItem == null)
                    throw new Exception(NOT_FOUND);

                //save model
                string[] arr_model = { "ItemCode", "ItemName", "ReportRef", "Sorted", "Remarks" };
                foreach (string pName in arr_model)
                {
                    PropertyInfo pInfo = oldItem.GetType().GetProperty(pName);
                    object v_to_set = ItemModel.GetType().GetProperty(pName).GetValue(ItemModel, null);
                    pInfo.SetValue(oldItem, v_to_set);
                }
                oldItem.ModifiedBy = MyAccount.Model.dKey.ToString();
                oldItem.ModifiedOn = Now;
                db.Entry(oldItem).State = EntityState.Modified;

                #region save columns changed

                if (lstColumns != null)
                {
                    int _index = 0;
                    foreach (string[] arr in lstColumns)
                    {
                        if (arr.Where(p => p.Trim() == "").Count() == arr.Length)
                            continue;
                        _index++;
                        string objKey = Convert.ToString(arr[0]).Trim();
                        string objstate = Convert.ToString(arr[1]).Trim().ToLower();

                        string _colname = arr[3] ?? string.Empty;
                        _colname = _colname.Trim();
                        string _colsort = arr[4] ?? string.Empty;
                        string _colwidth = arr[5] ?? string.Empty;
                        int ivalue = 0;

                        if (string.IsNullOrEmpty(_colname))
                            throw new Exception("Tên cột phụ lục không được để trống");

                        if (objKey == "0")
                        {
                            #region new item

                            TEMPLATES_TABLES_COLUMNS tcol = new TEMPLATES_TABLES_COLUMNS();
                            tcol.TableTemplateKey = ItemModel.dKey;
                            tcol.ColumnCaption = _colname;

                            if (!string.IsNullOrEmpty(_colsort))
                            {
                                if (!int.TryParse(_colsort, out ivalue))
                                    throw new Exception("Thứ tự cột không phải số. Vui lòng để trống nếu bỏ qua cột này");
                                tcol.Sorted = ivalue;
                            }
                            else
                            {
                                tcol.Sorted = _index;
                            }

                            //if (!string.IsNullOrEmpty(_colwidth))
                            //{
                            //    ivalue = 0;
                            //    if (!int.TryParse(_colwidth, out ivalue))
                            //        throw new Exception("Độ rộng cột không phải số. Vui lòng để trống nếu bỏ qua cột này");
                            //    tcol.WidthInPx = ivalue;
                            //}
                            tcol.WidthInPx = 0;
                            db.TEMPLATES_TABLES_COLUMNS.Add(tcol);

                            #endregion
                        }
                        else
                        {
                            if (objstate != "e") continue;// no changed row

                            #region edit value

                            TEMPLATES_TABLES_COLUMNS tcol = db.TEMPLATES_TABLES_COLUMNS.Find(Convert.ToInt32(objKey));
                            tcol.ColumnCaption = _colname.Trim();

                            if (!string.IsNullOrEmpty(_colsort))
                            {
                                if (!int.TryParse(_colsort, out ivalue))
                                    throw new Exception("Thứ tự cột không phải số. Vui lòng để trống nếu bỏ qua cột này");
                                tcol.Sorted = ivalue;
                            }
                            else
                            {
                                tcol.Sorted = _index;
                            }

                            if (!string.IsNullOrEmpty(_colwidth))
                            {
                                ivalue = 0;
                                if (!int.TryParse(_colwidth, out ivalue))
                                    throw new Exception("Độ rộng cột không phải số. Vui lòng để trống nếu bỏ qua cột này");
                                tcol.WidthInPx = ivalue;
                            }
                            db.Entry(tcol).State = EntityState.Modified;

                            #endregion
                        }
                    }


                }
                #endregion

                #region save columns deleted

                if (cols_deleted != null)
                {
                    foreach (string col_id in cols_deleted)
                    {
                        TEMPLATES_TABLES_COLUMNS tcol = db.TEMPLATES_TABLES_COLUMNS.Find(Convert.ToInt32(col_id));
                        if (tcol != null)
                            db.Entry(tcol).State = EntityState.Deleted;
                    }
                }

                #endregion

                db.SaveChanges();

                result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);
                result.Result_Message = UPDATE_SUCCESS;
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        private string CheckBeforeSave(TEMPLATES_TABLES ItemModel, bool isNew)
        {
            if (string.IsNullOrEmpty(ItemModel.ItemCode))
                return "Mã phụ lục không được để trống";
            if (string.IsNullOrEmpty(ItemModel.ItemName))
                return "Tên phụ lục không được để trống";

            ItemModel.ItemCode = ItemModel.ItemCode.Trim();
            if (isNew)
            {

            }
            else
            {

                var lst_templatesHdr = db.TEMPLATES_HEADERS_TABLES.Where(p => p.TemplateTableKey == ItemModel.dKey).ToList();
                var lsttemplatekeys = lst_templatesHdr.Select(p => p.TemplateHeaderKey).ToList();

                var lst_ReportHeader = db.REPORTS_HEADERS.Where(p => lsttemplatekeys.Contains(p.TemplateKey)).Count();
                if (lst_ReportHeader > 0)
                    return "Phụ lục hiện đang được sử dụng. Vui lòng sao chép sang mẫu mới để chỉnh sửa";
            }
            return string.Empty;
        }

        // GET: TEMPLATES_TABLES/Delete/5
        public ActionResult Delete(int? id)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_DELETE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                TEMPLATES_TABLES model = db.TEMPLATES_TABLES.Find(id);
                if (model == null)
                    throw new Exception(NOT_FOUND);

                model.ModifiedBy = MyAccount.Model.dKey.ToString();
                model.ModifiedOn = Now;
                model.RecordStatus = 0;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                result.Result_Message = DELETE_SUCCESS;
                result.RedirectTo = string.Format("/{0}/Index", m_ControllerName);
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    model.dKey.ToString(),
                    model.ItemCode,
                    model.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetList(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                var lstItems = (from d in db.TEMPLATES_TABLES
                                where d.RecordStatus == 1
                                orderby d.ReportRef, d.Sorted
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.ReportRef,
                                    d.Sorted
                                }).ToList();

                if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                    lstItems.Clear();

                TableModel table = new TableModel();
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemCode", Caption = "Mã Phụ Lục", Width = 15, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ItemName", Caption = "Tên Phụ Lục", Width = 40, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "Sort", Caption = "Thứ tự", Width = 5, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ReportRef", Caption = "Thuộc báo cáo", Width = 20, Filtered = true });

                int _index = 1;
                var q1 = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.ItemCode,
                    p.ItemName,
                    p.Sorted,
                    p.ReportRef
                }).ToList();

                #region manual filter

                string key;
                string filter_expr;
                foreach (TableHeaderColumns tcol in table.HeaderColumns.Where(p => p.Filtered).ToList())
                {
                    key = tcol.FieldName;
                    filter_expr = Request.QueryString[key];

                    if (!string.IsNullOrEmpty(filter_expr))
                    {
                        filter_expr = TextUtil.RemoveVni(Convert.ToString(filter_expr)).ToLower();

                        q1 = q1.Where(p => TextUtil.RemoveVni(Convert.ToString(p.GetType().GetProperty(key).GetValue(p, null)).ToLower()).Contains(filter_expr)).ToList();

                        table.FilterValues.Add(key, filter_expr ?? "");
                    }
                }

                #endregion

                table.CalcTotalPage(q1.Count);

                var data = q1.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetColumns(int? tabletemplatekey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                HsonTable table = new HsonTable();
                table.HeadersCaption = new string[] { "Key", "State", "Mã Cột", "Tên Cột", "Thứ Tự", "Độ Rộng (px)" };
                table.ColumnsHidden = new int[] { 0, 1, 2, 5 };

                //fields use in afterChange event
                var query = (from c in db.TEMPLATES_TABLES_COLUMNS
                             where c.TableTemplateKey == tabletemplatekey
                             orderby c.Sorted
                             select new
                             {
                                 c.dKey,
                                 State = "",
                                 c.ColumnCode,
                                 c.ColumnCaption,
                                 c.Sorted,
                                 c.WidthInPx
                             }).ToList();
                if (query.Count > 0)
                {
                    query.ForEach(item =>
                    {
                        table.Source.Add(item);
                    });
                }
                else
                {
                    table.Source.Add(new object[] { "", "", "", "", "", "" });
                }
                result.Data = table;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult CopyToNew(int? id)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_COPY", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                TEMPLATES_TABLES model = db.TEMPLATES_TABLES.Find(id);
                if (model == null)
                    throw new Exception(NOT_FOUND);

                var lst_columns = (from d in db.TEMPLATES_TABLES_COLUMNS
                                   where d.TableTemplateKey == model.dKey
                                   orderby d.Sorted
                                   select d).ToList();

                using (TransactionScope scope = new TransactionScope())
                {
                    TEMPLATES_TABLES model_copy = new TEMPLATES_TABLES();
                    foreach (PropertyInfo p_model in model_copy.GetType().GetProperties())
                    {
                        p_model.SetValue(model_copy, model.GetType().GetProperty(p_model.Name).GetValue(model, null));
                    }
                    model_copy.dKey = 0;
                    model_copy.ItemCode = "(copy) - " + model_copy.ItemCode;
                    model_copy.ItemName = "(copy) - " + model_copy.ItemName;
                    model_copy.RecordStatus = 1;
                    model_copy.CreatedBy = MyAccount.Model.dKey.ToString();
                    model_copy.ModifiedBy = MyAccount.Model.dKey.ToString();
                    model_copy.CreatedOn = Now;
                    model_copy.ModifiedOn = Now;
                    db.TEMPLATES_TABLES.Add(model_copy);
                    db.SaveChanges();

                    lst_columns.ForEach(col =>
                    {
                        TEMPLATES_TABLES_COLUMNS col_copy = new TEMPLATES_TABLES_COLUMNS();
                        foreach (PropertyInfo p_column in col_copy.GetType().GetProperties())
                        {
                            p_column.SetValue(col_copy, col.GetType().GetProperty(p_column.Name).GetValue(col, null));
                        }
                        col_copy.dKey = 0;
                        col_copy.TableTemplateKey = model_copy.dKey;

                        db.TEMPLATES_TABLES_COLUMNS.Add(col_copy);
                    });
                    db.SaveChanges();
                    scope.Complete();
                    result.Data = model_copy.dKey;
                }
                result.Result_Caption = "Sao chép thành công";
                result.Result_Message = "Đã sao chép bảng phụ lục";
                result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, result.Data);
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Caption,
                    model.dKey.ToString(),
                    model.ItemCode,
                    model.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
