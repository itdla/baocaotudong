﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;

namespace AutoReport.Controllers
{
    public class RPT_TEMPLATES_TABLENAMEController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();

        private string m_ControllerName = "RPT_TEMPLATES_TABLENAME";

        private string m_RegEx_Match = @"^[a-zA-Z][a-zA-Z0-9_]+$";
        // GET: RPT_TEMPLATES_TABLENAME/Index
        public ActionResult Index()
        {
            if (!CheckPermission("TEMPLATES_TABLENAME"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: RPT_TEMPLATES_TABLENAME/Create
        public ActionResult Create()
        {
            if (!CheckPermission("TEMPLATES_TABLENAME_ADD"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            RPT_TEMPLATES_TABLENAME item = new RPT_TEMPLATES_TABLENAME();
            item.AutoIndex = true;
            ViewBag.ControllerName = m_ControllerName;
            return View(item);
        }

        // POST: RPT_TEMPLATES_TABLENAME/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(RPT_TEMPLATES_TABLENAME iteminfo, List<string[]> lst_columns)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                result.strMess = CheckBeforeSave(iteminfo, true);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                iteminfo.Rpt_Templates_TableNameKey = Guid.NewGuid();
                iteminfo.RecordStatus = 1;
                iteminfo.CreatedBy = MyAccount.UserID;
                iteminfo.ModifiedBy = MyAccount.UserID;
                iteminfo.CreatedOn = DateTime.Now;
                iteminfo.ModifiedOn = DateTime.Now;

                db.RPT_TEMPLATES_TABLENAME.Add(iteminfo);

                int _col_sorted = 0;

                for (int i = 0; i < lst_columns.Count; i++)
                {
                    string[] arr_col = lst_columns[i];
                    if (string.IsNullOrEmpty(arr_col[1].Trim()) && string.IsNullOrEmpty(arr_col[2].Trim()))
                        continue;
                    _col_sorted = 0;
                    RPT_TEMPLATES_TABLECOLUMN item_column = new RPT_TEMPLATES_TABLECOLUMN();
                    item_column.Rpt_Templates_TableColumnKey = Guid.NewGuid();
                    item_column.Rpt_Templates_TableNameKey = iteminfo.Rpt_Templates_TableNameKey;
                    item_column.ColumnName = arr_col[1].Trim().Replace(" ", "");
                    if (string.IsNullOrEmpty(item_column.ColumnName))
                        throw new Exception(string.Format("Dòng {0} bảng phụ lục: mã quy định chưa xác định", i + 1));

                    item_column.ColumnCaption = arr_col[2];
                    if (string.IsNullOrEmpty(item_column.ColumnCaption))
                        throw new Exception(string.Format("Dòng {0} bảng phụ lục: tiêu đề chưa xác định", i + 1));

                    int.TryParse(arr_col[3], out _col_sorted);
                    item_column.ColumnOrder = _col_sorted;
                    item_column.RecordStatus = 1;

                    db.RPT_TEMPLATES_TABLECOLUMN.Add(item_column);
                }

                db.SaveChanges();
                result.Data = iteminfo;
                result.strMess = "Đã lưu thông tin";
                CreateLog(m_ControllerName, "Create", iteminfo.Rpt_Templates_TableNameKey.ToString(), iteminfo.TableID, iteminfo.TableName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        // GET: RPT_TEMPLATES_TABLENAME/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (!CheckPermission("TEMPLATES_TABLENAME_EDIT"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            RPT_TEMPLATES_TABLENAME item = db.RPT_TEMPLATES_TABLENAME.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            ViewBag.ControllerName = m_ControllerName;
            return View(item);
        }

        // POST: RPT_TEMPLATES_TABLENAME/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(RPT_TEMPLATES_TABLENAME iteminfo, List<string[]> lst_columns, string[] lst_columns_del)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                result.strMess = CheckBeforeSave(iteminfo, false);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                var oldItem = db.RPT_TEMPLATES_TABLENAME.Find(iteminfo.Rpt_Templates_TableNameKey);
                if (oldItem == null)
                    throw new Exception("không tìm thấy thông tin");

                string[] prop_set_list = { "TableID", "TableName", "Sorted", "ReportReferenceName" };
                foreach (string propname in prop_set_list)
                {
                    PropertyInfo pNew = iteminfo.GetType().GetProperty(propname);
                    if (pNew == null) continue;
                    object pNew_Value = pNew.GetValue(iteminfo);
                    oldItem.GetType().GetProperty(propname).SetValue(oldItem, pNew_Value, null);
                }

                oldItem.ModifiedBy = MyAccount.UserID;
                oldItem.ModifiedOn = DateTime.Now;
                db.Entry(oldItem).State = EntityState.Modified;

                #region delete row

                if (lst_columns_del != null && lst_columns_del.Length > 0)
                {
                    foreach (string del_id in lst_columns_del)
                    {
                        RPT_TEMPLATES_TABLECOLUMN itemdel = db.RPT_TEMPLATES_TABLECOLUMN.Find(Guid.Parse(del_id));
                        if (itemdel != null)
                            db.Entry(itemdel).State = EntityState.Deleted;
                    }
                }
                #endregion

                #region save row

                int _col_sorted = 0;
                for (int i = 0; i < lst_columns.Count; i++)
                {
                    string[] arr_col = lst_columns[i];
                    _col_sorted = 0;
                    RPT_TEMPLATES_TABLECOLUMN item_column = new RPT_TEMPLATES_TABLECOLUMN();
                    if (string.IsNullOrEmpty(arr_col[0]))
                    {
                        item_column.Rpt_Templates_TableColumnKey = Guid.NewGuid();
                        item_column.Rpt_Templates_TableNameKey = iteminfo.Rpt_Templates_TableNameKey;
                        item_column.ColumnName = arr_col[1];
                        if (string.IsNullOrEmpty(item_column.ColumnName))
                            throw new Exception(string.Format("Dòng {0} bảng phụ lục: mã quy định chưa xác định", i + 1));

                        item_column.ColumnCaption = arr_col[2];
                        if (string.IsNullOrEmpty(item_column.ColumnCaption))
                            throw new Exception(string.Format("Dòng {0} bảng phụ lục: tiêu đề chưa xác định", i + 1));

                        int.TryParse(arr_col[3], out _col_sorted);

                        item_column.ColumnOrder = _col_sorted;
                        item_column.RecordStatus = 1;

                        db.RPT_TEMPLATES_TABLECOLUMN.Add(item_column);
                    }
                    else
                    {
                        item_column = db.RPT_TEMPLATES_TABLECOLUMN.Find(Guid.Parse(arr_col[0]));
                        if (item_column != null)
                        {
                            if (string.IsNullOrEmpty(item_column.ColumnName))
                                throw new Exception(string.Format("Dòng {0} bảng phụ lục: mã quy định chưa xác định", i + 1));

                            item_column.ColumnCaption = arr_col[2];
                            if (string.IsNullOrEmpty(item_column.ColumnCaption))
                                throw new Exception(string.Format("Dòng {0} bảng phụ lục: tiêu đề chưa xác định", i + 1));

                            int.TryParse(arr_col[3], out _col_sorted);

                            item_column.ColumnOrder = _col_sorted;
                            item_column.RecordStatus = 1;

                            db.Entry(item_column).State = EntityState.Modified;
                        }
                    }
                }
                #endregion

                db.SaveChanges();
                result.Data = iteminfo;
                result.strMess = "Đã lưu thông tin";
                CreateLog(m_ControllerName, "Edit", iteminfo.Rpt_Templates_TableNameKey.ToString(), iteminfo.TableID, iteminfo.TableName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        // GET: RPT_TEMPLATES_TABLENAME/Delete/5
        public ActionResult Delete(Guid? id)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            result.strMess = "Đã xóa thông tin";
            RPT_TEMPLATES_TABLENAME item = db.RPT_TEMPLATES_TABLENAME.Find(id);
            try
            {
                if (!CheckPermission("TEMPLATES_TABLENAME_DELETE"))
                    throw new Exception(NO_PERMISSION);

                if (item == null)
                    throw new Exception("Không tìm thấy thông tin!");

                item.RecordStatus = 0;
                item.ModifiedBy = MyAccount.UserID;
                item.ModifiedOn = DateTime.Now;

                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                CreateLog(m_ControllerName, "Delete", item.Rpt_Templates_TableNameKey.ToString(), item.TableID, item.TableName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        private string CheckBeforeSave(RPT_TEMPLATES_TABLENAME iteminfo, bool isNew)
        {
            if (string.IsNullOrEmpty(iteminfo.TableID))
                return "Vui lòng điền mã phụ lục";
            iteminfo.TableID = iteminfo.TableID.Trim();
            iteminfo.TableID = iteminfo.TableID.Replace(" ", "");
            Match regmatch = Regex.Match(iteminfo.TableID, m_RegEx_Match);
            if (!regmatch.Success)
                return "Mã phụ lục nhập không đúng định dạng. Chỉ được nhập 'chữ', 'số', 'dấu gạch chân'. Bắt đầu bằng 'chữ'";
            if (string.IsNullOrEmpty(iteminfo.TableName))
                return "Vui lòng điền tên phụ lục";
            return string.Empty;
        }

        public JsonResult LoadList(int page, int pageSize, string TableID, string TableName, string Sorted, string ReportReferenceName)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            TableID = string.IsNullOrEmpty(TableID) ? "" : TableID;
            TableName = string.IsNullOrEmpty(TableName) ? "" : TableName;
            Sorted = string.IsNullOrEmpty(Sorted) ? "" : Sorted;
            ReportReferenceName = string.IsNullOrEmpty(ReportReferenceName) ? "" : ReportReferenceName;

            //Guid Document_ID = new Guid(this.Request["id"]);
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(TableID))
                table.KeysSearch.Add("TableID", TableID);
            if (!string.IsNullOrEmpty(TableName))
                table.KeysSearch.Add("TableName", TableName);
            if (!string.IsNullOrEmpty(Sorted))
                table.KeysSearch.Add("Sorted", Sorted);
            if (!string.IsNullOrEmpty(ReportReferenceName))
                table.KeysSearch.Add("ReportReferenceName", ReportReferenceName);

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            var searchQuery = from f in db.RPT_TEMPLATES_TABLENAME
                              orderby f.TableID
                              where f.RecordStatus == 1
                              select f;

            var fq1 = searchQuery.ToList().Select(p => new
            {
                p.Rpt_Templates_TableNameKey,
                p.TableID,
                p.TableName,
                p.Sorted,
                p.ReportReferenceName
            }).OrderBy(p => p.ReportReferenceName).ThenBy(p => p.Sorted).ToList();

            if (!string.IsNullOrEmpty(TableID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.TableID.Trim().ToLower()).Contains(Utils.RemoveVni(TableID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(TableName))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.TableName.Trim().ToLower()).Contains(Utils.RemoveVni(TableName.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(Sorted))
                fq1 = fq1.Where(p => Convert.ToString(p.Sorted) == Sorted.Trim()).ToList();
            if (!string.IsNullOrEmpty(ReportReferenceName))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.ReportReferenceName.Trim().ToLower()).Contains(Utils.RemoveVni(ReportReferenceName.Trim().ToLower()))).ToList();

            int _index = 1;
            var fq2 = fq1.ToList().Select(d => new
            {
                d.Rpt_Templates_TableNameKey,
                STT = (_index++).ToString(),
                d.TableID,
                d.TableName,
                d.Sorted,
                d.ReportReferenceName,
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/Edit/" + d.Rpt_Templates_TableNameKey + "'>Sửa</a>" +
                "<button type=\"button\" class=\"btn btn-danger btn_xoa\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.Rpt_Templates_TableNameKey + "'>Xóa </button>",
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "ItemKey", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã phụ lục", IsShow = true, FieldName = "TableID", Format = "", Width = 20, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên phụ lục", IsShow = true, FieldName = "TableName", Format = "", Width = 33, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Thứ tự", IsShow = true, FieldName = "Sorted", Format = "", Width = 8, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Thuộc báo cáo", IsShow = true, FieldName = "ReportReferenceName", Format = "", Width = 20, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 12, Width_Measure = "%" });
            //

            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_Templates_TableNameKey.ToString();
                item.isShowDelete = false;
                item.isShowDetail = false;
                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadColumnList(Guid? Templates_TableNameKey)
        {
            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Rpt_Templates_TableColumnKey",
                Field = "Rpt_Templates_TableColumnKey",
                DataType = GridDetail.eDataType.IntegerType,
                Width = "2%",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Mã quy định",
                Field = "ColumnName",
                DataType = GridDetail.eDataType.StringType,
                Width = "18%",
                Enable = true,
                IsShow = true
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tiêu đề",
                Field = "ColumnCaption",
                DataType = GridDetail.eDataType.StringType,
                Width = "60%",
                Enable = true,
                IsShow = true
            });

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Vị trí",
                Field = "ColumnOrder",
                DataType = GridDetail.eDataType.IntegerType,
                Width = "10%",
                Enable = true,
                IsShow = true
            });
            var zDetails = db.RPT_TEMPLATES_TABLECOLUMN.Where(p => p.Rpt_Templates_TableNameKey == Templates_TableNameKey && p.RecordStatus == 1).OrderBy(p => p.ColumnOrder).ToList();

            if (zDetails.Count > 0)
            {
                for (int i = 0; i < zDetails.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                        zDetails[i].Rpt_Templates_TableColumnKey,
                        zDetails[i].ColumnName,
                        zDetails[i].ColumnCaption,
                        zDetails[i].ColumnOrder
                    });
                }
            }
            else
            {
                arrBody.Add(new object[grid.Header.Count]);
            }
            grid.Data = arrBody;
            return new CustomJsonResult { Data = grid };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
