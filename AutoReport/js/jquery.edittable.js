﻿/*! editTable v0.1.1 by Alessandro Benoit */
(function ($, window, i) {

    'use strict';

    $.fn.editTable = function (options) {

        // Settings
        var s = $.extend({
            name: '',
            data: [['']],
            jsonData: false,
            headerCols: false,
            action: true,
            maxRows: 999,
            delrow: null,
        }, options),
            $el = $(this),
            defaultTableContent = '<thead><tr></tr></thead><tbody></tbody>',
            $table = $('<table/>', {
                class: 'inputtable' + ((!s.headerCols) ? ' wh' : ''),
                html: defaultTableContent
            }),
            defaultth = '<th><a class="addcol icon-button" href="#">+</a> <a class="delcol icon-button" href="#">-</a></th>',

            colnumber,
            rownumber,
            reset;

        // Increment for IDs
        i = i + 1;

        var mainRow = 0;
        // Build cell
        function buildCell(content, header, row, col) {

            content = (content === 0) ? "0" : (content || '').toString();

            var zTabIndex = header.Enable == false ? ' tabIndex="-1"  ' : ' ';

            var zDisable = header.Enable == false ? ' readonly ' : '';
            var placeholdertext = header.Enable == false ? '' : header.PlaceHolderText;

            var align = '';
            if (header.Align == 0) {

            } else if (header.Align == 1) {
                align = ' style="text-align: left;" ';
            } else if (header.Align == 2) {
                align = ' style="text-align: center;" ';
            } else {
                align = ' style="text-align: right;" ';
            }
            if (header.LayoutType == 3) {
                //hide
                return '<td style="display: none;"><input data-row="' + row + '" data-col="' + col + '" class="' + header.Field + ' ' + header.Field + '_' + row + '" type="text" name="" value="' + content.replace(/"/g, "&quot;") + '" ' + align + ' placeholder="' + placeholdertext + '" /></td>';
            }
            if (header.LayoutType == 2) {


                //DropdownList
                var zHtml = '<td>';
                zHtml += '<select data-row="' + row + '" data-col="' + col + '"  ' + zTabIndex + ' class="form-control edtitable ' + header.Field + ' ' + header.Field + "_" + row + ' name="" class="form-control" data-live-search="true" ' + zDisable + '><option>-Vui lòng chọn-</option>';
                for (var i = 0; i < header.DataSource.length; i++) {
                    var zSource = header.DataSource[i];

                    if (content != zSource.Value)
                        zHtml += '<option value="' + zSource.Value + '">' + zSource.Text + '</option>';
                    else
                        zHtml += '<option value="' + zSource.Value + '" selected>' + zSource.Text + '</option>';
                }
                zHtml += '</select></td>';
                return zHtml;
            } else {

                if (header.DataType == 0) {
                    //string
                    return '<td><input data-row="' + row + '" data-col="' + col + '" ' + zTabIndex + ' class="edtitable ' + header.Field + ' ' + header.Field + '_' + row + '" type="text" name="" value="' + content.replace(/"/g, "&quot;") + '" ' + align + zDisable + ' placeholder="' + placeholdertext + '" /></td>';
                }
                else if (header.DataType == 1) {
                    //int
                    if (header.LayoutType == 0) {
                        //STT
                        //return '<td><input data-row="' + row + '" class="int ' + header.Field + ' ' + header.Field + '_' + row + '" type="text" name="" value="' + row + '" ' + align + '/></td>';
                        return '<td><input data-row="' + row + '" data-col="' + col + '" ' + zTabIndex + ' class="edtitable int ' + header.Field + ' ' + header.Field + '_' + row + '" type="text" name="" value="' + content.replace(/"/g, "&quot;") + '" ' + align + zDisable + ' placeholder="' + placeholdertext + '" /></td>';
                    } else {
                        return '<td><input data-row="' + row + '" data-col="' + col + '" ' + zTabIndex + ' class="edtitable int ' + header.Field + ' ' + header.Field + '_' + row + '" type="text" name="" value="' + content.replace(/"/g, "&quot;") + '" ' + align + zDisable + ' placeholder="' + placeholdertext + '" /></td>';
                    }

                }
                else if (header.DataType == 2) {
                    //decimal
                    return '<td><input data-row="' + row + '" data-col="' + col + '" ' + zTabIndex + ' class="edtitable dec ' + header.Field + ' ' + header.Field + '_' + row + '"  type="text" name="" value="' + content.replace(/"/g, "&quot;") + '" ' + align + zDisable + ' placeholder="' + placeholdertext + '" /></td>';
                }
                else if (header.DataType == 3) {
                    //date
                    return '<td><input data-row="' + row + '" data-col="' + col + '" ' + zTabIndex + ' class="edtitable date ' + header.Field + ' ' + header.Field + '_' + row + '" type="text" name="" value="' + content.replace(/"/g, "&quot;") + '" ' + align + zDisable + ' placeholder="' + placeholdertext + '" /></td>';
                }
                else if (header.DataType == 4) {
                    //time
                    return '<td><input data-row="' + row + '" data-col="' + col + '" ' + zTabIndex + ' class="edtitable time' + header.Field + ' ' + header.Field + '_' + row + '"  type="text" name="" value="' + content.replace(/"/g, "&quot;") + '" ' + align + zDisable + ' placeholder="' + placeholdertext + '" /></td>';
                }
                else if (header.DataType == 3) {
                    //datetime
                    return '<td><input data-row="' + row + '" data-col="' + col + '" ' + zTabIndex + ' class="edtitable datetime' + header.Field + ' ' + header.Field + '_' + row + '" type="text" name="" value="' + content.replace(/"/g, "&quot;") + '" ' + align + zDisable + ' placeholder="' + placeholdertext + '" /></td>';
                }
                else if (header.DataType == 6) {
                    //datetime
                    return '<td><img data-row="' + row + '" data-col="' + col + '" ' + zTabIndex + ' class="edtitable ' + header.Field + ' ' + header.Field + '_' + row + '" name="" src="' + content.replace(/"/g, "&quot;") + '" ' + align + zDisable + '/></td>';
                }
            }

            return '<td><input data-row="' + row + '" data-col="' + col + '" ' + zTabIndex + ' class="edtitable ' + header.Field + ' ' + header.Field + '_' + row + '" type="text" name="" value="' + content.replace(/"/g, "&quot;") + '" ' + align + zDisable + ' placeholder="' + placeholdertext + '" /></td>';
        }

        // Build row
        function buildRow(data, len) {

            mainRow++;

            var rowcontent = '', b;

            data = data || '';

            for (b = 0; b < (len || data.length); b += 1) {
                var header = s.headerCols[b];
                rowcontent += buildCell(data[b], header, mainRow, b);
            }
            if (s.action) {
                return $('<tr/>', {
                    html: rowcontent + '<td data-row="' + mainRow + '"><a tabIndex="-1"  data-row="' + mainRow + '" class="addrow icon-button"  href="#">+</a> <a tabIndex="-1" data-row="' + mainRow + '" class="delrow icon-button" href="#">-</a></td>'
                });
            }
            else {
                return $('<tr/>', {
                    html: rowcontent + '<td style="display: none;" data-row="' + mainRow + '"><a tabIndex="-1" data-row="' + mainRow + '" class="addrow icon-button"  href="#">+</a> <a tabIndex="-1" data-row="' + mainRow + '" class="delrow icon-button" href="#">-</a></td>'
                });
            }
        }

        // Check button status (enable/disabled)
        function checkButtons() {
            if (colnumber < 2) {
                $table.find('.delcol').addClass('disabled');
            }
            if (rownumber < 2) {
                $table.find('.delrow').addClass('disabled');
            }
            if (s.maxRows && rownumber === s.maxRows) {
                $table.find('.addrow').addClass('disabled');
            }
        }

        // Fill table with data
        function fillTableData(data) {

            var a, crow = Math.min(s.maxRows, data.length);

            // Clear table
            $table.html(defaultTableContent);

            // Populate table headers
            if (s.headerCols) {
                // Fixed columns
                for (a = 0; a < s.headerCols.length; a += 1) {
                    if (s.headerCols[a].LayoutType == 3) {
                        $table.find('thead tr').append('<th style="display: none;" width="' + s.headerCols[a].Width + '">' + s.headerCols[a].Caption + '</th>');
                    } else {
                        $table.find('thead tr').append('<th width="' + s.headerCols[a].Width + '">' + s.headerCols[a].Caption + '</th>');
                    }

                }
                if (data.length != 0) {
                    for (a = 0; a < crow; a += 1) {
                        buildRow(data[a], s.headerCols.length).appendTo($table.find('tbody'));
                    }
                }
                else {
                    var html = '<tr><td colspan="' + (s.headerCols.headerCols.length + 1) + '">Không có dữ liệu</td>';
                    $table.find('tbody').append(html);
                }

            } else if (data[0]) {
                // Variable columns
                for (a = 0; a < data[0].length; a += 1) {
                    $table.find('thead tr').append(defaultth);
                }
                for (a = 0; a < crow; a += 1) {
                    buildRow(data[a]).appendTo($table.find('tbody'));
                }
            }

            // Append missing th
            //if (s.action)
            if (s.action)
                $table.find('thead tr').append('<th style="width: 6%;"></th>');
            else
                $table.find('thead tr').append('<th style="display: none;"></th>');
            // Count rows and columns
            colnumber = $table.find('thead th').length - 1;
            rownumber = $table.find('tbody tr').length;

            checkButtons();
        }

        // Export data
        function exportData() {
            var row = 0, data = [];

            $table.find('tbody tr').each(function () {

                row += 1;
                data[row] = [];

                $(this).find('input').each(function () {
                    data[row].push($(this).val());
                });

            });

            // Remove undefined
            data.splice(0, 1);

            return data;
        }

        // Fill the table with data from textarea or given properties
        if ($el.is('textarea')) {

            try {
                reset = JSON.parse($el.val());
            } catch (e) {
                reset = s.data;
            }

            $el.after($table);

            // If inside a form set the textarea content on submit
            if ($table.parents('form').length > 0) {
                $table.parents('form').submit(function () {
                    $el.val(JSON.stringify(exportData()));
                });
            }

        } else {
            reset = (JSON.parse(s.jsonData) || s.data);
            $el.append($table);
        }

        fillTableData(reset);

        // Add column
        $table.on('click', '.addcol', function () {

            var colid = parseInt($(this).closest('tr').children().index($(this).parent('th')), 10);

            colnumber += 1;

            $table.find('thead tr').find('th:eq(' + colid + ')').after(defaultth);

            $table.find('tbody tr').each(function () {
                $(this).find('td:eq(' + colid + ')').after(buildCell());
            });

            $table.find('.delcol').removeClass('disabled');

            return false;
        });

        // Remove column
        $table.on('click', '.delcol', function () {

            if ($(this).hasClass('disabled')) {
                return false;
            }

            var colid = parseInt($(this).closest('tr').children().index($(this).parent('th')), 10);

            colnumber -= 1;

            checkButtons();

            $(this).parent('th').remove();

            $table.find('tbody tr').each(function () {
                $(this).find('td:eq(' + colid + ')').remove();
            });

            return false;
        });

        // Add row
        $table.on('click', '.addrow', function () {

            if ($(this).hasClass('disabled')) {
                return false;
            }

            rownumber += 1;

            $(this).closest('tr').after(buildRow(0, colnumber));

            $table.find('.delrow').removeClass('disabled');

            checkButtons();
            UpdateMask();
            return false;
        });

        // Delete row
        $table.on('click', '.delrow', function () {
            if (s.delrow) {
                s.delrow($(this).attr('data-row'));
            }
            if ($(this).hasClass('disabled')) {
                return false;
            }

            rownumber -= 1;

            checkButtons();

            $(this).closest('tr').remove();

            $table.find('.addrow').removeClass('disabled');

            return false;
        });

        //var zIsActive = null;

        $table.on('keydown', 'input', function (event) {



            var zRow = $(this).attr('data-row');
            var zCol = $(this).attr('data-col');



            if (event.which == 37) {
                //left
                if ($(this).prop('selectionStart') == 0) {
                    if (zCol > 0) {
                        for (var i = zCol-1; i > 0; i--) {
                            if (s.headerCols[i].LayoutType != 3 && s.headerCols[i].Enable) {
                                zCol = i;
                                break;
                            }
                        }
                    }
                }
                
            } else if (event.which == 38) {
                //up
                if (zRow > 0) {
                    zRow--;
                }
            } else if (event.which == 39) {
                //right
                if ($(this).prop('selectionEnd') == $(this).val().length) {
                    for (var i = parseInt(zCol) + 1; i < s.headerCols.length; i++) {
                        if (s.headerCols[i].LayoutType != 3 && s.headerCols[i].Enable) {
                            zCol = i;
                            break;
                        }
                    }
                }
               
            } else if (event.which == 40) {
                //Down
                if (zRow < rownumber) {
                    zRow++;
                }
            } else if (event.which == 9) {
                //Enter
                //2019/02/20 Minh editted
                //event.which == 13
                //remove enter key, only tab key will add new row
                if (s.action) {
                    var zLastCol = null;
                    var zFirstCol = null;
                    for (var i = 0; i < s.headerCols.length; i++) {
                        if (s.headerCols[i].LayoutType != 3 && s.headerCols[i].Enable) {
                            zLastCol = i;
                            if (zFirstCol == null) {
                                zFirstCol = i;
                            }
                        }
                    }

                    if (zLastCol == zCol && zRow == rownumber) {

                        rownumber += 1;

                        $(this).closest('tr').after(buildRow(0, colnumber));

                        $table.find('.delrow').removeClass('disabled');
                        checkButtons();
                        UpdateMask();

                        zRow++;

                        if (s.headerCols[zFirstCol].LayoutType == 2) {
                            //$("input[data-row='" + zRow + "'][data-col='" + zFirstCol + "']").data('selectpicker').$searchbox.focus();
                        } else {
                            $table.find("input[data-row='" + zRow + "'][data-col='" + zFirstCol + "']").focus();
                            return false;
                        }

                        return;
                    }
                }
                
            }


            if (zRow != $(this).attr('data-row') || zCol != $(this).attr('data-col')) {
                if (s.headerCols[zCol].LayoutType == 2) {
                    //$("input[data-row='" + zRow + "'][data-col='" + zCol + "']").data('selectpicker').$button.focus();
                } else {
                    $table.find("input[data-row='" + zRow + "'][data-col='" + zCol + "']").focus();
                    return false;
                }
            }
        });

        // Select all content on click
        $table.on('click', 'input', function () {
            $(this).select();
        });

        // Return functions
        return {
            // Get an array of data
            getData: function () {
                return exportData();
            },
            // Get the JSON rappresentation of data
            getJsonData: function () {
                return JSON.stringify(exportData());
            },
            // Load an array of data
            loadData: function (data) {
                fillTableData(data);
            },
            // Load a JSON rappresentation of data
            loadJsonData: function (data) {
                fillTableData(JSON.parse(data));
            },
            // Reset data to the first instance
            reset: function () {
                fillTableData(reset);
            }
        };
    };

})(jQuery, this, 0);