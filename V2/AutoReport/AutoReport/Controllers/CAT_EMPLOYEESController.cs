﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using System.Transactions;
using AutoReport.Utils;
using System.Reflection;

namespace AutoReport.Controllers
{
    public class CAT_EMPLOYEESController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "CAT_EMPLOYEES";

        // GET: CAT_EMPLOYEES/Index
        public ActionResult Index()
        {
            if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: CAT_EMPLOYEES/Create
        public ActionResult Create()
        {
            if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;

            var lst_items = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).ToList();

            #region query data

            var query_emp = (from e in db.CAT_EMPLOYEES
                             where e.RecordStatus == 1
                             orderby e.ItemName
                             select new
                             {
                                 e.dKey,
                                 e.ItemCode,
                                 e.ItemName
                             }).ToList();

            var query_department = (from e in db.CAT_DEPARTMENTS
                                    where e.RecordStatus == 1
                                    orderby e.ItemName
                                    select new
                                    {
                                        e.dKey,
                                        e.ItemCode,
                                        e.ItemName
                                    }).ToList();


            var query_position = (from e in db.CAT_POSITIONS
                                  where e.RecordStatus == 1
                                  orderby e.ItemName
                                  select new
                                  {
                                      e.dKey,
                                      e.ItemCode,
                                      e.ItemName
                                  }).ToList();

            var query_gender = db.CAT_GENDERS.ToList();

            #endregion

            CAT_EMPLOYEES item = new CAT_EMPLOYEES()
            {
                ItemCode = "(Tự động phát sinh)",
                Sorted = lst_items.Count == 0 ? 1 : lst_items[lst_items.Count - 1].Sorted + 1,
                Gender = 0,
                WorkStatus = 1
            };

            ViewBag.ManagerList = new SelectList(query_emp, "dKey", "ItemName");
            ViewBag.DepartmentList = new SelectList(query_department, "dKey", "ItemName");
            ViewBag.PositionList = new SelectList(query_position, "dKey", "ItemName");
            ViewBag.GenderList = new SelectList(query_gender, "GenderKey", "GenderName");
            ViewBag.WorkStatusList = new SelectList(GetList_Employee_WorkStatus(), "Value", "Text");

            return View(item);
        }

        [HttpPost]
        public ActionResult Create(CAT_EMPLOYEES ItemModel, string SaveType)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, true);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);

                //auto-gen new item code
                var lst_items = db.CAT_EMPLOYEES.ToList();
                if (lst_items.Count > 0)
                {
                    ItemModel.ItemCode = lst_items[lst_items.Count - 1].ItemCode;
                    ItemModel.ItemCode = ItemModel.ItemCode.Replace("NV", "");
                    ItemModel.ItemCode = (Convert.ToInt32(ItemModel.ItemCode) + 1).ToString();

                    if (ItemModel.ItemCode.Length < 6)
                        ItemModel.ItemCode = Convert.ToInt32(ItemModel.ItemCode).ToString("000000");
                }
                else
                {
                    ItemModel.ItemCode = "NV000001";
                }

                //save master
                ItemModel.CreatedBy = MyAccount.Model.dKey.ToString();
                ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                ItemModel.CreatedOn = Now;
                ItemModel.ModifiedOn = Now;
                ItemModel.RecordStatus = 1;

                db.CAT_EMPLOYEES.Add(ItemModel);
                db.SaveChanges();

                result.Result_Status = 1;
                result.Result_Message = CREATE_SUCCESS;
                if (SaveType == "savenew")
                    result.RedirectTo = string.Format("/{0}/Create", m_ControllerName);
                else
                    result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        // GET: CAT_EMPLOYEES/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                return B_NotPermission();

            ViewBag.ControllerName = m_ControllerName;
            CAT_EMPLOYEES item = db.CAT_EMPLOYEES.Find(id);
            if (item == null)
                return B_NotFound();
            if (item.RecordStatus == 0)
                return B_NotFound();

            item.CreatedBy = GetCreatorName(item.CreatedBy);
            item.ModifiedBy = GetCreatorName(item.ModifiedBy);

            #region query data

            var query_emp = (from e in db.CAT_EMPLOYEES
                             where e.RecordStatus == 1
                             orderby e.ItemName
                             select new
                             {
                                 e.dKey,
                                 e.ItemCode,
                                 e.ItemName
                             }).ToList();

            var query_department = (from e in db.CAT_DEPARTMENTS
                                    where e.RecordStatus == 1
                                    orderby e.ItemName
                                    select new
                                    {
                                        e.dKey,
                                        e.ItemCode,
                                        e.ItemName
                                    }).ToList();


            var query_position = (from e in db.CAT_POSITIONS
                                  where e.RecordStatus == 1
                                  orderby e.ItemName
                                  select new
                                  {
                                      e.dKey,
                                      e.ItemCode,
                                      e.ItemName
                                  }).ToList();

            var query_gender = db.CAT_GENDERS.ToList();

            #endregion

            ViewBag.ManagerList = new SelectList(query_emp, "dKey", "ItemName");
            ViewBag.DepartmentList = new SelectList(query_department, "dKey", "ItemName");
            ViewBag.PositionList = new SelectList(query_position, "dKey", "ItemName");
            ViewBag.GenderList = new SelectList(query_gender, "GenderKey", "GenderName");
            ViewBag.WorkStatusList = new SelectList(GetList_Employee_WorkStatus(), "Value", "Text");

            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(CAT_EMPLOYEES ItemModel)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, false);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);
                CAT_EMPLOYEES oldItem = db.CAT_EMPLOYEES.Find(ItemModel.dKey);
                if (oldItem == null)
                    throw new Exception(NOT_FOUND);

                //save model
                string[] arr_model = { "ItemName", "DepartmentKey", "PosistionKey", "ManagerKey", "Gender", "BirthDay", "Address", "Phone", "Email", "OnlineContact", "WorkStatus", "Sorted", "Remarks" };
                foreach (string pName in arr_model)
                {
                    PropertyInfo pInfo = oldItem.GetType().GetProperty(pName);
                    object v_to_set = ItemModel.GetType().GetProperty(pName).GetValue(ItemModel, null);
                    pInfo.SetValue(oldItem, v_to_set);
                }
                oldItem.ModifiedBy = MyAccount.Model.dKey.ToString();
                oldItem.ModifiedOn = Now;
                db.Entry(oldItem).State = EntityState.Modified;

                db.SaveChanges();
                result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);
                result.Result_Message = UPDATE_SUCCESS;
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    ItemModel.ItemCode,
                    ItemModel.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        private string CheckBeforeSave(CAT_EMPLOYEES ItemModel, bool isNew)
        {
            if (string.IsNullOrEmpty(ItemModel.ItemCode))
                return "Mã nhân viên không được để trống";
            if (string.IsNullOrEmpty(ItemModel.ItemName))
                return "Tên nhân viên không được để trống";

            return string.Empty;
        }

        // GET: CAT_EMPLOYEES/Delete/5
        public ActionResult Delete(int? id)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_DELETE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                CAT_EMPLOYEES model = db.CAT_EMPLOYEES.Find(id);
                if (model == null)
                    throw new Exception(NOT_FOUND);

                model.ModifiedBy = MyAccount.Model.dKey.ToString();
                model.ModifiedOn = Now;
                model.RecordStatus = 0;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                result.Result_Message = DELETE_SUCCESS;
                result.RedirectTo = string.Format("/{0}/Index", m_ControllerName);
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    model.dKey.ToString(),
                    model.ItemCode,
                    model.ItemName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetList(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                var lstItems = (from d in db.CAT_EMPLOYEES
                                where d.RecordStatus == 1
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.DepartmentKey,
                                    d.PosistionKey,
                                    d.WorkStatus
                                }).ToList();
                if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                    lstItems.Clear();

                //working state
                var lst_work_states = GetList_Employee_WorkStatus();

                var q1 = lstItems.Select(d => new
                {
                    d.dKey,
                    DepartmentName = db.CAT_DEPARTMENTS.Find(d.DepartmentKey)?.ItemName,
                    EmployeeCode = d.ItemCode,
                    EmployeeName = d.ItemName,
                    PositionName = db.CAT_POSITIONS.Find(d.PosistionKey)?.ItemName,
                    WorkState = lst_work_states.Where(p => p.Value == (d.WorkStatus ?? -1).ToString()).FirstOrDefault()?.Text
                }).ToList();

                TableModel table = new TableModel();
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "STT", Caption = "STT", Width = 9, Alignment = "center", Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "DepartmentName", Caption = "Phòng Ban", Width = 25, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "EmployeeCode", Caption = "Mã Nhân Viên", Width = 15, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "EmployeeName", Caption = "Tên Nhân Viên", Width = 25, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "PositionName", Caption = "Chức Vụ", Width = 15, Filtered = true });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "WorkState", Caption = "Tình Trạng", Width = 10, Filtered = true });

                int _index = 1;
                var q2 = q1.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.DepartmentName,
                    p.EmployeeCode,
                    p.EmployeeName,
                    p.PositionName,
                    p.WorkState
                }).ToList();

                #region manual filter

                string key;
                string filter_expr;
                foreach (TableHeaderColumns tcol in table.HeaderColumns.Where(p => p.Filtered).ToList())
                {
                    key = tcol.FieldName;
                    filter_expr = Request.QueryString[key];

                    if (!string.IsNullOrEmpty(filter_expr))
                    {
                        filter_expr = TextUtil.RemoveVni(Convert.ToString(filter_expr)).ToLower();

                        q2 = q2.Where(p => TextUtil.RemoveVni(Convert.ToString(p.GetType().GetProperty(key).GetValue(p, null)).ToLower()).Contains(filter_expr)).ToList();

                        table.FilterValues.Add(key, filter_expr ?? "");
                    }
                }

                #endregion

                table.CalcTotalPage(q2.Count);

                var data = q2.Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();
                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
