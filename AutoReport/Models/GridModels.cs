﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Models
{
    public class GridModels
    {
        public int intStatus;
        public string strMess;
        public int intTotalPage;
        public int intCurrentPage;
        public string strURLDetail;
        public string Session;
        public List<GridItem> lstData;
    }
    public class GridItem
    {
        public ItemData objData;
        public string ID;
    }

    public class ItemData
    {
        public string Image;
        public string Title;
        public string Line1;
        public string Line1Type;
        public int Line1Decimal = 0;
        public string Line2;
        public string Line2Type;
        public int Line2Decimal = 0;
        public string Line3;
        public string Line3Type;
        public int Line3Decimal = 0;
        public string ID;
    }
}