﻿using AutoReport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoReport.Controllers
{
    public class SystemController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();

        // GET: System
        public ActionResult Login()
        {
            ViewBag.AccountSaved = "0";
            if (Request.Cookies["lremember"] != null)
                ViewBag.AccountSaved = Request.Cookies["lremember"].Value;

            ViewBag.UserSaved = "";
            if (Request.Cookies["luser"] != null)
                ViewBag.UserSaved = Request.Cookies["luser"].Value;

            ViewBag.PasswordSaved = "";
            if (Request.Cookies["lpassword"] != null)
                ViewBag.PasswordSaved = Request.Cookies["lpassword"].Value;
            return View();
        }
        public ActionResult Logout()
        {
            this.MyAccount = null;
            return RedirectToAction("Login", "System");
        }
        public ActionResult ForgotPassword(string email)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                var item = db.CAT_EMPLOYEES.Where(p=>p.Email==email).ToList();
                if (item.Count==0)
                    throw new Exception("Không tìm thấy thông tin!");
                Guid employeekey = item[0].EmployeeKey;
                var user = db.SYS_Users.Where(p => p.EmployeeKey == employeekey).FirstOrDefault();
                string passwordnew = RandomString(6);
                user.UserPassword = Utils.CreateMD5(passwordnew);
                db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    var client = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587)
                    {
                        Credentials = new System.Net.NetworkCredential("duandla@daihoclongan.edu.vn", "duandla123456"),
                        EnableSsl = true
                    };
                    client.Send("duandla@daihoclongan.edu.vn", email, "[Sở KHCN] - Thay đổi mật khẩu tài khoản", String.Format(@"Chào {0}Yêu cầu thay đổi thông tin đăng nhập hệ thống: http://baocao.dla.edu.vn{1}Tài khoản: {2}Mật khẩu: {3}", user.UserName + Environment.NewLine, Environment.NewLine, user.UserName + Environment.NewLine, passwordnew));
                    result.strMess = "Vui lòng kiểm tra Email: " + email;
                    db.SaveChanges();
                    result.Data = "/";
                }
                catch (Exception ex) {
                    result.intStatus = 0;
                    result.strMess = ex.Message;
                }                
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult LoginCheck(string user, string password, bool rememberlogin)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                string password_encrypt = Utils.CreateMD5(password);
                if(user == SuperUserName && password_encrypt == SuperUserPassword)
                {
                    SYS_Users userSU = new SYS_Users();
                    userSU.Active = true;
                    userSU.UserID = Guid.Parse("F1F1F1F1-C1C1-D0D0-A5A5-D4C1F233E101");
                    userSU.UserName = SuperUserName;
                    userSU.UserPassword = SuperUserPassword;
                    userSU.RecordStatus = 1;

                    this.MyAccount = userSU;
                    IsSuperUser = true;
                    Utils.IsSUForMenu = IsSuperUser;
                }
                else
                {
                    SYS_Users _Users = db.SYS_Users.Where(p => p.UserName == user && p.UserPassword == password_encrypt).FirstOrDefault();
                    if (_Users == null)
                        throw new Exception("Tài khoản hoặc mật khẩu không chính xác");
                    if (_Users.RecordStatus != 1)
                        throw new Exception("Tài khoản không tồn tại hoặc đã bị xóa");
                    if (!_Users.Active)
                        throw new Exception("Tài khoản không còn hiệu lực. Vui lòng liên hệ người quản trị!");

                    this.MyAccount = _Users;
                    IsSuperUser = false;
                    Utils.IsSUForMenu = IsSuperUser;
                    string c_key = "luser";
                    if (Request.Cookies[c_key] != null)
                    {
                        Response.Cookies.Remove(c_key);
                    }
                    HttpCookie c_user = new HttpCookie(c_key, user);
                    Response.Cookies.Add(c_user);

                    c_key = "lpassword";
                    if (Request.Cookies[c_key] != null)
                    {
                        Response.Cookies.Remove(c_key);
                    }
                    HttpCookie c_password = new HttpCookie(c_key, rememberlogin ? password : "");
                    Response.Cookies.Add(c_password);

                    c_key = "lremember";
                    if (Request.Cookies[c_key] != null)
                    {
                        Response.Cookies.Remove(c_key);
                    }
                    HttpCookie c_remember = new HttpCookie(c_key, rememberlogin ? "1" : "0");
                    Response.Cookies.Add(c_remember);
                }
                result.Data = "/";
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}