﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Utils
{
    public class TableModel
    {
        public TableModel()
        {
            HeaderColumns = new List<TableHeaderColumns>();
            DataSource = new List<TableRowsData>();
            TotalPages = 1;
            PageSize = 30;
            TotalRows = 0;
            FilterValues = new Dictionary<string, object>();
            Selectable = true;
        }

        public bool Selectable
        {
            get;
            set;
        }

        public List<TableHeaderColumns> HeaderColumns
        {
            get;
            set;
        }

        public List<TableRowsData> DataSource
        {
            get;
            set;
        }

        public Dictionary<string, object> FilterValues
        {
            get;
            set;
        }

        public void CalcTotalPage(int itemscount)
        {
            TotalRows = itemscount;
            TotalPages = itemscount / PageSize + (itemscount % PageSize != 0 ? 1 : 0);
        }


        /// <summary>
        /// auto calc based on DataSource records count
        /// </summary>
        public int TotalPages
        {
            get;
            set;
        }

        /// <summary>
        /// Default: 30 items per page
        /// </summary>
        public int PageSize
        {
            get;
            set;
        }

        /// <summary>
        /// Retrieve value from CalcTotalPage
        /// </summary>
        public int TotalRows
        {
            get;
            private set;
        }
    }
}