﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Controllers
{
    public class AjaxResult
    {
        public int intStatus = -1;
        public string refstate;
        public string redirectlink;
        public string strMess;
        public object Data;
    }
}