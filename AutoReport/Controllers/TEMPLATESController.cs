﻿using AutoReport.Models;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace AutoReport.Controllers
{
    public class TEMPLATESController : BaseController
    {
        // GET: TEMPLATES
        AutoReportEntities db = new AutoReportEntities();

        private string m_ControllerName = "TEMPLATES";

        public ActionResult Index()
        {
            ViewBag.ControllerName = m_ControllerName;
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            return View();
        }

        public ActionResult Create()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View(new RPT_TEMPLATES());
        }

        public ActionResult Edit(Guid id)
        {
            ViewBag.ControllerName = m_ControllerName;
            var item = db.RPT_TEMPLATES.Find(id);
            ViewBag.FileRaw = "#";

            //lấy danh sách các file version của report
            var fileversion = (from p in db.RPT_TEMPLATES_VERSIONS
                               where p.Rpt_TemplatesKey == id
                               orderby p.ModifiedOn
                               select p).ToList().LastOrDefault();

            if (fileversion != null)
                ViewBag.FileRaw = string.Format("{0}?Time={1}", fileversion.FileReference, DateTime.Now.Ticks);
            return View(item);
        }

        public ActionResult Delete(Guid id)
        {
            AjaxResult zResult = new AjaxResult();
            zResult.intStatus = 1;
            var zOldItem = db.RPT_TEMPLATES.Where(p => p.Rpt_TemplatesKey == id).FirstOrDefault();

            try
            {
                if (!CheckPermission("TEMPLATES_Delete"))
                    throw new Exception("Bạn không có quyền thực hiện chức năng này");

                if (zOldItem == null)
                    throw new Exception("Không tìm thấy thông tin");

                zOldItem.RecordStatus = 0;
                db.Entry(zOldItem).State = EntityState.Modified;
                db.SaveChanges();
                zResult.Data = zOldItem;

                zResult.strMess = "Đã xóa thông tin";
                CreateLog(m_ControllerName, "Delete", zOldItem.Rpt_TemplatesKey.ToString(), zOldItem.Templates_ID, zOldItem.Templates_Name, System.Reflection.MethodBase.GetCurrentMethod().Name, zResult.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), zResult.strMess);
            }
            catch (Exception ex)
            {
                zResult.Data = null;
                zResult.intStatus = 0;
                zResult.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (zResult) };
        }

        public JsonResult LoadList(int page, int pageSize, string Templates_ID, string Templates_Name)
        {
            //Guid Document_ID = new Guid(this.Request["id"]);
            Templates_ID = Templates_ID ?? "";
            Templates_Name = Templates_Name ?? "";

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(Templates_ID))
                table.KeysSearch.Add("Templates_ID", Templates_ID);
            if (!string.IsNullOrEmpty(Templates_Name))
                table.KeysSearch.Add("Templates_Name", Templates_Name);

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            var searchQuery = (from f in db.RPT_TEMPLATES
                               where f.RecordStatus == 1
                               select f).ToList();

            //lấy danh sách các file version của report
            var lst_templatekey = searchQuery.Select(p => p.Rpt_TemplatesKey).ToList();
            var lst_fileversion = (from p in db.RPT_TEMPLATES_VERSIONS
                                   where lst_templatekey.Contains(p.Rpt_TemplatesKey)
                                   select p).ToList();

            var fq1 = searchQuery.Select(p => new
            {
                p.Rpt_TemplatesKey,
                p.Templates_ID,
                p.Templates_Name,
                FileReference = lst_fileversion.Where(v => v.Rpt_TemplatesKey == p.Rpt_TemplatesKey).OrderBy(v => v.ModifiedOn).LastOrDefault()?.FileReference ?? "",
                p.FileName,
                SoTruong = db.RPT_TEMPLATES_DETAIL_VALUECONST.Where(d => d.Rpt_TemplatesKey == p.Rpt_TemplatesKey).Count(),
                SoPL = db.RPT_TEMPLATES_DETAIL_TABLEINDEX.Where(d => d.Rpt_TemplatesKey == p.Rpt_TemplatesKey).Count(),
                p.Note
            }).ToList();

            if (!string.IsNullOrEmpty(Templates_ID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Templates_ID.Trim().ToLower()).Contains(Utils.RemoveVni(Templates_ID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(Templates_Name))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Templates_Name.Trim().ToLower()).Contains(Utils.RemoveVni(Templates_Name.Trim().ToLower()))).ToList();

            int _index = 1;
            var fq2 = fq1.ToList().Select(d => new
            {
                d.Rpt_TemplatesKey,
                STT = _index++,
                d.Templates_ID,
                d.Templates_Name,
                d.SoTruong,
                d.SoPL,
                Tenfile = string.IsNullOrEmpty(d.FileReference) ? "-" : "<a href='" + d.FileReference + "?Time=" + DateTime.Now.Ticks + "' target='_blank'>Tải Về</a>",
                d.Note,
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/TEMPLATES/Edit/" + d.Rpt_TemplatesKey + "'>Sửa</a>" +
                "<button type=\"button\" class=\"btn btn-danger btn_xoa\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.Rpt_TemplatesKey + "'>Xóa</button>",
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã biểu mẫu", IsShow = true, Format = "", Width = 15, Width_Measure = "%", FieldName = "Templates_ID" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên biểu mẫu", IsShow = true, Format = "", Width = 25, Width_Measure = "%", FieldName = "Templates_Name" });
            table.lstHeader.Add(new TableHeader() { Caption = "Số Trường", IsShow = true, Format = "", Width = 8, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Số Phụ Lục", IsShow = true, Format = "", Width = 8, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "File", IsShow = true, Format = "", Width = 8, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Ghi Chú", IsShow = true, Format = "", Width = 17, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 12, Width_Measure = "%" });


            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_TemplatesKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public JsonResult LoadList_FileVersion(int page, int pageSize, string SearchKey)
        {

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = false;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            Guid templatekey = Guid.Parse(SearchKey);

            var searchQuery = db.RPT_TEMPLATES_VERSIONS.Where(p => p.Rpt_TemplatesKey == templatekey).OrderBy(p => p.ModifiedOn).ToList();
            table.intTotalPage = 1;

            //hiện số records giới hạn cho mỗi trang
            int _index = 1;
            var data = searchQuery.Select(p => new
            {
                p.Rpt_TemplatesVersionKey,
                STT = _index++,
                p.FileName,
                FileReference = "<a href='" + p.FileReference + "?Time=" + DateTime.Now.Ticks + "' target='_blank'><u>File</u></a>",
                UserUpload = db.SYS_Users.Find(p.ModifiedBy)?.UserName ?? "-",
                p.ModifiedOn
            }).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên File", IsShow = true, Format = "", Width = 35, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tải Về", IsShow = true, Format = "", Width = 10, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Người tải lên", IsShow = true, Format = "", Width = 18, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Ngày tải lên", IsShow = true, Format = "", Width = 20, Width_Measure = "%" });
            //

            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_TemplatesVersionKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadValueList(Guid? TemplatesKey)
        {
            List<object> arrBody = new List<object>();
            string[] arrHDR = { "", "", "" };
            GridDetail grid = new GridDetail();
            grid.ShowAction = false;

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Rpt_Templates_Detail_ValueConstKey",
                Field = "Rpt_Templates_Detail_ValueConstKey",
                DataType = GridDetail.eDataType.IntegerType,
                Width = "20",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Mã trường",
                Field = "CodeID",
                DataType = GridDetail.eDataType.StringType,
                Width = "75",
                IsShow = true,
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tên trường",
                Field = "CodeName",
                DataType = GridDetail.eDataType.StringType,
                Width = "75",
                Enable = true,
                IsShow = true
            });


            var zDetails = db.RPT_TEMPLATES_DETAIL_VALUECONST.Where(p => p.Rpt_TemplatesKey == TemplatesKey).OrderBy(p => p.Sort).ToList();
            if (zDetails.Count > 0)
            {
                for (int i = 0; i < zDetails.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                                  zDetails[i].Rpt_Templates_Detail_ValueConstKey,
                                  zDetails[i].CodeID,
                                  zDetails[i].CodeName,
                    });
                }
            }
            else
            {
                arrBody.Add(new object[]
                    {"","",""});
            }
            grid.Data = arrBody;
            //var data = new { header = arrHDR, body = arrBody };
            return new CustomJsonResult { Data = grid };
        }

        public ActionResult LoadTableList(Guid? TemplatesKey)
        {
            List<SelectListItem> selectLists = new List<SelectListItem>();
            var listtable = (from d in db.RPT_TEMPLATES_TABLENAME
                             where d.RecordStatus == 1
                             orderby d.ReportReferenceName, d.TableID
                             select d).ToList();

            foreach (var item in listtable)
            {
                SelectListItem selectListItem = new SelectListItem();
                selectListItem.Text = string.Format("{0} - {1}", item.ReportReferenceName, item.TableID);
                selectListItem.Value = item.Rpt_Templates_TableNameKey.ToString();
                selectLists.Add(selectListItem);
            }
            List<object> arrBody = new List<object>();
            string[] arrHDR = { "", "", "" };
            GridDetail grid = new GridDetail();

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Rpt_Templates_Detail_TableIndexKey",
                Field = "Rpt_Templates_Detail_TableIndexKey",
                DataType = GridDetail.eDataType.IntegerType,
                Width = "20",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Rpt_Templates_TableNameKey",
                Field = "Rpt_Templates_TableNameKey",
                DataType = GridDetail.eDataType.StringType,
                Width = "75",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tên bảng",
                Field = "TableName",
                DataType = GridDetail.eDataType.StringType,
                Width = "75",
                Enable = true,
                IsShow = true,
                LayoutType = GridDetail.eLayoutType.DropdownList,
                DataSource = selectLists
            });

            var zDetails = (from tl in db.RPT_TEMPLATES
                            join a in db.RPT_TEMPLATES_DETAIL_TABLEINDEX on tl.Rpt_TemplatesKey equals a.Rpt_TemplatesKey
                            join tbl in db.RPT_TEMPLATES_TABLENAME on a.Rpt_Templates_TableNameKey equals tbl.Rpt_Templates_TableNameKey
                            where tl.Rpt_TemplatesKey == TemplatesKey
                            orderby tbl.TableID
                            select new { a.Rpt_Templates_Detail_TableIndexKey, tbl.TableName, tbl.Rpt_Templates_TableNameKey }).ToList();

            if (zDetails.Count > 0)
            {
                for (int i = 0; i < zDetails.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                        zDetails[i].Rpt_Templates_Detail_TableIndexKey,
                        zDetails[i].Rpt_Templates_TableNameKey,
                        zDetails[i].Rpt_Templates_TableNameKey,
                    });
                }
            }
            else
            {
                arrBody.Add(new object[]
                    { "", "", ""});
            }
            grid.Data = arrBody;
            //var data = new { header = arrHDR, body = arrBody };
            return new CustomJsonResult { Data = grid };
        }

        [HttpPost]
        public ActionResult SaveTable(List<string[]> tblListItem, List<string[]> tblListvalueList, RPT_TEMPLATES Item)
        {
            AjaxResult zResult = new AjaxResult();

            List<Guid> ListIDHave = new List<Guid>();
            Item.Templates_ID = Item.Templates_ID.Trim();
            string action = "";//for log
            //Save RPT_TEMPLATES_TABLENAME
            //Create
            if (Item.Rpt_TemplatesKey == Guid.Empty)
            {
                action = "Create";
                Item.Rpt_TemplatesKey = Guid.NewGuid();
                Item.RecordStatus = 1;
                Item.CreatedBy = MyAccount.UserID;
                Item.CreatedOn = Now;
                Item.ModifiedBy = MyAccount.UserID;
                Item.ModifiedOn = Now;
                db.RPT_TEMPLATES.Add(Item);
            }
            //Edit
            else
            {
                action = "Edit";
                var oldItem = db.RPT_TEMPLATES.Find(Item.Rpt_TemplatesKey);
                oldItem.Templates_Name = Item.Templates_Name;
                oldItem.Templates_ID = Item.Templates_ID;
                oldItem.Note = Item.Note;
                oldItem.ModifiedBy = MyAccount.UserID;
                oldItem.ModifiedOn = Now;
                db.Entry(oldItem).State = System.Data.Entity.EntityState.Modified;
            }

            //Save RPT_TEMPLATES_DETAIL_TABLEINDEX
            foreach (var Detail in tblListItem)
            {
                if (!string.IsNullOrEmpty(Detail[1]))
                {
                    bool IsUpdate = true;
                    RPT_TEMPLATES_DETAIL_TABLEINDEX _item;
                    if (!string.IsNullOrEmpty(Detail[0]))
                    {
                        Guid ID = new Guid(Detail[0]);
                        _item = db.RPT_TEMPLATES_DETAIL_TABLEINDEX.Where(p => p.Rpt_Templates_Detail_TableIndexKey == ID).FirstOrDefault();
                    }
                    else
                    {
                        _item = new RPT_TEMPLATES_DETAIL_TABLEINDEX();
                        _item.Rpt_Templates_Detail_TableIndexKey = Guid.NewGuid();
                        IsUpdate = false;
                    }
                    _item.Rpt_TemplatesKey = Item.Rpt_TemplatesKey;
                    _item.Rpt_Templates_TableNameKey = new Guid(Detail[1]);

                    if (!IsUpdate)
                    {
                        db.RPT_TEMPLATES_DETAIL_TABLEINDEX.Add(_item);
                    }
                    else
                    {
                        db.Entry(_item).State = System.Data.Entity.EntityState.Modified;
                    }

                    ListIDHave.Add(_item.Rpt_Templates_Detail_TableIndexKey);
                }

            }
            var ListChiTietDel = db.RPT_TEMPLATES_DETAIL_TABLEINDEX.Where(p => !ListIDHave.Contains(p.Rpt_Templates_Detail_TableIndexKey) && p.Rpt_TemplatesKey == Item.Rpt_TemplatesKey).ToList();
            foreach (var item in ListChiTietDel)
            {
                db.Entry(item).State = System.Data.Entity.EntityState.Deleted;
            }

            //Save RPT_TEMPLATES_DETAIL_VALUECONST
            int _index = 0;
            foreach (var item_value_arr in tblListvalueList)
            {
                var lst_detail = item_value_arr.ToList();
                lst_detail.ForEach(p => p.Trim());
                var arr_detail = lst_detail.ToArray();

                if (!string.IsNullOrEmpty(arr_detail[1]) && !string.IsNullOrEmpty(arr_detail[2]))
                {
                    bool IsUpdate = true;
                    _index++;
                    RPT_TEMPLATES_DETAIL_VALUECONST _itemValue;
                    if (!string.IsNullOrEmpty(arr_detail[0]))
                    {
                        Guid ID = new Guid(arr_detail[0]);
                        _itemValue = db.RPT_TEMPLATES_DETAIL_VALUECONST.Where(p => p.Rpt_Templates_Detail_ValueConstKey == ID).FirstOrDefault();
                        _itemValue.Sort = _index;
                    }
                    else
                    {
                        _itemValue = new RPT_TEMPLATES_DETAIL_VALUECONST();
                        _itemValue.Rpt_Templates_Detail_ValueConstKey = Guid.NewGuid();
                        _itemValue.Rpt_TemplatesKey = Item.Rpt_TemplatesKey;
                        _itemValue.Sort = _index;
                        IsUpdate = false;
                    }

                    _itemValue.CodeID = arr_detail[1];
                    _itemValue.CodeName = arr_detail[2];

                    if (!IsUpdate)
                    {
                        db.RPT_TEMPLATES_DETAIL_VALUECONST.Add(_itemValue);
                    }
                    else
                    {
                        db.Entry(_itemValue).State = System.Data.Entity.EntityState.Modified;
                    }

                    ListIDHave.Add(_itemValue.Rpt_Templates_Detail_ValueConstKey);
                }

            }
            var ListChiTietDelvalue = db.RPT_TEMPLATES_DETAIL_VALUECONST.Where(p => !ListIDHave.Contains(p.Rpt_Templates_Detail_ValueConstKey) && p.Rpt_TemplatesKey == Item.Rpt_TemplatesKey).ToList();
            foreach (var item in ListChiTietDelvalue)
            {
                db.Entry(item).State = System.Data.Entity.EntityState.Deleted;
            }

            db.SaveChanges();
            zResult.intStatus = 1;
            zResult.strMess = "Lưu dữ liệu thành công";
            zResult.Data = Item;
            CreateLog(m_ControllerName, action, Item.Rpt_TemplatesKey.ToString(), Item.Templates_ID, Item.Templates_Name, System.Reflection.MethodBase.GetCurrentMethod().Name, zResult.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), zResult.strMess);
            return new CustomJsonResult { Data = (zResult) };
        }

        public ActionResult UploadFile_Raw(HttpPostedFileBase file)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                if (file != null)
                {
                    Guid id = new Guid(Request["id"]);
                    RPT_TEMPLATES template = db.RPT_TEMPLATES.Find(id);
                    if (template == null)
                        throw new Exception("Không tìm thấy thông tin.");

                    string webpage = "/Uploads/" + file.FileName;
                    var path = Server.MapPath("/Uploads/" + file.FileName);
                    file.SaveAs(path);

                    template.FileName = file.FileName;
                    template.FileReference = webpage;

                    db.Entry(template).State = EntityState.Modified;
                    db.SaveChanges();
                }
                result.strMess = "Đã lưu thông tin";
            }
            catch (Exception ex)
            {
                result.strMess = ex.Message;
                result.intStatus = 0;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult UploadFile_AutoGen(HttpPostedFileBase file)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                if (file != null)
                {
                    if (file.FileName.Length > 50)
                        throw new Exception("Tên file dài quá 50 ký tự. Vui lòng đổi tên file");

                    Guid id = new Guid(Request["id"]);
                    RPT_TEMPLATES template = db.RPT_TEMPLATES.Find(id);

                    //delete current org file
                    if (!string.IsNullOrEmpty(template.FileReference))
                        DeleteFileUploaded(Server.MapPath(template.FileReference));

                    //upload to version
                    string fileversion = string.Format("{0}{1}_{2}", PathUpload_TemplateVersion, Now_Full_String(), file.FileName);
                    file.SaveAs(Server.MapPath(fileversion));

                    RPT_TEMPLATES_VERSIONS itemversion = new RPT_TEMPLATES_VERSIONS();
                    itemversion.Rpt_TemplatesVersionKey = Guid.NewGuid();
                    itemversion.Rpt_TemplatesKey = id;
                    itemversion.FileName = file.FileName;
                    itemversion.FileReference = fileversion;
                    itemversion.CreatedBy = MyAccount.UserID;
                    itemversion.CreatedOn = Now;
                    itemversion.ModifiedBy = MyAccount.UserID;
                    itemversion.ModifiedOn = Now;
                    db.RPT_TEMPLATES_VERSIONS.Add(itemversion);
                    //

                    template.FileName = file.FileName;
                    template.FileReference = string.Format("{0}{1}", PathUpload_TemplateOrg, file.FileName);

                    //save new org file as current
                    file.SaveAs(Server.MapPath(template.FileReference));
                    db.Entry(template).State = EntityState.Modified;

                    string msg = string.Empty;
                    Dictionary<string, string> dic_fields = GenerateFieldsFromDocx(Server.MapPath(template.FileReference), out msg);
                    if (!string.IsNullOrEmpty(msg))
                        throw new Exception(msg);

                    var rptvalues = db.RPT_TEMPLATES_DETAIL_VALUECONST.Where(p => p.Rpt_TemplatesKey == id).ToList();
                    if (rptvalues.Count > 0)
                    {
                        rptvalues.ForEach(p =>
                        {
                            db.Entry(p).State = EntityState.Deleted;
                        });
                    }
                    int sort = 0;
                    foreach (KeyValuePair<string, string> field in dic_fields)
                    {
                        RPT_TEMPLATES_DETAIL_VALUECONST itemvalue = new RPT_TEMPLATES_DETAIL_VALUECONST();
                        itemvalue.Rpt_Templates_Detail_ValueConstKey = Guid.NewGuid();
                        itemvalue.Rpt_TemplatesKey = id;
                        itemvalue.CodeID = field.Key;
                        itemvalue.CodeName = field.Value;
                        itemvalue.Sort = sort++;
                        db.RPT_TEMPLATES_DETAIL_VALUECONST.Add(itemvalue);
                    }
                    db.SaveChanges();
                }
                result.strMess = "Đã lưu thông tin";
            }
            catch (Exception ex)
            {
                result.strMess = ex.Message;
                result.intStatus = 0;
            }

            return new CustomJsonResult { Data = (result) };
        }

        private Dictionary<string, string> GenerateFieldsFromDocx(string filedocx, out string msg)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            msg = string.Empty;
            try
            {
                using (DocumentFormat.OpenXml.Packaging.WordprocessingDocument doc = DocumentFormat.OpenXml.Packaging.WordprocessingDocument.Open(filedocx, true))
                {
                    var body = doc.MainDocumentPart.Document.Body;

                    string markstart = "<#";
                    string markend = "#>";
                    int paragraph_pos = 0;

                    System.Data.DataTable dtMatches = new System.Data.DataTable();
                    dtMatches.Columns.Add("id", typeof(int));
                    dtMatches.Columns.Add("pindex", typeof(int));
                    dtMatches.Columns.Add("expression", typeof(string));
                    dtMatches.Columns.Add("markstartindex", typeof(int));
                    dtMatches.Columns.Add("markendindex", typeof(int));

                    #region lấy mark expression

                    var text_paragraphs = body.Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>().ToList();

                    foreach (var text_p in text_paragraphs)
                    {
                        MatchCollection mx_start = Regex.Matches(text_p.Text, markstart);
                        if (mx_start.Count > 0)
                        {
                            for (int i = 0; i < mx_start.Count; i++)
                            {
                                dtMatches.Rows.Add(new object[]
                                {
                                    dtMatches.Rows.Count + 1,
                                    paragraph_pos,
                                    text_p.Text,
                                    mx_start[i].Index,
                                    DBNull.Value
                                });
                            }
                        }

                        MatchCollection mx_end = Regex.Matches(text_p.Text, markend);
                        if (mx_end.Count > 0)
                        {
                            for (int i = 0; i < mx_end.Count; i++)
                            {
                                dtMatches.Rows.Add(new object[]
                                {
                                    dtMatches.Rows.Count + 1,
                                    paragraph_pos,
                                    text_p.Text,
                                    DBNull.Value,
                                    mx_end[i].Index
                                });
                            }
                        }

                        paragraph_pos++;
                    }

                    #endregion

                    #region analyze

                    dtMatches.DefaultView.RowFilter = "markstartindex IS NOT NULL";
                    System.Data.DataTable dtMxStarts = dtMatches.DefaultView.ToTable();

                    dtMatches.DefaultView.RowFilter = "markendindex IS NOT NULL";
                    System.Data.DataTable dtMxEnds = dtMatches.DefaultView.ToTable();

                    //dtMxStarts.Rows.Count == dtMxEnds.Rows.Count
                    //data mở và đóng phải bằng nhau
                    if (dtMxStarts.Rows.Count == dtMxEnds.Rows.Count)
                    {
                        for (int i = 0; i < dtMxStarts.Rows.Count; i++)
                        {
                            System.Data.DataRow drStart = dtMxStarts.Rows[i];
                            System.Data.DataRow drEnd = dtMxEnds.Rows[i];

                            string expr_start = Convert.ToString(drStart["expression"]);
                            int pindex_start = Convert.ToInt32(drStart["pindex"]);
                            int pmark_start = Convert.ToInt32(drStart["markstartindex"]);

                            string expr_end = Convert.ToString(drEnd["expression"]);
                            int pindex_end = Convert.ToInt32(drEnd["pindex"]);
                            int pmark_end = Convert.ToInt32(drEnd["markendindex"]);

                            string expr_all = string.Empty;
                            string temp = "";

                            if (pindex_start == pindex_end)
                            {
                                //mark (start-end) trên cùng 1 paragraph
                                temp = expr_start.Substring(pmark_start, pmark_end + 2 - pmark_start);
                                expr_all = temp;

                                string[] arr_expr = StandardizeExpression(expr_all, markstart, markend, '|');
                                //0: code
                                //1: description
                                if (result.Where(p => p.Key == arr_expr[0]).Count() == 0)
                                    result.Add(arr_expr[0], arr_expr[1]);

                                text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(expr_all, markstart + arr_expr[0] + markend);
                            }
                            else if (pindex_start < pindex_end)
                            {
                                //mark end ở paragraph sau
                                //nối các expr lại thành 1

                                if (pindex_start == pindex_end - 1)
                                {
                                    temp = expr_start.Substring(pmark_start) + expr_end.Substring(0, pmark_end + 2);
                                    expr_all += temp;

                                    string[] arr_expr = StandardizeExpression(expr_all, markstart, markend, '|');
                                    //0: code
                                    //1: description
                                    if (result.Where(p => p.Key == arr_expr[0]).Count() == 0)
                                        result.Add(arr_expr[0], arr_expr[1]);

                                    text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(expr_start.Substring(pmark_start), markstart + arr_expr[0] + markend);
                                    text_paragraphs[pindex_end].Text = text_paragraphs[pindex_end].Text.Replace(expr_end.Substring(0, pmark_end + 2), "");
                                }
                                else
                                {

                                    for (int ex_index = pindex_start; ex_index <= pindex_end; ex_index++)
                                    {
                                        if (ex_index == pindex_start)
                                        {
                                            temp = text_paragraphs[ex_index].Text.Substring(pmark_start);
                                            expr_all += temp;
                                            //text_paragraphs[ex_index].Text = text_paragraphs[ex_index].Text.Replace(temp, "[the start-point string]");
                                        }
                                        else if (ex_index == pindex_end)
                                        {
                                            temp = text_paragraphs[ex_index].Text.Substring(0, pmark_end + 2);
                                            expr_all += temp;
                                            text_paragraphs[ex_index].Text = text_paragraphs[ex_index].Text.Replace(temp, "");

                                            if (text_paragraphs[ex_index].Text.Contains(markstart))
                                            {
                                                // still has mark start of next one
                                                //change the start-point
                                                if (i < dtMxStarts.Rows.Count - 1)
                                                    dtMxStarts.Rows[i + 1]["markstartindex"] = text_paragraphs[ex_index].Text.IndexOf(markstart);
                                            }
                                        }
                                        else
                                        {
                                            expr_all += text_paragraphs[ex_index].Text;
                                            text_paragraphs[ex_index].Text = "";
                                        }
                                    }

                                    string[] arr_expr = StandardizeExpression(expr_all, markstart, markend, '|');
                                    //0: code
                                    //1: description
                                    if (result.Where(p => p.Key == arr_expr[0]).Count() == 0)
                                        result.Add(arr_expr[0], arr_expr[1]);

                                    temp = text_paragraphs[pindex_start].Text.Substring(pmark_start);
                                    text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(temp, markstart + arr_expr[0] + markend);
                                }
                            }
                            else
                            {
                                //do nothing
                            }
                        }
                    }
                    else
                    {
                        msg = "Dữ liệu định dạng không hợp lệ. Vui lòng xem lại file mẫu";
                    }
                    #endregion

                    if (string.IsNullOrEmpty(msg))
                    {
                        if (DocumentFormat.OpenXml.Packaging.OpenXmlPackage.CanSave)
                            doc.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return result;
        }

        private string[] StandardizeExpression(string expression, string mark_start, string mark_end, char marksplit)
        {
            if (!string.IsNullOrEmpty(mark_start))
                expression = expression.Replace(mark_start, "");
            if (!string.IsNullOrEmpty(mark_end))
                expression = expression.Replace(mark_end, "");

            return expression.Split(marksplit);
        }

        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;

            Guid id = new Guid(Request["id"]);

            RPT_TEMPLATES template = db.RPT_TEMPLATES.Find(id);

            if (template != null)
            {
                if (file != null)
                {
                    var currenttime = DateTime.Now.Ticks;
                    string webpage = "/Uploads/" + currenttime + "_" + file.FileName;
                    var path = Server.MapPath("/Uploads/" + currenttime + "_" + file.FileName);
                    file.SaveAs(path);
                    /*
                     * Start - Read Field Name From File: Mr.Linh
                     */
                    var pathtemplate = Server.MapPath("/Uploads/" + currenttime + "_template_" + file.FileName);
                    file.SaveAs(pathtemplate);
                    var rpt = db.RPT_TEMPLATES_DETAIL_VALUECONST.Where(p => p.Rpt_TemplatesKey == id).ToList();
                    foreach (var item in rpt)
                    {
                        db.Entry(item).State = EntityState.Deleted;
                    }
                    db.SaveChanges();
                    List<string[]> data = ReadFieldName(path);
                    int i = 1;
                    foreach (var t in data)
                    {
                        RPT_TEMPLATES_DETAIL_VALUECONST a = new RPT_TEMPLATES_DETAIL_VALUECONST();
                        a.Rpt_Templates_Detail_ValueConstKey = Guid.NewGuid();
                        a.Rpt_TemplatesKey = id;
                        a.CodeID = t[0];
                        a.CodeName = t[1];
                        a.CodeValue = t[2];
                        a.Sort = i++;
                        db.RPT_TEMPLATES_DETAIL_VALUECONST.Add(a);
                    }
                    db.SaveChanges();
                    /*
                     * End - Read Field Name From File: Mr.Linh
                     */

                    template.FileName = file.FileName;
                    template.FileReference = webpage;
                }
                db.Entry(template).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                result.intStatus = -1;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult CopyTemplate(Guid? TemplateKey_Sample)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;

            try
            {
                RPT_TEMPLATES olditem = db.RPT_TEMPLATES.Find(TemplateKey_Sample);
                if (olditem == null)
                    throw new Exception("Không tìm thấy mẫu cần sao chép");

                #region copy template

                RPT_TEMPLATES newitem = new RPT_TEMPLATES();
                foreach (PropertyInfo pInfo in newitem.GetType().GetProperties())
                {
                    pInfo.SetValue(newitem, olditem.GetType().GetProperty(pInfo.Name).GetValue(olditem, null));
                }
                newitem.Rpt_TemplatesKey = Guid.NewGuid();
                newitem.FileName = string.Empty;
                newitem.FileReference = string.Empty;
                newitem.CreatedOn = Now;
                newitem.CreatedBy = MyAccount.UserID;
                newitem.ModifiedOn = Now;
                newitem.ModifiedBy = MyAccount.UserID;
                db.RPT_TEMPLATES.Add(newitem);

                #endregion

                #region copy table index

                foreach (RPT_TEMPLATES_DETAIL_TABLEINDEX item_old_tblindex in db.RPT_TEMPLATES_DETAIL_TABLEINDEX.Where(p => p.Rpt_TemplatesKey == TemplateKey_Sample).ToList())
                {
                    RPT_TEMPLATES_DETAIL_TABLEINDEX itemtblnew = new RPT_TEMPLATES_DETAIL_TABLEINDEX();
                    itemtblnew.Rpt_Templates_Detail_TableIndexKey = Guid.NewGuid();
                    itemtblnew.Rpt_TemplatesKey = newitem.Rpt_TemplatesKey;
                    itemtblnew.Rpt_Templates_TableNameKey = item_old_tblindex.Rpt_Templates_TableNameKey;
                    db.RPT_TEMPLATES_DETAIL_TABLEINDEX.Add(itemtblnew);
                }

                #endregion

                #region copy value const

                foreach (RPT_TEMPLATES_DETAIL_VALUECONST item_old_value in db.RPT_TEMPLATES_DETAIL_VALUECONST.Where(p => p.Rpt_TemplatesKey == TemplateKey_Sample).ToList())
                {
                    RPT_TEMPLATES_DETAIL_VALUECONST item_new_value = new RPT_TEMPLATES_DETAIL_VALUECONST();
                    item_new_value.Rpt_Templates_Detail_ValueConstKey = Guid.NewGuid();
                    item_new_value.Rpt_TemplatesKey = newitem.Rpt_TemplatesKey;
                    item_new_value.CodeID = item_old_value.CodeID;
                    item_new_value.CodeName = item_old_value.CodeName;
                    item_new_value.CodeValue = item_old_value.CodeValue;
                    item_new_value.Sort = item_old_value.Sort;

                    db.RPT_TEMPLATES_DETAIL_VALUECONST.Add(item_new_value);
                }
                #endregion

                db.SaveChanges();
                result.strMess = "Sao chép thành công! Hãy cập nhật file mẫu mới";
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        /// <summary>
        /// Mr.Linh
        /// </summary>
        /// <param name="path"></param>
        public List<string[]> ReadFieldName(string path)
        {
            List<string[]> ds = new List<string[]>();
            String read = string.Empty;
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
            Document doc = new Document();
            try
            {
                object fileName = path;
                // Define an object to pass to the API for missing parameters
                object missing = System.Type.Missing;
                doc = word.Documents.Open(ref fileName,
                        ref missing, ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing);
                for (int i = 0; i < doc.Paragraphs.Count; i++)
                {
                    read += doc.Paragraphs[i + 1].Range.Text.Trim() + System.Environment.NewLine;
                    if (Regex.Matches(read, "<#").Count == Regex.Matches(read, "#>").Count)
                    {
                        int start = -1, end = -1;
                        while ((start = read.IndexOf("<#")) >= 0)
                        {
                            if ((end = read.IndexOf("#>")) > 0)
                            {
                                string[] item = read.Substring(start + 2, end - start - 2).Split('|');
                                ds.Add(item);

                                string temp = read.Substring(start, end - start + 2);
                                string[] x = temp.Split('|');
                                string temp1 = "";
                                int j = 0;
                                while (temp.Length > 0)
                                {
                                    if (temp.Length > 250)
                                    {
                                        temp1 = temp.Substring(0, 250);
                                        temp = temp.Substring(250);
                                    }
                                    else
                                    {
                                        temp1 = temp;
                                        temp = "";
                                    }
                                    FindAndReplace(word, temp1, j == 0 ? x[0].Substring(2) : "");
                                    j++;
                                }
                                read = read.Substring(end + 2);
                                start = -1; end = -1;
                            }
                        }
                        read = string.Empty;
                    }
                }
                    ((_Document)doc).Close();
                ((_Application)word).Quit();

            }
            catch (Exception ex)
            {
                ((_Document)doc).Close();
                ((_Application)word).Quit();
            }
            //Loai bo trung
            for (int i = 0; i < ds.Count - 1; i++)
            {
                for (int j = i + 1; j < ds.Count; j++)
                {
                    string[] x = ds[i], y = ds[j];
                    if (x[0] == y[0])
                    {
                        ds.RemoveAt(j);
                    }
                }
            }
            return ds;
        }

        /// <summary>
        /// Mr.Linh
        /// </summary>
        /// <param name="st"></param>
        public string[] Split(string st)
        {
            string[] result = new string[3];
            result[0] = st.Substring(0, st.IndexOf(":"));
            st = st.Substring(st.IndexOf(":") + 1);
            result[1] = st.Substring(0, st.IndexOf(":"));
            st = st.Substring(st.IndexOf(":") + 1);
            result[2] = st;
            return result;
        }

        private void FindAndReplace(Microsoft.Office.Interop.Word.Application doc, object findText, object replaceWithText)
        {
            object missing = System.Reflection.Missing.Value;
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            //execute find and replace

            doc.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }


    }
}