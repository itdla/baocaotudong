﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using Spire.Doc;
using Spire.Doc.Documents;

namespace AutoReport.Controllers
{
    public class RPT_INPUT_VALUEDATAController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();

        private string m_ControllerName = "RPT_INPUT_VALUEDATA";

        public ActionResult RptValuesInput_Index()
        {
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult RptValuesInput_Edit(Guid? id)
        {
            ViewBag.ControllerName = m_ControllerName;
            RPT_REPORTHEADER item = db.RPT_REPORTHEADER.Find(id);

            Guid rptkey = id.Value;
            Guid userkey = MyAccount.UserID;
            RPT_REPORT_SENT_APPROVED rpt_inputcomplete = db.RPT_REPORT_SENT_APPROVED.Where(p => p.Rpt_ReportHeaderKey == rptkey && p.Rpt_DataType == 0 && p.CreatedBy == userkey).FirstOrDefault();
            ViewBag.InputCompleted = rpt_inputcomplete == null ? "no" : (rpt_inputcomplete.InputStatus == Convert.ToInt32(eRptInputStatus.Done) ? "yes" : "no");

            ViewBag.BackToIndexLink = "/RPT_INPUT_PREVIEW/RptInputListIndex";
            return View(item);
        }

        public ActionResult RptValuesInput_SelectDataFromTableIndex()
        {
            ViewBag.ControllerName = m_ControllerName;
            var rptList = (from p in db.RPT_REPORTHEADER
                           where p.RecordStatus == 1
                           orderby p.Rpt_Year, p.Rpt_Month
                           select p).ToList();

            ViewBag.ReportList = new SelectList(rptList, "Rpt_ReportHeaderKey", "Rpt_Name");
            return View();
        }

        public ActionResult LoadList_ValuesInput_Index(int page, int pageSize, string Rpt_ID, string Rpt_Name, int? Rpt_Month, int? Rpt_Year, string DateEnd, string TinhTrang)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            Rpt_ID = Rpt_ID ?? "";
            Rpt_Name = Rpt_Name ?? "";
            Rpt_Month = Rpt_Month ?? 0;
            Rpt_Year = Rpt_Year ?? 0;
            DateEnd = DateEnd ?? "";
            TinhTrang = TinhTrang ?? "";

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(Rpt_ID))
                table.KeysSearch.Add("Rpt_ID", Rpt_ID);
            if (!string.IsNullOrEmpty(Rpt_Name))
                table.KeysSearch.Add("Rpt_Name", Rpt_Name);
            if (Rpt_Month != 0)
                table.KeysSearch.Add("Rpt_Month", Rpt_Month.ToString());
            if (Rpt_Year != 0)
                table.KeysSearch.Add("Rpt_Year", Rpt_Year.ToString());
            if (!string.IsNullOrEmpty(DateEnd))
                table.KeysSearch.Add("DateEnd", DateEnd);
            if (!string.IsNullOrEmpty(TinhTrang))
                table.KeysSearch.Add("TinhTrang", TinhTrang);

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();

            Guid userkey = MyAccount.UserID;
            Guid rpt_emp = MyAccount.EmployeeKey == null ? Guid.Empty : MyAccount.EmployeeKey.Value;
            int rpt_data_type = Convert.ToInt32(eRptDataType.DataValue);
            List<int?> rptstatus_allow = new List<int?>();
            rptstatus_allow.Add(Convert.ToInt32(eRptHeaderStatus.Created));
            rptstatus_allow.Add(Convert.ToInt32(eRptHeaderStatus.Declined));

            //lấy danh sách report create/return
            var searchQuery = (from f in db.RPT_REPORTHEADER
                               where f.RecordStatus == 1 &&
                               rptstatus_allow.Contains(f.RptStatus)
                               orderby f.Rpt_Year descending, f.Rpt_Month descending
                               select f).ToList();
            var lst_rptkey = searchQuery.Select(p => p.Rpt_ReportHeaderKey).ToList();

            //lấy danh sách các report có phân công cho nhân sự
            Guid empkey = MyAccount.EmployeeKey == null ? Guid.Empty : MyAccount.EmployeeKey.Value;
            var lst_value_enlist = (from p in db.RPT_REPORTVALUES_EMPLOYEES_ENLISTED
                                    join d in db.RPT_REPORTDETAIL_VALUECONST on p.Rpt_ReportDetail_ValueConstKey equals d.Rpt_ReportDetail_ValueConstKey
                                    where lst_rptkey.Contains(d.Rpt_ReportHeaderKey) &&
                                    p.EmployeeKey == empkey
                                    select d.Rpt_ReportHeaderKey).ToList();
            //filter lại 
            searchQuery = searchQuery.Where(p => lst_value_enlist.Contains(p.Rpt_ReportHeaderKey)).ToList();

            //lấy danh sách tình trạng hoàn thành của reports
            var lst_completed = (from d in db.RPT_REPORT_SENT_APPROVED
                                 where lst_rptkey.Contains(d.Rpt_ReportHeaderKey) && d.Rpt_DataType == rpt_data_type
                                 select new
                                 {
                                     d.Rpt_Report_Sent_ApprovedKey,
                                     d.Rpt_ReportHeaderKey,
                                     d.InputStatus,
                                     d.CreatedBy
                                 }).ToList();

            var fq1 = searchQuery.Select(p => new
            {
                p.Rpt_ReportHeaderKey,
                p.Rpt_ID,
                p.Rpt_Name,
                p.Rpt_Month,
                p.Rpt_Year,
                DateEnd = p.DateEnd == null ? "" : p.DateEnd.Value.ToString("dd/MM/yyyy"),
                TinhTrang = (lst_completed.Where(d => d.Rpt_ReportHeaderKey == p.Rpt_ReportHeaderKey && d.CreatedBy == userkey).FirstOrDefault()?.InputStatus) ?? 0
            });

            if (!string.IsNullOrEmpty(Rpt_ID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_ID.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_ID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(Rpt_Name))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_Name.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_Name.Trim().ToLower()))).ToList();
            if (Rpt_Month != 0)
                fq1 = fq1.Where(p => p.Rpt_Month == Rpt_Month).ToList();
            if (Rpt_Year != 0)
                fq1 = fq1.Where(p => p.Rpt_Year == Rpt_Year).ToList();
            if (!string.IsNullOrEmpty(DateEnd))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.DateEnd.Trim().ToLower()).Contains(Utils.RemoveVni(DateEnd.Trim().ToLower()))).ToList();

            var fq2 = fq1.Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                d.Rpt_ID,
                d.Rpt_Name,
                d.Rpt_Month,
                d.Rpt_Year,
                d.DateEnd,
                TinhTrang = EnumUtil.GetEnumDescText((eRptInputStatus)d.TinhTrang),
                HanhDong = "<a type=\"button\" class=\"btn btn-" + (d.TinhTrang != 1 ? "success" : "primary") + "\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/RptValuesInput_Edit/" + d.Rpt_ReportHeaderKey + "'>" + (d.TinhTrang != 1 ? "Nhập liệu" : "Xem") + "</a>"
            }).ToList();

            if (!string.IsNullOrEmpty(TinhTrang))
                fq2 = fq2.Where(p => Utils.RemoveVni(p.TinhTrang.Trim().ToLower()).Contains(Utils.RemoveVni(TinhTrang.Trim().ToLower()))).ToList();

            int _index = 1;
            var fq3 = fq2.Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                STT = _index++,
                d.Rpt_ID,
                d.Rpt_Name,
                d.Rpt_Month,
                d.Rpt_Year,
                d.DateEnd,
                d.TinhTrang,
                d.HanhDong
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq3.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Báo Cáo", IsShow = true, Format = "", Width = 20, Width_Measure = "%", FieldName = "Rpt_ID" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Báo Cáo", IsShow = true, Format = "", Width = 33, Width_Measure = "%", FieldName = "Rpt_Name" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tháng", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Month", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Năm", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Year", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Ngày hết hạn", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "DateEnd", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Tình trạng", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "TinhTrang", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 12, Width_Measure = "%" });
            //

            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportHeaderKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadValueList_Input(Guid? RptHeaderKey, string inputcompleted)
        {
            //inputcompleted: yes|no
            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();
            grid.ShowAction = false;

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "field key",
                Field = "Rpt_ReportDetail_ValueConstKey",
                DataType = GridDetail.eDataType.StringType,
                Width = "2%",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Mã Trường",
                Field = "CodeID",
                DataType = GridDetail.eDataType.StringType,
                Width = "3%",
                Enable = false,
                IsShow = true,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tên Trường",
                Field = "CodeName",
                DataType = GridDetail.eDataType.StringType,
                Width = "40%",
                Enable = false,
                IsShow = true
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Dữ Liệu",
                Field = "ContentValue",
                DataType = GridDetail.eDataType.StringType,
                Width = "55%",
                Enable = string.IsNullOrEmpty(inputcompleted) ? true : inputcompleted.ToLower() == "no",
                PlaceHolderText = "<Double-Click hoặc Enter>",
                IsShow = true,
            });
            var searchquery = (from v in db.RPT_REPORTDETAIL_VALUECONST
                            orderby v.CodeID
                            where v.Rpt_ReportHeaderKey == RptHeaderKey
                            orderby v.Sort
                            select new
                            {
                                v.Rpt_ReportDetail_ValueConstKey,
                                v.CodeID,
                                v.CodeName,
                                v.ContentValue
                            }).ToList();
            var lst_detailkey = searchquery.Select(p => p.Rpt_ReportDetail_ValueConstKey).ToList();

            //lấy danh sách các trường được phân công cho nhân sự
            Guid empkey = MyAccount.EmployeeKey == null ? Guid.Empty : MyAccount.EmployeeKey.Value;
            var lst_value_enlisted = (from p in db.RPT_REPORTVALUES_EMPLOYEES_ENLISTED
                                      where p.EmployeeKey == empkey &&
                                      lst_detailkey.Contains(p.Rpt_ReportDetail_ValueConstKey)
                                      select p).ToList();
            var lst_detail_enlist_key = lst_value_enlisted.Select(p => p.Rpt_ReportDetail_ValueConstKey).ToList();

            //filter danh sách được trường được nhập
            var data = searchquery.Where(p => lst_detail_enlist_key.Contains(p.Rpt_ReportDetail_ValueConstKey)).ToList();

            if (data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                        data[i].Rpt_ReportDetail_ValueConstKey.ToString(),
                        data[i].CodeID,
                        data[i].CodeName,
                        data[i].ContentValue
                    });
                }
            }
            else
            {
                arrBody.Add(new object[grid.Header.Count]);
            }
            grid.Data = arrBody;
            //var data = new { header = arrHDR, body = arrBody };
            return new CustomJsonResult { Data = grid };
        }

        public ActionResult SaveValues_Input(Guid? RptHeaderKey, List<string[]> lst_item_const, string[] lst_item_const_id_deleted)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            var rptHeader = db.RPT_REPORTHEADER.Find(RptHeaderKey);
            try
            {
                if (rptHeader == null)
                    throw new Exception("Không tìm thấy thông tin");

                #region save items const

                for (int i = 0; i < lst_item_const.Count; i++)
                {
                    var lstTemp = lst_item_const[i].ToList();
                    lstTemp.ForEach(p => p.Trim());
                    string[] data = lstTemp.ToArray();

                    if (string.IsNullOrEmpty(data[0]))
                    {
                        int empty_cols = data.Where(p => string.IsNullOrEmpty(p)).Count();
                        if (empty_cols == data.Length)
                            continue;

                        RPT_REPORTDETAIL_VALUECONST itemnew = new RPT_REPORTDETAIL_VALUECONST();
                        itemnew.Rpt_ReportDetail_ValueConstKey = Guid.NewGuid();
                        itemnew.Rpt_ReportHeaderKey = RptHeaderKey.Value;
                        itemnew.Rpt_Templates_Detail_ValueConstKey = string.IsNullOrEmpty(data[0]) ? Guid.Empty : Guid.Parse(data[0]);
                        itemnew.CodeID = data[1];
                        itemnew.CodeName = data[2];
                        itemnew.Sort = i;
                        itemnew.ContentValue = data[3].Trim();
                        itemnew.CreatedOn = DateTime.Now;
                        itemnew.ModifiedOn = DateTime.Now;
                        itemnew.CreatedBy = MyAccount.UserID;
                        itemnew.ModifiedBy = MyAccount.UserID;
                        db.RPT_REPORTDETAIL_VALUECONST.Add(itemnew);
                    }
                    else
                    {
                        Guid key = Guid.Parse(data[0]);
                        RPT_REPORTDETAIL_VALUECONST itemedit = db.RPT_REPORTDETAIL_VALUECONST.Find(key);
                        if (itemedit == null) continue;
                        itemedit.CodeID = data[1];
                        itemedit.CodeName = data[2];
                        itemedit.ContentValue = data[3].Trim();
                        itemedit.Sort = i;
                        itemedit.ModifiedOn = DateTime.Now;
                        itemedit.ModifiedBy = MyAccount.UserID;
                        db.Entry(itemedit).State = EntityState.Modified;
                    }
                }

                #endregion

                #region delete item const

                if (lst_item_const_id_deleted != null)
                {
                    foreach (string id in lst_item_const_id_deleted)
                    {
                        RPT_REPORTDETAIL_VALUECONST itemdel = db.RPT_REPORTDETAIL_VALUECONST.Find(Guid.Parse(id));
                        if (itemdel != null)
                            db.Entry(itemdel).State = EntityState.Deleted;
                    }
                }

                #endregion

                result.strMess = "Đã lưu thông tin";
                db.SaveChanges();
                CreateLog(m_ControllerName, "Save-Data", rptHeader.Rpt_ReportHeaderKey.ToString(), rptHeader.Rpt_ID, rptHeader.Rpt_Name, System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult Update_InputValue_Complete(Guid? Report_TableIndex_Key)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            var rptHeader = db.RPT_REPORTHEADER.Find(Report_TableIndex_Key);
            try
            {
                if (Report_TableIndex_Key == null)
                    throw new Exception("Không tìm thấy thông tin!");
                if (MyAccount.EmployeeKey == null)
                    throw new Exception("Tài khoản đang dùng không thuộc sở hữu của nhân viên nào <br />Vui lòng thiết lập lại thông tin tải khoản");
                bool _isNew = false;
                Guid rptkey = Report_TableIndex_Key.Value;
                Guid userkey = MyAccount.UserID;

                int rptType = Convert.ToInt32(eRptDataType.DataValue);

                RPT_REPORT_SENT_APPROVED item = db.RPT_REPORT_SENT_APPROVED
                    .Where(p => p.Rpt_ReportHeaderKey == rptkey && p.Rpt_DataType == rptType && p.CreatedBy == userkey)
                    .FirstOrDefault();
                if (item == null)
                {
                    _isNew = true;
                    item = new RPT_REPORT_SENT_APPROVED();
                    item.EmployeeKey = MyAccount.EmployeeKey.Value;
                    item.CreatedBy = userkey;
                    item.CreatedOn = Now;
                }

                item.Rpt_ReportHeaderKey = rptkey;
                item.Rpt_DataType = rptType;
                item.InputStatus = Convert.ToInt32(eRptInputStatus.Done);
                item.ModifiedBy = userkey;
                item.ModifiedOn = Now;

                if (_isNew)
                    db.RPT_REPORT_SENT_APPROVED.Add(item);
                else
                    db.Entry(item).State = EntityState.Modified;

                db.SaveChanges();
                result.Data = rptkey;
                result.strMess = "Đã hoàn thành nhập liệu";
                CreateLog(m_ControllerName, "Update-Status", rptHeader.Rpt_ReportHeaderKey.ToString(), rptHeader.Rpt_ID, rptHeader.Rpt_Name, System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult GetTableIndexListByRptHeader(Guid? RptHeaderKey)
        {
            var lstTblIndex = (from p in db.RPT_REPORTDETAIL_TABLEINDEX
                               join t in db.RPT_TEMPLATES_TABLENAME
                                   on p.Rpt_Templates_TableNameKey equals t.Rpt_Templates_TableNameKey
                               where p.Rpt_ReportHeaderKey == RptHeaderKey
                               select new
                               {
                                   p.Rpt_ReportDetail_TableIndexKey,
                                   t.TableName
                               }).ToList();

            AjaxResult result = new AjaxResult();
            
            return new CustomJsonResult { Data = lstTblIndex };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
