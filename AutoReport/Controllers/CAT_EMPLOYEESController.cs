﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;

namespace AutoReport.Controllers
{
    public class CAT_EMPLOYEESController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();
        private string m_ControllerName = "CAT_EMPLOYEES";

        // GET: CAT_EMPLOYEES
        public ActionResult Index()
        {
            if (!CheckPermission("EMPLOYEES"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            CreateLog(m_ControllerName, "View", "", "", "", System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), "");
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        // GET: CAT_EMPLOYEES/Create
        public ActionResult Create()
        {
            if (!CheckPermission("EMPLOYEES_ADD"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;

            CAT_EMPLOYEES item = new CAT_EMPLOYEES();
            item.EmployeeID = "(Tự động phát sinh)";
            item.Gender = 0;
            item.DepartmentKey = Guid.Empty;
            ViewBag.GenderList = new SelectList(db.CAT_GENDERS.ToList(), "GenderKey", "GenderName");
            ViewBag.DepartmentList = new SelectList(db.CAT_DEPARTMENTS.Where(p => p.RecordStatus == 1).OrderBy(p => p.DepartmentName).ToList(), "DepartmentKey", "DepartmentName");
            ViewBag.PositionList = new SelectList(db.CAT_POSITIONS.Where(p => p.RecordStatus == 1).OrderBy(p => p.PositionName).ToList(), "PositionKey", "PositionName");
            return View(item);
        }

        // POST: CAT_EMPLOYEES/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(CAT_EMPLOYEES iteminfo)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                result.strMess = CheckBeforeSave(iteminfo, true);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                string prefix = "NV";
                var lastitem = db.CAT_EMPLOYEES.Where(p => p.EmployeeID.Contains(prefix))
                    .OrderBy(p => p.EmployeeID).ToList().LastOrDefault();
                if (lastitem == null)
                    iteminfo.EmployeeID = prefix + "000001";
                else
                {
                    int lastnum = Convert.ToInt32(lastitem.EmployeeID.Replace(prefix, ""));
                    int nextnum = lastnum + 1;
                    if (nextnum.ToString().Length > 6)
                        iteminfo.EmployeeID = prefix + nextnum.ToString();
                    else
                        iteminfo.EmployeeID = prefix + nextnum.ToString("000000");
                }

                iteminfo.EmployeeKey = Guid.NewGuid();
                iteminfo.Active = true;
                iteminfo.RecordStatus = 1;
                iteminfo.CreatedBy = MyAccount.UserID;
                iteminfo.ModifiedBy = MyAccount.UserID;
                iteminfo.CreatedOn = DateTime.Now;
                iteminfo.ModifiedOn = DateTime.Now;

                db.CAT_EMPLOYEES.Add(iteminfo);
                db.SaveChanges();
                result.Data = iteminfo;
                result.strMess = "Đã lưu thông tin";
                CreateLog(m_ControllerName, "Create", iteminfo.EmployeeKey.ToString(), iteminfo.EmployeeID, iteminfo.EmployeeName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        // GET: CAT_EMPLOYEES/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (!CheckPermission("EMPLOYEES_EDIT"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_EMPLOYEES iteminfo = db.CAT_EMPLOYEES.Find(id);
            if (iteminfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.GenderList = new SelectList(db.CAT_GENDERS.ToList(), "GenderKey", "GenderName");
            ViewBag.DepartmentList = new SelectList(db.CAT_DEPARTMENTS.Where(p => p.RecordStatus == 1).OrderBy(p => p.DepartmentName).ToList(), "DepartmentKey", "DepartmentName");
            ViewBag.PositionList = new SelectList(db.CAT_POSITIONS.Where(p => p.RecordStatus == 1).OrderBy(p => p.PositionName).ToList(), "PositionKey", "PositionName");

            return View(iteminfo);
        }

        // POST: CAT_EMPLOYEES/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(CAT_EMPLOYEES iteminfo)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                result.strMess = CheckBeforeSave(iteminfo, false);
                if (!string.IsNullOrEmpty(result.strMess))
                    throw new Exception(result.strMess);

                CAT_EMPLOYEES oldItem = db.CAT_EMPLOYEES.Find(iteminfo.EmployeeKey);
                if (oldItem == null)
                    throw new Exception("Không tìm thấy thông tin");

                string[] prop_set_list = { "EmployeeName", "Gender", "BirthDay", "Address", "Phone", "Email", "OnlineContact", "DepartmentKey", "PositionKey", "Note" };

                foreach (string propname in prop_set_list)
                {
                    PropertyInfo pNew = iteminfo.GetType().GetProperty(propname);
                    if (pNew == null) continue;
                    object pNew_Value = pNew.GetValue(iteminfo);
                    oldItem.GetType().GetProperty(propname).SetValue(oldItem, pNew_Value, null);
                }

                oldItem.ModifiedBy = MyAccount.UserID;
                oldItem.ModifiedOn = DateTime.Now;

                db.Entry(oldItem).State = EntityState.Modified;
                db.SaveChanges();
                result.Data = iteminfo;
                result.strMess = "Đã lưu thông tin";
                CreateLog(m_ControllerName, "Edit", iteminfo.EmployeeKey.ToString(), iteminfo.EmployeeID, iteminfo.EmployeeName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.Data = null;
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        private string CheckBeforeSave(CAT_EMPLOYEES iteminfo, bool isNew)
        {
            if (string.IsNullOrEmpty(iteminfo.EmployeeID))
                return "Mã nhân viên không được để trống";
            if (string.IsNullOrEmpty(iteminfo.EmployeeName))
                return "Họ tên không được để trống";
            if (iteminfo.Gender == null)
                return "Chưa chọn giới tính";
            //if (iteminfo.DepartmentKey == null)
            //    return "Chưa chọn phòng ban";
            //if (iteminfo.PositionKey == null)
            //    return "Chưa chọn chức vụ";
            return string.Empty;
        }

        // GET: CAT_EMPLOYEES/Delete/5
        public ActionResult Delete(Guid? id)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            result.strMess = "Đã xóa thông tin";
            CAT_EMPLOYEES item = db.CAT_EMPLOYEES.Find(id);
            try
            {
                if (!CheckPermission("EMPLOYEES_EDIT"))
                    throw new Exception(NO_PERMISSION);

                if (item == null)
                    throw new Exception("Không tìm thấy thông tin!");

                item.RecordStatus = 0;
                item.ModifiedBy = MyAccount.UserID;
                item.ModifiedOn = DateTime.Now;

                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                CreateLog(m_ControllerName, "Create", item.EmployeeKey.ToString(), item.EmployeeID, item.EmployeeName, System.Reflection.MethodBase.GetCurrentMethod().Name, result.intStatus == 1 ? eWorkStatus.Success.ToString() : eWorkStatus.Error.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult LoadList(int page, int pageSize,
            string DepartmentName,
            string EmployeeID,
            string EmployeeName,
            string PositionName)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            //Guid Document_ID = new Guid(this.Request["id"]);
            DepartmentName = DepartmentName ?? "";
            EmployeeID = EmployeeID ?? "";
            EmployeeName = EmployeeName ?? "";
            PositionName = PositionName ?? "";

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(DepartmentName))
                table.KeysSearch.Add("DepartmentName", DepartmentName);
            if (!string.IsNullOrEmpty(EmployeeID))
                table.KeysSearch.Add("EmployeeID", EmployeeID);
            if (!string.IsNullOrEmpty(EmployeeName))
                table.KeysSearch.Add("EmployeeName", EmployeeName);
            if (!string.IsNullOrEmpty(PositionName))
                table.KeysSearch.Add("PositionName", PositionName);

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            var searchQuery = from e in db.CAT_EMPLOYEES
                              where e.RecordStatus == 1
                              select e;

            //lấy data-ref
            var fq1 = searchQuery.ToList().Select(d => new
            {
                d.EmployeeKey,
                DepartmentName = db.CAT_DEPARTMENTS.Find(d.DepartmentKey)?.DepartmentName ?? "",
                EmployeeID = d.EmployeeID ?? "",
                EmployeeName = d.EmployeeName ?? "",
                PositionName = db.CAT_POSITIONS.Find(d.PositionKey)?.PositionName ?? "",
                GenderName = db.CAT_GENDERS.Find(d.Gender)?.GenderName ?? "",
                BirthDay = d.BirthDay ?? "",
                Phone = d.Phone ?? ""
            }).OrderBy(p => p.DepartmentName).ThenBy(p => p.EmployeeName).ToList();

            //lọc theo điều kiện
            if (!string.IsNullOrEmpty(DepartmentName))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.DepartmentName.Trim().ToLower()).Contains(Utils.RemoveVni(DepartmentName.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(EmployeeID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.EmployeeID.Trim().ToLower()).Contains(Utils.RemoveVni(EmployeeID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(EmployeeName))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.EmployeeName.Trim().ToLower()).Contains(Utils.RemoveVni(EmployeeName.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(PositionName))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.PositionName.Trim().ToLower()).Contains(Utils.RemoveVni(PositionName.Trim().ToLower()))).ToList();

            int _index = 1;
            var fq2 = fq1.Select(p => new
            {
                p.EmployeeKey,
                STT = _index++,
                p.DepartmentName,
                p.EmployeeID,
                p.EmployeeName,
                p.PositionName,
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/Edit/" + p.EmployeeKey + "'>Sửa</a>" +
                "<button type=\"button\" class=\"btn btn-danger btn_xoa\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + p.EmployeeKey + "'>Xóa </button>"
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 7, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Phòng Ban", IsShow = true, Format = "", FieldName = "DepartmentName", Width = 20, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Nhân Viên", IsShow = true, Format = "", FieldName = "EmployeeID", Width = 10, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Họ Tên", IsShow = true, Format = "", FieldName = "EmployeeName", Width = 30, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Chức Vụ", IsShow = true, Format = "", FieldName = "PositionName", Width = 20, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 12, Width_Measure = "%" });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].EmployeeKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult AutoComplete_Department(string pattern)
        {
            var result = (from p in db.CAT_DEPARTMENTS
                          where p.RecordStatus == 1 &&
                          p.DepartmentName.Contains(pattern)
                          orderby p.DepartmentName
                          select new
                          {
                              data = p.DepartmentKey,
                              value = p.DepartmentName
                          }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
