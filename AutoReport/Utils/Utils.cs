﻿using AutoReport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AutoReport
{
    public static class Utils
    {
        public static bool IsSUForMenu = false;
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        public static List<SelectListItem> GetTypeList()
        {
            List<SelectListItem> lsItems = new List<SelectListItem>();
            lsItems.Add(new SelectListItem() { Text = "string", Value = "string" });
            lsItems.Add(new SelectListItem() { Text = "int", Value = "int" });
            lsItems.Add(new SelectListItem() { Text = "bool", Value = "bool" });
            return lsItems;
        }
        public static bool CheckPermission(SYS_Users zMyAuth, string RoleCode)
        {
            if (IsSUForMenu) return true;

            AutoReportEntities zDB = new AutoReportEntities();
            try
            {
                if (zMyAuth == null)
                {
                    return false;
                }

                if (string.IsNullOrEmpty(RoleCode))
                {
                    return true;
                }
                else
                {
                    var zRole = zDB.SYS_Role.Where(p => p.Code == RoleCode).FirstOrDefault();
                    if (zRole != null)
                    {
                        var zRoleAccess = zDB.SYS_User_Access.Where(p => p.RoleID == zRole.RoleID && p.UserID == zMyAuth.UserID && p.IsAccess).FirstOrDefault();
                        if (zRoleAccess != null)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                zDB.Dispose();
            }
        }

        public static string RemoveVni(string expression)
        {
            string result = string.Empty;
            if (string.IsNullOrEmpty(expression))
                return result;
            string uCharsL = "à á ả ã ạ ă ằ ắ ẳ ẵ ặ â ầ ấ ẩ ẫ ậ đ è é ẻ ẽ ẹ ê ề ế ể ễ ệ ì í ỉ ĩ ị ò ó ỏ õ ọ ô ồ ố ổ ỗ ộ ơ ờ ớ ở ỡ ợ ù ú ủ ũ ụ ư ừ ứ ử ữ ự ỳ ý ỷ ỹ ỵ";
            string uCharsU = "À Á Ả Ã Ạ Ă Ằ Ắ Ẳ Ẵ Ặ Â Ầ Ấ Ẩ Ẫ Ậ Đ È É Ẻ Ẽ Ẹ Ê Ề Ế Ể Ễ Ệ Ì Í Ỉ Ĩ Ị Ò Ó Ỏ Õ Ọ Ô Ồ Ố Ổ Ỗ Ộ Ơ Ờ Ớ Ở Ỡ Ợ Ù Ú Ủ Ũ Ụ Ư Ừ Ứ Ử Ữ Ự Ỳ Ý Ỷ Ỹ Ỵ";
            string nCharsN = "a a a a a a a a a a a a a a a a a d e e e e e e e e e e e i i i i i o o o o o o o o o o o o o o o o o u u u u u u u u u u u y y y y y";

            string[] arrCharsL = uCharsL.Split(' ');
            string[] arrCharsU = uCharsU.Split(' ');
            string[] arrCharsN = nCharsN.Split(' ');

            foreach (char c in expression)
            {
                if (!char.IsLetter(c))
                {
                    result += c;
                    continue;
                }

                int _index = char.IsUpper(c) ? Array.IndexOf(arrCharsU, c.ToString()) : Array.IndexOf(arrCharsL, c.ToString());
                if (_index != -1)
                    result = char.IsUpper(c) ? result + arrCharsN[_index].ToUpper() : result + arrCharsN[_index];
                else
                    result += c;
            }
            return result;
        }
    }
}