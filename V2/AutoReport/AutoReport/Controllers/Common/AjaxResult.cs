﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Controllers
{
    public class AjaxResult
    {
        public AjaxResult()
        {
            Result_Status = 0;
            Result_State = string.Empty;
            Result_Message = string.Empty;
            Result_Caption = "Đã Lưu Thông Tin";
            Data = null;
        }

        public int Result_Status
        {
            get;
            set;
        }

        public string Result_State
        {
            get;
            set;
        }

        public string Result_Caption
        {
            get;
            set;
        }

        public string Result_Message
        {
            get;
            set;
        }

        public object Data
        {
            get;
            set;
        }

        public string RedirectTo
        {
            get;
            set;
        }
    }
}