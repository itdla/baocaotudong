﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using System.Transactions;
using AutoReport.Utils;
using System.Reflection;
using System.Text;

namespace AutoReport.Controllers
{
    public class SYS_USERSController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "SYS_USERS";

        // GET: SYS_USERS/Index
        public ActionResult Index()
        {
            if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                return B_NotPermission();
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult Login(string url)
        {
            ViewBag.AccountSaved = "0";
            if (Request.Cookies["lremember"] != null)
                ViewBag.AccountSaved = Request.Cookies["lremember"].Value;

            ViewBag.UserSaved = "";
            if (Request.Cookies["luser"] != null)
                ViewBag.UserSaved = Request.Cookies["luser"].Value;

            ViewBag.PasswordSaved = "";
            if (Request.Cookies["lpassword"] != null)
                ViewBag.PasswordSaved = Request.Cookies["lpassword"].Value;

            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult DoLogin(string UserName, string Password, bool SaveAccount)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                Password = Password ?? "";
                string pwd_cookie = Password;
                Password = TextUtil.CreateMD5(Password);

                SYS_USERS item = db.SYS_USERS.Where(p => p.UserName == UserName && p.UserPassword == Password).ToList().FirstOrDefault();
                if (item == null)
                    throw new Exception("Tài khoản hoặc mật khẩu không chính xác");

                if (!item.IsActivated)
                    throw new Exception("Tài khoản chưa được kích hoạt!");

                if (item.IsLocked)
                    throw new Exception("Tài khoản tạm thời bị khóa! Vui lòng liên hệ người quản trị!");

                if (item.IsExpired)
                {
                    if (item.ExpiredDate != null)
                    {
                        DateTime expire_date = item.ExpiredDate.Value;
                        if (Now.Date > expire_date.Date)
                        {
                            throw new Exception("Tài khoản đã hết hạn sử dụng! Vui lòng liên hệ người quản trị!");
                        }
                    }
                }
                #region save cookie

                string c_key = "luser";
                if (Request.Cookies[c_key] != null)
                {
                    Response.Cookies.Remove(c_key);
                }
                HttpCookie c_user = new HttpCookie(c_key, UserName);
                Response.Cookies.Add(c_user);

                c_key = "lpassword";
                if (Request.Cookies[c_key] != null)
                {
                    Response.Cookies.Remove(c_key);
                }
                HttpCookie c_password = new HttpCookie(c_key, SaveAccount ? pwd_cookie : "");
                Response.Cookies.Add(c_password);

                c_key = "lremember";
                if (Request.Cookies[c_key] != null)
                {
                    Response.Cookies.Remove(c_key);
                }
                HttpCookie c_remember = new HttpCookie(c_key, SaveAccount ? "1" : "0");
                Response.Cookies.Add(c_remember);

                #endregion

                if (MyAccount == null)
                    MyAccount = new AccountLogged();

                MyAccount.Model = item;

                string currenturl = Convert.ToString(Session["CurrentAccessUrl"]);
                result.RedirectTo = string.IsNullOrEmpty(currenturl) ? "/Home/Index" : currenturl;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult Create()
        {
            if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                return B_NotPermission();

            #region query data

            var query_emp = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).OrderBy(p => p.ItemName).ToList();

            #endregion

            ViewBag.ControllerName = m_ControllerName;
            ViewBag.EmployeeList = new SelectList(query_emp, "dKey", "ItemName");

            var lst_items = db.SYS_USERS.Where(p => p.RecordStatus == 1).ToList();
            SYS_USERS item = new SYS_USERS()
            {
                Sorted = lst_items.Count == 0 ? 1 : lst_items[lst_items.Count - 1].Sorted + 1,
                IsDefault = false,
                IsLocked = false,
                IsActivated = true,
                IsExpired = true,
                ExpiredDate = Now.AddDays(30)
            };

            return View(item);
        }

        [HttpPost]
        public ActionResult Create(SYS_USERS ItemModel, List<string> lstAuthChecked, string SaveType)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_CREATE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, true);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);
                using (TransactionScope scope = new TransactionScope())
                {
                    ItemModel.UserPassword = TextUtil.CreateMD5(ItemModel.UserPassword);
                    ItemModel.PasswordRetype = null;
                    ItemModel.CreatedBy = MyAccount.Model.dKey.ToString();
                    ItemModel.ModifiedBy = MyAccount.Model.dKey.ToString();
                    ItemModel.CreatedOn = Now;
                    ItemModel.ModifiedOn = Now;
                    ItemModel.RecordStatus = 1;
                    ItemModel.IsDefault = false;
                    ItemModel.IsLocked = false;
                    db.SYS_USERS.Add(ItemModel);
                    db.SaveChanges();

                    if (lstAuthChecked != null)
                    {
                        foreach (string authcode in lstAuthChecked)
                        {
                            //format: U_<rolekey>.Format("0000")
                            string key = authcode.Replace("U_", "");

                            SYS_ROLES_ACCESS ra = new SYS_ROLES_ACCESS();
                            ra.UserKey = ItemModel.dKey;
                            ra.RoleKey = Convert.ToInt32(key);
                            ra.IsAccess = true;
                            db.SYS_ROLES_ACCESS.Add(ra);
                        }
                        db.SaveChanges();
                    }
                    scope.Complete();
                }

                result.Result_Status = 1;
                result.Result_Message = CREATE_SUCCESS;
                if (SaveType == "savenew")
                    result.RedirectTo = string.Format("/{0}/Create", m_ControllerName);
                else
                    result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    null,
                    ItemModel.UserName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        // GET: SYS_USERS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                return B_NotPermission();

            #region query data

            var query_emp = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).OrderBy(p => p.ItemName).ToList();

            #endregion

            ViewBag.ControllerName = m_ControllerName;
            SYS_USERS item = db.SYS_USERS.Find(id);
            if (item == null)
                return B_NotFound();
            if (item.RecordStatus == 0)
                return B_NotFound();

            item.CreatedBy = GetCreatorName(item.CreatedBy);
            item.ModifiedBy = GetCreatorName(item.ModifiedBy);

            ViewBag.EmployeeList = new SelectList(query_emp, "dKey", "ItemName");
            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(SYS_USERS ItemModel, List<string> lstAuthChecked)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_EDIT", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                result.Result_Message = CheckBeforeSave(ItemModel, false);
                if (!string.IsNullOrEmpty(result.Result_Message))
                    throw new Exception(result.Result_Message);

                SYS_USERS oldItem = db.SYS_USERS.Find(ItemModel.dKey);
                if (oldItem == null)
                    throw new Exception(NOT_FOUND);

                if (oldItem.UserPassword != ItemModel.UserPassword)
                {
                    if (ItemModel.UserPassword != ItemModel.PasswordRetype)
                        throw new Exception("Mật khẩu xác nhận không chính xác");
                    ItemModel.UserPassword = TextUtil.CreateMD5(ItemModel.UserPassword);
                }
                //save model
                string[] arr_model = { "UserName", "UserPassword", "Abbr", "IsLocked", "IsActivated", "IsExpired", "ExpiredDate", "EmployeeKey", "Sorted", "Remarks" };
                foreach (string pName in arr_model)
                {
                    PropertyInfo pInfo = oldItem.GetType().GetProperty(pName);
                    object v_to_set = ItemModel.GetType().GetProperty(pName).GetValue(ItemModel, null);
                    pInfo.SetValue(oldItem, v_to_set);
                }
                oldItem.ModifiedBy = MyAccount.Model.dKey.ToString();
                oldItem.ModifiedOn = Now;
                db.Entry(oldItem).State = EntityState.Modified;

                if (lstAuthChecked != null)
                {
                    var lst_roles = db.SYS_ROLES_ACCESS.Where(p => p.UserKey == ItemModel.dKey).ToList();
                    lst_roles.ForEach(p => p.IsAccess = false);// set all to false for later code
                    foreach (string authcode in lstAuthChecked)
                    {
                        //format: U_<rolekey>.Format("0000")
                        int rolekey = Convert.ToInt32(authcode.Replace("U_", ""));
                        var _role = lst_roles.Where(p => p.RoleKey == rolekey).FirstOrDefault();
                        if (_role != null)
                        {
                            _role.IsAccess = true;
                            db.Entry(_role).State = EntityState.Modified;
                        }
                        else
                        {
                            _role = new SYS_ROLES_ACCESS();
                            _role.UserKey = ItemModel.dKey;
                            _role.RoleKey = rolekey;
                            _role.IsAccess = true;
                            db.SYS_ROLES_ACCESS.Add(_role);
                        }
                    }
                    db.SaveChanges();
                }

                db.SaveChanges();
                result.RedirectTo = string.Format("/{0}/Edit/{1}", m_ControllerName, ItemModel.dKey);
                result.Result_Message = UPDATE_SUCCESS;
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    ItemModel.dKey.ToString(),
                    null,
                    ItemModel.UserName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        private string CheckBeforeSave(SYS_USERS ItemModel, bool isNew)
        {
            if (string.IsNullOrEmpty(ItemModel.UserName))
                return "Tên đăng nhập không được để trống";
            if (string.IsNullOrEmpty(ItemModel.UserPassword))
                return "Mật khẩu không được để trống";

            if (isNew)
            {
                if (ItemModel.UserPassword != ItemModel.PasswordRetype)
                    return "Mật khẩu xác nhận không chính xác";
            }
            return string.Empty;
        }

        // GET: SYS_USERS/Delete/5
        public ActionResult Delete(int? id)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                if (!GrantAccess(string.Format("{0}_DELETE", m_ControllerName)))
                    throw new Exception(NOT_PERMISSION);

                SYS_USERS model = db.SYS_USERS.Find(id);
                if (model == null)
                    throw new Exception(NOT_FOUND);

                model.ModifiedBy = MyAccount.Model.dKey.ToString();
                model.ModifiedOn = Now;
                model.RecordStatus = 0;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                result.Result_Message = DELETE_SUCCESS;
                result.RedirectTo = string.Format("/{0}/Index", m_ControllerName);
                result.Result_Status = 1;

                WriteLog(
                    m_ControllerName,
                    Request.RequestContext.RouteData.Values["action"].ToString(),
                    MethodBase.GetCurrentMethod().ToString(),
                    result.Result_Status.ToString(),
                    result.Result_Message,
                    model.dKey.ToString(),
                    null,
                    model.UserName
                    );
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetList(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                page = page == 0 ? 1 : page;
                var lstItems = (from d in db.SYS_USERS
                                where d.RecordStatus == 1
                                select new
                                {
                                    d.dKey,
                                    d.UserName,
                                    d.Abbr,
                                    d.EmployeeKey,
                                    d.Remarks,
                                    d.IsActivated,
                                    d.IsLocked,
                                    d.ExpiredDate
                                }).ToList();
                if (!GrantAccess(string.Format("{0}_INDEX", m_ControllerName)))
                    lstItems.Clear();

                TableModel table = new TableModel();
                table.HeaderColumns.Add(new TableHeaderColumns() { Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { Caption = "STT", Width = 9, Alignment = "center" });
                table.HeaderColumns.Add(new TableHeaderColumns() { Caption = "Tên Người Dùng", Width = 20 });
                table.HeaderColumns.Add(new TableHeaderColumns() { Caption = "Nhân Sự", Width = 20 });
                table.HeaderColumns.Add(new TableHeaderColumns() { Caption = "Tình Trạng", Width = 20 });
                table.HeaderColumns.Add(new TableHeaderColumns() { Caption = "Ghi Chú", Width = 30 });

                table.CalcTotalPage(lstItems.Count);
                int _index = 1;
                var lst_Emp = db.CAT_EMPLOYEES.Where(p => p.RecordStatus == 1).ToList();

                var data = lstItems.Select(p => new
                {
                    p.dKey,
                    STT = _index++,
                    p.UserName,
                    EmpName = lst_Emp.Where(d => d.dKey == p.EmployeeKey).FirstOrDefault()?.ItemName ?? "-",
                    TinhTrang = !p.IsActivated ? "Chưa kích hoạt" : p.IsLocked ? "Bị Khóa" : (p.ExpiredDate == null ? "Hoạt động" : (Now.Date - p.ExpiredDate.Value).TotalDays < 0 ? "Hết Hạn" : "Hoạt động"),
                    p.Remarks
                }).Skip((page - 1) * table.PageSize).Take(table.PageSize).ToList();

                foreach (var item in data)
                {
                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = item.dKey,
                        DataArray = item
                    });
                }
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult LoadTree(int userkey)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                List<TreeCheckHierarchical> treelist = new List<TreeCheckHierarchical>();

                var query = (from r in db.SYS_ROLES
                             orderby r.ParentKey, r.Sorted
                             select r).ToList();

                var lst_user_roles = (from ur in db.SYS_ROLES_ACCESS
                                      where ur.UserKey == userkey
                                      select ur).ToList();

                foreach (SYS_ROLES role in query)
                {
                    var treenode = new TreeCheckHierarchical();
                    treenode.Node_Id = "U_" + role.dKey.ToString("0000");
                    treenode.Node_Text = role.ItemName;
                    treenode.NodeParent_Id = "U_" + (role.ParentKey ?? 0).ToString("0000");
                    treenode.NodeParent_Text = treenode.NodeParent_Id == "U_0000" ? "" : query.Find(p => p.dKey == role.ParentKey)?.ItemName;
                    treenode.IsTopMostParent = treenode.NodeParent_Id == "U_0000";
                    treenode.IsLeaf = query.Where(p => p.ParentKey == role.dKey).Count() == 0;
                    treenode.Sorted = role.Sorted ?? 0;
                    treenode.IsChecked = lst_user_roles.Where(p => p.RoleKey == role.dKey && p.IsAccess == true).Count() != 0;

                    treelist.Add(treenode);
                }
                treelist = treelist.OrderBy(p => p.NodeParent_Id).ThenBy(p => p.Sorted).ToList();
                result.Data = CreateTreeCheck(treelist);
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Data = "<p>Không có dữ liệu<p>";
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        private string CreateTreeCheck(List<TreeCheckHierarchical> treelist)
        {
            StringBuilder builder = new StringBuilder();
            string nl = Environment.NewLine;

            builder.Append("<ul class=\"ultree\">" + nl);
            foreach (TreeCheckHierarchical item in treelist.Where(p => p.IsTopMostParent).ToList())
            {
                builder.Append("<li aria-expanded=\"true\">" + nl);
                builder.Append(string.Format("<input type=\"checkbox\" id=\"{0}\" data-parent=\"\" class=\"fancytree-checkbox\" {1}/>", item.Node_Id, item.IsChecked ? "checked" : "") + nl);
                builder.Append(string.Format("<span class=\"fancytree-title\">{0}</span>", item.Node_Text) + nl);
                AppendNodeTree(treelist, builder, item.Node_Id);
                builder.Append("</li>" + nl);
            }
            builder.Append("</ul>");
            return builder.ToString();
        }

        private void AppendNodeTree(List<TreeCheckHierarchical> treelist, StringBuilder builder, string parentid)
        {
            string nl = Environment.NewLine;
            var lst_child = treelist.Where(p => p.NodeParent_Id == parentid).ToList();
            if (lst_child.Count > 0)
            {
                var lst_child_nonchildren = lst_child.Where(p => p.IsLeaf).ToList();
                var lst_child_children = lst_child.Where(p => !p.IsLeaf).ToList();

                builder.Append("<ul class=\"ultree\">" + nl);

                if (lst_child_nonchildren.Count > 0)
                {
                    builder.Append("<li aria-expanded=\"true\">" + nl);
                    builder.Append("<div class=\"li-leaf\">" + nl);
                    foreach (TreeCheckHierarchical item in lst_child_nonchildren)
                    {
                        builder.Append(string.Format("<input type=\"checkbox\" id=\"{0}\" data-parent=\"{1}\" class=\"fancytree-checkbox {1}\" {2}/>", item.Node_Id, item.NodeParent_Id, item.IsChecked ? "checked" : "") + nl);
                        builder.Append(string.Format("<span class=\"fancytree-title\">{0}</span>", item.Node_Text) + nl);
                    }
                    builder.Append("</div>" + nl);
                    builder.Append("</li>" + nl);
                }

                foreach (TreeCheckHierarchical item in lst_child_children)
                {
                    builder.Append("<li aria-expanded=\"true\">" + nl);
                    builder.Append(string.Format("<input type=\"checkbox\" id=\"{0}\" data-parent=\"{1}\" class=\"fancytree-checkbox {1}\" {2}/>", item.Node_Id, item.NodeParent_Id, item.IsChecked ? "checked" : "") + nl);
                    builder.Append(string.Format("<span class=\"fancytree-title\">{0}</span>", item.Node_Text) + nl);
                    AppendNodeTree(treelist, builder, item.Node_Id);
                    builder.Append("</li>" + nl);
                }

                //foreach (TreeCheckHierarchical item in lst_child)
                //{
                //    builder.Append("<li aria-expanded=\"true\">" + nl);
                //    builder.Append(string.Format("<input type=\"checkbox\" id=\"{0}\" data-parent=\"{1}\" class=\"fancytree-checkbox {1}\" />", item.Node_Id, item.NodeParent_Id) + nl);
                //    builder.Append(string.Format("<span class=\"fancytree-title\">{0}</span>", item.Node_Text) + nl);
                //    AppendNodeTree(treelist, builder, item.Node_Id);
                //    builder.Append("</li>" + nl);
                //}
                builder.Append("</ul>");
            }

        }

    }
}
