﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoReport.Models
{
    public class TableModels
    {
        public int intStatus;
        public string strMess;
        public int intTotalPage;
        public int intCurrentPage;
        public bool isShowSearch;
        public List<TableHeader> lstHeader;
        public string strURLDetail;
        public string strURLEdit;
        public string strURLAdd;
        public string strURLDelete;
        public string Session;
        public List<TableItem> lstData;
        public bool isShowAction;

        /// <summary>
        /// To enable search filter, manual code
        /// </summary>
        public bool EnableSearch = false;
        public Dictionary<string, string> KeysSearch;

        public List<ExButton> LstExColumn = new List<ExButton>();
    }
    public class ExButton
    {
        public string ID;
        public string strURL;
        public bool IsShow;
        public string Format;
        public string ICon;
        public string Title;
        public string Color;
    }

    public enum DataType
    {
        NormalType = 1,
        DateType = 2,
        TimeType = 3,
        Boolean = 4,
        RowNumber = 99,
    }

    public enum DataContentAlignment
    {
        Left = 1,
        Center = 2,
        Right = 3
    }

    public class TableHeader
    {
        /// <summary>
        /// For seach-box
        /// </summary>
        public string FieldName = "";

        /// <summary>
        /// Table Header Caption
        /// </summary>
        public string Caption;

        /// <summary>
        /// Default button action show
        /// </summary>
        public bool IsShow = true;

        /// <summary>
        /// Default column data content (not Header) alignment. 1=left; 2=center; 3=right
        /// </summary>
        public int ColumnAlignment = 1;

        /// <summary>
        /// Default field value content formation
        /// </summary>
        public string Format = "";

        /// <summary>
        /// Default field value content data type
        /// </summary>
        public DataType Type = DataType.NormalType;

        /// <summary>
        /// Calculate as percentage
        /// </summary>
        public int Width = 0;

        /// <summary>
        /// Measurement of html elements width. Manual set, default = 'px' (set by '%' or 'px' only)
        /// </summary>
        public string Width_Measure = "px";
    }

    public class TableItem
    {
        public object objData;
        public bool isShowDetail;
        public bool isShowEdit;
        public bool isShowDelete;
        public bool isShowAdd;
        public bool[] ExButtonShow;
        public string ID;
        public bool IsEditModal = false;
        public bool IsDetailModal = false;
    }
}