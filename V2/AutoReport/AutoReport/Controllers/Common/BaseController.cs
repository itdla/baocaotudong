﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using AutoReport.Utils;

namespace AutoReport.Controllers
{
    public class BaseController : Controller
    {
        private string Path_SystemLog = "/Log/";

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.Url.ToString().Contains("Login") && !filterContext.HttpContext.Request.Url.ToString().Contains("System"))
            {
                if (this.MyAccount == null)
                {
                    filterContext.Result = new RedirectResult("/SYS_USERS/Login");
                    //Session["CurrentAccessUrl"] = filterContext.HttpContext.Request.Url.AbsolutePath;
                    return;
                }
            }

            //base.OnActionExecuting(filterContext);
        }

        public AccountLogged MyAccount
        {
            get { return Session["MyAccount"] as AccountLogged; }
            set { Session["MyAccount"] = value; }
        }

        protected const string NOT_PERMISSION = "Bạn không có quyền truy cập chức năng này";
        protected const string NOT_FOUND = "Thông tin bạn đang muốn truy cập không tìm thấy hoặc đã bị xóa";
        protected const string CREATE_SUCCESS = "Thêm mới thành công";
        protected const string UPDATE_SUCCESS = "Cập nhật thành công";
        protected const string DELETE_SUCCESS = "Xóa thành công";

        protected const string Path_Template_Main = "/FilesContent/Templates/Main/";
        protected const string Path_Template_Revision = "/FilesContent/Templates/Revision/";

        protected const string Path_RptHeader_Main = "/FilesContent/Header/Main/";
        protected const string Path_RptHeader_Revision = "/FilesContent/Header/Revision/";
        protected const string Path_RptHeader_Attached = "/FilesContent/Header/Attached/";
        protected const string Path_RptHeader_Donwload = "/FilesContent/Header/Download/";

        protected DateTime Now
        {
            get { return DateTime.Now; }
        }

        protected string Now_Full_String()
        {
            return string.Format("{0}{1}{2}{3}{4}{5}", new object[]
                {
                    Now.Year.ToString(),
                    Now.Month.ToString("00"),
                    Now.Day.ToString("00"),
                    Now.Hour.ToString("00"),
                    Now.Minute.ToString("00"),
                    Now.Second.ToString("00")
                });
        }

        protected ActionResult LoadHeaderLinkReturn(string[] lstLinks, string[] lstText)
        {
            AjaxResult result = new AjaxResult();
            result.Result_Status = 1;
            result.Data = string.Empty;
            try
            {
                StringBuilder sb = new StringBuilder();
                string nl = Environment.NewLine;

                if (lstLinks != null)
                {
                    for (int i = 0; i < lstLinks.Length; i++)
                    {
                        if (sb.Length > 0)
                            sb.Append(nl);
                        sb.Append(string.Format("<a href=\"{0}\" class=\"breadcrumb-item\"> {1}</a>", lstLinks[i], lstText[i]));
                    }
                }
                result.Data = sb.ToString();
            }
            catch (Exception ex)
            {
                result.Result_Message = ex.Message;
                result.Result_State = "ERROR";
                result.Result_Status = 0;
            }
            return new CustomJsonResult() { Data = result };
        }

        protected List<SelectListItem> GetList_Employee_WorkStatus()
        {
            List<SelectListItem> emp_work_status = new List<SelectListItem>();
            emp_work_status.Add(new SelectListItem() { Text = "-", Value = "-1" });
            emp_work_status.Add(new SelectListItem() { Text = "Đã Nghỉ Việc", Value = "0" });
            emp_work_status.Add(new SelectListItem() { Text = "Còn làm việc", Value = "1" });


            return emp_work_status;
        }

        protected List<TEMPLATES_HEADERS_DEPARTMENTS> GetListTemplateRestricted()
        {
            List<TEMPLATES_HEADERS_DEPARTMENTS> lstresult = new List<TEMPLATES_HEADERS_DEPARTMENTS>();
            try
            {
                using (AutoReport_V3Entities db = new AutoReport_V3Entities())
                {
                    var employee = db.CAT_EMPLOYEES.Find(MyAccount.Model.EmployeeKey);
                    var department = db.CAT_DEPARTMENTS.Find(employee.DepartmentKey);
                    if (MyAccount.Model.IsDefault)
                    {
                        //lấy toàn bộ templates nếu là tài khoản cao nhất
                        var lsttemplate = db.TEMPLATES_HEADERS.Where(p => p.RecordStatus == 1).ToList();
                        lsttemplate.ForEach(item =>
                        {
                            lstresult.Add(new TEMPLATES_HEADERS_DEPARTMENTS()
                            {
                                TemplateHeaderKey = item.dKey,
                                DepartmentKey = department.dKey
                            });
                        });
                    }
                    else
                    {
                        //chỉ lấy danh sách templates giới hạn
                        var lsttemplate_restricted = db.TEMPLATES_HEADERS_DEPARTMENTS.Where(p => p.DepartmentKey == department.dKey).ToList();
                        lsttemplate_restricted.ForEach(item =>
                        {
                            lstresult.Add(new TEMPLATES_HEADERS_DEPARTMENTS()
                            {
                                TemplateHeaderKey = item.dKey,
                                DepartmentKey = item.DepartmentKey
                            });
                        });
                    }
                }
            }
            catch 
            {

            }
            
            return lstresult;
        }

        public bool GrantAccess(string Code)
        {
            string error = string.Empty;
            bool result = false;
            try
            {
                if (MyAccount == null)
                    return result;
                if (MyAccount.Model.IsDefault)
                    return true;

                int userkey = MyAccount.Model.dKey;
                using (AutoReport_V3Entities db = new AutoReport_V3Entities())
                {
                    var query = (from ra in db.SYS_ROLES_ACCESS
                                 join rm in db.SYS_ROLES
                                 on ra.RoleKey equals rm.dKey
                                 where ra.UserKey == userkey && rm.ItemCode == Code && ra.IsAccess == true
                                 select new
                                 {
                                     rm.ItemName,
                                     ra.IsAccess
                                 }).ToList();
                    result = query.Count() > 0;
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            return result;
        }

        protected RedirectToRouteResult B_NotFound()
        {
            return RedirectToAction("NotFound", "Home");
        }

        protected RedirectToRouteResult B_NotPermission()
        {
            return RedirectToAction("NotPermission", "Home");
        }

        protected RedirectToRouteResult B_DataRestricted()
        {
            return RedirectToAction("DataRestricted", "Home");
        }

        protected string GetCreatorName(string id)
        {
            string result = "N/A";
            string msg = string.Empty;
            try
            {
                AutoReport_V3Entities db = new AutoReport_V3Entities();
                int key = Convert.ToInt32(id);
                SYS_USERS item = db.SYS_USERS.Find(key);
                if (item != null)
                    result = item.UserName;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return result;
        }

        protected void WriteLog(string ControllerName, string ActionName, string MethodName, string ProgressResult, string ProgressResultMessage, string RefKey, string RefID, string RefName)
        {
            try
            {
                using (AutoReport_V3Entities db = new AutoReport_V3Entities())
                {
                    var itemlog = new SYS_LOG_ACTIONS();
                    itemlog.LogControllerName = ControllerName;
                    itemlog.LogAction = ActionName;
                    itemlog.LogMethodName = MethodName;
                    itemlog.LogResult = ProgressResult;
                    itemlog.LogMessage = ProgressResultMessage;
                    itemlog.DataKey = RefKey;
                    itemlog.DataID = RefID;
                    itemlog.DataName = RefName;
                    itemlog.UserKey = MyAccount.Model.dKey;
                    itemlog.UserName = MyAccount.Model.UserName;
                    itemlog.DateCreated = Now;

                    db.SYS_LOG_ACTIONS.Add(itemlog);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                StringBuilder builder = new StringBuilder();
                string nl = Environment.NewLine;
                builder.Append(string.Format("[{0}]", Now.ToString("yyyy/MM/dd HH:mm:ss")) + nl);
                builder.Append(string.Format("Error occured: ") + nl);
                builder.Append(string.Format(" At Controller:{0}, at Action: {1}, at Method: {2}", ControllerName, ActionName, MethodName) + nl);
                builder.Append(nl);
                builder.Append(string.Format("Detail: ") + nl);
                builder.Append(GetExceptionInner(ex));
                builder.Append(nl);
                builder.Append(nl);

                LogSystem(builder.ToString());
            }
        }

        private string GetExceptionInner(Exception ex)
        {
            StringBuilder builder = new StringBuilder();
            string nl = Environment.NewLine;
            builder.Append(ex.Message + nl);
            if (ex.InnerException != null)
                builder.Append(GetExceptionInner(ex.InnerException));
            return builder.ToString();
        }

        private void LogSystem(string msg)
        {
            string logfile = string.Format("{0}{1}{2}_log.txt", Now.Year, Now.Month, Now.Day);
            System.IO.File.AppendAllText(Server.MapPath(logfile), msg);
        }
    }
}