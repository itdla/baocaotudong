﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;

namespace AutoReport.Controllers
{
    public class RPT_INPUT_PREVIEWController : BaseController
    {
        private AutoReportEntities db = new AutoReportEntities();

        private string m_ControllerName = "RPT_INPUT_PREVIEW";

        //xem danh sách báo cáo theo tình trạng
        public ActionResult IndexViewSearch()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult RptInputListIndex()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult Details(Guid? id)
        {
            ViewBag.ControllerName = m_ControllerName;
            if (id == null)
            {
                return HttpNotFound();
            }
            ViewBag.TemplatesList = new SelectList(db.RPT_TEMPLATES.Where(p => p.RecordStatus == 1).ToList().OrderBy(p => p.Templates_Name).ToList(), "Rpt_TemplatesKey", "Templates_Name");
            RPT_REPORTHEADER iteminfo = db.RPT_REPORTHEADER.Find(id);
            if (iteminfo == null)
            {
                return HttpNotFound();
            }
            return View(iteminfo);
        }

        public ActionResult RptValuesInput_Index()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult RptSummaryChartYear()
        {
            ViewBag.ControllerName = m_ControllerName;

            var lst_YearRpt = (from r in db.RPT_REPORTHEADER
                               where r.RptStatus != 0
                               select r.Rpt_Year).ToList();

            int min_Year = DateTime.Now.Year;
            if (lst_YearRpt.Count > 0)
                min_Year = lst_YearRpt.Min();

            List<SelectListItem> lstYear = new List<SelectListItem>();
            for (int i = min_Year; i <= DateTime.Now.Year; i++)
                lstYear.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString(), Selected = i == DateTime.Now.Year });
            List<SelectListItem> lstMonth = new List<SelectListItem>();
            lstMonth.Add(new SelectListItem() { Text = "(Tất cả)", Value = "0", Selected = true });
            for (int i = 1; i <= 12; i++)
                lstMonth.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });

            ViewBag.ListRptYear = lstYear;
            ViewBag.ListRptMonth = lstMonth;

            return View();
        }

        public ActionResult RptSummaryGridYear()
        {
            ViewBag.ControllerName = m_ControllerName;

            var lst_YearRpt = (from r in db.RPT_REPORTHEADER
                               where r.RptStatus != 0
                               select r.Rpt_Year).ToList();

            int min_Year = DateTime.Now.Year;
            if (lst_YearRpt.Count > 0)
                min_Year = lst_YearRpt.Min();

            List<SelectListItem> lstYear = new List<SelectListItem>();
            for (int i = min_Year; i <= DateTime.Now.Year; i++)
                lstYear.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString(), Selected = i == DateTime.Now.Year });
            List<SelectListItem> lstMonth = new List<SelectListItem>();
            lstMonth.Add(new SelectListItem() { Text = "(Tất cả)", Value = "0", Selected = true });
            for (int i = 1; i <= 12; i++)
                lstMonth.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });

            List<SelectListItem> lstStatus = new List<SelectListItem>();
            lstStatus.Add(new SelectListItem() { Text = "(Tất cả)", Value = "0", Selected = true });
            lstStatus.Add(new SelectListItem() { Text = "Hoàn Thành", Value = "1", Selected = false });
            lstStatus.Add(new SelectListItem() { Text = "Đang Làm", Value = "2", Selected = false });
            lstStatus.Add(new SelectListItem() { Text = "Quá Hạn", Value = "3", Selected = false });


            ViewBag.ListRptYear = lstYear;
            ViewBag.ListRptMonth = lstMonth;
            ViewBag.ListStatus = lstStatus;
            ViewBag.ListTemplates = new SelectList(db.RPT_TEMPLATES.Where(p => p.RecordStatus == 1).OrderBy(p => p.Templates_Name).ToList(), "Rpt_TemplatesKey", "Templates_Name");

            return View();
        }

        public ActionResult RptValuesInput_Edit(Guid? id)
        {
            ViewBag.ControllerName = m_ControllerName;
            RPT_REPORTHEADER item = db.RPT_REPORTHEADER.Find(id);

            Guid rptkey = id.Value;
            Guid userkey = MyAccount.UserID;
            RPT_REPORT_SENT_APPROVED rpt_inputcomplete = db.RPT_REPORT_SENT_APPROVED.Where(p => p.Rpt_ReportHeaderKey == rptkey && p.Rpt_DataType == 0 && p.CreatedBy == userkey).FirstOrDefault();
            ViewBag.InputCompleted = rpt_inputcomplete == null ? "no" : "yes";

            return View(item);
        }

        public ActionResult RptReviewInput_Index()
        {
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult RptReviewInput_Detail(Guid? id)
        {
            ViewBag.ControllerName = m_ControllerName;

            #region điều kiện duyệt

            //số người tham gia nhập liệu bảng phụ lục
            var lst_p_input_tbls = (from re in db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                                    join tbl in db.RPT_REPORTDETAIL_TABLEINDEX
                                        on re.Rpt_ReportDetail_TableIndexKey equals tbl.Rpt_ReportDetail_TableIndexKey
                                    where re.Activate == true && tbl.Rpt_ReportHeaderKey == id
                                    select new
                                    {
                                        tbl.Rpt_ReportHeaderKey,
                                        re.EmployeeKey
                                    }).ToList().Distinct().ToList();

            List<int> lst_status = new List<int>();
            lst_status.AddRange(new int[]
            {
                Convert.ToInt32(eRptInputStatus.Done)
            });//tình trạng nhập liệu

            int rptInputDataValue = Convert.ToInt32(eRptDataType.DataValue);
            int rptInputDataTable = Convert.ToInt32(eRptDataType.DataTable);

            //danh sách nhân viên đã hoàn thành nhập liệu trường dữ liệu
            var lst_p_values_inputcompleted = (from d in db.RPT_REPORT_SENT_APPROVED
                                               where d.Rpt_DataType == rptInputDataValue
                                               && d.Rpt_ReportHeaderKey == id
                                               select d).ToList();

            //danh sách nhân viên đã hoàn thành nhập liệu bảng phụ lục
            var lst_p_tbl_inputcompleted = (from d in db.RPT_REPORT_SENT_APPROVED
                                            where d.Rpt_DataType == rptInputDataTable
                                            && d.Rpt_ReportHeaderKey == id
                                            select d).ToList();

            lst_p_values_inputcompleted = lst_p_values_inputcompleted
                .Where(p => p.InputStatus != null && lst_status.Contains(p.InputStatus.Value)).ToList();

            lst_p_tbl_inputcompleted = lst_p_tbl_inputcompleted
                .Where(p => p.InputStatus != null && lst_status.Contains(p.InputStatus.Value)).ToList();

            #endregion

            RPT_REPORTHEADER item = db.RPT_REPORTHEADER.Find(id);
            ViewBag.AllowApprove = lst_p_input_tbls.Count == lst_p_tbl_inputcompleted.Count &&
                (item.Person_InputValues == null || (item.Person_InputValues != null && lst_p_values_inputcompleted.Count > 0));
            return View(item);
        }

        public ActionResult RptReviewInput_Decline(Guid? id)
        {
            RPT_REPORTHEADER item = db.RPT_REPORTHEADER.Find(id);
            ViewBag.ControllerName = m_ControllerName;
            return View(item);
        }

        public ActionResult RptReviewFileAttach(Guid? id)
        {
            ViewBag.ControllerName = m_ControllerName;
            RPT_REPORTHEADER item = db.RPT_REPORTHEADER.Find(id);
            return View(item);
        }

        public ActionResult LoadList_ReviewInput(int page, int pageSize, string Rpt_Name, int? Rpt_Month, int? Rpt_Year, string NgayHetHan, string GiaTriXong, string PhuLucXong)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */

            Rpt_Name = Rpt_Name ?? "";
            Rpt_Month = Rpt_Month ?? 0;
            Rpt_Year = Rpt_Year ?? 0;
            NgayHetHan = NgayHetHan ?? "";
            GiaTriXong = GiaTriXong ?? "";
            PhuLucXong = PhuLucXong ?? "";

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(Rpt_Name))
                table.KeysSearch.Add("Rpt_Name", Rpt_Name);
            if (Rpt_Month != 0)
                table.KeysSearch.Add("Rpt_Month", Rpt_Month.ToString());
            if (Rpt_Year != 0)
                table.KeysSearch.Add("Rpt_Year", Rpt_Year.ToString());
            if (!string.IsNullOrEmpty(NgayHetHan))
                table.KeysSearch.Add("NgayHetHan", NgayHetHan);
            if (!string.IsNullOrEmpty(GiaTriXong))
                table.KeysSearch.Add("GiaTriXong", GiaTriXong);
            if (!string.IsNullOrEmpty(PhuLucXong))
                table.KeysSearch.Add("PhuLucXong", PhuLucXong);

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter

            int rptHeaderStatus = Convert.ToInt32(eRptHeaderStatus.Created);
            int rptInputDataValue = Convert.ToInt32(eRptDataType.DataValue);
            int rptInputDataTable = Convert.ToInt32(eRptDataType.DataTable);

            Guid userkey = MyAccount.UserID;
            var searchQuery = (from f in db.RPT_REPORTHEADER
                               where f.RecordStatus == 1 &&
                               f.RptStatus == rptHeaderStatus &&
                               f.CreatedBy == userkey
                               orderby f.CreatedOn
                               select f).ToList();

            if (MyAccount.IsDefaultUser != null && !MyAccount.IsDefaultUser.Value)
            {
                //org account
                var emp = db.CAT_EMPLOYEES.Find(MyAccount.EmployeeKey);
                if (emp != null)
                {
                    Guid? departmentkey = emp.DepartmentKey;

                    var query_template = db.RPT_TEMPLATES_DEPARTMENT.Where(p => p.DepartmentKey == departmentkey).ToList()
                        .Select(p => p.Rpt_TemplatesKey).ToList();

                    searchQuery = searchQuery.Where(p => query_template.Contains(p.Rpt_TemplatesKey)).ToList();
                }
                else
                {
                    searchQuery.Clear();
                }
            }

            var lst_rptkey = searchQuery.Select(p => p.Rpt_ReportHeaderKey).ToList();

            List<int> lst_status = new List<int>();
            lst_status.AddRange(new int[]
            {
                Convert.ToInt32(eRptInputStatus.Done)
            });//tình trạng nhập liệu

            //số người tham gia nhập liệu bảng phụ lục
            var lst_p_input_tbls = (from re in db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                                    join tbl in db.RPT_REPORTDETAIL_TABLEINDEX
                                        on re.Rpt_ReportDetail_TableIndexKey equals tbl.Rpt_ReportDetail_TableIndexKey
                                    where re.Activate == true &&
                                    lst_rptkey.Contains(tbl.Rpt_ReportHeaderKey)
                                    select new
                                    {
                                        tbl.Rpt_ReportHeaderKey,
                                        re.EmployeeKey
                                    }).ToList().Distinct().ToList();
            //số người tham giam nhập liệu trường dữ liệu
            var lst_p_input_values = (from re in db.RPT_REPORTVALUES_EMPLOYEES_ENLISTED
                                      join v in db.RPT_REPORTDETAIL_VALUECONST
                                         on re.Rpt_ReportDetail_ValueConstKey equals v.Rpt_ReportDetail_ValueConstKey
                                      where re.Activate == true &&
                                      lst_rptkey.Contains(v.Rpt_ReportHeaderKey)
                                      select new
                                      {
                                          v.Rpt_ReportHeaderKey,
                                          re.EmployeeKey
                                      }).ToList().Distinct().ToList();

            //danh sách nhân viên đã hoàn thành nhập liệu trường dữ liệu
            var lst_p_values_inputcompleted = (from d in db.RPT_REPORT_SENT_APPROVED
                                               where d.Rpt_DataType == rptInputDataValue &&
                                               lst_rptkey.Contains(d.Rpt_ReportHeaderKey)
                                               select d).ToList();

            //danh sách nhân viên đã hoàn thành nhập liệu bảng phụ lục
            var lst_p_tbl_inputcompleted = (from d in db.RPT_REPORT_SENT_APPROVED
                                            where d.Rpt_DataType == rptInputDataTable &&
                                            lst_rptkey.Contains(d.Rpt_ReportHeaderKey)
                                            select d).ToList();

            lst_p_values_inputcompleted = lst_p_values_inputcompleted
                .Where(p => p.InputStatus != null && lst_status.Contains(p.InputStatus.Value)).ToList();

            lst_p_tbl_inputcompleted = lst_p_tbl_inputcompleted
                .Where(p => p.InputStatus != null && lst_status.Contains(p.InputStatus.Value)).ToList();

            var fq1 = searchQuery.Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                d.Rpt_ID,
                d.Rpt_Name,
                d.Rpt_Month,
                d.Rpt_Year,
                NgayHetHan = d.DateEnd == null ? "-" : d.DateEnd.Value.ToString("dd/MM/yyyy"),
                Completed_Values = lst_p_input_values.Where(p => p.Rpt_ReportHeaderKey == d.Rpt_ReportHeaderKey).Count() == 0 ? -1 : (lst_p_input_values.Where(p => p.Rpt_ReportHeaderKey == d.Rpt_ReportHeaderKey).Count() != lst_p_values_inputcompleted.Where(p => p.Rpt_ReportHeaderKey == d.Rpt_ReportHeaderKey).Count() ? 0 : 1),
                Completed_Tbls = lst_p_input_tbls.Where(p => p.Rpt_ReportHeaderKey == d.Rpt_ReportHeaderKey).Count() == 0 ? -1 : (lst_p_input_tbls.Where(p => p.Rpt_ReportHeaderKey == d.Rpt_ReportHeaderKey).Count() != lst_p_tbl_inputcompleted.Where(p => p.Rpt_ReportHeaderKey == d.Rpt_ReportHeaderKey).Count() ? 0 : 1)
            }).ToList();

            var fq2 = fq1.Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                d.Rpt_ID,
                d.Rpt_Name,
                d.Rpt_Month,
                d.Rpt_Year,
                d.NgayHetHan,
                GiaTriXong = TinhTrangNhapLieu(d.Completed_Values),
                PhuLucXong = TinhTrangNhapLieu(d.Completed_Tbls),
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/RptReviewInput_Detail/" + d.Rpt_ReportHeaderKey + "'>Xem</a>"
            }).ToList();

            if (!string.IsNullOrEmpty(Rpt_Name))
                fq2 = fq2.Where(p => Utils.RemoveVni(p.Rpt_Name.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_Name.Trim().ToLower()))).ToList();
            if (Rpt_Month != 0)
                fq2 = fq2.Where(p => p.Rpt_Month == Rpt_Month).ToList();
            if (Rpt_Year != 0)
                fq2 = fq2.Where(p => p.Rpt_Year == Rpt_Year).ToList();
            if (!string.IsNullOrEmpty(NgayHetHan))
                fq2 = fq2.Where(p => Utils.RemoveVni(p.NgayHetHan.Trim().ToLower()).Contains(Utils.RemoveVni(NgayHetHan.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(GiaTriXong))
                fq2 = fq2.Where(p => Utils.RemoveVni(p.GiaTriXong.Trim().ToLower()).Contains(Utils.RemoveVni(GiaTriXong.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(PhuLucXong))
                fq2 = fq2.Where(p => Utils.RemoveVni(p.PhuLucXong.Trim().ToLower()).Contains(Utils.RemoveVni(PhuLucXong.Trim().ToLower()))).ToList();

            int _index = 1;
            var fq3 = fq2.Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                STT = _index++,
                d.Rpt_Name,
                d.Rpt_Month,
                d.Rpt_Year,
                d.NgayHetHan,
                d.GiaTriXong,
                d.PhuLucXong,
                d.HanhDong
            }).ToList();

            table.intTotalPage = GetTotalPage(fq3.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq3.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 5, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            //table.lstHeader.Add(new TableHeader() { Caption = "Mã Báo Cáo", IsShow = true, Format = "", Width = 15, Width_Measure = "%", FieldName = "Rpt_ID" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Báo Cáo", IsShow = true, Format = "", Width = 43, Width_Measure = "%", FieldName = "Rpt_Name" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tháng", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Month", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Năm", IsShow = true, Format = "", Width = 6, Width_Measure = "%", FieldName = "Rpt_Year", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Ngày hết hạn", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "NgayHetHan", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Nhập dữ liệu", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "GiaTriXong", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Nhập phụ lục", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "PhuLucXong", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 10, Width_Measure = "%" });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportHeaderKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        private string TinhTrangNhapLieu(int status)
        {
            string result = "-";
            switch (status)
            {
                case -1:
                    result = "<font color=\"gray\"><i>Chưa phân công</i></font>";
                    break;
                case 0:
                    result = "<font color=\"red\">Chưa Xong</font>";
                    break;
                case 1:
                    result = "<b>Đã xong</b>";
                    break;
                default:
                    break;
            }
            return result;
        }

        public ActionResult LoadTableData_Review(Guid? Rpt_ReportDetail_TableIndexKey, bool editible)
        {
            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();
            grid.ShowAction = editible;

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = Convert.ToString(Rpt_ReportDetail_TableIndexKey),
                Field = "Rpt_ReportDetail_TableIndexKey",
                DataType = GridDetail.eDataType.StringType,
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });

            #region create table source

            var zColumns = (from rpt_col in db.RPT_REPORTDETAIL_TABLECOLUMN
                            join template_col in db.RPT_TEMPLATES_TABLECOLUMN
                                on rpt_col.Rpt_Templates_TableColumnKey equals template_col.Rpt_Templates_TableColumnKey
                            where rpt_col.Rpt_ReportDetail_TableIndexKey == Rpt_ReportDetail_TableIndexKey
                            orderby template_col.ColumnOrder
                            select new
                            {
                                rpt_col.Rpt_ReportDetail_TableColumnKey,
                                template_col.ColumnName,
                                template_col.ColumnCaption
                            }).ToList();

            for (int i = 0; i < zColumns.Count; i++)
            {
                grid.Header.Add(new GridDetail.HeaderColumns()
                {
                    Caption = zColumns[i].ColumnCaption,
                    Field = zColumns[i].ColumnName,
                    DataType = GridDetail.eDataType.StringType,
                    Enable = grid.ShowAction,
                    IsShow = true
                });
            }

            #endregion

            #region create data source

            List<Guid> lst_tblcol_key = zColumns.Select(p => p.Rpt_ReportDetail_TableColumnKey).ToList();

            var zDetails = (from colv in db.RPT_REPORTDETAIL_TABLECOLUMN_VALUE
                            where lst_tblcol_key.Contains(colv.Rpt_ReportDetail_TableColumnKey)
                            orderby colv.RowIndex
                            select new
                            {
                                colv.Rpt_ReportDetail_TableColumnKey,
                                colv.RowIndex,
                                colv.ContentValue,
                                colv.CreatedOn,
                                colv.CreatedBy,
                                colv.ModifiedOn,
                                colv.ModifiedBy
                            }).ToList();

            if (zDetails.Count > 0)
            {
                var lst_users_input = zDetails.OrderBy(p => p.ModifiedOn).Select(p => p.CreatedBy).Distinct().ToList();

                foreach (Guid userkey in lst_users_input)
                {
                    var datafilter = zDetails.Where(p => p.CreatedBy == userkey).ToList();
                    int row_start = datafilter.Min(p => p.RowIndex).Value;
                    int row_end = datafilter.Max(p => p.RowIndex).Value;
                    while (row_start <= row_end)
                    {
                        string[] arr_itemval = new string[lst_tblcol_key.Count + 1];
                        arr_itemval[0] = string.Empty;
                        for (int i = 0; i < lst_tblcol_key.Count; i++)
                        {
                            var itemval = datafilter.Where(p => p.RowIndex == row_start && p.Rpt_ReportDetail_TableColumnKey == lst_tblcol_key[i]).FirstOrDefault();
                            arr_itemval[i + 1] = itemval == null ? "" : itemval.ContentValue;
                        }
                        arrBody.Add(arr_itemval);
                        row_start++;
                    }
                }
            }
            else
            {
                arrBody.Add(new string[lst_tblcol_key.Count + 1]);
            }

            #endregion

            grid.Data = arrBody;

            Dictionary<string, object> lst_result = new Dictionary<string, object>();
            lst_result.Add("result", zColumns.Count > 0 ? "" : "Chưa có");
            lst_result.Add("msg", zColumns.Count > 0 ? "" : "Phụ lục chưa được khởi tạo");
            lst_result.Add("gridlayout", grid);
            return new CustomJsonResult { Data = lst_result };
        }

        public ActionResult LoadValueList_Input(Guid? RptHeaderKey, string inputcompleted)
        {
            //inputcompleted: yes|no
            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();
            grid.ShowAction = string.IsNullOrEmpty(inputcompleted) ? true : inputcompleted.ToLower() == "no";

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "field key",
                Field = "Rpt_ReportDetail_ValueConstKey",
                DataType = GridDetail.eDataType.StringType,
                Width = "2%",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Mã Trường",
                Field = "CodeID",
                DataType = GridDetail.eDataType.StringType,
                Width = "3%",
                Enable = grid.ShowAction,
                IsShow = true,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tên Trường",
                Field = "CodeName",
                DataType = GridDetail.eDataType.StringType,
                Width = "40%",
                Enable = grid.ShowAction,
                IsShow = true
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Dữ Liệu",
                Field = "ContentValue",
                DataType = GridDetail.eDataType.StringType,
                Width = "55%",
                Enable = grid.ShowAction,
                IsShow = true,
            });
            var zDetails = (from v in db.RPT_REPORTDETAIL_VALUECONST
                            orderby v.CodeID
                            where v.Rpt_ReportHeaderKey == RptHeaderKey
                            orderby v.Sort
                            select new
                            {
                                v.Rpt_ReportDetail_ValueConstKey,
                                v.CodeID,
                                v.CodeName,
                                v.ContentValue
                            }).ToList();
            if (zDetails.Count > 0)
            {
                for (int i = 0; i < zDetails.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                        zDetails[i].Rpt_ReportDetail_ValueConstKey.ToString(),
                        zDetails[i].CodeID,
                        zDetails[i].CodeName,
                        zDetails[i].ContentValue
                    });
                }
            }
            else
            {
                arrBody.Add(new object[grid.Header.Count]);
            }
            grid.Data = arrBody;
            //var data = new { header = arrHDR, body = arrBody };
            return new CustomJsonResult { Data = grid };
        }

        public ActionResult LoadTableList_Review(Guid? RptHeaderKey)
        {
            //Guid current_employee = MyAccount.EmployeeKey ?? Guid.Empty;
            RPT_REPORTHEADER itemHdr = db.RPT_REPORTHEADER.Find(RptHeaderKey);

            var query = (from rpt_tbls in db.RPT_REPORTDETAIL_TABLEINDEX
                         join template_tbls in db.RPT_TEMPLATES_TABLENAME
                             on rpt_tbls.Rpt_Templates_TableNameKey equals template_tbls.Rpt_Templates_TableNameKey
                         where rpt_tbls.Rpt_ReportHeaderKey == RptHeaderKey
                         orderby template_tbls.TableID
                         select new
                         {
                             rpt_tbls.Rpt_ReportDetail_TableIndexKey,
                             template_tbls.TableID,
                             template_tbls.TableName
                         }).ToList();

            //foreach (var item in query)
            //{
            //    string div_btn_id = "btn_section_tbl_" + item.TableID;
            //    string div_tbl_id = "div_section_tbl_" + item.TableID;
            //}
            Dictionary<string, object> lst_result = new Dictionary<string, object>();
            lst_result.Add("TablesList", query);
            return new CustomJsonResult { Data = lst_result };
        }

        public ActionResult LoadFileAttached_ListReview(int page, int pageSize, string SearchKey)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */
            Guid rptKey = string.IsNullOrEmpty(SearchKey) ? Guid.Empty : Guid.Parse(SearchKey);

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter

            List<int?> lst_status_allowed = new List<int?>();
            lst_status_allowed.Add(Convert.ToInt32(eRptHeaderStatus.Created));
            lst_status_allowed.Add(Convert.ToInt32(eRptHeaderStatus.Declined));

            var searchQuery = db.RPT_REPORTFILESATTACH.Where(p => p.RecordStatus == 1 && p.Rpt_ReportHeaderKey == rptKey).ToList();

            int _index = 1;
            var fq2 = searchQuery.Select(d => new
            {
                d.Rpt_ReportFileAttachedKey,
                STT = _index++,
                d.AttachedFileName,
                d.Note,
                LinkTai = string.Format("<a href='{0}?Time={1}' target='_blank'>Tải File</a>", d.FileReference, Now.Ticks)
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên file", IsShow = true, Format = "", Width = 38, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Ghi Chú", IsShow = true, Format = "", Width = 45, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tải về", IsShow = true, Format = "", Width = 10, Width_Measure = "%" });
            //

            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportFileAttachedKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadList_ViewSearch(int page, int pageSize, string Rpt_ID, string Rpt_Name, string NgayTao, string NgayDuyet)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */

            Rpt_ID = Rpt_ID ?? "";
            Rpt_Name = Rpt_Name ?? "";
            NgayTao = NgayTao ?? "";
            NgayDuyet = NgayDuyet ?? "";

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = true;

            table.strURLEdit = string.Format("/{0}/Edit", m_ControllerName);
            table.strURLDetail = string.Format("/{0}/Details", m_ControllerName);
            table.strURLDelete = string.Format("/{0}/Delete", m_ControllerName);

            table.KeysSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(Rpt_ID))
                table.KeysSearch.Add("Rpt_ID", Rpt_ID);
            if (!string.IsNullOrEmpty(Rpt_Name))
                table.KeysSearch.Add("Rpt_Name", Rpt_Name);
            if (!string.IsNullOrEmpty(NgayTao))
                table.KeysSearch.Add("NgayTao", NgayTao);
            if (!string.IsNullOrEmpty(NgayDuyet))
                table.KeysSearch.Add("NgayDuyet", NgayDuyet);
            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter

            int rpt_status_approved = Convert.ToInt32(eRptHeaderStatus.Approved);
            var searchQuery = (from f in db.RPT_REPORTHEADER
                               where f.RecordStatus == 1
                               && f.RptStatus == rpt_status_approved
                               orderby f.Rpt_ID, f.Rpt_Year descending, f.Rpt_Month descending
                               select f).ToList();

            if (MyAccount.IsDefaultUser != null && !MyAccount.IsDefaultUser.Value)
            {
                //org account
                var emp = db.CAT_EMPLOYEES.Find(MyAccount.EmployeeKey);
                if (emp != null)
                {
                    Guid? departmentkey = emp.DepartmentKey;

                    var query_template = db.RPT_TEMPLATES_DEPARTMENT.Where(p => p.DepartmentKey == departmentkey).ToList()
                        .Select(p => p.Rpt_TemplatesKey).ToList();

                    searchQuery = searchQuery.Where(p => query_template.Contains(p.Rpt_TemplatesKey)).ToList();
                }
                else
                {
                    searchQuery.Clear();
                }
            }

            if (MyAccount.IsDefaultUser != null && !MyAccount.IsDefaultUser.Value)
            {
                var depkey = db.CAT_EMPLOYEES.Find(MyAccount.EmployeeKey)?.DepartmentKey;
                var templates_restricted = (from d in db.RPT_TEMPLATES_DEPARTMENT
                                            where d.DepartmentKey == depkey
                                            select d).ToList().Select(p => p.Rpt_TemplatesKey).ToList();

                searchQuery = searchQuery.Where(p => templates_restricted.Contains(p.Rpt_TemplatesKey)).ToList();
            }

            var lst_rptHdr = searchQuery.Select(p => p.Rpt_ReportHeaderKey).ToList();
            var lst_fileattached = (from p in db.RPT_REPORTFILESATTACH
                                    where lst_rptHdr.Contains(p.Rpt_ReportHeaderKey)
                                    select p).ToList();

            var fq1 = searchQuery.Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                d.Rpt_ID,
                d.Rpt_Name,
                NgayTao = d.CreatedOn == null ? "" : d.CreatedOn.Value.ToString("dd/MM/yyyy"),
                NgayDuyet = d.ApprovedOn == null ? "" : d.ApprovedOn.Value.ToString("dd/MM/yyyy"),
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/Details/" + d.Rpt_ReportHeaderKey + "'>Xem</a>" +
                "<a class=\"btn btn-primary btn_xuat\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.Rpt_ReportHeaderKey + "'>Xuất </a>" +

                (lst_fileattached.Where(p => p.Rpt_ReportHeaderKey == d.Rpt_ReportHeaderKey).ToList().Count == 0 ? "" :
                "<a class=\"btn btn-primary\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/RptReviewFileAttach/" + d.Rpt_ReportHeaderKey + "'>File đính kèm</a>")
            }).ToList();

            if (!string.IsNullOrEmpty(Rpt_ID))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_ID.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_ID.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(Rpt_Name))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.Rpt_Name.Trim().ToLower()).Contains(Utils.RemoveVni(Rpt_Name.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(NgayTao))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.NgayTao.Trim().ToLower()).Contains(Utils.RemoveVni(NgayTao.Trim().ToLower()))).ToList();
            if (!string.IsNullOrEmpty(NgayDuyet))
                fq1 = fq1.Where(p => Utils.RemoveVni(p.NgayDuyet.Trim().ToLower()).Contains(Utils.RemoveVni(NgayDuyet.Trim().ToLower()))).ToList();

            int _index = 1;
            var fq2 = fq1.Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                STT = _index++,
                d.Rpt_Name,
                d.NgayTao,
                d.NgayDuyet,
                d.HanhDong
            }).ToList();

            table.intTotalPage = GetTotalPage(fq2.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq2.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 11, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Báo Cáo", IsShow = true, Format = "", Width = 43, Width_Measure = "%", FieldName = "Rpt_Name" });
            table.lstHeader.Add(new TableHeader() { Caption = "Ngày Tạo", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "NgayTao" });
            table.lstHeader.Add(new TableHeader() { Caption = "Ngày Duyệt", IsShow = true, Format = "", Width = 10, Width_Measure = "%", FieldName = "NgayDuyet" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 25, Width_Measure = "%" });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportHeaderKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadValueList_Details(Guid? RptHeaderKey)
        {
            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();
            grid.ShowAction = false;

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "field key",
                Field = "Rpt_ReportDetail_ValueConstKey",
                DataType = GridDetail.eDataType.StringType,
                Width = "2%",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Mã Trường",
                Field = "CodeID",
                DataType = GridDetail.eDataType.StringType,
                Width = "18%",
                Enable = false,
                IsShow = true,
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tên Trường",
                Field = "CodeName",
                DataType = GridDetail.eDataType.StringType,
                Width = "30%",
                Enable = false,
                IsShow = true
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Dữ Liệu",
                Field = "ContentValue",
                DataType = GridDetail.eDataType.StringType,
                Width = "40%",
                Enable = false,
                IsShow = true
            });
            var zDetails = (from v in db.RPT_REPORTDETAIL_VALUECONST
                            orderby v.CodeID
                            where v.Rpt_ReportHeaderKey == RptHeaderKey
                            orderby v.Sort
                            select new
                            {
                                v.Rpt_ReportDetail_ValueConstKey,
                                v.CodeID,
                                v.CodeName,
                                v.ContentValue
                            }).ToList();
            if (zDetails.Count > 0)
            {
                for (int i = 0; i < zDetails.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                        zDetails[i].Rpt_ReportDetail_ValueConstKey.ToString(),
                        zDetails[i].CodeID,
                        zDetails[i].CodeName,
                        zDetails[i].ContentValue
                    });
                }
            }
            else
            {
                arrBody.Add(new object[grid.Header.Count]);
            }
            grid.Data = arrBody;
            //var data = new { header = arrHDR, body = arrBody };
            return new CustomJsonResult { Data = grid };
        }

        public ActionResult LoadTableList_Details(Guid? RptHeaderKey)
        {
            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();
            grid.ShowAction = false;

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Mã Phụ Lục",
                Field = "TableID",
                DataType = GridDetail.eDataType.StringType,
                Width = "20%",
                Enable = false,
                IsShow = true
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tên Phụ Lục",
                Field = "TableName",
                DataType = GridDetail.eDataType.StringType,
                Width = "90%",
                Enable = false,
                IsShow = true
            });

            var zDetails = (from t in db.RPT_REPORTDETAIL_TABLEINDEX
                            join tbl in db.RPT_TEMPLATES_TABLENAME
                               on t.Rpt_Templates_TableNameKey equals tbl.Rpt_Templates_TableNameKey
                            orderby tbl.TableID
                            where t.Rpt_ReportHeaderKey == RptHeaderKey
                            select new
                            {
                                t.Rpt_ReportDetail_TableIndexKey,
                                t.Rpt_Templates_TableNameKey,
                                tbl.TableID,
                                tbl.TableName
                            }).ToList();

            if (zDetails.Count > 0)
            {
                for (int i = 0; i < zDetails.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                        zDetails[i].TableID,
                        zDetails[i].TableName
                    });
                }
            }
            else
            {
                arrBody.Add(new object[grid.Header.Count]);
            }
            grid.Data = arrBody;
            return new CustomJsonResult { Data = grid };
        }

        public ActionResult LoadList_PersonList_Decline(int page, int pageSize, string SearchKey)
        {
            //SearchKey: list of selected user, splittedby ';'. struct: userkey | enable (this user is selected or not)
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();

            Guid RptHeaderKey = string.IsNullOrEmpty(SearchKey) ? Guid.Empty : Guid.Parse(SearchKey);

            var lst_tables = (from t in db.RPT_REPORTDETAIL_TABLEINDEX
                              where t.Rpt_ReportHeaderKey == RptHeaderKey
                              select new
                              {
                                  t.Rpt_ReportDetail_TableIndexKey
                              }).ToList().Select(p => p.Rpt_ReportDetail_TableIndexKey).ToList();

            var query_emp_enlisted = (from d in db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                                      join t in db.RPT_REPORTDETAIL_TABLEINDEX
                                          on d.Rpt_ReportDetail_TableIndexKey equals t.Rpt_ReportDetail_TableIndexKey
                                      join tbl in db.RPT_TEMPLATES_TABLENAME
                                          on t.Rpt_Templates_TableNameKey equals tbl.Rpt_Templates_TableNameKey
                                      where lst_tables.Contains(d.Rpt_ReportDetail_TableIndexKey)
                                      select new
                                      {
                                          d.EmployeeKey,
                                          tbl.TableID
                                      }).ToList();

            var query = (from a in db.RPT_REPORT_SENT_APPROVED
                         join emp in db.CAT_EMPLOYEES
                             on a.EmployeeKey equals emp.EmployeeKey
                         where a.Rpt_ReportHeaderKey == RptHeaderKey
                         select new
                         {
                             a.Rpt_Report_Sent_ApprovedKey,
                             a.Rpt_DataType,
                             emp.EmployeeKey,
                             emp.EmployeeName,
                             a.InputStatus
                         }).ToList();

            var data = query.ToList().Select(d => new
            {
                d.Rpt_Report_Sent_ApprovedKey,
                d.Rpt_DataType,
                HanhDong = "<input type=\"checkbox\" id=\"chk_emp_index_" + d.Rpt_Report_Sent_ApprovedKey + "\" style=\"width: 24px; height: 24px;\">",
                d.EmployeeName,
                Data_List = d.Rpt_DataType == 0 ? "(Trường dữ liệu)" :
                    query_emp_enlisted.Where(p => p.EmployeeKey == d.EmployeeKey).Select(p => p.TableID).Aggregate("", (current, next) => next + "<br /> " + current),
                TinhTrang = EnumUtil.GetEnumDescText((eRptInputStatus)d.InputStatus)
            }).OrderBy(p => p.Rpt_DataType).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            table.intTotalPage = GetTotalPage(query.Count, pageSize);

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Data_Type", IsShow = false, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Chọn", IsShow = true, Format = "", Width = 200 });
            table.lstHeader.Add(new TableHeader() { Caption = "Người Nhập Liệu", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Danh sách dữ liệu", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tình trạng", IsShow = true, Format = "" });

            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_Report_Sent_ApprovedKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadList_PersonInput_Status(Guid? id)
        {
            Guid RptHeaderKey = id == null ? Guid.Empty : id.Value;

            List<object> arrBody = new List<object>();
            GridDetail grid = new GridDetail();
            grid.ShowAction = false;

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Người Nhập Liệu",
                Field = "EmployeeName",
                DataType = GridDetail.eDataType.StringType,
                Width = "25%",
                Enable = grid.ShowAction,
                IsShow = true,
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Danh sách dữ liệu",
                Field = "Data_List",
                DataType = GridDetail.eDataType.StringType,
                Width = "65%",
                Enable = grid.ShowAction,
                IsShow = true
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tình trạng",
                Field = "Rpt_StatusName",
                DataType = GridDetail.eDataType.StringType,
                Width = "10%",
                Enable = grid.ShowAction,
                IsShow = true,
            });

            //danh sách chi tiết nhân viên tham gia nhập liệu bảng phụ lục
            var lst_emps_tbls_enlisted = (from d in db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                                          join t in db.RPT_REPORTDETAIL_TABLEINDEX
                                              on d.Rpt_ReportDetail_TableIndexKey equals t.Rpt_ReportDetail_TableIndexKey
                                          join tp in db.RPT_TEMPLATES_TABLENAME
                                              on t.Rpt_Templates_TableNameKey equals tp.Rpt_Templates_TableNameKey
                                          join emp in db.CAT_EMPLOYEES
                                              on d.EmployeeKey equals emp.EmployeeKey
                                          where t.Rpt_ReportHeaderKey == RptHeaderKey
                                          select new
                                          {
                                              d.EmployeeKey,
                                              emp.EmployeeName,
                                              tp.TableID,
                                              tp.TableName,
                                          }).ToList();

            //danh sách phụ lục/trường dữ liệu của từng nhân viên đã nhập xong
            var lst_tbls_fields_completed = (from a in db.RPT_REPORT_SENT_APPROVED
                                             join emp in db.CAT_EMPLOYEES
                                                 on a.EmployeeKey equals emp.EmployeeKey
                                             where a.Rpt_ReportHeaderKey == RptHeaderKey
                                             select new
                                             {
                                                 a.Rpt_Report_Sent_ApprovedKey,
                                                 a.Rpt_DataType,
                                                 emp.EmployeeKey,
                                                 emp.EmployeeName,
                                                 a.InputStatus
                                             }).ToList();

            var itemRptHdr = (from r in db.RPT_REPORTHEADER
                              join e in db.CAT_EMPLOYEES
                                   on r.Person_InputValues equals e.EmployeeKey
                              select new
                              {
                                  e.EmployeeKey,
                                  e.EmployeeName
                              }).ToList().FirstOrDefault();

            var lst_emps_tbls = lst_emps_tbls_enlisted.Select(p => new
            {
                p.EmployeeKey,
                p.EmployeeName
            }).Distinct().ToList();

            if (lst_emps_tbls_enlisted.Count > 0)
            {
                foreach (var item in lst_emps_tbls)
                {
                    string part_empName = string.Empty;
                    string part_datalist = string.Empty;
                    string part_datastatus = string.Empty;

                    var lst_tbls_completed = lst_tbls_fields_completed.Where(p => p.EmployeeKey == item.EmployeeKey && p.Rpt_DataType == 1).ToList();

                    part_empName = item.EmployeeName;
                    if (itemRptHdr != null)
                    {
                        if (itemRptHdr.EmployeeKey == item.EmployeeKey)
                        {
                            var lst_values_completed = lst_tbls_fields_completed.Where(p => p.EmployeeKey == item.EmployeeKey && p.Rpt_DataType == 0).ToList();

                            part_datalist = "(Trường dữ liệu)";
                            part_datastatus = EnumUtil.GetEnumDescText(lst_values_completed.Count == 0 ? eRptInputStatus.Inputting : eRptInputStatus.Done);

                            arrBody.Add(new object[]
                            {
                                part_empName,
                                part_datalist,
                                part_datastatus
                            });
                        }
                    }

                    part_datalist = lst_emps_tbls_enlisted.Where(p => p.EmployeeKey == item.EmployeeKey)
                        .OrderBy(p => p.TableID).ToList()
                        .Select(p => p.TableID).Aggregate((x, y) => x + "; " + y);

                    part_datastatus = lst_tbls_completed.Count == 0 ? EnumUtil.GetEnumDescText(eRptInputStatus.Inputting) :
                        lst_tbls_completed.Where(p => p.InputStatus == Convert.ToInt32(eRptInputStatus.Inputting)).Count() > 0 ? EnumUtil.GetEnumDescText(eRptInputStatus.Inputting) :
                        EnumUtil.GetEnumDescText(eRptInputStatus.Done);

                    arrBody.Add(new object[]
                    {
                        part_empName,
                        part_datalist,
                        part_datastatus
                    });
                }
            }
            else
            {
                if (itemRptHdr != null)
                {
                    string part_empName = string.Empty;
                    string part_datalist = string.Empty;
                    string part_datastatus = string.Empty;
                    var lst_values_completed = lst_tbls_fields_completed.Where(p => p.EmployeeKey == itemRptHdr.EmployeeKey && p.Rpt_DataType == 0).ToList();

                    part_empName = itemRptHdr.EmployeeName;
                    part_datalist = "(Trường dữ liệu)";
                    part_datastatus = EnumUtil.GetEnumDescText(lst_values_completed.Count == 0 ? eRptInputStatus.Inputting : eRptInputStatus.Done);

                    arrBody.Add(new object[]
                    {
                        part_empName,
                        part_datalist,
                        part_datastatus
                    });
                }
            }

            if (arrBody.Count == 0)
            {
                arrBody.Add(new object[grid.Header.Count]);
            }
            grid.Data = arrBody;
            return new CustomJsonResult { Data = grid };
        }

        public string ThoiHan(DateTime? DateStart, DateTime? DateEnd, int? rptStatus)
        {
            if (DateStart == null || DateEnd == null)
                return "";

            int Total = Convert.ToInt32((DateEnd.Value - DateStart.Value).TotalDays) + 1;
            int x = Convert.ToInt32((DateTime.Now - DateStart.Value).TotalDays) + 1;
            int y = 100;
            int songay = Total - x;
            string color = "progress-bar-danger", thongbao = songay + " ngày";
            if (rptStatus == 1)
            {
                return "<div>Hoàn thành</div>";
            }
            else if (songay < 0)
            {
                thongbao = "Quá hạn " + Math.Abs(songay) + " ngày";
            }
            else if (songay == 0)
            {
                thongbao = "Hôm nay";
            }
            else if (songay < 3)
            {
                y = 100 * x / Total;
            }
            else if (songay < 7)
            {
                y = 100 * x / Total;
                color = "progress-bar-warning";
            }
            else
            {
                y = 100 * x / Total;
                color = "progress-bar-success";
            }
            string kq = "<div class=\"progress progress_sm\">"
                              + "<div class=\"progress-bar " + color + "\" role=\"progressbar\" data-transitiongoal=\"" + y + "\" aria-valuenow=\"" + y + "\" style=\"width: " + y + "%;\"></div>"
                        + "</div>"
                        + "<small>" + thongbao + "</small>";

            return kq;
        }

        public ActionResult ExportWord_OpenXml(string rpt_headerkey)
        {
            AjaxResult zResult = new AjaxResult();
            try
            {
                if (string.IsNullOrEmpty(rpt_headerkey))
                {
                    zResult.intStatus = 0;
                    throw new Exception("không tìm thấy thông tin");
                }

                Guid headerkey = Guid.Parse(rpt_headerkey);
                RPT_REPORTHEADER rptHeader = db.RPT_REPORTHEADER.Find(headerkey);
                if (rptHeader == null)
                    throw new Exception("không tìm thấy thông tin báo cáo");

                if (!System.IO.File.Exists(Server.MapPath(rptHeader.FileReference)))
                    throw new Exception("không tìm thấy file dữ liệu mẫu");

                DateTime _now = DateTime.Now;
                string file_sample = Server.MapPath(rptHeader.FileReference);
                string file_path = Server.MapPath(PathUpload_HeaderExportTemp);
                string filename = string.Format("{0}_{1}", Now.Ticks, rptHeader.FileName);

                //multi-export: copy ra 1 file mới để edit & save lại
                System.IO.File.Copy(file_sample, file_path + filename);

                #region lấy dữ liệu

                var query_tables_valueconst = (from rv in db.RPT_REPORTDETAIL_VALUECONST
                                               where rv.Rpt_ReportHeaderKey == headerkey
                                               orderby rv.Sort
                                               select new
                                               {
                                                   rv.CodeID,
                                                   rv.CodeName,
                                                   rv.ContentValue,
                                                   rv.Sort
                                               }).ToList();

                var query_tables_lst = from ti in db.RPT_REPORTDETAIL_TABLEINDEX
                                       join tbl in db.RPT_TEMPLATES_TABLENAME
                                           on ti.Rpt_Templates_TableNameKey equals tbl.Rpt_Templates_TableNameKey
                                       where ti.Rpt_ReportHeaderKey == headerkey
                                       orderby tbl.TableID
                                       select new
                                       {
                                           ti.Rpt_ReportDetail_TableIndexKey,
                                           ti.Rpt_Templates_TableNameKey,
                                           tbl.TableID,
                                           tbl.TableName
                                       };

                var query_tables_column_lst = from ti in db.RPT_REPORTDETAIL_TABLEINDEX
                                              join dc in db.RPT_REPORTDETAIL_TABLECOLUMN
                                                  on ti.Rpt_ReportDetail_TableIndexKey equals dc.Rpt_ReportDetail_TableIndexKey
                                              join c in db.RPT_TEMPLATES_TABLECOLUMN
                                                  on dc.Rpt_Templates_TableColumnKey equals c.Rpt_Templates_TableColumnKey
                                              where ti.Rpt_ReportHeaderKey == headerkey
                                              orderby ti.Rpt_Templates_TableNameKey, c.ColumnOrder
                                              select new
                                              {
                                                  ti.Rpt_ReportDetail_TableIndexKey,
                                                  ti.Rpt_Templates_TableNameKey,
                                                  dc.Rpt_ReportDetail_TableColumnKey,
                                                  dc.Rpt_Templates_TableColumnKey,
                                                  c.ColumnName,
                                                  c.ColumnCaption,
                                                  c.ColumnDataType,
                                                  c.ColumnOrder,
                                                  c.HiddenOnGUI
                                              };
                #endregion

                using (DocumentFormat.OpenXml.Packaging.WordprocessingDocument doc = DocumentFormat.OpenXml.Packaging.WordprocessingDocument.Open(file_path + filename, true))
                {
                    //var codeList = query_tables_valueconst.ToList().Select(p => p.CodeID).ToList();

                    var body = doc.MainDocumentPart.Document.Body;
                    //var paragraphs = body.Elements<DocumentFormat.OpenXml.Wordprocessing.Paragraph>();
                    var tables = body.Elements<DocumentFormat.OpenXml.Wordprocessing.Table>();


                    #region thay trường dữ liệu

                    //int _pos = -1;
                    //bool _markstart_detected = false;
                    List<int> lst_pos = new List<int>();

                    var text_paragraphs = body.Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>().ToList();

                    #region mark expression

                    string markstart = "<#";
                    string markend = "#>";
                    int paragraph_pos = 0;

                    DataTable dtMatches = new DataTable();
                    dtMatches.Columns.Add("id", typeof(int));
                    dtMatches.Columns.Add("pindex", typeof(int));
                    dtMatches.Columns.Add("expression", typeof(string));
                    dtMatches.Columns.Add("markstartindex", typeof(int));
                    dtMatches.Columns.Add("markendindex", typeof(int));

                    foreach (var text_p in text_paragraphs)
                    {
                        MatchCollection mx_start = Regex.Matches(text_p.Text, markstart);
                        if (mx_start.Count > 0)
                        {
                            for (int i = 0; i < mx_start.Count; i++)
                            {
                                dtMatches.Rows.Add(new object[]
                                {
                                    dtMatches.Rows.Count + 1,
                                    paragraph_pos,
                                    text_p.Text,
                                    mx_start[i].Index,
                                    DBNull.Value
                                });
                            }
                        }

                        MatchCollection mx_end = Regex.Matches(text_p.Text, markend);
                        if (mx_end.Count > 0)
                        {
                            for (int i = 0; i < mx_end.Count; i++)
                            {
                                dtMatches.Rows.Add(new object[]
                                {
                                    dtMatches.Rows.Count + 1,
                                    paragraph_pos,
                                    text_p.Text,
                                    DBNull.Value,
                                    mx_end[i].Index
                                });
                            }
                        }

                        paragraph_pos++;
                    }

                    #endregion

                    #region analyze

                    dtMatches.DefaultView.RowFilter = "markstartindex IS NOT NULL";
                    System.Data.DataTable dtMxStarts = dtMatches.DefaultView.ToTable();

                    dtMatches.DefaultView.RowFilter = "markendindex IS NOT NULL";
                    System.Data.DataTable dtMxEnds = dtMatches.DefaultView.ToTable();

                    if (dtMxStarts.Rows.Count == dtMxEnds.Rows.Count)
                    {
                        for (int i = 0; i < dtMxStarts.Rows.Count; i++)
                        {
                            DataRow drStart = dtMxStarts.Rows[i];
                            DataRow drEnd = dtMxEnds.Rows[i];

                            string expr_start = Convert.ToString(drStart["expression"]);
                            int pindex_start = Convert.ToInt32(drStart["pindex"]);
                            int pmark_start = Convert.ToInt32(drStart["markstartindex"]);

                            string expr_end = Convert.ToString(drEnd["expression"]);
                            int pindex_end = Convert.ToInt32(drEnd["pindex"]);
                            int pmark_end = Convert.ToInt32(drEnd["markendindex"]);

                            string expr_all = string.Empty;
                            string temp = "";

                            if (pindex_start == pindex_end)
                            {
                                //mark (start-end) trên cùng 1 paragraph
                                temp = expr_start.Substring(pmark_start, pmark_end + 2 - pmark_start);
                                expr_all = temp;

                                temp = temp.Replace(markstart, "");
                                temp = temp.Replace(markend, "");
                                var item_const_value = query_tables_valueconst.Where(p => p.CodeID == temp).FirstOrDefault();
                                if (item_const_value != null)
                                {
                                    text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(expr_all, item_const_value.ContentValue);
                                    ApplyFontStyle(text_paragraphs[pindex_start].Parent as DocumentFormat.OpenXml.Wordprocessing.Run);
                                }
                            }
                            else if (pindex_start < pindex_end)
                            {
                                //mark end ở paragraph sau
                                //nối các expr lại thành 1

                                if (pindex_start == pindex_end - 1)
                                {
                                    temp = expr_start.Substring(pmark_start) + expr_end.Substring(0, pmark_end + 2);
                                    expr_all += temp;

                                    temp = temp.Replace(markstart, "");
                                    temp = temp.Replace(markend, "");
                                    var item_const_value = query_tables_valueconst.Where(p => p.CodeID == temp).FirstOrDefault();
                                    if (item_const_value != null)
                                    {
                                        text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(expr_start.Substring(pmark_start), item_const_value.ContentValue);
                                        text_paragraphs[pindex_end].Text = text_paragraphs[pindex_end].Text.Replace(expr_end.Substring(0, pmark_end + 2), "");

                                        ApplyFontStyle(text_paragraphs[pindex_start].Parent as DocumentFormat.OpenXml.Wordprocessing.Run);
                                    }
                                }
                                else
                                {
                                    for (int ex_index = pindex_start; ex_index <= pindex_end; ex_index++)
                                    {
                                        if (ex_index == pindex_start)
                                        {
                                            temp = text_paragraphs[ex_index].Text.Substring(pmark_start);
                                            expr_all += temp;
                                        }
                                        else if (ex_index == pindex_end)
                                        {
                                            temp = text_paragraphs[ex_index].Text.Substring(0, pmark_end + 2);
                                            expr_all += temp;
                                            text_paragraphs[ex_index].Text = text_paragraphs[ex_index].Text.Replace(temp, "");

                                            if (text_paragraphs[ex_index].Text.Contains(markstart))
                                            {
                                                // still has mark start of next one
                                                //change the start-point
                                                if (i < dtMxStarts.Rows.Count - 1)
                                                    dtMxStarts.Rows[i + 1]["markstartindex"] = text_paragraphs[ex_index].Text.IndexOf(markstart);
                                            }
                                        }
                                        else
                                        {
                                            expr_all += text_paragraphs[ex_index].Text;
                                            text_paragraphs[ex_index].Text = "";
                                        }
                                    }

                                    temp = text_paragraphs[pindex_start].Text.Substring(pmark_start);
                                    var item_const_value = query_tables_valueconst.Where(p => p.CodeID == temp).FirstOrDefault();
                                    if (item_const_value != null)
                                    {
                                        text_paragraphs[pindex_start].Text = text_paragraphs[pindex_start].Text.Replace(temp, item_const_value.ContentValue);
                                        ApplyFontStyle(text_paragraphs[pindex_start].Parent as DocumentFormat.OpenXml.Wordprocessing.Run);
                                    }
                                }
                            }
                            else
                            {
                                //do nothing
                            }
                        }
                    }

                    #endregion

                    #endregion

                    #region điền bảng phụ lục

                    //duyệt các bảng phụ lục của report header
                    foreach (var tbl_tableindex in query_tables_lst.ToList())
                    {
                        //duyệt qua từng table trong file word
                        foreach (var wtbl in tables.ToList())
                        {
                            //lấy toàn bộ Rows trong table
                            var wtbl_rows = wtbl.Elements<DocumentFormat.OpenXml.Wordprocessing.TableRow>().ToList();
                            if (wtbl_rows.Count == 0)
                                continue;

                            //lấy toàn bộ Cells của first-row
                            var wtbl_cells = wtbl_rows[0].Elements<DocumentFormat.OpenXml.Wordprocessing.TableCell>().ToList();
                            if (wtbl_cells.Count == 0)
                                continue;

                            //lấy toàn bộ paragraphs.Text của first-cell
                            var wtbl_paragraphs = wtbl_cells[0].Elements<DocumentFormat.OpenXml.Wordprocessing.Paragraph>()
                                .SelectMany(p => p.Elements<DocumentFormat.OpenXml.Wordprocessing.Run>())
                                .SelectMany(p => p.Elements<DocumentFormat.OpenXml.Wordprocessing.Text>())
                                .ToList();
                            if (wtbl_paragraphs.Count == 0)
                                continue;

                            /*
                             * wtbl_paragraphs.Count có thể nhiều hơn 1 do table bị qua trang hoặc do Enter-New Row,
                             * nên cộng các paragraph lại thành 1 chuỗi
                             */
                            string wtbl_tableindex_id = wtbl_paragraphs.Select(p => p.Text).Aggregate((x, y) => x + y);

                            if (tbl_tableindex.TableID == wtbl_tableindex_id)
                            {
                                #region lấy dữ liệu của bảng phụ lục tương ứng

                                var lst_columnskey_tbl = query_tables_column_lst.ToList().Where(p => p.Rpt_Templates_TableNameKey == tbl_tableindex.Rpt_Templates_TableNameKey)
                                        .Select(p => p.Rpt_ReportDetail_TableColumnKey).ToList();

                                var query_column_data = (from v in db.RPT_REPORTDETAIL_TABLECOLUMN_VALUE
                                                         join c in db.RPT_REPORTDETAIL_TABLECOLUMN
                                                             on v.Rpt_ReportDetail_TableColumnKey equals c.Rpt_ReportDetail_TableColumnKey
                                                         join tc in db.RPT_TEMPLATES_TABLECOLUMN
                                                             on c.Rpt_Templates_TableColumnKey equals tc.Rpt_Templates_TableColumnKey
                                                         where lst_columnskey_tbl.Contains(c.Rpt_ReportDetail_TableColumnKey)
                                                         orderby v.CreatedBy, v.RowIndex, tc.ColumnOrder
                                                         select new
                                                         {
                                                             c.Rpt_ReportDetail_TableColumnKey,
                                                             v.RowIndex,
                                                             tc.ColumnName,
                                                             tc.ColumnCaption,
                                                             tc.ColumnOrder,
                                                             v.ContentValue,
                                                             v.CreatedBy
                                                         }).ToList();

                                #endregion

                                #region điền dữ liệu vào phụ lục đã đánh dấu trong file word 

                                int row_index = -1;
                                int col_index = 0;
                                Guid person_input = Guid.Empty;

                                foreach (var data in query_column_data)
                                {
                                    if (row_index == data.RowIndex.Value)
                                    {
                                        //dữ liệu cùng row
                                        if (person_input != data.CreatedBy.Value)
                                        {
                                            /*
                                             * khác người nhập ==> tạo dòng mới
                                             * trường hợp có nhiều người cùng nhập, RowIndex của từng người có thể trùng nhau
                                             */

                                            wtbl.Append(new DocumentFormat.OpenXml.Wordprocessing.TableRow());
                                            var last_row = wtbl.Elements<DocumentFormat.OpenXml.Wordprocessing.TableRow>().ToList().Last();
                                            foreach (var colName in lst_columnskey_tbl)
                                            {
                                                last_row.Append(new DocumentFormat.OpenXml.Wordprocessing.TableCell());
                                            }

                                            col_index = 0;
                                            row_index = data.RowIndex.Value;
                                            person_input = data.CreatedBy.Value;
                                        }
                                    }
                                    else
                                    {
                                        //dữ liệu khác row

                                        wtbl.Append(new DocumentFormat.OpenXml.Wordprocessing.TableRow());
                                        /*
                                         * trong file mẫu luôn chừa sẵn 1 row đầu tiên để làm mẫu chèn các row tiếp theo,
                                         * nếu không phải dòng mẫu thì khi tạo row mới, cần tạo số Cells tương ứng
                                         */

                                        var last_row = wtbl.Elements<DocumentFormat.OpenXml.Wordprocessing.TableRow>().ToList().Last();
                                        foreach (var colName in lst_columnskey_tbl)
                                        {
                                            last_row.Append(new DocumentFormat.OpenXml.Wordprocessing.TableCell());
                                        }
                                        if (row_index == -1)
                                        {
                                            var rowlist = wtbl.Elements<DocumentFormat.OpenXml.Wordprocessing.TableRow>().ToList();
                                            var sample_row = rowlist[rowlist.Count - 2];
                                            sample_row.Remove();
                                        }

                                        col_index = 0;
                                        row_index = data.RowIndex.Value;
                                        person_input = data.CreatedBy.Value;
                                    }

                                    var currentrow = wtbl.Elements<DocumentFormat.OpenXml.Wordprocessing.TableRow>().ToList().Last();

                                    var lst_cells = currentrow.Elements<DocumentFormat.OpenXml.Wordprocessing.TableCell>().ToList();
                                    lst_cells[col_index].Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(
                                        new DocumentFormat.OpenXml.Wordprocessing.Run(
                                            new DocumentFormat.OpenXml.Wordprocessing.Text(data.ContentValue))));

                                    col_index++;
                                }

                                #endregion

                                #region xóa dòng đầu tiên, dòng đánh dấu phụ lục

                                var first_row = wtbl.Elements<DocumentFormat.OpenXml.Wordprocessing.TableRow>().ToList().First();
                                first_row.Remove();

                                #endregion
                            }

                        }
                    }

                    #endregion

                    if (DocumentFormat.OpenXml.Packaging.OpenXmlPackage.CanSave)
                        doc.Save();
                }

                //throw new Exception("Code ended by user due to test");

                Dictionary<string, string> lst_result = new Dictionary<string, string>();
                lst_result.Add("folderpath", filename);
                lst_result.Add("filename", filename);

                zResult.intStatus = 1;
                zResult.strMess = "Đang kết xuất file...";
                zResult.Data = lst_result;
                return new CustomJsonResult { Data = (zResult) };
            }
            catch (Exception ex)
            {
                zResult.intStatus = 0;
                zResult.strMess = ex.Message;
                zResult.Data = null;
                return new CustomJsonResult { Data = (zResult) };
            }
        }

        private void ApplyFontStyle(DocumentFormat.OpenXml.Wordprocessing.Run pRun_Parent)
        {
            if (pRun_Parent != null)
            {
                var runProp = pRun_Parent.Elements<DocumentFormat.OpenXml.Wordprocessing.RunProperties>().FirstOrDefault();
                if (runProp == null)
                {
                    runProp = new DocumentFormat.OpenXml.Wordprocessing.RunProperties();
                    runProp.Append(new DocumentFormat.OpenXml.Wordprocessing.RunFonts() { Ascii = "Times New Roman" });
                    runProp.Append(new DocumentFormat.OpenXml.Wordprocessing.FontSize() { Val = new DocumentFormat.OpenXml.StringValue("26") });//26 half-point font size ==> Font Size in real-set = 13
                    pRun_Parent.RunProperties = runProp;
                }
                else
                {
                    runProp.PrependChild(new DocumentFormat.OpenXml.Wordprocessing.RunFonts() { Ascii = "Times New Roman" });
                    runProp.PrependChild(new DocumentFormat.OpenXml.Wordprocessing.FontSize() { Val = new DocumentFormat.OpenXml.StringValue("26") });//26 half-point font size ==> Font Size in real-set = 13
                }
            }
        }

        public ActionResult DownloadFileExport(string filepathfull, string downloadfilename)
        {
            string file_path = Server.MapPath(PathUpload_HeaderExportTemp + downloadfilename);//filepathfull: no use
            return File(file_path, "application/force-download", downloadfilename);
        }

        public ActionResult Rpt_ReportDeclined(string[] lst_decline, string decline_reason)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            try
            {
                if (lst_decline == null)
                    throw new Exception("Chưa chọn danh sách trả về");
                if (string.IsNullOrEmpty(decline_reason))
                    throw new Exception("Vui lòng nêu lý do trả về");

                foreach (string key in lst_decline)
                {
                    string declinekey = key.Replace("chk_emp_index_", "");//see LoadList_PersonList_Decline

                    RPT_REPORT_SENT_APPROVED item = db.RPT_REPORT_SENT_APPROVED.Find(Convert.ToInt64(declinekey));
                    if (item == null)
                        continue;

                    item.InputStatus = Convert.ToInt32(eRptInputStatus.Inputting);
                    item.Note = decline_reason;
                    item.ModifiedOn = DateTime.Now;
                    item.ModifiedBy = MyAccount.UserID;
                    db.Entry(item).State = EntityState.Modified;
                }

                db.SaveChanges();
                result.strMess = "Đã trả lại báo cáo để nhập liệu";
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }

            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult Rpt_ReportApproved(Guid? id)
        {
            AjaxResult result = new AjaxResult();
            result.intStatus = 1;
            RPT_REPORTHEADER itemRpt = db.RPT_REPORTHEADER.Find(id);
            try
            {
                if (id == null)
                    throw new Exception("Chưa xác định báo cáo");

                if (itemRpt == null)
                    throw new Exception("Không tìm thấy thông tin");

                itemRpt.RptStatus = Convert.ToInt32(eRptHeaderStatus.Approved);
                itemRpt.ModifiedOn = Now;
                itemRpt.ModifiedBy = MyAccount.UserID;
                itemRpt.ApprovedOn = Now;
                itemRpt.ApprovedBy = MyAccount.UserID;
                db.Entry(itemRpt).State = EntityState.Modified;

                db.SaveChanges();
                result.strMess = "Đã duyệt";
                CreateLog(m_ControllerName, "Approve", itemRpt.Rpt_ReportHeaderKey.ToString(), itemRpt.Rpt_ID, itemRpt.Rpt_Name, System.Reflection.MethodBase.GetCurrentMethod().Name, eWorkStatus.Success.ToString(), result.strMess);
            }
            catch (Exception ex)
            {
                result.intStatus = 0;
                result.strMess = ex.Message;
            }
            return new CustomJsonResult { Data = (result) };
        }

        public ActionResult LoadChart_Doughnut_ReportInYear(string pYear)
        {
            CanvasChart chartinfo = new CanvasChart();
            chartinfo.ChartType = "pie";

            int _year = string.IsNullOrEmpty(pYear) ? DateTime.Now.Year : Convert.ToInt32(pYear);

            var query_rpts = (from r in db.RPT_REPORTHEADER
                              where r.RecordStatus == 1 &&
                              r.Rpt_Year == _year
                              select r).ToList();
            var lst_completed = query_rpts.Where(p => p.RptStatus == Convert.ToInt32(eRptHeaderStatus.Approved)).ToList();
            var lst_still_not_overdue = query_rpts.Where(p => p.RptStatus != Convert.ToInt32(eRptHeaderStatus.Approved) && (p.DateEnd == null || (p.DateEnd != null && p.DateEnd.Value.Date >= DateTime.Now.Date))).ToList();

            var lst_id_completed = lst_completed.Select(p => p.Rpt_ReportHeaderKey).ToList();
            var lst_id_still = lst_still_not_overdue.Select(p => p.Rpt_ReportHeaderKey).ToList();

            var lst_still_overdue = query_rpts.Where(p => !lst_id_completed.Contains(p.Rpt_ReportHeaderKey) && !lst_id_still.Contains(p.Rpt_ReportHeaderKey)).ToList();

            chartinfo.DataList_Donut.Add(new Data_DoughnutChart(lst_completed.Count, "Hoàn Thành"));
            chartinfo.DataList_Donut.Add(new Data_DoughnutChart(lst_still_not_overdue.Count, "Đang Làm"));
            chartinfo.DataList_Donut.Add(new Data_DoughnutChart(lst_still_overdue.Count, "Quá Hạn"));

            //chartinfo.DataList_Donut.Add(new Data_DoughnutChart(45, "Hoàn Thành"));
            //chartinfo.DataList_Donut.Add(new Data_DoughnutChart(13, "Đang Làm"));
            //chartinfo.DataList_Donut.Add(new Data_DoughnutChart(2, "Quá Hạn"));

            chartinfo.ChartTitle = "Thống kê báo cáo năm " + pYear;
            return new CustomJsonResult { Data = chartinfo };
        }

        public ActionResult LoadChart_BarV_ReportInYear(string pYear)
        {
            CanvasChart chartinfo = new CanvasChart();
            chartinfo.ChartType = "stackedColumn";
            int _year = string.IsNullOrEmpty(pYear) ? DateTime.Now.Year : Convert.ToInt32(pYear);

            var query_rpts = (from r in db.RPT_REPORTHEADER
                              where r.RecordStatus == 1 &&
                              r.Rpt_Year == _year
                              select r).ToList();
            var lst_completed = query_rpts.Where(p => p.RptStatus == Convert.ToInt32(eRptHeaderStatus.Approved)).ToList();
            var lst_still_not_overdue = query_rpts.Where(p => p.RptStatus != Convert.ToInt32(eRptHeaderStatus.Approved) && (p.DateEnd == null || (p.DateEnd != null && p.DateEnd.Value.Date >= DateTime.Now.Date))).ToList();

            var lst_id_completed = lst_completed.Select(p => p.Rpt_ReportHeaderKey).ToList();
            var lst_id_still = lst_still_not_overdue.Select(p => p.Rpt_ReportHeaderKey).ToList();

            var lst_still_overdue = query_rpts.Where(p => !lst_id_completed.Contains(p.Rpt_ReportHeaderKey) && !lst_id_still.Contains(p.Rpt_ReportHeaderKey)).ToList();

            for (int i = 1; i < 13; i++)
            {
                //completed
                chartinfo.DataList_StackBarVM.Add(new Data_StackBarVMChart(i, lst_completed.Where(p => p.Rpt_Month == i).Count(),
                    string.Format("Tháng {0}", i), "c"));

                //still: not overdue
                chartinfo.DataList_StackBarVM.Add(new Data_StackBarVMChart(i, lst_still_not_overdue.Where(p => p.Rpt_Month == i).Count(),
                    string.Format("Tháng {0}", i), "s"));

                //still: overdued
                chartinfo.DataList_StackBarVM.Add(new Data_StackBarVMChart(i, lst_still_overdue.Where(p => p.Rpt_Month == i).Count(),
                    string.Format("Tháng {0}", i), "o"));
            }

            chartinfo.ChartTitle = "Thống kê báo cáo năm " + pYear;
            //chartinfo.AxisX_Title = "Theo Tháng";
            chartinfo.AxisY_Title = "Tổng số báo cáo";

            return new CustomJsonResult { Data = chartinfo };
        }

        public ActionResult LoadGridSummary_ByYear(int page, int pageSize, string RptSelectedYear, string RptSelectedMonth, string RptStatus, string RptTemplate)
        {
            /*
             * khi tạo table header có searchbox, nếu có set fieldname nào
             * thì phải khai báo thêm parameter tương ứng để có thể search
             */

            RptSelectedYear = RptSelectedYear ?? string.Empty;
            RptSelectedMonth = RptSelectedMonth ?? string.Empty;
            RptStatus = RptStatus ?? string.Empty;
            RptTemplate = RptTemplate ?? string.Empty;

            int _year = Convert.ToInt32(string.IsNullOrEmpty(RptSelectedYear) ? "0" : RptSelectedYear);
            int _month = Convert.ToInt32(string.IsNullOrEmpty(RptSelectedMonth) ? "0" : RptSelectedMonth);
            int _status = Convert.ToInt32(string.IsNullOrEmpty(RptStatus) ? "0" : RptStatus);
            Guid _tempplate = string.IsNullOrEmpty(RptTemplate) ? Guid.Empty : Guid.Parse(RptTemplate);

            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;
            table.EnableSearch = false;

            table.KeysSearch = new Dictionary<string, string>();

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter

            var searchQuery = (from f in db.RPT_REPORTHEADER
                               where f.RecordStatus == 1 &&
                               (f.Rpt_Year == _year || _year == 0) &&
                               (f.Rpt_Month == _month || _month == 0) &&
                               (f.Rpt_TemplatesKey == _tempplate || _tempplate == Guid.Empty)
                               orderby f.Rpt_ID, f.Rpt_Year descending, f.Rpt_Month descending
                               select f).ToList();

            if (_status == 1)//hoàn thành
                searchQuery = searchQuery.Where(p => p.RptStatus == Convert.ToInt32(eRptHeaderStatus.Approved)).ToList();
            else if (_status == 2)//đang làm
                searchQuery = searchQuery.Where(p => p.RptStatus != Convert.ToInt32(eRptHeaderStatus.Approved) &&
                 (p.DateEnd == null || (p.DateEnd != null && DateTime.Now.Date <= p.DateEnd.Value.Date))).ToList();
            else if (_status == 3)//quá hạn
                searchQuery = searchQuery.Where(p => p.RptStatus != Convert.ToInt32(eRptHeaderStatus.Approved) &&
                (p.DateEnd != null && DateTime.Now.Date > p.DateEnd.Value.Date)).ToList();

            string[] statusdesc = { "-", "Hoàn Thành", "Đang làm", "Quá Hạn" };
            int _index = 1;
            var fq1 = searchQuery.ToList().Select(p => new
            {
                p.Rpt_ReportHeaderKey,
                STT = _index++,
                p.Rpt_Name,
                p.Rpt_Month,
                p.Rpt_Year,
                NgayDenHan = p.DateEnd == null ? "-" : p.DateEnd.Value.ToString("dd/MM/yyyy"),
                TinhTrang = statusdesc[p.RptStatus == Convert.ToInt32(eRptHeaderStatus.Approved) ? 1 :
                    (p.DateEnd == null || (p.DateEnd != null && DateTime.Now.Date <= p.DateEnd.Value.Date)) ? 2 :
                    (p.DateEnd != null && DateTime.Now.Date > p.DateEnd.Value.Date) ? 3 : 0]
            }).ToList();

            table.intTotalPage = GetTotalPage(fq1.Count, pageSize);
            if (page > table.intTotalPage)
            {
                page = table.intTotalPage;
                table.intCurrentPage = table.intTotalPage;
            }

            //hiện số records giới hạn cho mỗi trang
            var data = fq1.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "", Width = 1, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "", Width = 8, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Báo Cáo", IsShow = true, Format = "", Width = 54, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tháng", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Năm", IsShow = true, Format = "", Width = 6, Width_Measure = "%", ColumnAlignment = (int)DataContentAlignment.Center });
            table.lstHeader.Add(new TableHeader() { Caption = "Ngày Đến Hạn", IsShow = true, Format = "", Width = 15, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tình Trạng", IsShow = true, Format = "", Width = 10, Width_Measure = "%" });
            //

            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportHeaderKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        public ActionResult LoadList_RptInputIndex(int page, int pageSize, string SearchKey)
        {
            //SearchKey: list of selected user, splittedby ';'. struct: userkey | enable (this user is selected or not)
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();

            string html_btn_edit = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='{0}'>Nhập liệu</a>";
            string html_btn_view = "<a type=\"button\" class=\"btn btn-primary\" style=\"font-size: 10px;margin-right: 5px;\" href='{0}'>Xem</a>";
            string html_btn_none = "-";

            Guid empKey = MyAccount.EmployeeKey == null ? Guid.Empty : MyAccount.EmployeeKey.Value;

            //lấy danh sách các báo cáo còn đang nhập liệu
            List<int?> lst_rpt_state_allow = new List<int?>();
            lst_rpt_state_allow.Add(Convert.ToInt32(eRptHeaderStatus.Created));
            lst_rpt_state_allow.Add(Convert.ToInt32(eRptHeaderStatus.Declined));
            var lst_rpt_header = (from p in db.RPT_REPORTHEADER
                                  where p.RecordStatus == 1 &&
                                  lst_rpt_state_allow.Contains(p.RptStatus)
                                  select p).ToList();
            var lst_rptkey = lst_rpt_header.Select(p => p.Rpt_ReportHeaderKey).ToList();

            #region Filter các báo cáo có phân công cho nhân viên hiện tại

            //Phase 1: phân công phụ lục
            var lst_tables_emp_enlisted = (from p in db.RPT_REPORTTABLES_EMPLOYEES_ENLISTED
                                           join rt in db.RPT_REPORTDETAIL_TABLEINDEX
                                           on p.Rpt_ReportDetail_TableIndexKey equals rt.Rpt_ReportDetail_TableIndexKey
                                           where p.EmployeeKey == empKey && lst_rptkey.Contains(rt.Rpt_ReportHeaderKey)
                                           select rt.Rpt_ReportHeaderKey).Distinct().ToList();
            //Phase 2: phân công nhập liệu
            var lst_values_emp_enlisted = (from p in db.RPT_REPORTVALUES_EMPLOYEES_ENLISTED
                                           join rv in db.RPT_REPORTDETAIL_VALUECONST
                                           on p.Rpt_ReportDetail_ValueConstKey equals rv.Rpt_ReportDetail_ValueConstKey
                                           where p.EmployeeKey == empKey && lst_rptkey.Contains(rv.Rpt_ReportHeaderKey)
                                           select rv.Rpt_ReportHeaderKey).Distinct().ToList();
            List<Guid> lst_enlisted = new List<Guid>();
            lst_enlisted.AddRange(lst_tables_emp_enlisted);
            lst_enlisted.AddRange(lst_values_emp_enlisted);
            lst_rpt_header = lst_rpt_header.Where(p => lst_enlisted.Contains(p.Rpt_ReportHeaderKey)).ToList();

            #endregion

            #region filter tình trạng nhập liệu các báo cáo của nhân viên hiện tại

            var lst_input_completed = (from p in db.RPT_REPORT_SENT_APPROVED
                                       where p.InputStatus == 1 &&
                                       p.EmployeeKey == empKey &&
                                       lst_enlisted.Contains(p.Rpt_ReportHeaderKey)
                                       select p).ToList();

            #endregion

            var data = lst_rpt_header.Select(d => new
            {
                d.Rpt_ReportHeaderKey,
                d.Rpt_Name,
                NhapPhuLuc = lst_tables_emp_enlisted.Contains(d.Rpt_ReportHeaderKey) ?
                (lst_input_completed.Where(p => p.Rpt_DataType == Convert.ToInt32(eRptDataType.DataTable) && p.Rpt_ReportHeaderKey == d.Rpt_ReportHeaderKey).Count() == 0 ? string.Format(html_btn_edit, "/RPT_INPUT_TABLEDATA/RptDataInput_Edit/" + d.Rpt_ReportHeaderKey) : string.Format(html_btn_view, "/RPT_INPUT_TABLEDATA/RptDataInput_Edit/" + d.Rpt_ReportHeaderKey)) : html_btn_none,
                NhapTruong = lst_values_emp_enlisted.Contains(d.Rpt_ReportHeaderKey) ?
                (lst_input_completed.Where(p => p.Rpt_DataType == Convert.ToInt32(eRptDataType.DataValue) && p.Rpt_ReportHeaderKey == d.Rpt_ReportHeaderKey).Count() == 0 ? string.Format(html_btn_edit, "/RPT_INPUT_VALUEDATA/RptValuesInput_Edit/" + d.Rpt_ReportHeaderKey) : string.Format(html_btn_view, "/RPT_INPUT_VALUEDATA/RptValuesInput_Edit/" + d.Rpt_ReportHeaderKey)) : html_btn_none
            }).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            table.intTotalPage = GetTotalPage(lst_rpt_header.Count, pageSize);

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên báo Cáo", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Phụ Lục", IsShow = true, Format = "", Width = 10, Width_Measure = "%" });
            table.lstHeader.Add(new TableHeader() { Caption = "Trường", IsShow = true, Format = "", Width = 10, Width_Measure = "%" });
            //
            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_ReportHeaderKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
