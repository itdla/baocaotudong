﻿function LoadTableCustom(divtag, url, page) {
    var element = '#' + divtag;
    if ($(element).length == 0)//wrong name
        return;
    else
        $(element).empty();// clear content

    var url_combine = '';
    if (url.indexOf("?") == -1) {
        url_combine = url + '?&page=' + page;
    }
    else {
        url_combine = url + '&page=' + page;
    }

    $.post(url_combine, function (ajresult) {
        if (ajresult.Result_Status == 1) {
            var htmlcontent = '';
            var hasFilter = false;
            if (!ajresult.Data.Selectable)
                $(element).addClass('disabled-selection');

            htmlcontent += '<div class="table100 ver5 m-b-110">';//div-table-container

            //create header
            htmlcontent += '<div class="table100-head' + (ajresult.Data.DataSource.length >= 10 ? ' table100-fixheader' : '') + '">';//div-table-header columns
            htmlcontent += '<table>';
            htmlcontent += '<thead>';
            htmlcontent += '<tr class="row100 head">';
            for (var i = 0; i < ajresult.Data.HeaderColumns.length; i++) {
                if (ajresult.Data.HeaderColumns[i].Filtered)
                    hasFilter = true;

                var colhdr = ajresult.Data.HeaderColumns[i];

                var width = colhdr.Width;
                var width_unit = colhdr.Width_Unit;

                var format = 'style="width: ' + width + width_unit + '; text-align: ' + colhdr.Alignment + ';'
                format += '"';
                htmlcontent += '<th class="cell100' + (colhdr.ColumnVisible ? "" : " cell100-hide") + '" ' + format + '>' + colhdr.Caption + '</th>';
            }
            htmlcontent += '</tr>';
            htmlcontent += '</thead>';

            //filtered
            if (hasFilter) {
                htmlcontent += '<tbody>';
                htmlcontent += '<tr class="row100 body">';

                for (var i = 0; i < ajresult.Data.HeaderColumns.length; i++) {
                    var colhdr = ajresult.Data.HeaderColumns[i];
                    var fieldname = ajresult.Data.HeaderColumns[i].FieldName;

                    htmlcontent += '<td class="cell100' + (colhdr.ColumnVisible ? "" : " cell100-hide") + '">';
                    if (colhdr.ColumnVisible) {
                        if (ajresult.Data.HeaderColumns[i].Filtered) {
                            var filter_val = ajresult.Data.FilterValues[fieldname];
                            htmlcontent += '<input type="text" class="header-box-filter100" value="' + (filter_val == null ? '' : filter_val) + '" data-fieldname="' + fieldname + '" />';
                        }
                        else {
                            htmlcontent += '<input type="text" class="header-box-filter100" value="" disabled/>';
                        }
                    }
                    htmlcontent += '</td>'
                }
                htmlcontent += '</tr>';
                htmlcontent += '</tbody>';
            }

            htmlcontent += '</table>';
            htmlcontent += '</div>';//end of div-table-header columns

            //create body content
            htmlcontent += '<div class="table100-body js-pscroll">';//div-table-body-content columns
            htmlcontent += '<table>';
            htmlcontent += '<tbody>';

            for (var row = 0; row < ajresult.Data.DataSource.length; row++) {
                var tblrow = ajresult.Data.DataSource[row];
                htmlcontent += '<tr class="row100 body"' + (tblrow.DataKey == null ? '' : ' data-id=' + tblrow.DataKey) + ' data-row=' + (row) + '>';

                var hdr = 0;
                $.each(tblrow.DataArray, function (col, value) {
                    var format = '';
                    var colhdr = ajresult.Data.HeaderColumns[hdr];

                    var width = colhdr.Width;
                    var width_unit = colhdr.Width_Unit;

                    //format width only 1st row, others will be auto applied
                    format = 'style="'
                    if (row == 0) {
                        format += 'width: ' + width + width_unit + ';'
                    }
                    format += ' text-align:' + colhdr.Alignment + ';"';

                    htmlcontent += '<td class="cell100' + (colhdr.ColumnVisible ? "" : " cell100-hide") + '"' + format + ' data-col=' + (col) + '>' + (value == null ? '-' : value) + '</td>';
                    hdr++;
                });
                htmlcontent += '</tr>';
            }

            htmlcontent += '</tbody>';
            htmlcontent += '</table>';
            htmlcontent += '</div>';//end of div-table-body-content columns

            var totalpages = ajresult.Data.TotalPages;

            htmlcontent += '<div class="table100-footer">';//div table footer pagination
            htmlcontent += '<span>Tổng số: ' + ajresult.Data.TotalRows + ' dòng</span>';
            if (totalpages > 1) {
                //create table footer pagination
                htmlcontent += '<ul class="paginationw3 at-right" data-tp=' + totalpages + ' data-cp=' + page + '>';
                htmlcontent += '<li><a href="#" data-cmd="prev">Trang Trước</a></li>';
                htmlcontent += '<li><a href="#" data-cmd="first">Đầu</a></li>';

                for (var p = 0; p < totalpages; p++) {
                    var data_cmd = '';
                    if (p == 0)
                        data_cmd = 'first';
                    else if (p == totalpages - 1)
                        data_cmd = 'last'
                    else {
                    }

                    if (totalpages > 5 && p == totalpages - 1) {
                        htmlcontent += '<li class="li-items-pick pick-next li-table-100-paginationw3-show"><a href="#" data-cmd="pick-next">...</a></li>';
                    }
                    htmlcontent += '<li class="li-paginationw3-item li-paginationw3-' + (p + 1) + (p < 5 || (p >= 5 && p == totalpages - 1) ? ' li-table-100-paginationw3-show' : ' li-table-100-paginationw3-hide') + (p + 1 == page ? ' active' : '') + '" data-id=' + (p + 1) + '><a href="#" data-cmd=' + data_cmd + '>' + (p + 1) + '</a></li>';

                    if (totalpages > 5 && p == 0) {
                        htmlcontent += '<li class="li-items-pick pick-prev li-table-100-paginationw3-hide"><a href="#" data-cmd="pick-prev">...</a></li>';
                    }
                }

                htmlcontent += '<li><a href="#" data-cmd="last">Cuối</a></li>';
                htmlcontent += '<li><a href="#" data-cmd="next">Trang Sau</a></li>';
                htmlcontent += '</ul>';

            }
            htmlcontent += '</div>';//end of div table footer pagination
            htmlcontent += '</div>';//end of div-table-container
            $(element).html(htmlcontent);
        }
    });

    if (!$(element).hasClass('table-solved')) {
        $(element).addClass('table-solved');

        //filter on input-enter
        $(element).on('keypress', '.table100-head .header-box-filter100', function (e) {
            if (e.keyCode == 13) {
                var url_combine = '';
                if (url.indexOf("?") == -1) {
                    url_combine = url + '?&page=' + page;
                }
                else {
                    url_combine = url + '&page=' + page;
                }

                $(this).closest('td').closest('tr').find('input[type="text"]').each(function () {
                    var fieldname = $(this).attr('data-fieldname');
                    if (fieldname != null) {
                        var fieldval = this.value;
                        if (fieldval != null && fieldval != '') {
                            url_combine += '&' + fieldname + '=' + fieldval
                        }
                    }
                    
                });
                //alert(url_combine);
                LoadTableCustom(divtag, url_combine, 1);
            }
        });

        //table row click: row selected
        $(element).on('click', '.table100-body table tbody tr', function () {
            if ($(element).hasClass('disabled-selection'))
                return;

            var row_current = $(this);
            if (row_current.hasClass('tr-selected')) {
                //click itself
                row_current.removeClass('tr-selected');
                row_current.find('td').each(function () {
                    var col = $(this);
                    col.removeClass('tr-selected');
                });

            }
            else {
                //click other one
                row_current.closest('tbody').find('tr.tr-selected').each(function () {
                    //un select all
                    var row = $(this);
                    row.removeClass('tr-selected');
                    row.find('td').each(function () {
                        var col = $(this);
                        col.removeClass('tr-selected');
                    });
                });

                row_current.addClass('tr-selected');
                row_current.find('td').each(function () {
                    var col = $(this);
                    col.addClass('tr-selected');
                });
            }

        });

        //footer page item click
        $(element).on('click', '.table100-footer .paginationw3 li a', function (v) {
            var cmd = $(this).attr('data-cmd');
            var ctrl_ul = $(this).closest('li').closest('ul');
            var max_count = parseInt(ctrl_ul.attr('data-tp'));

            if (cmd == 'next') {
                var li_active = ctrl_ul.find('li.li-paginationw3-item.active');
                if (li_active == null)
                    return;

                var li_a_index = parseInt(li_active.attr('data-id'));

                if (li_a_index < max_count) {
                    li_active.removeClass('active');

                    var next_index = li_a_index + 1;

                    var ctrl_next = ctrl_ul.find('li.li-paginationw3-' + next_index);
                    var next_is_hide = ctrl_next.hasClass('li-table-100-paginationw3-hide');

                    ctrl_next.removeClass('li-table-100-paginationw3-hide');
                    ctrl_next.removeClass('li-table-100-paginationw3-show');
                    ctrl_next.addClass('li-table-100-paginationw3-show');
                    ctrl_next.addClass('active');

                    if (next_is_hide) {
                        //next item is now shown, but hidden before
                        //show 5 items in limit
                        //from last to previous-5th
                        var lst_li_show = ctrl_ul.find('li.li-paginationw3-item.li-table-100-paginationw3-show').get().reverse();
                        for (i = 0; i < lst_li_show.length; i++) {
                            $(lst_li_show[i]).removeClass('li-table-100-paginationw3-hide');
                            $(lst_li_show[i]).removeClass('li-table-100-paginationw3-show');
                            if (i <= 5) {
                                $(lst_li_show[i]).addClass('li-table-100-paginationw3-show');
                            }
                            else
                                $(lst_li_show[i]).addClass('li-table-100-paginationw3-hide');
                        }

                        if (next_index >= 7) {
                            //show 1st page if total >=7 and show pick-prev button
                            ctrl_ul.find('li.li-paginationw3-item').each(function () {
                                $(this).removeClass('li-table-100-paginationw3-hide');
                                $(this).removeClass('li-table-100-paginationw3-show');
                                $(this).addClass('li-table-100-paginationw3-show');
                                return false;
                            });
                        }

                        if (next_index == max_count - 1) {
                            var ctrl_pick_next = ctrl_ul.find('li.li-items-pick.pick-next');
                            ctrl_pick_next.removeClass('li-table-100-paginationw3-hide');
                            ctrl_pick_next.removeClass('li-table-100-paginationw3-show');
                            ctrl_pick_next.addClass('li-table-100-paginationw3-hide');

                            //remove 1 more page count from left-side
                            lst_li_show = ctrl_ul.find('li.li-paginationw3-item.li-table-100-paginationw3-show').get();
                            $(lst_li_show[1]).removeClass('li-table-100-paginationw3-hide');
                            $(lst_li_show[1]).removeClass('li-table-100-paginationw3-show');
                            $(lst_li_show[1]).addClass('li-table-100-paginationw3-hide');
                        }

                        var ctrl_pick_prev = ctrl_ul.find('li.li-items-pick.pick-prev');
                        ctrl_pick_prev.removeClass('li-table-100-paginationw3-hide');
                        ctrl_pick_prev.removeClass('li-table-100-paginationw3-show');
                        ctrl_pick_prev.addClass('li-table-100-paginationw3-show');
                    }
                    else {
                        //no need to activate show/hide code
                    }
                }
            }
            else if (cmd == 'prev') {
                var li_active = ctrl_ul.find('li.li-paginationw3-item.active');
                if (li_active == null)
                    return;

                var li_a_index = parseInt(li_active.attr('data-id'));

                if (li_a_index > 1) {
                    li_active.removeClass('active');

                    var next_index = li_a_index - 1;

                    var ctrl_next = ctrl_ul.find('li.li-paginationw3-' + next_index);
                    var next_is_hide = ctrl_next.hasClass('li-table-100-paginationw3-hide');

                    ctrl_next.removeClass('li-table-100-paginationw3-hide');
                    ctrl_next.removeClass('li-table-100-paginationw3-show');
                    ctrl_next.addClass('li-table-100-paginationw3-show');
                    ctrl_next.addClass('active');

                    if (next_is_hide) {
                        //next item is now shown, but hidden before
                        //show 5 items in limit
                        //from last to previous-5th
                        var lst_li_show = ctrl_ul.find('li.li-paginationw3-item.li-table-100-paginationw3-show').get();
                        for (i = 1; i < lst_li_show.length; i++) {
                            $(lst_li_show[i]).removeClass('li-table-100-paginationw3-hide');
                            $(lst_li_show[i]).removeClass('li-table-100-paginationw3-show');
                            if (i <= 5) {
                                $(lst_li_show[i]).addClass('li-table-100-paginationw3-show');
                            }
                            else
                                $(lst_li_show[i]).addClass('li-table-100-paginationw3-hide');
                        }

                        if (next_index <= max_count - 5) {
                            //hide last page
                            $(ctrl_ul.find('li.li-paginationw3-item').get().reverse()).each(function () {
                                $(this).removeClass('li-table-100-paginationw3-hide');
                                $(this).removeClass('li-table-100-paginationw3-show');
                                if (next_index == max_count - 5)
                                    $(this).addClass('li-table-100-paginationw3-hide');
                                else
                                    $(this).addClass('li-table-100-paginationw3-show')
                                return false;
                            });
                        }

                        if (next_index == 2) {
                            var ctrl_pick_prev = ctrl_ul.find('li.li-items-pick.pick-prev');
                            ctrl_pick_prev.removeClass('li-table-100-paginationw3-hide');
                            ctrl_pick_prev.removeClass('li-table-100-paginationw3-show');
                            ctrl_pick_prev.addClass('li-table-100-paginationw3-hide');

                            //remove 1 more page count from right-side
                            lst_li_show = ctrl_ul.find('li.li-paginationw3-item.li-table-100-paginationw3-show').get().reverse();
                            $(lst_li_show[1]).removeClass('li-table-100-paginationw3-hide');
                            $(lst_li_show[1]).removeClass('li-table-100-paginationw3-show');
                            $(lst_li_show[1]).addClass('li-table-100-paginationw3-hide');
                        }

                        var ctrl_pick_next = ctrl_ul.find('li.li-items-pick.pick-next');
                        ctrl_pick_next.removeClass('li-table-100-paginationw3-hide');
                        ctrl_pick_next.removeClass('li-table-100-paginationw3-show');
                        ctrl_pick_next.addClass('li-table-100-paginationw3-show');

                    }
                    else {
                        //no need to activate show/hide code
                    }
                }
            }
            else if (cmd == 'first') {
                var lst_li_items = ctrl_ul.find('li.li-paginationw3-item').get();
                for (i = 0; i < lst_li_items.length; i++) {
                    $(lst_li_items[i]).removeClass('li-table-100-paginationw3-hide');
                    $(lst_li_items[i]).removeClass('li-table-100-paginationw3-show');
                    $(lst_li_items[i]).removeClass('active');
                    if (i == 0)
                        $(lst_li_items[i]).addClass('active');

                    if (i < 5 || i == lst_li_items.length - 1)
                        $(lst_li_items[i]).addClass('li-table-100-paginationw3-show');
                    else
                        $(lst_li_items[i]).addClass('li-table-100-paginationw3-hide');
                }

                var ctrl_pick_next = ctrl_ul.find('li.li-items-pick.pick-next');
                ctrl_pick_next.removeClass('li-table-100-paginationw3-hide');
                ctrl_pick_next.removeClass('li-table-100-paginationw3-show');
                ctrl_pick_next.addClass('li-table-100-paginationw3-show');

                var ctrl_pick_prev = ctrl_ul.find('li.li-items-pick.pick-prev');
                ctrl_pick_prev.removeClass('li-table-100-paginationw3-hide');
                ctrl_pick_prev.removeClass('li-table-100-paginationw3-show');
                ctrl_pick_prev.addClass('li-table-100-paginationw3-hide');
            }
            else if (cmd == 'last') {
                var lst_li_items = ctrl_ul.find('li.li-paginationw3-item').get().reverse();
                for (i = 0; i < lst_li_items.length; i++) {
                    $(lst_li_items[i]).removeClass('li-table-100-paginationw3-hide');
                    $(lst_li_items[i]).removeClass('li-table-100-paginationw3-show');
                    $(lst_li_items[i]).removeClass('active');
                    if (i == 0)
                        $(lst_li_items[i]).addClass('active');

                    if (i < 5 || i == lst_li_items.length - 1)
                        $(lst_li_items[i]).addClass('li-table-100-paginationw3-show');
                    else
                        $(lst_li_items[i]).addClass('li-table-100-paginationw3-hide');
                }

                var ctrl_pick_next = ctrl_ul.find('li.li-items-pick.pick-next');
                ctrl_pick_next.removeClass('li-table-100-paginationw3-hide');
                ctrl_pick_next.removeClass('li-table-100-paginationw3-show');
                ctrl_pick_next.addClass('li-table-100-paginationw3-hide');

                var ctrl_pick_prev = ctrl_ul.find('li.li-items-pick.pick-prev');
                ctrl_pick_prev.removeClass('li-table-100-paginationw3-hide');
                ctrl_pick_prev.removeClass('li-table-100-paginationw3-show');
                ctrl_pick_prev.addClass('li-table-100-paginationw3-show');
            }
            else if (cmd == 'pick-next') {
                var lst_li_items_show = ctrl_ul.find('li.li-paginationw3-item.li-table-100-paginationw3-show').get();
                var index_next_to_last = parseInt($(lst_li_items_show[lst_li_items_show.length - 2]).attr('data-id'));

                //hide all, remove active
                for (i = 0; i < lst_li_items_show.length; i++) {
                    $(lst_li_items_show[i]).removeClass('li-table-100-paginationw3-hide');
                    $(lst_li_items_show[i]).removeClass('li-table-100-paginationw3-show');
                    $(lst_li_items_show[i]).removeClass('active');

                    //show 1st 'n last
                    if (i == 0 || i == lst_li_items_show.length - 1)
                        $(lst_li_items_show[i]).addClass('li-table-100-paginationw3-show');
                    else
                        $(lst_li_items_show[i]).addClass('li-table-100-paginationw3-hide');
                }

                index_next_to_last++;
                var stop_index = index_next_to_last + 4;
                if (stop_index > max_count) {
                    stop_index = max_count;
                    index_next_to_last = stop_index - 4;// count the last

                    //hide pick-next button
                    var ctrl_pick_next = ctrl_ul.find('li.li-items-pick.pick-next');
                    ctrl_pick_next.removeClass('li-table-100-paginationw3-hide');
                    ctrl_pick_next.removeClass('li-table-100-paginationw3-show');
                    ctrl_pick_next.addClass('li-table-100-paginationw3-hide');
                }
                else {

                    //show pick-next button
                    var ctrl_pick_next = ctrl_ul.find('li.li-items-pick.pick-next');
                    ctrl_pick_next.removeClass('li-table-100-paginationw3-hide');
                    ctrl_pick_next.removeClass('li-table-100-paginationw3-show');
                    ctrl_pick_next.addClass('li-table-100-paginationw3-show');
                }
                //show pick-prev button
                var ctrl_pick_prev = ctrl_ul.find('li.li-items-pick.pick-prev');
                ctrl_pick_prev.removeClass('li-table-100-paginationw3-hide');
                ctrl_pick_prev.removeClass('li-table-100-paginationw3-show');
                ctrl_pick_prev.addClass('li-table-100-paginationw3-show');

                //show next picked items
                for (i = index_next_to_last; i <= stop_index; i++) {
                    var next_ctrl = ctrl_ul.find('li.li-paginationw3-item.li-paginationw3-' + i);
                    next_ctrl.removeClass('li-table-100-paginationw3-hide');
                    next_ctrl.removeClass('li-table-100-paginationw3-show');
                    next_ctrl.removeClass('active');
                    next_ctrl.addClass('li-table-100-paginationw3-show');

                    if (i == index_next_to_last)
                        next_ctrl.addClass('active');
                }

                if (stop_index == max_count - 1) {
                    var last_item = lst_li_items_show[lst_li_items_show.length - 1];
                    $(last_item).removeClass('li-table-100-paginationw3-hide');
                    $(last_item).removeClass('li-table-100-paginationw3-show');

                    $(last_item).addClass('li-table-100-paginationw3-hide');
                }
            }
            else if (cmd == 'pick-prev') {
                var lst_li_items_show = ctrl_ul.find('li.li-paginationw3-item.li-table-100-paginationw3-show').get().reverse();//reverse
                var index_next_to_last = parseInt($(lst_li_items_show[lst_li_items_show.length - 2]).attr('data-id'));// from last to first

                //hide all, remove active
                for (i = 0; i < lst_li_items_show.length; i++) {
                    $(lst_li_items_show[i]).removeClass('li-table-100-paginationw3-hide');
                    $(lst_li_items_show[i]).removeClass('li-table-100-paginationw3-show');
                    $(lst_li_items_show[i]).removeClass('active');

                    //show 1st 'n last
                    if (i == 0 || i == lst_li_items_show.length - 1)
                        $(lst_li_items_show[i]).addClass('li-table-100-paginationw3-show');
                    else
                        $(lst_li_items_show[i]).addClass('li-table-100-paginationw3-hide');
                }

                index_next_to_last--;
                var stop_index = index_next_to_last - 4;
                if (stop_index < 1) {
                    stop_index = 1;
                    index_next_to_last = stop_index + 4;//count the first

                    //hide pick-prev button
                    var ctrl_pick_prev = ctrl_ul.find('li.li-items-pick.pick-prev');
                    ctrl_pick_prev.removeClass('li-table-100-paginationw3-hide');
                    ctrl_pick_prev.removeClass('li-table-100-paginationw3-show');
                    ctrl_pick_prev.addClass('li-table-100-paginationw3-hide');
                }
                else {
                    //show pick-prev button
                    var ctrl_pick_prev = ctrl_ul.find('li.li-items-pick.pick-prev');
                    ctrl_pick_prev.removeClass('li-table-100-paginationw3-hide');
                    ctrl_pick_prev.removeClass('li-table-100-paginationw3-show');
                    ctrl_pick_prev.addClass('li-table-100-paginationw3-show');
                }

                //show pick-next button
                var ctrl_pick_next = ctrl_ul.find('li.li-items-pick.pick-next');
                ctrl_pick_next.removeClass('li-table-100-paginationw3-hide');
                ctrl_pick_next.removeClass('li-table-100-paginationw3-show');
                ctrl_pick_next.addClass('li-table-100-paginationw3-show');

                //show next picked items
                for (i = index_next_to_last; i >= stop_index; i--) {
                    var next_ctrl = ctrl_ul.find('li.li-paginationw3-item.li-paginationw3-' + i);
                    next_ctrl.removeClass('li-table-100-paginationw3-hide');
                    next_ctrl.removeClass('li-table-100-paginationw3-show');
                    next_ctrl.removeClass('active');
                    next_ctrl.addClass('li-table-100-paginationw3-show');

                    if (i == stop_index)
                        next_ctrl.addClass('active');
                }

                if (stop_index == 2) {
                    var last_item = lst_li_items_show[lst_li_items_show.length - 1];
                    $(last_item).removeClass('li-table-100-paginationw3-hide');
                    $(last_item).removeClass('li-table-100-paginationw3-show');

                    $(last_item).addClass('li-table-100-paginationw3-hide');
                }
            }
            else {
                ctrl_ul.find('li.active').removeClass('active');
                $(this).closest('li').addClass('active');
            }

            var selectedpage = ctrl_ul.find('li.active').attr('data-id');
            ctrl_ul.attr('data-cp', selectedpage);

            LoadTableCustom(divtag, url, selectedpage);
        });
    }
}

function GetTableSource(divtag) {
    var arr_result = new Array();
    var element = '#' + divtag;
    if ($(element).length == 0)//wrong name
        return arr_result;

    var tbl = $(element).find('.table100-body table');

    var arr_fields = [];
    var initheader = false;

    tbl.find('tbody tr').each(function () {
        var row = $(this);
        var arr = [];
        row.find('td').each(function () {
            if (!initheader)
                arr_fields[arr_fields.length] = $(this).attr('data-col');
            arr[arr.length] = this.innerText;
        });

        if (!initheader) {
            initheader = true;
            arr_result.push(arr_fields);
        }
        arr_result.push(arr);
    });
    return arr_result;
}