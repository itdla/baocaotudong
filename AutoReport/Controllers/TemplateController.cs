﻿//using AutoReport.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace AutoReport.Controllers
//{
//    public class TemplateController : BaseController
//    {
//        // GET: RPT_TEMPLATES
//        AutoReportEntities db = new AutoReportEntities();
//        public ActionResult Index()
//        {
//            return View();
//        }
//        public ActionResult Create()
//        {
//            return View();
//        }
//        public ActionResult CreateTable()
//        {
//            if (Session["RPT_TEMPLATES_DETAIL_TABLEINDEX"] != null)
//            {
//                return View(((List<RPT_TEMPLATES_DETAIL_TABLEINDEX>)Session["RPT_TEMPLATES_DETAIL_TABLEINDEX"]).FirstOrDefault());
//            }
//            return View();
//        }
//        public ActionResult LoadValueList_E(Guid? id)
//        {
//            List<object> arrBody = new List<object>();
//            string[] arrHDR = { "", "", "" };
//            GridDetail grid = new GridDetail();

//            grid.Header.Add(new GridDetail.HeaderColumns()
//            {
//                Caption = "Rpt_Templates_Detail_ValueKey",
//                Field = "Rpt_Templates_Detail_ValueKey",
//                DataType = GridDetail.eDataType.IntegerType,
//                Width = "20",
//                Enable = false,
//                IsShow = false,
//                LayoutType = GridDetail.eLayoutType.Hide
//            });
//            grid.Header.Add(new GridDetail.HeaderColumns()
//            {
//                Caption = "Tên trường",
//                Field = "CodeID",
//                DataType = GridDetail.eDataType.StringType,
//                Width = "75",
//                IsShow = true,
//            });
//            grid.Header.Add(new GridDetail.HeaderColumns()
//            {
//                Caption = "Giá trị",
//                Field = "CodeName",
//                DataType = GridDetail.eDataType.StringType,
//                Width = "75",
//                Enable = true,
//                IsShow = true
//            });


//            var zDetails = db.RPT_TEMPLATES_DETAIL_VALUE.Where(p => p.Rpt_TemplatesKey == id).ToList();
//            if (zDetails.Count > 0)
//            {
//                for (int i = 0; i < zDetails.Count; i++)
//                {
//                    arrBody.Add(new object[]
//                    {
//                          zDetails[i].Rpt_Templates_Detail_ValueKey,
//                          zDetails[i].CodeID,
//                          zDetails[i].CodeName,
//                    });
//                }
//            }
//            else
//            {
//                arrBody.Add(new object[]
//                    {"","",""});
//            }
//            grid.Data = arrBody;
//            //var data = new { header = arrHDR, body = arrBody };
//            return new CustomJsonResult { Data = grid };
//        }
//        public ActionResult LoadTableList_E(Guid? id)
//        {
//            List<object> arrBody = new List<object>();
//            string[] arrHDR = { "", "", "", "", "", "" };
//            GridDetail grid = new GridDetail();

//            grid.Header.Add(new GridDetail.HeaderColumns()
//            {
//                Caption = "Rpt_Templates_Detail_TableKey",
//                Field = "Rpt_Templates_Detail_TableKey",
//                DataType = GridDetail.eDataType.IntegerType,
//                Width = "20",
//                Enable = false,
//                IsShow = false,
//                LayoutType = GridDetail.eLayoutType.Hide
//            });
//            grid.Header.Add(new GridDetail.HeaderColumns()
//            {
//                Caption = "Tên cột",
//                Field = "ColumnName",
//                DataType = GridDetail.eDataType.StringType,
//                Width = "75",
//                Enable = true,
//                IsShow = true
//            });
//            grid.Header.Add(new GridDetail.HeaderColumns()
//            {
//                Caption = "Tiêu đề",
//                Field = "ColumnCaption",
//                DataType = GridDetail.eDataType.StringType,
//                Width = "75",
//                Enable = true,
//                IsShow = true
//            });
//            grid.Header.Add(new GridDetail.HeaderColumns()
//            {
//                Caption = "Loại dữ liệu",
//                Field = "ColumnDataType",
//                DataType = GridDetail.eDataType.IntegerType,
//                Width = "75",
//                Enable = true,
//                IsShow = true
//            });
//            grid.Header.Add(new GridDetail.HeaderColumns()
//            {
//                Caption = "Vị trí",
//                Field = "ColumnOrder",
//                DataType = GridDetail.eDataType.IntegerType,
//                Width = "75",
//                Enable = true,
//                IsShow = true
//            });
//            List<RPT_TEMPLATES_DETAIL_TABLEINDEX> zDetails;
//            if (id != null)
//            {
//                zDetails = db.RPT_TEMPLATES_DETAIL_TABLEINDEX.Where(p => p.Rpt_TemplatesKey == id).ToList();
//            }
//            else
//            {
//                zDetails = (List<RPT_TEMPLATES_DETAIL_TABLEINDEX>)Session["RPT_TEMPLATES_DETAIL_TABLEINDEX"];
//                if (zDetails == null)
//                {
//                    zDetails = new List<RPT_TEMPLATES_DETAIL_TABLEINDEX>();
//                }
//            }
//            if (zDetails.Count > 0)
//            {
//                for (int i = 0; i < zDetails.Count; i++)
//                {
//                    arrBody.Add(new object[]
//                    {
//                          zDetails[i].Rpt_Templates_Detail_TableKey,
//                          zDetails[i].ColumnName,
//                          zDetails[i].ColumnCaption,
//                          zDetails[i].ColumnDataType,
//                          zDetails[i].ColumnOrder,
//                    });
//                }
//            }
//            else
//            {
//                arrBody.Add(new object[]
//                    {"","","","","",""});
//            }
//            grid.Data = arrBody;
//            //var data = new { header = arrHDR, body = arrBody };
//            return new CustomJsonResult { Data = grid };
//        }
//        [HttpPost]
//        public ActionResult SaveTableDetailList(List<string[]> ListItemString, string TableName, string TableID)
//        {
//            AjaxResult zResult = new AjaxResult();
//            List<Guid> ListIdChiTiet = new List<Guid>();
//            List<RPT_TEMPLATES_DETAIL_TABLEINDEX> Items = (List<RPT_TEMPLATES_DETAIL_TABLEINDEX>)Session["RPT_TEMPLATES_DETAIL_TABLEINDEX"];
//            if (Items == null)
//            {
//                Items = new List<RPT_TEMPLATES_DETAIL_TABLEINDEX>();
//            }
//            foreach (var Detail in ListItemString)
//            {
//                if (!string.IsNullOrEmpty(Detail[1]) && !string.IsNullOrEmpty(Detail[2]) && !string.IsNullOrEmpty(Detail[3]))
//                {
//                    Detail[1] = Detail[1].Replace(".", "");
//                    Detail[2] = Detail[2].Replace(".", "");
//                    Detail[3] = Detail[3].Replace(".", "");
//                    Detail[4] = Detail[4].Replace(".", "");
//                    bool IsUpdate = true;
//                    RPT_TEMPLATES_DETAIL_TABLEINDEX _item;
//                    if (!string.IsNullOrEmpty(Detail[0]))
//                    {
//                        Guid ID = new Guid(Detail[0]);
//                        _item = Items.Where(p => p.Rpt_Templates_Detail_TableKey == ID).FirstOrDefault();

//                    }
//                    else
//                    {
//                        _item = new RPT_TEMPLATES_DETAIL_TABLEINDEX();
//                        _item.Rpt_Templates_Detail_TableKey = Guid.NewGuid();
//                        IsUpdate = false;
//                    }
//                    _item.TableName = TableName;
//                    _item.TableID = TableID;
//                    _item.ColumnName = Detail[1];
//                    _item.ColumnCaption = Detail[2];
//                    _item.ColumnDataType = Detail[3];
//                    _item.ColumnOrder = int.Parse(Detail[4]);

//                    if (!IsUpdate)
//                    {
//                        Items.Add(_item);
//                    }
//                    ListIdChiTiet.Add(_item.Rpt_Templates_Detail_TableKey);


//                }

//            }
//            var ListChiTietDel = Items.Where(p => !ListIdChiTiet.Contains(p.Rpt_Templates_Detail_TableKey)).ToList();
//            foreach (var item in ListChiTietDel)
//            {
//                Items.Remove(item);
//            }
//            Session["RPT_TEMPLATES_DETAIL_TABLEINDEX"] = Items;
//            zResult.intStatus = 1;
//            zResult.strMess = "Tạo bảng thành công";
//            return new CustomJsonResult { Data = (zResult) };
//        }
//        [HttpPost]
//        public ActionResult SaveTemplate(List<string[]> ListItemString, RPT_TEMPLATES Item)
//        {
//            AjaxResult zResult = new AjaxResult();
//            List<Guid> ListIdChiTiet = new List<Guid>();

//            //Save RPT_TEMPLATES
//            Item.Rpt_TemplatesKey = Guid.NewGuid();
//            Item.CreatedBy = Guid.Empty;
//            Item.CreatedOn = DateTime.Now;
//            db.RPT_TEMPLATES.Add(Item);

//            //Save RPT_TEMPLATES_DETAIL_VALUE
//            foreach (var Detail in ListItemString)
//            {
//                if (!string.IsNullOrEmpty(Detail[1]) && !string.IsNullOrEmpty(Detail[2]) && !string.IsNullOrEmpty(Detail[3]))
//                {
//                    Detail[1] = Detail[1].Replace(".", "");
//                    Detail[2] = Detail[2].Replace(".", "");
//                    bool IsUpdate = true;
//                    RPT_TEMPLATES_DETAIL_VALUE _item;
//                    if (!string.IsNullOrEmpty(Detail[0]))
//                    {
//                        Guid ID = new Guid(Detail[0]);
//                        _item = db.RPT_TEMPLATES_DETAIL_VALUE.Where(p => p.Rpt_Templates_Detail_ValueKey == ID).FirstOrDefault();
//                        _item.Rpt_TemplatesKey = Item.Rpt_TemplatesKey;
//                    }
//                    else
//                    {
//                        _item = new RPT_TEMPLATES_DETAIL_VALUE();
//                        _item.Rpt_Templates_Detail_ValueKey = Guid.NewGuid();
//                        IsUpdate = false;
//                    }
//                    _item.CodeName = Detail[1];
//                    _item.CodeID = Detail[2];

//                    if (!IsUpdate)
//                    {
//                        db.RPT_TEMPLATES_DETAIL_VALUE.Add(_item);
//                    }
//                    else
//                    {
//                        db.Entry(_item).State = System.Data.Entity.EntityState.Modified;
//                    }

//                }

//            }
//            var ListChiTietDel = db.RPT_TEMPLATES_DETAIL_VALUE.Where(p => !ListIdChiTiet.Contains(p.Rpt_Templates_Detail_ValueKey)).ToList();
//            foreach (var item in ListChiTietDel)
//            {
//                db.Entry(item).State = System.Data.Entity.EntityState.Deleted;
//            }

//            //Save RPT_TEMPLATES_DETAIL_TABLEINDEX
//            List<RPT_TEMPLATES_DETAIL_TABLEINDEX> Items = (List<RPT_TEMPLATES_DETAIL_TABLEINDEX>)Session["RPT_TEMPLATES_DETAIL_TABLEINDEX"];
//            if (Items != null && Items.Count > 0)
//            {
//                foreach(var item in Items)
//                {
//                    item.Rpt_TemplatesKey = Item.Rpt_TemplatesKey;
//                    db.RPT_TEMPLATES_DETAIL_TABLEINDEX.Add(item);
//                }
//            }

//            Session["RPT_TEMPLATES_DETAIL_TABLEINDEX"] = null;
//            db.SaveChanges();
//            zResult.intStatus = 1;
//            zResult.strMess = "Tạo bảng thành công";
//            return new CustomJsonResult { Data = (zResult) };
//        }

//        public JsonResult LoadTableList(int page, int pageSize, string SearchKey, Guid? id)
//        {
//            //Guid Document_ID = new Guid(this.Request["id"]);
//            page = page == 0 ? 1 : page;
//            var table = new TableModels();
//            table.intStatus = 1;
//            table.intCurrentPage = page;
//            table.isShowSearch = false;
//            table.isShowAction = false;

//            table.strURLEdit = "/Web_Mod/Edit";
//            table.strURLDetail = "/Web_Mod/Details";
//            table.strURLDelete = "/Web_Mod/Delete";

//            table.lstData = new List<TableItem>();
//            table.lstHeader = new List<TableHeader>();
//            //Filter
//            var searchQuery = from f in db.RPT_TEMPLATES_DETAIL_TABLEINDEX
//                              where f.Rpt_TemplatesKey == id
//                              orderby f.ColumnOrder descending
//                              select f;

//            //if (!string.IsNullOrEmpty(SearchKey))
//            //{
//            //    searchQuery = searchQuery.Where(p => p.d.Name.Contains(SearchKey) || p.d.FileName.Contains(SearchKey));
//            //}

//            var data = searchQuery.ToList().Select(d => new
//            {
//                d.Rpt_Templates_Detail_TableKey,
//                d.TableName,
//                HanhDong = "<button type=\"button\" class=\"btn btn-success btn_thanhcong\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.Rpt_Templates_Detail_TableKey + "'>Chỉnh sửa</button>" +
//                "<button type=\"button\" class=\"btn btn-danger btn_xoa\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.Rpt_Templates_Detail_TableKey + "'>Xóa </button>",


//            }).Skip((page - 1) * pageSize).Take(pageSize).OrderByDescending(p => p.TableName).ToList();

//            table.intTotalPage = GetTotalPage(searchQuery.Count(), pageSize);

//            //Set column
//            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "" });
//            table.lstHeader.Add(new TableHeader() { Caption = "Tên Bảng", IsShow = true, Format = "" });
//            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 200 });


//            //
//            for (int i = 0; i < data.Count; i++)
//            {
//                TableItem item = new TableItem();
//                item.ID = data[i].Rpt_Templates_Detail_TableKey.ToString();
//                item.isShowDelete = false;

//                item.isShowDetail = false;

//                item.isShowEdit = false;

//                item.objData = data[i];
//                table.lstData.Add(item);
//            }

//            return new CustomJsonResult { Data = table };
//        }
//    }
//}