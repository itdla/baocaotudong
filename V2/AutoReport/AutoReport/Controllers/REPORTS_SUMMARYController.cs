﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoReport.Models;
using System.Transactions;
using AutoReport.Utils;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace AutoReport.Controllers
{
    public class REPORTS_SUMMARYController : BaseController
    {
        private AutoReport_V3Entities db = new AutoReport_V3Entities();
        private string m_ControllerName = "REPORTS_SUMMARY";

        // GET: REPORTS_SUMMARY
        public ActionResult SummaryGrid()
        {
            if (!GrantAccess(string.Format("{0}_GRID", m_ControllerName)))
                return B_NotPermission();
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult SummaryChart()
        {
            if (!GrantAccess(string.Format("{0}_CHART", m_ControllerName)))
                return B_NotPermission();
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }

        public ActionResult RTB(string[] lstLinks, string[] lstText)
        {
            return LoadHeaderLinkReturn(lstLinks, lstText);
        }

        public ActionResult GetSummaryGrid(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                string StrFromDate = Request.QueryString["StrFromDate"] ?? "";
                string StrToDate = Request.QueryString["StrToDate"] ?? "";

                DateTime? FromDate = null;
                if (!string.IsNullOrEmpty(StrFromDate))
                    FromDate = DateTime.ParseExact(StrFromDate + " 00:00:00", TextUtil.DateLocalFormat + " hh:mm:ss", null);
                DateTime? ToDate = null;
                if (!string.IsNullOrEmpty(StrToDate))
                    ToDate = DateTime.ParseExact(StrToDate + " 23:59:59", TextUtil.DateLocalFormat + " HH:mm:ss", null);

                page = page == 0 ? 1 : page;
                //giới hạn số báo cáo, theo nhân viên của phòng ban
                var lstRestricted = GetListTemplateRestricted().Select(p => p.TemplateHeaderKey).ToList();

                var lstItems = (from d in db.REPORTS_HEADERS
                                where d.RecordStatus == 1 && lstRestricted.Contains(d.TemplateKey) && 
                                (FromDate == null || (FromDate != null && d.CreatedOn >= FromDate)) &&
                                (ToDate == null || (ToDate != null && d.CreatedOn <= ToDate))
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.ApproveStatus,
                                    d.DateEnd,
                                    d.CreatedBy
                                }).ToList();

                if (!GrantAccess(string.Format("{0}_GRID", m_ControllerName)))
                    lstItems.Clear();

                TableModel table = new TableModel()
                {
                    Selectable = false,
                };
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "DepartmentName", Caption = "Phòng Ban", Width = 45, Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "Total", Caption = "Số báo cáo", Width = 10, Filtered = false, Alignment = "right" });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "Completed", Caption = "Hoàn thành", Width = 10, Filtered = false, Alignment = "right" });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "OnGoing", Caption = "Đang thực hiện", Width = 15, Filtered = false, Alignment = "right" });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "Overdue", Caption = "Quá hạn", Width = 10, Filtered = false, Alignment = "right" });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "BtnView", Caption = "Chi tiết", Width = 9, Filtered = false, Alignment = "right" });

                var lst_department = db.CAT_DEPARTMENTS.Where(p => p.RecordStatus == 1).OrderBy(p => p.Sorted).ToList();
                var lst_creator = (from e in db.CAT_EMPLOYEES
                                   join u in db.SYS_USERS
                                   on e.dKey equals u.EmployeeKey
                                   select new
                                   {
                                       UserKey = u.dKey,
                                       u.EmployeeKey,
                                       EmployeeName = e.ItemName
                                   }).ToList();

                foreach (var dep in lst_department)
                {
                    var lst_empployee_key = db.CAT_EMPLOYEES.Where(p => p.DepartmentKey == dep.dKey).Select(p => p.dKey).ToList();
                    var lst_report_creator_key = lst_creator.Where(p => lst_empployee_key.Contains(p.EmployeeKey)).ToList();
                    var lst_creator_keys = lst_report_creator_key.Select(p => p.UserKey).ToList();

                    int total_reports = lstItems.Where(p => lst_creator_keys.Contains(Convert.ToInt32(p.CreatedBy))).Count();
                    if (total_reports == 0)
                        continue;

                    int total_completed = lstItems.Where(p => lst_creator_keys.Contains(Convert.ToInt32(p.CreatedBy)) && p.ApproveStatus == 5).Count();
                    int total_ongoing = lstItems.Where(p => lst_creator_keys.Contains(Convert.ToInt32(p.CreatedBy)) && p.ApproveStatus < 5 && (p.DateEnd == null || (p.DateEnd != null && p.DateEnd.Value.Date >= Now.Date))).Count();
                    int total_overdue = lstItems.Where(p => lst_creator_keys.Contains(Convert.ToInt32(p.CreatedBy)) && p.ApproveStatus < 5 && p.DateEnd != null && p.DateEnd.Value.Date < Now.Date).Count();

                    List<object> data_arr = new List<object>();
                    data_arr.AddRange(new object[]
                    {
                        dep.dKey,
                        dep.ItemName,
                        total_reports,
                        total_completed,
                        total_ongoing,
                        total_overdue,
                        string.Format("<a class=\"btn btn-default btn100-action btn-detail-view\" data-id=\"{0}\"><i class=\"mi-2x mi-search at-right\"></i></a>", dep.dKey)
                    });

                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = dep.dKey,
                        DataArray = data_arr.ToArray()
                    });
                }

                table.CalcTotalPage(table.DataSource.Count);
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetSummaryDetailByDepartmentKey(int page)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                string StrFromDate = Request.QueryString["StrFromDate"];
                string StrToDate = Request.QueryString["StrToDate"];
                string StrDepartmentKey = Request.QueryString["StrDepKey"];

                DateTime? FromDate = null;
                if (!string.IsNullOrEmpty(StrFromDate))
                    FromDate = DateTime.ParseExact(StrFromDate + " 00:00:00", TextUtil.DateLocalFormat + " hh:mm:ss", null);
                DateTime? ToDate = null;
                if (!string.IsNullOrEmpty(StrToDate))
                    ToDate = DateTime.ParseExact(StrToDate + " 23:59:59", TextUtil.DateLocalFormat + " HH:mm:ss", null);

                page = page == 0 ? 1 : page;
                var lstRestricted = GetListTemplateRestricted().Select(p => p.TemplateHeaderKey).ToList();
                var lstItems = (from d in db.REPORTS_HEADERS
                                where d.RecordStatus == 1 &&
                                lstRestricted.Contains(d.TemplateKey) &&
                                (FromDate == null || (FromDate != null && d.CreatedOn >= FromDate)) &&
                                (ToDate == null || (ToDate != null && d.CreatedOn <= ToDate))
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.ApproveStatus,
                                    d.DateEnd,
                                    d.CreatedBy,
                                    d.CreatedOn,
                                }).ToList();

                TableModel table = new TableModel()
                {
                    Selectable = false,
                };
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "dKey", Caption = "KEY", Width = 1, ColumnVisible = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "ReportHeaderName", Caption = "Tên Báo Cáo", Width = 64, Filtered = false });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "DateCreated", Caption = "Ngày Tạo", Width = 10, Filtered = false, Alignment = "center" });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "DateDue", Caption = "Hạn chót", Width = 10, Filtered = false, Alignment = "center" });
                table.HeaderColumns.Add(new TableHeaderColumns() { FieldName = "Status", Caption = "Tình Trạng", Width = 15, Filtered = false, Alignment = "center" });

                var department = db.CAT_DEPARTMENTS.Find(Convert.ToInt32(StrDepartmentKey));
                var lst_creator = (from e in db.CAT_EMPLOYEES
                                   join u in db.SYS_USERS
                                   on e.dKey equals u.EmployeeKey
                                   select new
                                   {
                                       UserKey = u.dKey,
                                       u.EmployeeKey,
                                       EmployeeName = e.ItemName
                                   }).ToList();

                var lst_empployee_key = db.CAT_EMPLOYEES.Where(p => p.DepartmentKey == department.dKey).Select(p => p.dKey).ToList();
                var lst_report_creator_key = lst_creator.Where(p => lst_empployee_key.Contains(p.EmployeeKey)).ToList();
                var lst_creator_keys = lst_report_creator_key.Select(p => p.UserKey).ToList();

                var lstReportHeader = lstItems.Where(p => lst_creator_keys.Contains(Convert.ToInt32(p.CreatedBy))).OrderBy(p => p.CreatedOn).ToList();
                foreach (var itemHdr in lstReportHeader)
                {
                    string status_desc = "";
                    if (itemHdr.ApproveStatus == 5)
                        status_desc = "Hoàn thành";
                    else
                    {
                        if (itemHdr.DateEnd == null || (itemHdr.DateEnd != null && itemHdr.DateEnd.Value.Date >= Now))
                            status_desc = "Đang thực hiện";
                        else
                            status_desc = "Quá Hạn";
                    }
                    List<object> data_arr = new List<object>();
                    data_arr.AddRange(new object[]
                    {
                        itemHdr.dKey,
                        itemHdr.ItemName,
                        itemHdr.CreatedOn.Value.ToString(TextUtil.DateLocalFormat),
                        itemHdr.DateEnd == null ? "-" : itemHdr.DateEnd.Value.ToString(TextUtil.DateLocalFormat),
                        status_desc
                    });

                    table.DataSource.Add(new TableRowsData()
                    {
                        DataKey = itemHdr.dKey,
                        DataArray = data_arr.ToArray()
                    });
                }
                table.CalcTotalPage(table.DataSource.Count);
                result.Data = table;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        public ActionResult GetSummaryChartPie(string StrFromDate, string StrToDate)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                DateTime? FromDate = null;
                if (!string.IsNullOrEmpty(StrFromDate))
                    FromDate = DateTime.ParseExact(StrFromDate + " 00:00:00", TextUtil.DateLocalFormat + " hh:mm:ss", null);
                DateTime? ToDate = null;
                if (!string.IsNullOrEmpty(StrToDate))
                    ToDate = DateTime.ParseExact(StrToDate + " 23:59:59", TextUtil.DateLocalFormat + " HH:mm:ss", null);

                //giới hạn số báo cáo, theo nhân viên của phòng ban
                var lstRestricted = GetListTemplateRestricted().Select(p => p.TemplateHeaderKey).ToList();

                var lstItems = (from d in db.REPORTS_HEADERS
                                where d.RecordStatus == 1 && (FromDate == null || (FromDate != null && d.CreatedOn >= FromDate)) &&
                                lstRestricted.Contains(d.TemplateKey) &&
                                (ToDate == null || (ToDate != null && d.CreatedOn <= ToDate))
                                select new
                                {
                                    d.dKey,
                                    d.ItemCode,
                                    d.ItemName,
                                    d.ApproveStatus,
                                    d.DateEnd,
                                    d.CreatedBy
                                }).ToList();
                if (!GrantAccess(string.Format("{0}_CHART", m_ControllerName)))
                    lstItems.Clear();

                List<object> datasource = new List<object>();

                //hoàn thành
                int count_status = lstItems.Where(p => p.ApproveStatus == 5).Count();
                datasource.Add(new { y = count_status, label = "Hoàn thành", name= "Hoàn thành" });

                //đang thực hiện
                count_status = lstItems.Where(p => p.ApproveStatus != 5 && (p.DateEnd == null || (p.DateEnd != null && p.DateEnd.Value.Date >= Now.Date))).Count();
                if (count_status > 0)
                    datasource.Add(new { y = count_status, label = "Đang thực hiện", name= "Đang thực hiện" });

                //quá hạn
                count_status = lstItems.Where(p => p.ApproveStatus != 5 && p.DateEnd != null && p.DateEnd.Value.Date < Now.Date).Count();
                if (count_status > 0)
                    datasource.Add(new { y = count_status, label = "Quá hạn", name= "Quá hạn" });

                result.Data = datasource;
                result.Result_Status = 1;
            }
            catch (Exception ex)
            {
                result.Result_State = "ERROR";
                result.Result_Message = ex.Message;
            }
            return new CustomJsonResult() { Data = result };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}