﻿using AutoReport.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AutoReport.Controllers
{
    public class TEMPLATES_TABLENAMEController : BaseController
    {
        // GET: TEMPLATES_TABLENAME
        private AutoReportEntities db = new AutoReportEntities();

        private string m_ControllerName = "TEMPLATES_TABLENAME";

        public ActionResult Index()
        {
            if (!CheckPermission("TEMPLATES_TABLENAME"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            return View();
        }
        public ActionResult Create()
        {
            if (!CheckPermission("TEMPLATES_TABLENAME_Create"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            return View(new RPT_TEMPLATES_TABLENAME());
        }
        public ActionResult Edit(Guid id)
        {
            if (!CheckPermission("TEMPLATES_TABLENAME_Edit"))
            {
                return RedirectToAction("NotPermission", "Home");
            }
            ViewBag.ControllerName = m_ControllerName;
            return View(db.RPT_TEMPLATES_TABLENAME.Find(id));
        }

        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            AjaxResult zResult = new AjaxResult();
            if (!CheckPermission("TEMPLATES_TABLENAME_Delete"))
            {
                zResult.strMess = "Bạn không có quyền thực hiện chức năng này";
                return new CustomJsonResult { Data = (zResult) };
            }
            try
            {
                //if (CheckPermission("Delete"))
                {
                    var zOldItem = db.RPT_TEMPLATES_TABLENAME.Where(p => p.Rpt_Templates_TableNameKey == id).FirstOrDefault();
                    if (zOldItem != null)
                    {
                        zOldItem.RecordStatus = -1;
                        db.Entry(zOldItem).State = System.Data.Entity.EntityState.Modified;
                        var ListColumn = db.RPT_TEMPLATES_TABLECOLUMN.Where(p => p.Rpt_Templates_TableNameKey == id).ToList();
                        foreach (var item in ListColumn)
                        {
                            item.RecordStatus = 0;
                            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        }
                        db.SaveChanges();
                        zResult.Data = zOldItem;
                        zResult.intStatus = 1;
                        zResult.strMess = "Xóa thành công";

                    }
                    else
                    {
                        zResult.intStatus = -1;
                        zResult.strMess = "Xóa thành công";
                    }
                }

            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder zSb = new StringBuilder();
                foreach (var failure in ex.EntityValidationErrors)
                {
                    //  sb.AppendFormat("{0} failed valGroupIDation", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        zSb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        zSb.AppendLine();
                    }
                }

                zResult.intStatus = 99;
                zResult.Data = id;
                zResult.strMess = zSb.ToString();
            }
            return new CustomJsonResult { Data = (zResult) };

        }
        public JsonResult LoadList(int page, int pageSize, string SearchKey)
        {
            //Guid Document_ID = new Guid(this.Request["id"]);
            page = page == 0 ? 1 : page;
            var table = new TableModels();
            table.intStatus = 1;
            table.intCurrentPage = page;
            table.isShowSearch = false;
            table.isShowAction = false;

            table.strURLEdit = "/Web_Mod/Edit";
            table.strURLDetail = "/Web_Mod/Details";
            table.strURLDelete = "/Web_Mod/Delete";

            table.lstData = new List<TableItem>();
            table.lstHeader = new List<TableHeader>();
            //Filter
            var searchQuery = from f in db.RPT_TEMPLATES_TABLENAME
                              orderby f.TableID
                              where f.RecordStatus == 1
                              select f;

            int _index = 1;
            var data = searchQuery.ToList().Select(d => new
            {
                d.Rpt_Templates_TableNameKey,
                STT = _index++,
                d.TableID,
                d.TableName,
                HanhDong = "<a type=\"button\" class=\"btn btn-success\" style=\"font-size: 10px;margin-right: 5px;\" href='/" + m_ControllerName + "/Edit/" + d.Rpt_Templates_TableNameKey + "'>Chỉnh sửa</a>" +
                "<button type=\"button\" class=\"btn btn-danger btn_xoa\" style=\"font-size: 10px;margin-right: 5px;\" data-id='" + d.Rpt_Templates_TableNameKey + "'>Xóa </button>",
            }).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            table.intTotalPage = GetTotalPage(searchQuery.Count(), pageSize);

            //Set column
            table.lstHeader.Add(new TableHeader() { Caption = "#ID", IsShow = false, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "STT", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Mã Bảng", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Tên Bảng", IsShow = true, Format = "" });
            table.lstHeader.Add(new TableHeader() { Caption = "Hành động", IsShow = true, Format = "", Width = 200 });
            //

            for (int i = 0; i < data.Count; i++)
            {
                TableItem item = new TableItem();
                item.ID = data[i].Rpt_Templates_TableNameKey.ToString();
                item.isShowDelete = false;

                item.isShowDetail = false;

                item.isShowEdit = false;

                item.objData = data[i];
                table.lstData.Add(item);
            }

            return new CustomJsonResult { Data = table };
        }
        public ActionResult LoadColumnList(Guid? Templates_TableNameKey)
        {
            List<object> arrBody = new List<object>();
            string[] arrHDR = { "", "", "", "", "", "" };
            GridDetail grid = new GridDetail();

            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Rpt_Templates_TableColumnKey",
                Field = "Rpt_Templates_TableColumnKey",
                DataType = GridDetail.eDataType.IntegerType,
                Width = "20",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tên cột",
                Field = "ColumnName",
                DataType = GridDetail.eDataType.StringType,
                Width = "75",
                Enable = true,
                IsShow = true
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Tiêu đề",
                Field = "ColumnCaption",
                DataType = GridDetail.eDataType.StringType,
                Width = "75",
                Enable = true,
                IsShow = true
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "ColumnDataType",
                Field = "ColumnDataType",
                DataType = GridDetail.eDataType.StringType,
                Width = "20",
                Enable = false,
                IsShow = false,
                LayoutType = GridDetail.eLayoutType.Hide
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Loại",
                Field = "ColumnDataTypeName",
                DataType = GridDetail.eDataType.StringType,
                Width = "100",
                IsShow = true,
                LayoutType = GridDetail.eLayoutType.DropdownList,
                DataSource = Utils.GetTypeList()
            });
            grid.Header.Add(new GridDetail.HeaderColumns()
            {
                Caption = "Vị trí",
                Field = "ColumnOrder",
                DataType = GridDetail.eDataType.IntegerType,
                Width = "75",
                Enable = true,
                IsShow = true
            });
            List<RPT_TEMPLATES_TABLECOLUMN> zDetails = db.RPT_TEMPLATES_TABLECOLUMN.Where(p => p.Rpt_Templates_TableNameKey == Templates_TableNameKey && p.RecordStatus == 1).OrderBy(p => p.ColumnOrder).ToList();

            if (zDetails.Count > 0)
            {
                for (int i = 0; i < zDetails.Count; i++)
                {
                    arrBody.Add(new object[]
                    {
                                  zDetails[i].Rpt_Templates_TableColumnKey,
                                  zDetails[i].ColumnName,
                                  zDetails[i].ColumnCaption,
                                  zDetails[i].ColumnDataType,
                                  zDetails[i].ColumnDataType,
                                  zDetails[i].ColumnOrder,
                    });
                }
            }
            else
            {
                arrBody.Add(new object[]
                    {"","","","","",""});
            }
            grid.Data = arrBody;
            //var data = new { header = arrHDR, body = arrBody };
            return new CustomJsonResult { Data = grid };
        }

        [HttpPost]
        public ActionResult SaveTable(List<string[]> tblListItem, RPT_TEMPLATES_TABLENAME Item)
        {
            AjaxResult zResult = new AjaxResult();
            List<Guid> ListIDHave = new List<Guid>();

            //Save RPT_TEMPLATES_TABLENAME
            //Create
            if (Item.Rpt_Templates_TableNameKey == Guid.Empty)
            {
                Item.Rpt_Templates_TableNameKey = Guid.NewGuid();
                Item.RecordStatus = 1;
                db.RPT_TEMPLATES_TABLENAME.Add(Item);
            }
            //Edit
            else
            {
                var oldItem = db.RPT_TEMPLATES_TABLENAME.Find(Item.Rpt_Templates_TableNameKey);
                oldItem.TableName = Item.TableName;
                oldItem.TableID = Item.TableID;
                db.Entry(oldItem).State = System.Data.Entity.EntityState.Modified;
            }

            //Save RPT_TEMPLATES_TABLECOLUMN
            foreach (var Detail in tblListItem)
            {
                if (!string.IsNullOrEmpty(Detail[1]) && !string.IsNullOrEmpty(Detail[2]) && !string.IsNullOrEmpty(Detail[3]) && !string.IsNullOrEmpty(Detail[5]))
                {
                    bool IsUpdate = true;
                    RPT_TEMPLATES_TABLECOLUMN _item;
                    if (!string.IsNullOrEmpty(Detail[0]))
                    {
                        Guid ID = new Guid(Detail[0]);
                        _item = db.RPT_TEMPLATES_TABLECOLUMN.Where(p => p.Rpt_Templates_TableColumnKey == ID).FirstOrDefault();
                    }
                    else
                    {
                        _item = new RPT_TEMPLATES_TABLECOLUMN();
                        _item.Rpt_Templates_TableColumnKey = Guid.NewGuid();
                        _item.Rpt_Templates_TableNameKey = Item.Rpt_Templates_TableNameKey;
                        _item.RecordStatus = 1;
                        IsUpdate = false;
                    }
                    _item.ColumnName = Detail[1];
                    _item.ColumnCaption = Detail[2];
                    _item.ColumnDataType = Detail[3];
                    _item.ColumnOrder = int.Parse(Detail[5]);

                    if (!IsUpdate)
                    {
                        db.RPT_TEMPLATES_TABLECOLUMN.Add(_item);
                    }
                    else
                    {
                        db.Entry(_item).State = System.Data.Entity.EntityState.Modified;
                    }

                    ListIDHave.Add(_item.Rpt_Templates_TableColumnKey);
                }

            }
            var ListChiTietDel = db.RPT_TEMPLATES_TABLECOLUMN.Where(p => !ListIDHave.Contains(p.Rpt_Templates_TableColumnKey) && p.Rpt_Templates_TableNameKey == Item.Rpt_Templates_TableNameKey).ToList();
            foreach (var item in ListChiTietDel)
            {
                db.Entry(item).State = System.Data.Entity.EntityState.Deleted;
            }
            db.SaveChanges();
            zResult.intStatus = 1;
            zResult.strMess = "Lưu dữ liệu thành công";
            zResult.Data = Item;
            return new CustomJsonResult { Data = (zResult) };
        }
    }
}