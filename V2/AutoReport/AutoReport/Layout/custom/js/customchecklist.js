﻿function CheckBoxList(element, source)
{
    var objelement = '#' + element;
    //source: key - caption - checked
    var htmlContent = '';
    htmlContent += '<ul>';
    htmlContent += '<input type="checkbox" data-id="treeroot" class="fancytree-checkbox"/>';
    htmlContent += '<span class="fancytree-title">(Chọn tất cả)</span>';
    $.each(source, function (rowIndex, rowArr) {
        htmlContent += '<li aria-expanded=\"true\">';
        htmlContent += '<input type="checkbox" class="fancytree-checkbox" data-id="' + arr[0] + '" data-parent="treeroot" ' + (arr[2] === true ? ' checked ' : '') + '/>';
        htmlContent += '<span class="fancytree-title">' + arr[1] + '</span>';
        htmlContent += '</li>';
    });
    htmlContent += '</ul>';

    $(objelement).empty();
    $(objelement).append(htmlContent);

    if (!$(objelement).hasClass('treecheckloaded')) {
        $(objelement).addClass('treecheckloaded');
        $(objelement).on('change', 'input[type=checkbox]', function () {
            var id = $(this).attr('data-id');
            var chkall = $(this).prop('checked');

            if (id == 'treeroot') {
                $(objelement).find('input[type=checkbox]').each(function () {
                    var parent_id = $(this).attr('data-parent');
                    if (parent_id == id) {
                        this.checked = chkall;
                    }
                });
            }
        });
    }
}

function CheckBoxList_GetData(element) {
    var objelement = '#' + element;
    var arr = [];
    $(objelement).find('input[type=checkbox]').each(function () {
        var data_parent = $(this).attr('data-parent');
        if (data_parent != null) {
            arr[arr.length] = $(this).attr('data-id');
        }
    });

    return arr;
}